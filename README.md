# Dillon 后台管理系统

基于 java Swing 和 Spring Boot/Spring Cloud & Alibaba 前后端分离的分布式微服务架构


# 平台简介

dillon-boot是一套全部开源的快速开发平台，给个人及企业免费使用。

* 采用前后端分离的模式，前端使用java Swing。
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Redis。
* 流量控制框架选型Sentinel，分布式事务选型Seata。

# 系统模块

~~~
org.dillon     
├── dillon-ui              // 前端框架 [80]
        └──dillon-swingcomponent             //swing组件库
        └──dillon-ui-javafx                  //javafx版前端
        └──dillon-ui-swing                   //javaSwing版前端
├── dillon-gateway         // 网关模块 [8080]
├── dillon-auth            // 认证中心 [9200]
├── dillon-common          // 通用模块
│       └── dillon-common-core                         // 核心模块
│       └── dillon-common-datascope                    // 权限范围
│       └── dillon-common-datasource                   // 多数据源
│       └── dillon-common-log                          // 日志记录
│       └── dillon-common-redis                        // 缓存服务
│       └── dillon-common-security                     // 安全模块
│       └── dillon-common-swagger                      // 系统接口
├── dillon-service         // 业务模块
│       └── dillon-system                              // 系统模块 [9201]
│                 └── dillon-system-api                          // 系统接口
│                 └── dillon-system-biz                         // 系统管理
├──pom.xml                // 公共依赖
~~~

# 环境部署

## 准备工作

~~~
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.7.0 (推荐8.0.27版本)
Redis >= 3.0
Maven >= 3.0
sentinel >= 1.6.0
~~~

# 运行系统

## 后端运行

~~~
1、前往Gitee下载页面(https://gitee.com/lwdillon/dillon-boot)下载解压到工作目录
2、导入到IntelliJ IDEA，菜单 File -> open，然后选择工作目录/dillon-boot/pom.xml，然后点击 Open 按钮，即可成功导入。
IntelliJ IDEA会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定）
3、创建数据库dillon-cloud并导入数据脚本dillon_cloud.sql（必须），quartz.sql（可选）
4、创建数据库dillon-config并导入数据脚本dillon_config.sql（必须）
5、配置nacos持久化，修改dillon-register/src/main/resources/application.yml文件，增加支持mysql数据源配置
~~~

```aidl
db:
  num: 1
  user: ${MYSQL_USER:root}
  password: ${MYSQL_PWD:Dillon@123456}
  url:
    0: jdbc:mysql://localhost:3306/dillon-config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
```

# 后台启动类顺序

1. ` com.alibaba.nacos.DillonNacosApplication （nacos注册中心 必须）`
2. ` org.dillon.gateway.DillonGatewayApplication （网关模块 必须） `
3. ` org.dillon.auth.DillonAuthApplication （认证模块 必须）`
4. ` org.dillon.system.DillonSystemApplication （系统模块 必须）`

# 配置中心

~~~
         ,--.
       ,--.'|
   ,--,:  : |                                           Nacos 
,`--.'`|  ' :                       ,---.               Running in stand alone mode, All function modules
|   :  :  | |                      '   ,'\   .--.--.    Port: 8848
:   |   \ | :  ,--.--.     ,---.  /   /   | /  /    '   Pid: 96698
|   : '  '; | /       \   /     \.   ; ,. :|  :  /`./   Console: http://172.16.173.219:8848/nacos/index.html
'   ' ;.    ;.--.  .-. | /    / ''   | |: :|  :  ;_
|   | | \   | \__\/: . ..    ' / '   | .; : \  \    `.      https://nacos.io
'   : |  ; .' ," .--.; |'   ; :__|   :    |  `----.   \
|   | '`--'  /  /  ,.  |'   | '.'|\   \  /  /  /`--'  /
'   : |     ;  :   .'   \   :    : `----'  '--'.     /
;   |.'     |  ,     .-./\   \  /            `--'---'
'---'        `--`---'     `----'
~~~

上面是nacos启动输出信息，点击上面的连接进入nacos管理中心，用户名和密码都是nacos，在配置列表中找到"dillon-system-biz-dev.yml"，点击编辑，配置正确数据库信息：

```aidl
 datasource:
          # 主库数据源
          master:
            driver-class-name: com.mysql.cj.jdbc.Driver
            url: jdbc:mysql://localhost:3306/dillon-cloud?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
            username: root
            password: Dillon@123456
```

# swing前台启动类

* `DillonBootUIApplication`

## 演示图

<table>
    <tr>
        <td>登录</td> 
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/login.png"/></td>
    </tr>
    <tr>
         <td>主页</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/home.png"/></td>
    </tr>
    <tr>
        <td>功能搜索</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/search.png"/></td>
    </tr>
    <tr>
        <td>用户管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/user1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/user2.png"/></td>
    </tr>
    <tr>
        <td>角色管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/role1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/role2.png"/></td>
    </tr>
    <tr>
        <td>分配用户</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/role3.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/role4.png"/></td>
    </tr>
    <tr>
        <td>个人信息</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/personal1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/personal2.png"/></td>
    </tr>
    <tr>
        <td>菜单管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/menu1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/menu2.png"/></td>
    </tr>
    <tr>
        <td>部门管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dept1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dept2.png"/></td>
    </tr>
    <tr>
        <td>岗位管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/post1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/post2.png"/></td>
    </tr>
    <tr>
        <td>字典管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dictType1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dictType2.png"/></td>
    </tr>
    <tr>
        <td>字典数据</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dictData1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/dictData2.png"/></td>
    </tr>
    <tr>
        <td>参数设置</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/config1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/config2.png"/></td>
    </tr>
    <tr>
        <td>通知公告</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/notice1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/notice2.png"/></td>
    </tr>
    <tr>
        <td>日志管理</td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/optlog1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/optlog2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/loginlog1.png"/></td>
        <td><img src="https://gitee.com/lwdillon/dillon-boot/raw/master/demomap/loginlog2.png"/></td>
    </tr>



</table>
