package org.dillon.job;

import org.dillon.common.security.annotation.EnableCustomConfig;
import org.dillon.common.security.annotation.EnableRyFeignClients;
import org.dillon.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 定时任务
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class DillonJobApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DillonJobApplication.class, args);

    }
}
