package org.dillon.system.api;

import com.alibaba.cola.dto.SingleResponse;
import org.dillon.system.api.domain.SysFile;
import org.dillon.system.api.factory.RemoteFileFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.dillon.common.core.constant.ServiceNameConstants;

/**
 * 文件服务
 * 
 * @author liwen
 */
@FeignClient(contextId = "remoteFileService", value = ServiceNameConstants.FILE_SERVICE, fallbackFactory = RemoteFileFallbackFactory.class)
public interface RemoteFileService
{
    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public SingleResponse<SysFile> upload(@RequestPart(value = "file") MultipartFile file);
}
