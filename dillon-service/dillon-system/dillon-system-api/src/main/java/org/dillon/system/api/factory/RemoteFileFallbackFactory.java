package org.dillon.system.api.factory;

import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.dillon.system.api.RemoteFileService;
import org.dillon.system.api.domain.SysFile;

/**
 * 文件服务降级处理
 * 
 * @author liwen
 */
@Component
public class RemoteFileFallbackFactory implements FallbackFactory<RemoteFileService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteFileFallbackFactory.class);

    @Override
    public RemoteFileService create(Throwable throwable)
    {
        log.error("文件服务调用失败:{}", throwable.getMessage());
        return new RemoteFileService()
        {
            @Override
            public SingleResponse<SysFile> upload(MultipartFile file)
            {
                return SingleResponse.buildFailure(Constants.FAIL.toString(),"上传文件失败:" + throwable.getMessage());
            }
        };
    }
}
