package org.dillon.system.api.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TreeselectKeyModel implements Serializable {

    private List<Long> keyIds;
    private List<TreeSelect> treeSelects;
}
