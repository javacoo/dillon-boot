package org.dillon.system.api.domain;

import com.alibaba.cola.dto.DTO;
import org.dillon.system.api.model.TreeSelect;
import lombok.Data;

import java.util.List;

@Data
public class UserInfoModel extends DTO {

    private List<SysRole> roles;
    private List<SysPost> posts;
    private List<TreeSelect> depts;
    private SysUser user;
    private List<Long> roleIds;
    private List<Long> postsIds;

}
