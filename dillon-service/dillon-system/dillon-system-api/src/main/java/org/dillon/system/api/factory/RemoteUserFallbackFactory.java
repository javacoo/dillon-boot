package org.dillon.system.api.factory;

import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.dillon.system.api.RemoteUserService;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.model.LoginUser;

/**
 * 用户服务降级处理
 * 
 * @author liwen
 */
@Component
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteUserFallbackFactory.class);

    @Override
    public RemoteUserService create(Throwable throwable)
    {
        log.error("用户服务调用失败:{}", throwable.getMessage());
        return new RemoteUserService()
        {
            @Override
            public SingleResponse<LoginUser> getUserInfo(String username, String source)
            {
                return SingleResponse.buildFailure(Constants.FAIL.toString(),"获取用户失败:" + throwable.getMessage());
            }

            @Override
            public SingleResponse<Boolean> registerUserInfo(SysUser sysUser, String source)
            {
                return SingleResponse.buildFailure(Constants.FAIL.toString(),"注册用户失败:" + throwable.getMessage());
            }
        };
    }
}
