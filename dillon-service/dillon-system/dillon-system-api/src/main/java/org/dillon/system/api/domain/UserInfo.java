package org.dillon.system.api.domain;

import com.alibaba.cola.dto.DTO;
import lombok.Data;

import java.util.Set;

@Data
public class UserInfo extends DTO {

    private Set<String> roles;
    // 权限集合
    private Set<String> permissions;

    private SysUser user;

    private String roleGroup;

    private String postGroup;

}
