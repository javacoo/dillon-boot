package org.dillon.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.cola.dto.PageResponse;
import com.github.pagehelper.PageHelper;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysLogininfor;
import org.dillon.system.api.domain.SysOperLog;
import org.dillon.system.service.ISysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.controller.BaseController;
import org.dillon.common.core.web.page.TableDataInfo;
import org.dillon.common.log.annotation.Log;
import org.dillon.common.log.enums.BusinessType;
import org.dillon.common.security.annotation.InnerAuth;
import org.dillon.common.security.annotation.RequiresPermissions;

/**
 * 系统访问记录
 * 
 * @author liwen
 */
@RestController
@RequestMapping("/logininfor")
public class SysLogininforController extends BaseController
{
    @Autowired
    private ISysLogininforService logininforService;

    @RequiresPermissions("system:logininfor:list")
    @PostMapping("/list")
    public PageResponse<SysLogininfor> list(@RequestBody PageInfo<SysLogininfor> pageQuery) {
        PageHelper.startPage(pageQuery.getPageIndex(), pageQuery.getPageSize(), pageQuery.getOrderBy()).setReasonable(true);
        List<SysLogininfor> list = logininforService.selectLogininforList(pageQuery.getData());
        int total = (int) new com.github.pagehelper.PageInfo(list).getTotal();
        return PageResponse.of(list, total, pageQuery.getPageSize(), pageQuery.getPageIndex());
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:logininfor:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysLogininfor logininfor)
    {
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        ExcelUtil<SysLogininfor> util = new ExcelUtil<SysLogininfor>(SysLogininfor.class);
        util.exportExcel(response, list, "登录日志");
    }

    @RequiresPermissions("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public OptResult remove(@PathVariable Long[] infoIds)
    {
        return toAjax(logininforService.deleteLogininforByIds(infoIds));
    }

    @RequiresPermissions("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/clean")
    public OptResult clean()
    {
        logininforService.cleanLogininfor();
        return OptResult.success();
    }

    @InnerAuth
    @PostMapping
    public OptResult add(@RequestBody SysLogininfor logininfor)
    {
        return toAjax(logininforService.insertLogininfor(logininfor));
    }
}
