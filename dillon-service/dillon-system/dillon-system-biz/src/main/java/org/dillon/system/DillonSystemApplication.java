package org.dillon.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.dillon.common.security.annotation.EnableCustomConfig;
import org.dillon.common.security.annotation.EnableRyFeignClients;
import org.dillon.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 * @author liwen
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class DillonSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DillonSystemApplication.class, args);

    }
}
