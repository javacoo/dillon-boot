package org.dillon.system.controller;

import com.alibaba.cola.dto.PageResponse;
import com.github.pagehelper.PageHelper;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.controller.BaseController;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.common.log.annotation.Log;
import org.dillon.common.log.enums.BusinessType;
import org.dillon.common.security.annotation.InnerAuth;
import org.dillon.common.security.annotation.RequiresPermissions;
import org.dillon.system.api.domain.SysOperLog;
import org.dillon.system.service.ISysOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 操作日志记录
 *
 * @author liwen
 */
@RestController
@RequestMapping("/operlog")
public class SysOperlogController extends BaseController {
    @Autowired
    private ISysOperLogService operLogService;

    @RequiresPermissions("system:operlog:list")
    @PostMapping("/list")
    public PageResponse<SysOperLog> list(@RequestBody PageInfo<SysOperLog> pageQuery) {
        PageHelper.startPage(pageQuery.getPageIndex(), pageQuery.getPageSize(), pageQuery.getOrderBy()).setReasonable(true);
        List<SysOperLog> list = operLogService.selectOperLogList(pageQuery.getData());
        int total = (int) new com.github.pagehelper.PageInfo(list).getTotal();
        return PageResponse.of(list, total, pageQuery.getPageSize(), pageQuery.getPageIndex());
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:operlog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysOperLog operLog) {
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
        util.exportExcel(response, list, "操作日志");
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:operlog:remove")
    @DeleteMapping("/{operIds}")
    public OptResult remove(@PathVariable Long[] operIds) {
        return toAjax(operLogService.deleteOperLogByIds(operIds));
    }

    @RequiresPermissions("system:operlog:remove")
    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public OptResult clean() {
        operLogService.cleanOperLog();
        return OptResult.success();
    }

    @InnerAuth
    @PostMapping
    public OptResult add(@RequestBody SysOperLog operLog) {
        return toAjax(operLogService.insertOperlog(operLog));
    }
}
