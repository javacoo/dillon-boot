package org.dillon.system.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.constant.UserConstants;
import org.dillon.common.core.utils.StringUtils;
import org.dillon.common.core.web.controller.BaseController;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.log.annotation.Log;
import org.dillon.common.log.enums.BusinessType;
import org.dillon.common.security.annotation.RequiresPermissions;
import org.dillon.common.security.utils.SecurityUtils;
import org.dillon.system.api.model.TreeselectKeyModel;
import org.dillon.system.api.model.SysMenuModel;
import org.dillon.system.api.model.TreeSelect;
import org.dillon.system.api.domain.SysMenu;
import org.dillon.system.api.model.RouterVo;
import org.dillon.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单信息
 *
 * @author liwen
 */
@RestController
@RequestMapping("/menu")
public class SysMenuController extends BaseController {
    @Autowired
    private ISysMenuService menuService;

    /**
     * 获取菜单列表
     */
    @RequiresPermissions("system:menu:list")
    @PostMapping("/list")
    public MultiResponse<SysMenuModel> list(@RequestBody SysMenuModel menuModel) {
        Long userId = SecurityUtils.getUserId();
        SysMenu menu = new SysMenu();
        BeanUtil.copyProperties(menuModel, menu, true);
        List<SysMenu> menus = menuService.selectMenuList(menu, userId);
        List<SysMenuModel> menuModels = BeanUtil.copyToList(menus, SysMenuModel.class);
        return MultiResponse.of(menuModels);
    }

    /**
     * 根据菜单编号获取详细信息
     */
    @RequiresPermissions("system:menu:query")
    @GetMapping(value = "/{menuId}")
    public SingleResponse<SysMenuModel> getInfo(@PathVariable Long menuId) {
        SysMenu sysMenu = menuService.selectMenuById(menuId);
        SysMenu parentMenu = menuService.selectMenuById(sysMenu.getParentId());
        SysMenuModel sysMenuModel = BeanUtil.toBean(sysMenu, SysMenuModel.class);
        sysMenuModel.setParentMenuModel(BeanUtil.toBean(parentMenu, SysMenuModel.class));

        return SingleResponse.of(sysMenuModel);
    }

    /**
     * 获取菜单下拉树列表
     */
    @PostMapping("/treeselect")
    public MultiResponse<TreeSelect> treeselect(@RequestBody SysMenu menu) {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuList(menu, userId);
        return MultiResponse.of(menuService.buildMenuTreeSelect(menus));
    }

    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public SingleResponse<TreeselectKeyModel> roleMenuTreeselect(@PathVariable("roleId") Long roleId) {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuList(userId);
        TreeselectKeyModel model=new TreeselectKeyModel();
        model.setKeyIds(menuService.selectMenuListByRoleId(roleId));
        model.setTreeSelects(menuService.buildMenuTreeSelect(menus));
        return SingleResponse.of(model);
    }

    /**
     * 新增菜单
     */
    @RequiresPermissions("system:menu:add")
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public OptResult add(@Validated @RequestBody SysMenuModel menuModel) {
        SysMenu menu = BeanUtil.toBean(menuModel, SysMenu.class);
        if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
            return OptResult.error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.ishttp(menu.getPath())) {
            return OptResult.error("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        menu.setCreateBy(SecurityUtils.getUsername());
        return toAjax(menuService.insertMenu(menu));
    }

    /**
     * 修改菜单
     */
    @RequiresPermissions("system:menu:edit")
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public OptResult edit(@Validated @RequestBody SysMenuModel menuModel) {
        SysMenu menu = BeanUtil.toBean(menuModel, SysMenu.class);
        if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
            return OptResult.error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.ishttp(menu.getPath())) {
            return OptResult.error("修改菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        } else if (menu.getMenuId().equals(menu.getParentId())) {
            return OptResult.error("修改菜单'" + menu.getMenuName() + "'失败，上级菜单不能选择自己");
        }
        menu.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(menuService.updateMenu(menu));
    }

    /**
     * 删除菜单
     */
    @RequiresPermissions("system:menu:remove")
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{menuId}")
    public OptResult remove(@PathVariable("menuId") Long menuId) {
        if (menuService.hasChildByMenuId(menuId)) {
            return OptResult.error("存在子菜单,不允许删除");
        }
        if (menuService.checkMenuExistRole(menuId)) {
            return OptResult.error("菜单已分配,不允许删除");
        }
        return toAjax(menuService.deleteMenuById(menuId));
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("/getRouters")
    public MultiResponse<RouterVo> getRouters() {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);
        return MultiResponse.of(menuService.buildMenus(menus));
    }
}