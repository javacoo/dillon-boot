package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.system.api.domain.SysDept;
import org.dillon.system.api.model.SysMenuModel;
import org.dillon.system.api.model.TreeSelect;
import org.dillon.system.api.model.TreeselectKeyModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "sysDeptFeign", url = "${server.url}", path = "/system")
public interface SysDeptFeign {

    @PostMapping("/dept/list")
    MultiResponse<SysDept> list(SysDept dept);

    @PostMapping("/dept/treeselect")
    MultiResponse<TreeSelect> treeselect(SysDept dept);

    @PostMapping("/dept")
    OptResult add(SysDept dept);

    @PutMapping("/dept")
    OptResult edit(SysDept dept);

    @DeleteMapping("/dept/{deptId}")
    OptResult remove(@PathVariable("deptId") Long deptId);

    @GetMapping(value = "/dept/{deptId}")
    SingleResponse<SysDept> getInfo(@PathVariable("deptId") Long deptId);

    @GetMapping(value = "/dept/roleDeptTreeselect/{roleId}")
    SingleResponse<TreeselectKeyModel> roleDeptTreeselect(@PathVariable("roleId") Long roleId);
}
