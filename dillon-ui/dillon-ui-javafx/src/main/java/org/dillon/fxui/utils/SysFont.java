package org.dillon.fxui.utils;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.util.Enumeration;

/**
 * 界面字体
 */
public class SysFont {
    public static final Font APP_FONT;
    public static final Font ACENS_FONT;

    static {
        APP_FONT = FontLoader.loadFont("font/msyh.ttf");
        ACENS_FONT = FontLoader.loadFont("font/Acens.ttf");
    }

    public static Font FONT_16_BOLD = APP_FONT.deriveFont(Font.BOLD, 20);//主要颜色
    public static Font FONT_16_PLAIN = APP_FONT.deriveFont(Font.PLAIN, 20);//主要颜色


    /**
     * 统一设置字体，父界面设置之后，所有由父界面进入的子界面都不需要再次设置字体
     */
    public static void initGlobalFont(Font font) {
        FontUIResource fontRes = new FontUIResource(font);
        for (Enumeration<Object> keys = UIManager.getDefaults().keys();
             keys.hasMoreElements(); ) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                UIManager.put(key, fontRes);
            }
        }
    }
}