package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.utils.StringUtils;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.common.core.web.page.TableDataInfo;
import org.dillon.system.api.domain.SysDictData;
import org.dillon.system.api.domain.SysPost;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * sysPostFeign
 *
 * @author liwen
 * @date 2022/07/11
 */
@FeignClient(name = "sysDictDataFeign", url = "${server.url}", path = "/system")
public interface SysDictDataFeign {

    @PostMapping("/dict/data/list")
    PageResponse<SysDictData>  list(PageInfo<SysDictData> pageQuery);

    /**
     * 查询字典数据详细
     */
    @GetMapping(value = "/dict/data/{dictCode}")
    SingleResponse<SysDictData> getInfo(@PathVariable("dictCode") Long dictCode);

    /**
     * 根据字典类型查询字典数据信息
     */
    @GetMapping(value = "/dict/data/type/{dictType}")
    MultiResponse<SysDictData> dictType(@PathVariable("dictType") String dictType);

    /**
     * 新增字典类型
     */
    @PostMapping("/dict/data")
    OptResult add(@Validated @RequestBody SysDictData dict);

    /**
     * 修改保存字典类型
     */
    @PutMapping("/dict/data")
    OptResult edit(@Validated @RequestBody SysDictData dict);

    /**
     * 删除字典类型
     */
    @DeleteMapping("/dict/data/{dictCodes}")
    OptResult remove(@PathVariable("dictCodes") Long[] dictCodes);
}
