package org.dillon.fxui.observable;

import java.util.Observable;

/**
 * @Description: 刷新通知操作
 * @param:
 * @return:
 * @auther: liwen
 * @date: 2022/3/25 7:29 下午
 */
public class RefreshObservable extends Observable {

    public void notify(Object arg) {
        setChanged();
        notifyObservers(arg);
    }
}
