package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysRole;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.domain.UserInfoModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 系统用户假装
 *
 * @author liwen
 * @date 2022/07/01
 */
@FeignClient(name = "sysUserFeign", url = "${server.url}", path = "/system")
public interface SysUserFeign {
    /**
     * 列表
     *
     * @param pageQuery 页面查询
     * @return {@link PageResponse}<{@link SysUser}>
     */
    @PostMapping("/user/list")
    PageResponse<SysUser> list(PageInfo<SysUser> pageQuery);

    /**
     * 删除
     *
     * @param userIds 用户id
     * @return {@link OptResult}
     */
    @DeleteMapping("/user/{userIds}")
    OptResult remove(@PathVariable("userIds") Long[] userIds);

    /**
     * 编辑
     *
     * @param user 用户
     * @return {@link OptResult}
     */
    @PutMapping("/user")
    OptResult edit(SysUser user);

    /**
     * 添加
     *
     * @param user 用户
     * @return {@link OptResult}
     */
    @PostMapping("/user")
    OptResult add(SysUser user);

    /**
     * 重置pwd
     *
     * @param user 用户
     * @return {@link OptResult}
     */
    @PutMapping("/user/resetPwd")
    OptResult resetPwd(SysUser user);

    /**
     * 改变状态
     *
     * @param user 用户
     * @return {@link OptResult}
     */
    @PutMapping("/user/changeStatus")
    OptResult changeStatus(SysUser user);

    @GetMapping( "/user/{userId}")
    SingleResponse<UserInfoModel> getInfo(@PathVariable(value = "userId", required = false) Long userId);


    @GetMapping("/user/authRole/{userId}")
    SingleResponse<UserInfoModel> authRole(@PathVariable("userId") Long userId);

    @PutMapping("/user/authRole")
    OptResult insertAuthRole(@RequestParam("userId") Long userId,@RequestParam("roleIds") Long[] roleIds);
}
