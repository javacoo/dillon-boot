package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.PageResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysOperLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "sysOperlogFeign", url = "${server.url}", path = "/system")
public interface SysOperlogFeign {
    @PostMapping("/operlog/list")
    PageResponse<SysOperLog> list( PageInfo<SysOperLog> pageQuery);

    @DeleteMapping("/operlog/{operIds}")
    OptResult remove(@PathVariable("operIds") Long[] operIds);

    @DeleteMapping("/operlog/clean")
    OptResult clean();

    @PostMapping("/operlog")
    OptResult add(SysOperLog operLog);
}
