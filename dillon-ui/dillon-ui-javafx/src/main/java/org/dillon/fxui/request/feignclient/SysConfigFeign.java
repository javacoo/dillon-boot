package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * sysPostFeign
 *
 * @author liwen
 * @date 2022/07/11
 */
@FeignClient(name = "sysConfigFeign", url = "${server.url}", path = "/system")
public interface SysConfigFeign {

    @PostMapping("/config/list")
    PageResponse<SysConfig> list(PageInfo<SysConfig> pageQuery);

    /**
     * 得到信息
     * 根据 参数编号获取详细信息
     *
     * @param configId config id
     * @return {@link OptResult}
     */
    @GetMapping(value = "/config/{configId}")
    SingleResponse<SysConfig> getInfo(@PathVariable("configId") Long configId);

    /**
     * 添加
     * 新增 参数
     *
     * @param config 帖子
     * @return {@link OptResult}
     */
    @PostMapping("/config")
    OptResult add(SysConfig config);

    /**
     * 编辑
     * 修改 参数
     *
     * @param config 帖子
     * @return {@link OptResult}
     */
    @PutMapping("/config")
    OptResult edit(SysConfig config);

    /**
     * 删除
     * 删除 参数
     *
     * @param configIds config id
     * @return {@link OptResult}
     */
    @DeleteMapping("/config/{configIds}")
    OptResult remove(@PathVariable("configIds") Long[] configIds);



}
