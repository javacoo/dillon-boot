package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysDictType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * sysPostFeign
 *
 * @author liwen
 * @date 2022/07/11
 */
@FeignClient(name = "sysDictTypeFeign", url = "${server.url}", path = "/system")
public interface SysDictTypeFeign {

    @PostMapping("/dict/type/list")
    PageResponse<SysDictType> list(PageInfo<SysDictType> pageQuery);


    /**
     * 查询字典类型详细
     */
    @GetMapping(value = "/dict/type/{dictId}")
    SingleResponse<SysDictType> getInfo(@PathVariable("dictId") Long dictId);

    /**
     * 新增字典类型
     */
    @PostMapping("/dict/type")
    OptResult add(@Validated @RequestBody SysDictType dict);

    /**
     * 修改字典类型
     */
    @PutMapping("/dict/type")
    OptResult edit(@Validated @RequestBody SysDictType dict);

    /**
     * 删除字典类型
     */
    @DeleteMapping("/dict/type/{dictIds}")
    OptResult remove(@PathVariable("dictIds") Long[] dictIds);

    /**
     * 刷新字典缓存
     */
    @DeleteMapping("/dict/type/refreshCache")
    OptResult refreshCache();

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/dict/type/optionselect")
    MultiResponse<SysDictType> optionselect();
}
