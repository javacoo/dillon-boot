package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysPost;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * sysPostFeign
 *
 * @author liwen
 * @date 2022/07/11
 */
@FeignClient(name = "sysPostFeign", url = "${server.url}", path = "/system")
public interface SysPostFeign {

    @PostMapping("/post/list")
    PageResponse<SysPost> list(PageInfo<SysPost> pageQuery);

    /**
     * 得到信息
     * 根据岗位编号获取详细信息
     *
     * @param postId post id
     * @return {@link OptResult}
     */
    @GetMapping(value = "/post/{postId}")
    SingleResponse<SysPost> getInfo(@PathVariable("postId") Long postId);

    /**
     * 添加
     * 新增岗位
     *
     * @param post 帖子
     * @return {@link OptResult}
     */
    @PostMapping("/post")
    OptResult add(SysPost post);

    /**
     * 编辑
     * 修改岗位
     *
     * @param post 帖子
     * @return {@link OptResult}
     */
    @PutMapping("/post")
    OptResult edit(SysPost post);

    /**
     * 删除
     * 删除岗位
     *
     * @param postIds post id
     * @return {@link OptResult}
     */
    @DeleteMapping("/post/{postIds}")
    OptResult remove(@PathVariable("postIds") Long[] postIds);


    /**
     * optionselect
     *
     * @return {@link MultiResponse}<{@link SysPost}>
     */
    @GetMapping("/post/optionselect")
    MultiResponse<SysPost> optionselect();
}
