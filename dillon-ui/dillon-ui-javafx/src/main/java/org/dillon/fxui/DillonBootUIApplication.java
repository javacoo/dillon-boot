package org.dillon.fxui;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;

import javax.swing.*;

/**
 * @className: MepscUIApplication
 * @author: liwen
 * @date: 2022/3/22 17:40
 */
@EnableFeignClients("org.dillon.fxui.request") // 激活 @FeignClient
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
public class DillonBootUIApplication {


    @Value("${spring.application.name}")
    private String appName;

    public DillonBootUIApplication() {


    }

    public void initUI() {


    }

    public static void main(String[] args) {
        ApplicationContext ctx = new SpringApplicationBuilder(DillonBootUIApplication.class).headless(false).run(args);

    }
}
