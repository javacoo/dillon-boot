package org.dillon.fxui.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class MenuVO implements Serializable {
    private String id;
    private String text;
    private String icon;
    private String tooltipText;
    private String panelClass;
    private boolean colse;
    private boolean haveCase;
    private List<MenuItemVO> menuItemBeanList = new ArrayList<>();


    public MenuVO() {
    }


    public MenuVO(String id, String text, String icon) {
        this.id = id;
        this.text = text;
        this.icon = icon;
    }

    @Override
    public String toString() {
        return text;
    }
}