package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.constant.UserConstants;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.common.core.web.page.TableDataInfo;
import org.dillon.system.api.domain.SysRole;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.domain.SysUserRole;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@FeignClient(name = "sysRoleFeign", url = "${server.url}", path = "/system")
public interface SysRoleFeign {

    @GetMapping("/role/optionselect")
    MultiResponse<SysRole> optionselect();

    @PostMapping("/role/list")
    PageResponse<SysRole> list(@RequestBody PageInfo<SysRole> pageQuery);


    /**
     * 根据角色编号获取详细信息
     */
    @GetMapping(value = "/role/{roleId}")
    SingleResponse<SysRole> getInfo(@PathVariable("roleId") Long roleId);

    /**
     * 新增角色
     */
    @PostMapping("/role")
    OptResult add(SysRole role);

    /**
     * 修改保存角色
     */
    @PutMapping("/role")
    OptResult edit(SysRole role);

    /**
     * 修改保存数据权限
     */
    @PutMapping("/role/dataScope")
    OptResult dataScope(@RequestBody SysRole role);

    /**
     * 状态修改
     */
    @PutMapping("/role/changeStatus")
    OptResult changeStatus(SysRole role);

    /**
     * 删除角色
     */
    @DeleteMapping("/role/{roleIds}")
    OptResult remove(@PathVariable("roleIds") Long[] roleIds);

    /**
     * 查询已分配用户角色列表
     */
    @PostMapping("/role/authUser/allocatedList")
    PageResponse<SysUser> allocatedList(PageInfo<SysUser> pageInfo);

    /**
     * 查询未分配用户角色列表
     */
    @PostMapping("/role/authUser/unallocatedList")
    PageResponse<SysUser> unallocatedList(PageInfo<SysUser> pageInfo);

    /**
     * 取消授权用户
     */
    @PutMapping("/role/authUser/cancel")
    OptResult cancelAuthUser(SysUserRole userRole);

    /**
     * 批量取消授权用户
     */
    @PutMapping("/role/authUser/cancelAll")
    OptResult cancelAuthUserAll(@RequestParam("roleId") Long roleId, @RequestParam("userIds") Long[] userIds);

    /**
     * 批量选择用户授权
     */
    @PutMapping("/role/authUser/selectAll")
    OptResult selectAuthUserAll(@RequestParam("roleId") Long roleId, @RequestParam("userIds") Long[] userIds);
}
