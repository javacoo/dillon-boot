package org.dillon.fxui.request.feignclient;

import com.alibaba.cola.dto.PageResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysLogininfor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "sysLogininforFeign", url = "${server.url}", path = "/system")
public interface SysLogininforFeign {
    @PostMapping("/logininfor/list")
    PageResponse<SysLogininfor> list(PageInfo<SysLogininfor> pageQuery);

    @DeleteMapping("/logininfor/{operIds}")
    OptResult remove(@PathVariable("operIds") Long[] operIds);

    @DeleteMapping("/logininfor/clean")
    OptResult clean();


}
