import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestExcel {

    public static void main(String[] args) {


        ExcelReader reader = ExcelUtil.getReader("/Users/liwen/Desktop/excel/凭证_2022081815482447_100496 的副本.xlsx");

        List<Map<String, Object>> readAll = reader.readAll();

        Map<Object, List<Map<String, Object>>> collect = readAll.stream().collect(Collectors.groupingBy(obj -> obj.get("账簿编码（分文件的条件）")));

        for (Map.Entry<Object, List<Map<String, Object>>> entry : collect.entrySet()) {
            String fileName = entry.getKey().toString() + ".xlsx";
            List<Map<String, Object>> value = entry.getValue();

            // 通过工具类创建writer
            ExcelWriter writer = ExcelUtil.getWriter("/Users/liwen/Desktop/excel/new/" + fileName);
            writer.write(value, true);
            // 关闭writer，释放内存
            writer.close();
        }

        System.err.println("----生成完毕！");
    }
}
