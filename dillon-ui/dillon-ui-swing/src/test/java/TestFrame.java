import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.extras.components.FlatTriStateCheckBox;
import com.formdev.flatlaf.icons.FlatClearIcon;
import com.jidesoft.combobox.TreeExComboBoxSearchable;
import com.jidesoft.swing.CheckBoxTree;
import com.jidesoft.swing.TristateCheckBox;
import org.dillon.swing.WPaginationPane;
import org.dillon.swing.WTreeComboBox;
import org.dillon.swingui.theme.MyDark;
import org.dillon.swingui.view.system.user.PersonalCenterPanel;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.TimingSource;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.Arrays;

public class TestFrame extends JFrame {


    public TestFrame() {

        SwingUtilities.invokeLater(() -> {
            initUI();
            showTestPanel();
        });
    }

    public void initUI() {
        TimingSource ts = new SwingTimerTimingSource();
        Animator.setDefaultTimingSource(ts);
        ts.init();
        UIManager.put("TitlePane.centerTitleIfMenuBarEmbedded", true);
        UIManager.put("TitlePane.iconSize", new Dimension(30, 30));
        UIManager.put("TitlePane.unifiedBackground", true);
        UIManager.put("MenuItem.selectionType", "underline");
        UIManager.put("MenuItem.selectionType", "underline");
        UIManager.put("TristateCheckBox.icon",new FlatClearIcon());

        UIDefaults defaults = UIManager.getDefaults();
//        defaults.put("ExComboBoxUI", "org.dillon.swing.WTreeComboBoxUI");
        FlatDarculaLaf.setup();

    }

    public void showTestPanel() {
        WPaginationPane wpa = new WPaginationPane();
        JPanel panel = new JPanel(new BorderLayout());
        wpa.setTotal((int) (Math.random() * 10000));
        JComboBox box = new JComboBox(new Object[]{"a", "b", "c"});
        box.setEditable(false);

        WTreeComboBox treeComboBox2 = new WTreeComboBox(){
            @Override
            protected boolean isValidSelection(TreePath path) {
                TreeNode treeNode = (TreeNode) path.getLastPathComponent();
                return treeNode.isLeaf();
            }
        };
        treeComboBox2.setSearchUserObjectToSelect(true);
        TreeExComboBoxSearchable treeComboBoxSearchable = new TreeExComboBoxSearchable(treeComboBox2);
        treeComboBoxSearchable.setRecursive(true);
        CheckBoxTree tree= new CheckBoxTree();
        tree.setCheckBox(new TristateCheckBox());
        TristateCheckBox tristateCheckBox=  new TristateCheckBox();

        JTextField field = new JTextField();
        String[] users = {"aaa", "bbb", "ccc"};
        AutoCompleteDecorator.decorate(field, Arrays.asList(users), false);
        panel.add(field, BorderLayout.NORTH);
        panel.add(tree, BorderLayout.CENTER);
        panel.add(wpa, BorderLayout.SOUTH);

        this.setJMenuBar(null);
        this.setContentPane(panel);
        this.setLocationRelativeTo(null);
        JRootPane rootPane = this.getRootPane();
        if (rootPane != null) {
            rootPane.setWindowDecorationStyle(JRootPane.NONE);
        }
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new TestFrame();
    }


}
