package org.dillon.swingui.bean;

import lombok.Data;

/**
 * 用户登录对象
 *
 * @author liwen
 */
@Data
public class LoginVO {
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;

    private String code;

    private String uuid;

    public LoginVO() {
    }

    public LoginVO(String username, String password, String code, String uuid) {
        this.username = username;
        this.password = password;
        this.code = code;
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return username;
    }
}
