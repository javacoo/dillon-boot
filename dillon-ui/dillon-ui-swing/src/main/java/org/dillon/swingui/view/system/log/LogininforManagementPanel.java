package org.dillon.swingui.view.system.log;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysLogininforFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysLogininfor;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 登录日志
 * 面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class LogininforManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "访问编号", "用户名称", "登录地址", "登录状态", "操作信息", "登录日期"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;

    private JTextField ipTextField;
    private JTextField nameTextField;
    private JComboBox statusCombo;

    private WaitPane waitPane;
    private WLocalDateCombo beginTime;
    private WLocalDateCombo endTime;


    public LogininforManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout());
        toolBar.add(new JLabel("登录地址"));

        toolBar.add(ipTextField = createTextField("请输入登录地址"));
        ipTextField.setColumns(15);
        toolBar.add(new JLabel("用户名称"));
        toolBar.add(nameTextField = createTextField("请输入用户名称"));
        nameTextField.setColumns(15);
        toolBar.add(new JLabel("状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "成功", "失败"}));

        toolBar.add(new JLabel("操作时间"));
        toolBar.add(beginTime=new WLocalDateCombo());
        toolBar.add(new JLabel("-"));
        toolBar.add(endTime=new WLocalDateCombo());
        beginTime.setValue(DateUtil.offsetDay(new Date(),-30).toLocalDateTime().toLocalDate());


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            ipTextField.setText("");
            nameTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);


        OperateInfoPanel operateInfoPanel = new OperateInfoPanel();
        operateInfoPanel.getInfoPanel().add(toolBar);

        JButton emptyButton = new JButton("清空");
        operateInfoPanel.getOperatePanel().add(emptyButton);
        emptyButton.addActionListener(e -> {
            clean();
        });

        JButton delButton = new JButton("删除");
        operateInfoPanel.getOperatePanel().add(delButton);
        delButton.addActionListener(e -> {
            delPost(true);
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));

        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(operateInfoPanel, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);
                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
                table.getColumnExt("登录状态").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        JButton button = new JButton("0".equals(value) ? "成功" : "失败");
                        button.setBackground("0".equals(value) ? new Color(24, 144, 255, 88) : new Color(245, 108, 108, 41));
                        button.setForeground("0".equals(value) ? new Color(0x1890ff) : new Color(0xf56c6c));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });

            }
        });
        super.updateUI();

    }

    private void updateData() {
        final SysLogininfor sysOperLog = new SysLogininfor();
        sysOperLog.setUserName(nameTextField.getText());
        sysOperLog.setIpaddr(ipTextField.getText());
        sysOperLog.setStatus(statusCombo.getSelectedIndex() == 0 ? null : (statusCombo.getSelectedIndex() == 1 ? "0" : "1"));
        sysOperLog.getParams().put("beginTime", beginTime.getValue());
        sysOperLog.getParams().put("endTime", endTime.getValue());
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysOperLog);

        SwingWorker<PageResponse<SysLogininfor>, Object> worker = new SwingWorker<PageResponse<SysLogininfor>, Object>() {
            @Override
            protected PageResponse<SysLogininfor> doInBackground() throws Exception {


                return Request.connector(SysLogininforFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysLogininfor sysOperLog : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(sysOperLog.getInfoId());
                            rowV.add(sysOperLog.getUserName());
                            rowV.add(sysOperLog.getIpaddr());
                            rowV.add(sysOperLog.getStatus());
                            rowV.add(sysOperLog.getMsg());
                            rowV.add(DateUtil.format(sysOperLog.getAccessTime(), "yyyy-MM-dd HH:mm:ss"));
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    
                }
            }
        };
        tableModel.setRowCount(0);
        
        worker.execute();
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    private void clean() {

        int opt = WOptionPane.showOptionDialog(this, "确定要清空所有日志数据吗", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysLogininforFeign.class).clean();
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delPost(boolean isMore) {
        Long sysOperLogId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalPostId = sysOperLogId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysLogininforFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        swingWorker.execute();

    }


    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
