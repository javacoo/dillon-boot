package org.dillon.swingui.view.system.dept;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.jidesoft.combobox.TreeExComboBox;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.swing.WOptionPane;
import org.dillon.swing.WTreeComboBox;
import org.dillon.swing.WaitPane;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysDeptFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.system.api.domain.SysDept;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 部门管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class DeptManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"部门名称", "排序", "状态", "创建时间", "操作"};

    private JXTreeTable treeTable;

    private JPanel optDeptPanel;

    private TreeExComboBox parentDeptCombo;

    private JTextField nameTextField;
    private JComboBox statusCombo;


    /**
     * 目录单选按钮
     */
    private JTextField deptNameTextField;
    private JSpinner sortSpinner;

    private JTextField principalTextField;
    private JTextField contactNumberTextField;
    private JTextField emailTextField;
    private JRadioButton normalBut;
    private JRadioButton disableBut;
    private JToggleButton exButton;
private WaitPane waitPane;
    public DeptManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("部门名称"));
        toolBar.add(nameTextField = createTextField("请输入部门名称"));
        nameTextField.setColumns(20);
        toolBar.add(new JLabel("部门状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "停用"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        JButton addButton = new JButton("新增");
        toolBar.add(addButton);
        addButton.addActionListener(e -> {
            treeTable.clearSelection();
            showDeptAddDialog();
        });

         exButton = new JToggleButton("展开/折叠");
        toolBar.add(exButton);
        exButton.addActionListener(e -> {
            if (exButton.isSelected()) {
                treeTable.expandAll();
            } else {
                treeTable.collapseAll();
            }
        });

        treeTable = new JXTreeTable(new DeptTreeTableModel(null));
        treeTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        treeTable.setRowHeight(50);
        treeTable.setLeafIcon(null);
        treeTable.setClosedIcon(null);
        treeTable.setOpenIcon(null);
        treeTable.setShowHorizontalLines(true);
        treeTable.setIntercellSpacing(new Dimension(1, 1));
        JScrollPane tsp = new JScrollPane(treeTable);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.setBorder(BorderFactory.createEmptyBorder(7,7,7,7));

        this.setLayout(new BorderLayout(0,10));
        this.add(toolBar, BorderLayout.NORTH);
        this.add(panel);


    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                treeTable.setHighlighters(rollover);
                treeTable.setIntercellSpacing(new Dimension(0, 1));
                treeTable.setLeafIcon(null);
                treeTable.setClosedIcon(null);
                treeTable.setOpenIcon(null);

                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                treeTable.getColumn("排序").setCellRenderer(dc);

            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showDeptEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delDept());
        del.setForeground(UIManager.getColor("App.accentColor"));

        JButton add = new JButton("新增");
        add.addActionListener(e -> showDeptAddDialog());
        add.setForeground(UIManager.getColor("App.accentColor"));
        add.setIcon(new FlatSVGIcon("icons/xinzeng.svg", 15, 15));
        optBar.add(edit);
        optBar.add(add);
        optBar.add(del);
        optBar.setPreferredSize(new Dimension(210, 45));
        return optBar;

    }


    private void showDeptAddDialog() {

        int opt = WOptionPane.showOptionDialog(null, getOptDeptPanel(false), "添加部门", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            addDept();
        }
    }


    private void showDeptEditDialog() {
        int opt = WOptionPane.showOptionDialog(null, getOptDeptPanel(true), "修改部门", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            editDept();
        }
    }

    private void updateData() {
        SysDept sysDept = new SysDept();
        sysDept.setDeptName(nameTextField.getText());
        int selectIndex = statusCombo.getSelectedIndex();
        sysDept.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));
        SwingWorker<Tree<Long>, Object> worker = new SwingWorker<Tree<Long>, Object>() {
            @Override
            protected Tree<Long> doInBackground() throws Exception {

                List<SysDept> sysDeptList = Request.connector(SysDeptFeign.class).list(sysDept).getData();
                long min = sysDeptList.stream().mapToLong(value -> value.getParentId()).min().orElse(0L);
                TreeNodeConfig config = new TreeNodeConfig();
                config.setWeightKey("orderNum");
                config.setParentIdKey("parentId");
                config.setNameKey("deptName");
                Tree<Long> treeList = TreeUtil.buildSingle(sysDeptList, min, config, ((object, treeNode) -> {
                    treeNode.setId(object.getDeptId());
                    treeNode.setParentId(object.getParentId());
                    treeNode.setName(object.getDeptName());
                    treeNode.putExtra("orderNum", object.getOrderNum());
                    treeNode.putExtra("status", object.getStatus());
                    treeNode.putExtra("createTime", DateUtil.format(object.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                }));
                return treeList;

            }

            @Override
            protected void done() {
                try {
                    if (CollUtil.isNotEmpty(get())) {
                        updateTreeTableRoot(get());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    
                }
            }
        };
        
        worker.execute();
    }

    private void updateTreeTableRoot(Object root) {

        treeTable.setTreeTableModel(new DeptTreeTableModel(root));
        treeTable.getColumnExt("操作").setMinWidth(210);
        treeTable.getColumnExt("操作").setMaxWidth(210);
        treeTable.getColumnExt("排序").setWidth(80);
        treeTable.getColumnExt("排序").setMaxWidth(80);

        treeTable.getColumnExt("状态").setCellRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                JLabel label = new JLabel("0".equals(value) ? "正常" : "停用");
                label.setForeground("0".equals(value) ? new Color(0x1890ff) : new Color(0xf56c6c));
                FlatSVGIcon icon = new FlatSVGIcon("icons/yuan.svg", 10, 10);
                icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
                    return label.getForeground();
                }));
                label.setIcon(icon);
                panel.add(label);
                panel.setBackground(component.getBackground());
                return panel;
            }
        });
        treeTable.getColumnExt("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
        treeTable.getColumnExt("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));

        treeTable.expandAll();
        exButton.setSelected(true);
    }

    public JPanel getOptDeptPanel(Boolean isEidt) {
        optDeptPanel = new JPanel(new MigLayout("fill,wrap 4,hidemode 3,insets 10", "[right][grow,180::]30[right][grow,180::]", "[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]"));

        parentDeptCombo = new WTreeComboBox();
        deptNameTextField = createTextField("请输入部门名称");
        sortSpinner = new JSpinner(new SpinnerNumberModel());
        principalTextField = createTextField("请输入负责人");
        contactNumberTextField = createTextField("请输入联系电话");
        emailTextField = createTextField("请输入邮箱");
        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("停用");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);

        optDeptPanel.add(new JLabel("上级菜单"));
        optDeptPanel.add(parentDeptCombo, "span 3,growx,growy");

        optDeptPanel.add(new JLabel("部门名称"));
        optDeptPanel.add(deptNameTextField, "growx,growy");
        optDeptPanel.add(new JLabel("显示排序"));
        optDeptPanel.add(sortSpinner, "growx,growy");

        optDeptPanel.add(new JLabel("负责人"));
        optDeptPanel.add(principalTextField, "growx,growy");
        optDeptPanel.add(new JLabel("联系电话"));
        optDeptPanel.add(contactNumberTextField, "growx,growy");

        optDeptPanel.add(new JLabel("邮箱"));
        optDeptPanel.add(emailTextField, "growx,growy");
        optDeptPanel.add(new JLabel("部门状态"));
        JToolBar toolBar4 = new JToolBar();
        toolBar4.add(normalBut);
        toolBar4.add(disableBut);
        optDeptPanel.add(toolBar4, "growx");

        getDeptTree(isEidt);

        return optDeptPanel;
    }

    private void getDeptTree(boolean isEidt) {
        final SysDept sysDept = new SysDept();
        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                Long deptId = (Long) tree.get("id");
                sysDept.setDeptId(deptId);
                sysDept.setDeptName(tree.getName() + "");
            }
        }else {
            sysDept.setDeptId(0L);
            sysDept.setDeptName("主类目");
        }

        SwingWorker<SysDept, Tree<Long>> worker = new SwingWorker<SysDept, Tree<Long>>() {
            @Override
            protected SysDept doInBackground() throws Exception {

                List<SysDept> sysDeptModelList = Request.connector(SysDeptFeign.class).list(new SysDept()).getData();
                TreeNodeConfig config = new TreeNodeConfig();
                config.setWeightKey("orderNum");
                config.setParentIdKey("parentId");
                config.setNameKey("deptName");
                config.setIdKey("deptId");
                Tree<Long> tree = TreeUtil.buildSingle(sysDeptModelList, 0L, config, ((object, treeNode) -> {
                    treeNode.setId(object.getDeptId());
                    treeNode.setParentId(object.getParentId());
                    treeNode.setName(object.getDeptName());
                    treeNode.putExtra("orderNum", object.getOrderNum());
                    treeNode.putExtra("status", object.getStatus());
                    treeNode.putExtra("leader", object.getLeader());
                    treeNode.putExtra("phone", object.getPhone());
                    treeNode.putExtra("email", object.getEmail());
                    treeNode.putExtra("createTime", DateUtil.format(object.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));

                }));

                publish(tree);
                if (isEidt) {
                    SysDept deptModel = Request.connector(SysDeptFeign.class).getInfo(sysDept.getDeptId()).getData();
                    return deptModel;
                } else {
                    SysDept parentDept = new SysDept();
                    parentDept.setDeptId(sysDept.getDeptId());
                    parentDept.setDeptName(sysDept.getDeptName());
                    sysDept.setParentDept(parentDept);
                    return sysDept;
                }

            }

            @Override
            protected void process(List<Tree<Long>> chunks) {
                for (Tree<Long> tree : chunks) {
                    createDectTree(tree);
                }
            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        SysDept deptModel = get();
                        updateValue(deptModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    private void createDectTree(Tree tree) {
        SysDept sysDept = BeanUtil.toBean(tree, SysDept.class);
        sysDept.setDeptName("主类目");
        sysDept.setDeptId(0L);
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(sysDept);
        if (CollectionUtil.isNotEmpty(tree.getChildren())) {
            createDectTreeDate(rootNode, tree.getChildren());
        }
        parentDeptCombo.setTreeModel(new DefaultTreeModel(rootNode));
        parentDeptCombo.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                if (value instanceof DefaultMutableTreeNode) {
                    if (((DefaultMutableTreeNode) value).getUserObject() instanceof Tree) {
                        return new JLabel(((SysDept) ((DefaultMutableTreeNode) value).getUserObject()).getDeptName());
                    }
                }
                return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            }
        });

    }

    private void createDectTreeDate(DefaultMutableTreeNode fatherNode, List<Tree> treeList) {

        for (Tree treeNode : treeList) {

            SysDept sysDeptModel = BeanUtil.toBean(treeNode, SysDept.class);
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(sysDeptModel);
            fatherNode.add(node);

            if (CollectionUtil.isNotEmpty(treeNode.getChildren())) {
                createDectTreeDate(node, treeNode.getChildren());
            }
        }
    }


    private void updateValue(SysDept sysDept) {

        SysDept parentDept= sysDept.getParentDept();
        if (parentDept == null) {
            parentDept = new SysDept();
            parentDept.setDeptId(0L);
            parentDept.setDeptName("主类目");
        }
        parentDeptCombo.setSelectedItem(parentDept);
        deptNameTextField.setText(sysDept.getDeptName());
        sortSpinner.setValue(sysDept.getOrderNum() == null ? 0 : sysDept.getOrderNum());
        principalTextField.setText(sysDept.getLeader());
        contactNumberTextField.setText(sysDept.getPhone());
        emailTextField.setText(sysDept.getEmail());
        normalBut.setSelected("0".equals(sysDept.getStatus()));
        disableBut.setSelected("1".equals(sysDept.getStatus()));

    }

    private SysDept getValue() {
        SysDept sysDept = new SysDept();
        Object obj = parentDeptCombo.getSelectedItem();
        SysDept parentDeptModel = null;
        if (obj instanceof TreePath) {
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) ((TreePath) obj).getLastPathComponent();
            parentDeptModel = (SysDept) defaultMutableTreeNode.getUserObject();
        } else if (obj instanceof SysDept) {
            parentDeptModel = (SysDept) obj;
        }
        sysDept.setParentId(parentDeptModel.getDeptId());
        sysDept.setDeptName(deptNameTextField.getText());
        sysDept.setOrderNum((Integer) sortSpinner.getValue());
        sysDept.setLeader(principalTextField.getText());
        sysDept.setPhone(contactNumberTextField.getText());
        sysDept.setEmail(emailTextField.getText());
        sysDept.setStatus(normalBut.isSelected() ? "0" : "1");
        return sysDept;
    }

    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加部门
     */
    private void addDept() {

        SysDept sysDept = getValue();
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDeptFeign.class).add(sysDept);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void editDept() {

        SysDept sysDept = getValue();

        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                Long deptId = (Long) tree.get("id");
                sysDept.setDeptId(deptId);
            }
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDeptFeign.class).edit(sysDept);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delDept() {
        Long deptId = null;
        String deptName = null;

        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                deptId = (Long) tree.get("id");
                deptName = tree.getName().toString();
            }
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除[" + deptName + "]？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalDeptId = deptId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDeptFeign.class).remove(finalDeptId);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }

    class DeptTreeTableModel extends AbstractTreeTableModel {


        public DeptTreeTableModel() {
        }

        public DeptTreeTableModel(Object root) {
            super(root);
        }


        public void setRoot(Object root) {
            this.root = root;
            modelSupport.fireNewRoot();
        }

        @Override
        public boolean isCellEditable(Object node, int column) {
            if (column == 4) {
                return true;
            }
            return super.isCellEditable(node, column);
        }

        @Override
        public Object getChild(Object parent, int index) {

            Tree parentTree = (Tree) parent;

            return parentTree.getChildren().get(index);
        }

        @Override
        public int getChildCount(Object parent) {
            if (parent instanceof Tree) {
                List<Tree> children = ((Tree) parent).getChildren();

                if (children != null) {
                    return children.size();
                }
            }
            return 0;
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            if (parent instanceof Tree && child instanceof Tree) {
                Tree parentFile = (Tree) parent;
                List<Tree> treeList = parentFile.getChildren();

                for (int i = 0, len = treeList.size(); i < len; i++) {
                    if (treeList.get(i).equals(child)) {
                        return i;
                    }
                }
            }
            return -1;
        }

        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
                case 0:
                    return String.class;
                case 1:
                    return Integer.class;
                case 2:
                    return String.class;
                case 3:
                    return String.class;
                case 4:
                    return String.class;
                default:
                    return super.getColumnClass(column);
            }
        }

        @Override
        public String getColumnName(int column) {
            return COLUMN_ID[column];
        }


        @Override
        public int getColumnCount() {
            return COLUMN_ID.length;
        }

        @Override
        public Object getValueAt(Object node, int column) {
            if (node instanceof Tree) {
                Tree tree = (Tree) node;
                switch (column) {
                    case 0:
                        return tree.getName();
                    case 1:
                        return tree.get("orderNum");
                    case 2:
                        return tree.get("status");
                    case 3:
                        return tree.get("createTime");
                    default:
                        return null;


                }
            }

            return null;
        }

    }


}
