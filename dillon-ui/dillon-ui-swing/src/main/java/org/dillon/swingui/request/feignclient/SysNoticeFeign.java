package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.domain.SysNotice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * sysPostFeign
 *
 * @author liwen
 * @date 2022/07/11
 */
@FeignClient(name = "noticeConfigFeign", url = "${server.url}", path = "/system")
public interface SysNoticeFeign {

    @PostMapping("/notice/list")
    PageResponse<SysNotice> list(PageInfo<SysNotice> pageQuery);

    /**
     * 得到信息
     * 根据 参数编号获取详细信息
     *
     * @param noticeId sysNotice id
     * @return {@link OptResult}
     */
    @GetMapping(value = "/notice/{noticeId}")
    SingleResponse<SysNotice> getInfo(@PathVariable("noticeId") Long noticeId);

    /**
     * 添加
     * 新增 参数
     *
     * @param sysNotice 帖子
     * @return {@link OptResult}
     */
    @PostMapping("/notice")
    OptResult add(SysNotice sysNotice);

    /**
     * 编辑
     * 修改 参数
     *
     * @param sysNotice 帖子
     * @return {@link OptResult}
     */
    @PutMapping("/notice")
    OptResult edit(SysNotice sysNotice);

    /**
     * 删除
     * 删除 参数
     *
     * @param noticeIds sysNotice id
     * @return {@link OptResult}
     */
    @DeleteMapping("/notice/{noticeIds}")
    OptResult remove(@PathVariable("noticeIds") Long[] noticeIds);



}
