package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.GeoPosition;

/**
 * Created by liwen on 16/1/26.
 */
public class TailPoint {
    private GeoPosition geoPosition;
    private String alt;//高度

    public TailPoint(GeoPosition geoPosition, String alt) {
        this.geoPosition = geoPosition;
        this.alt = alt;
    }

    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }
}
