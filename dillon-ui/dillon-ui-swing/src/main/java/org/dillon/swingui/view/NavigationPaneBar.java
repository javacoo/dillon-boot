package org.dillon.swingui.view;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.icons.FlatClearIcon;
import org.dillon.swing.AnimatedSwitchIcon;
import org.dillon.swing.CheckHeaderCellRenderer;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.system.api.model.RouterVo;
import lombok.extern.slf4j.Slf4j;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysMenuFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.system.api.model.TreeSelect;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.StackLayout;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 导航窗格酒吧
 *
 * @author liwen
 * @version： 0.0.1
 * @description:
 * @className: NavigationBar
 * @author: liwen
 * @date: 2021/11/2 09:24
 * @date 2022/07/06
 */
@Slf4j
public class NavigationPaneBar extends JPanel implements Observer {


    /**
     * 导航树
     */
    private JXTree navigationTree;
    /**
     * 树窗格
     */
    private JPanel treePane;
    /**
     * 菜单面板
     */
    private JPanel menuPane;
    /**
     * 菜单栏
     */
    private JToolBar menuBar;
    /**
     * 根节点
     */
    private DefaultMutableTreeNode rootNode;
    /**
     * 树模型
     */
    private DefaultTreeModel treeModel;
    /**
     * 树α
     */
    private float treeAlpha = 1f;
    /**
     * 菜单α
     */
    private float menuAlpha = 0f;

    private JPopupMenu popupMenu;

    /**
     * 导航窗格酒吧
     */
    public NavigationPaneBar() {
        ApplicatonStore.addMenuRefreshObservable(this);
        initComponents();
        initListener();
    }

    /**
     * 初始化组件
     */
    private void initComponents() {
        this.setLayout(new StackLayout());
        this.setOpaque(false);
        this.setPreferredSize(new Dimension(240, 0));

        this.add(getTreePane(), StackLayout.TOP);
        this.add(getMenuPane(), StackLayout.BOTTOM);
    }

    /**
     * 得到树面板
     *
     * @return {@link JPanel}
     */
    public JPanel getTreePane() {
        if (treePane == null) {

            JScrollPane scrollPane = new JScrollPane(getNavigationTree());
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setOpaque(false);
            scrollPane.getViewport().setOpaque(false);
            scrollPane.setBorder(BorderFactory.createEmptyBorder());

            treePane = new JPanel(new BorderLayout()) {
                @Override
                protected void paintComponent(Graphics g) {
                    Graphics2D g2 = (Graphics2D) g;
                    g2.setComposite(AlphaComposite.SrcOver.derive(treeAlpha));
                    super.paintComponent(g);
                }
            };
            treePane.add(scrollPane);
        }
        return treePane;
    }

    /**
     * 把菜单面板
     *
     * @return {@link JPanel}
     */
    public JPanel getMenuPane() {
        if (menuPane == null) {
            menuPane = new JPanel(new BorderLayout()) {
                @Override
                protected void paintComponent(Graphics g) {
                    Graphics2D g2 = (Graphics2D) g;
                    g2.setComposite(AlphaComposite.SrcOver.derive(menuAlpha));
                    super.paintComponent(g);
                }
            };
            menuPane.add(getMenuBar());
            menuPane.setVisible(false);
        }
        return menuPane;
    }

    /**
     * init侦听器
     */
    private void initListener() {
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

                updateUI();
            }
        });

        navigationTree.addTreeWillExpandListener(new TreeWillExpandListener() {
            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
                if (treeNode.getLevel() < 2) {
                    navigationTree.collapseAll();

                }
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
            }
        });

    }

    /**
     * 把菜单栏
     *
     * @return {@link JToolBar}
     */
    public JToolBar getMenuBar() {
        if (menuBar == null) {
            menuBar = new JToolBar(JToolBar.VERTICAL);
            menuBar.setFloatable(false);
            menuBar.setLayout(new VerticalLayout(10));
        }
        return menuBar;
    }

    /**
     * 得到导航树
     *
     * @return {@link JXTree}
     */
    private JXTree getNavigationTree() {

        if (navigationTree == null) {
            navigationTree = new JXTree(new DefaultTreeModel(new DefaultMutableTreeNode("")));
            navigationTree.setOpaque(false);
            navigationTree.setToggleClickCount(1);
            navigationTree.setRowHeight(50);
            navigationTree.setFont(navigationTree.getFont().deriveFont(Font.BOLD));
            navigationTree.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    treeSelect();
                }
            });
//            navigationTree.setBorder(null);
//            navigationTree.setExpandedIcon(null);
//            navigationTree.setCollapsedIcon(null);
            navigationTree.setHighlighters(new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, Color.cyan, UIManager.getColor("App.selectionForeground")));
            navigationTree.setRolloverEnabled(true);

            navigationTree.setCellRenderer(new DefaultTreeCellRenderer() {
                @Override
                public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

                    JLabel label = new JLabel(value.toString()){
                        @Override
                        public void setForeground(Color fg) {
                            super.setForeground(fg);
                            if (getIcon() != null && getIcon() instanceof FlatSVGIcon) {
                                FlatSVGIcon icon = (FlatSVGIcon) getIcon();
                                icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
                                    return getForeground();
                                }));
                            }
                        }
                    };
                    if (sel) {
                        label.setForeground(new Color(0xffffff));
                    }
//                    JPanel box = new JPanel(new BorderLayout()) {
//                        @Override
//                        public void setForeground(Color fg) {
//                            if (sel) {
//                                label.setForeground(new Color(0xffffff));
//                            } else {
//                                label.setForeground(fg);
//                            }
//                            if (label.getIcon() != null && label.getIcon() instanceof FlatSVGIcon) {
//                                FlatSVGIcon icon = (FlatSVGIcon) label.getIcon();
//                                icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
//                                    return label.getForeground();
//                                }));
//                            }
//
//                        }
//                    };
//                    box.setOpaque(false);
                    label.setIconTextGap(10);
//                    label.setPreferredSize(new Dimension(NavigationPaneBar.this.getWidth() - 45, tree.getRowHeight()));
//                    if (!leaf) {
//                        JLabel arrorLabel = new JLabel("");
//                        arrorLabel.setIcon(expanded ? UIManager.getIcon("Tree.expandedIcon") : UIManager.getIcon("Tree.collapsedIcon"));
//                        box.add(arrorLabel, BorderLayout.EAST);
//                    }
                    if (value instanceof DefaultMutableTreeNode) {
                        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) value;

                        if (defaultMutableTreeNode.getUserObject() instanceof RouterVo) {
                            RouterVo menu = (RouterVo) defaultMutableTreeNode.getUserObject();
                            label.setText(menu.getMeta().getTitle());
                            FlatSVGIcon icon = getSvgIcon("icons/menu/" + menu.getMeta().getIcon() + ".svg", 20, 20);
                            label.setIcon(icon);
                            if (sel) {
                                icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
                                    return label.getForeground();
                                }));
                            }
//                            box.add(label, BorderLayout.CENTER);

                        }
                    }
                    label.setFont(label.getFont().deriveFont(Font.BOLD));
                    return label;
                }
            });

        }
        return navigationTree;
    }

    /**
     * 树选择
     */
    private void treeSelect() {

        // TODO 自动生成的方法存根
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) navigationTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        if (node.getUserObject() instanceof RouterVo) {

            RouterVo menu = (RouterVo) node.getUserObject();
            if (menu.getChildren() == null || menu.getChildren().isEmpty()) {
                addTab(menu);
            }

        }
    }

    /**
     * 添加选项卡
     *
     * @param menu 菜单
     */
    private void addTab(RouterVo menu) {
        if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab(menu.getMeta().getTitle()) == -1) {
            FlatSVGIcon icon;
            icon = getSvgIcon("icons/menu/" + menu.getMeta().getIcon() + ".svg", 20, 20);

            MainFrame.getInstance().getMainPane().getTabbedPane().addTab(menu.getMeta().getTitle(), icon, ApplicatonStore.getNavigatonPanel(menu.getComponent()));
        }
        MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab(menu.getMeta().getTitle()));

    }

    /**
     * 构建树和菜单数据
     *
     * @param menuList 菜单列表
     */
    private void buildTreeAndMenuData(List<RouterVo> menuList) {
        rootNode = new DefaultMutableTreeNode("root");
        createMenuTreeDate(rootNode, menuList);

        navigationTree.setModel(treeModel = new DefaultTreeModel(rootNode));
        navigationTree.expandRow(0);
        navigationTree.setShowsRootHandles(true);
        navigationTree.setRootVisible(false);
        navigationTree.expandRow(0);

        createMenuBarTreeDate(menuList);

    }

    private void createMenuTreeDate(DefaultMutableTreeNode fatherNode, List<RouterVo> routerVoList) {

        for (RouterVo treeNode : routerVoList) {

            DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeNode);
            fatherNode.add(node);

            if (CollectionUtil.isNotEmpty(treeNode.getChildren())) {
                createMenuTreeDate(node, treeNode.getChildren());
            }
        }
    }

    private void createMenuBarTreeDate(List<RouterVo> routerVoList) {
        getMenuBar().removeAll();
        ButtonGroup group = new ButtonGroup();
        for (RouterVo treeNode : routerVoList) {

            JToggleButton menu = new JToggleButton();
            menu.setToolTipText(treeNode.getMeta().getTitle());
            menu.setOpaque(false);
            menu.setIcon(getSvgIcon("icons/menu/" + treeNode.getMeta().getIcon() + ".svg", 25, 25));
            menu.setPreferredSize(new Dimension(48, 40));
            findMenuChildren(menu, treeNode);
            getMenuBar().add(menu);
            group.add(menu);

        }
    }


    /**
     * 发现儿童菜单
     * 递归查找子节点
     *
     * @param menu     菜单
     * @param menuBean 菜单豆
     */
    public void findMenuChildren(JToggleButton menu, RouterVo menuBean) {
        if (CollectionUtil.isEmpty(menuBean.getChildren())) {
            menu.addActionListener(e -> {
                addTab(menuBean);
            });
            return;
        }
        menu.addActionListener(e -> showPopupMenuButtonActionPerformed((Component) e.getSource(), menuBean));
        menu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                showPopupMenuButtonActionPerformed((Component) e.getSource(), menuBean);
            }

            @Override
            public void mouseExited(MouseEvent e) {
//                if (popupMenu !=  null&&popupMenu.getInvoker()!=e.getSource()) {
//                    popupMenu.setVisible(false);
//                }
            }
        });
    }


    /**
     * 显示弹出菜单按钮执行操作
     *
     * @param menuBean 菜单豆
     * @param invoker  调用程序
     */
    private void showPopupMenuButtonActionPerformed(Component invoker, RouterVo menuBean) {

        if (CollectionUtil.isEmpty(menuBean.getChildren())) {
            return;
        }
        popupMenu = new JPopupMenu();
        for (RouterVo it : menuBean.getChildren()) {
            if (CollectionUtil.isEmpty(it.getChildren())) {
                JCheckBoxMenuItem itemNode = new JCheckBoxMenuItem(it.getMeta().getTitle());
                itemNode.setIcon(getSvgIcon("icons/menu/" + it.getMeta().getIcon() + ".svg", 25, 25));
                itemNode.addActionListener(e1 -> addTab(it));
                itemNode.setPreferredSize(new Dimension(180, 40));
                popupMenu.add(itemNode);
            } else {
                JMenu menu = new JMenu(it.getMeta().getTitle());
                menu.setIcon(getSvgIcon("icons/menu/" + it.getMeta().getIcon() + ".svg", 25, 25));
                createMenuTreeDate(menu, it.getChildren());
                popupMenu.add(menu);
            }

        }
        popupMenu.show(invoker, invoker.getWidth(), 0);

    }

    private void createMenuTreeDate(JMenu menu, List<RouterVo> routerVoList) {

        for (RouterVo menuItem : routerVoList) {

            if (CollectionUtil.isEmpty(menuItem.getChildren())) {
                JCheckBoxMenuItem itemNode = new JCheckBoxMenuItem(menuItem.getMeta().getTitle());
                itemNode.setIcon(getSvgIcon("icons/menu/" + menuItem.getMeta().getIcon() + ".svg", 25, 25));
                itemNode.addActionListener(e1 -> addTab(menuItem));
                itemNode.setPreferredSize(new Dimension(180, 40));
                menu.add(itemNode);
            } else {
                JMenu menuP = new JMenu(menuItem.getMeta().getTitle());
                menu.setIcon(getSvgIcon("icons/menu/" + menuItem.getMeta().getIcon() + ".svg", 25, 25));
                createMenuTreeDate(menuP, menuItem.getChildren());
                menu.add(menuP);
            }
        }
    }

    /**
     * 初始化数据
     */
    public void updateData() {

        SwingWorker<MultiResponse<RouterVo>, Object> worker = new SwingWorker<MultiResponse<RouterVo>, Object>() {
            @Override
            protected MultiResponse<RouterVo> doInBackground() throws Exception {

                return Request.connector(SysMenuFeign.class).getRouters();


            }

            @Override
            protected void done() {
                try {
                    if (get().isSuccess()) {
                        buildTreeAndMenuData(get().getData());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();
    }

//    @Override
//    public void updateUI() {
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                if (navigationTree != null) {
//                    navigationTree.setExpandedIcon(null);
//                    navigationTree.setCollapsedIcon(null);
//                }
//            }
//        });
//        navigationTree.updateUI();
//        super.updateUI();
//
//    }

    /**
     * 让svg图标
     *
     * @param path 路径
     * @param w    w
     * @param h    h
     * @return {@link FlatSVGIcon}
     */
    private FlatSVGIcon getSvgIcon(String path, int w, int h) {
        FlatSVGIcon flatSVGIcon = new FlatSVGIcon(path, w, h);
        flatSVGIcon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
            return UIManager.getColor("Label.foreground") == null ? new Color(0xf8f8f8) : UIManager.getColor("Label.foreground");
        }));
        return flatSVGIcon;
    }

    /**
     * 让树α
     *
     * @return float
     */
    public float getTreeAlpha() {
        return treeAlpha;
    }

    /**
     * 设置树α
     *
     * @param treeAlpha 树α
     */
    public void setTreeAlpha(float treeAlpha) {
        this.treeAlpha = treeAlpha;
        getTreePane().repaint();
    }

    /**
     * 把菜单α
     *
     * @return float
     */
    public float getMenuAlpha() {
        return menuAlpha;
    }

    /**
     * 设置菜单α
     *
     * @param menuAlpha 菜单α
     */
    public void setMenuAlpha(float menuAlpha) {
        this.menuAlpha = menuAlpha;
        getMenuPane().repaint();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
