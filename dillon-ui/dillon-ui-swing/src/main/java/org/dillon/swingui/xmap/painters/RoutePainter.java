
package org.dillon.swingui.xmap.painters;

import org.dillon.swingui.bean.LineBean;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.Painter;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Paints a route
 *
 * @author Martin Steiger
 */
public class RoutePainter implements Painter<JXMapViewer> {
    private Color color = Color.RED;
    private boolean antiAlias = true;

    private List<LineBean> track;

    /**
     * @param track the track
     */
    public RoutePainter(List<LineBean> track) {
        // copy the list so that changes in the 
        // original list do not have an effect here
        this.track = new ArrayList<LineBean>(track);
    }


    @Override
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = (Graphics2D) g.create();

        // convert from viewport to world bitmap
        Rectangle rect = map.getViewportBounds();
        g.translate(-rect.x, -rect.y);

        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        // do the drawing
        g.setColor(new Color(0x03CB7E));
        float strokeWidth = 4f;
        g.setStroke(new BasicStroke(strokeWidth));
        for (LineBean lineBean : track) {
            Point2D p0 = map.getTileFactory().geoToPixel(lineBean.getSendGeoPosition(), map.getZoom());
            Point2D pe = map.getTileFactory().geoToPixel(lineBean.getReceivingGeoPosition(), map.getZoom());
            g.drawLine((int) p0.getX(), (int) p0.getY(), (int) pe.getX(), (int) pe.getY());
            // Computes the norm and the inverse norm
            double dx = pe.getX() - p0.getX();
            double dy = pe.getY() - p0.getY();
            double absSize = 6;
            double dist = Math.max(1, Math.sqrt(dx * dx + dy * dy));
            double unitX = dx / dist;
            double unitY = dy / dist;
            double nx = unitX * absSize;
            double ny = unitY * absSize;

            // Allow for stroke width in the end point used and the
            // orthogonal vectors describing the direction of the
            // marker
            double strokeX = unitX * strokeWidth;
            double strokeY = unitY * strokeWidth;
            Point2D pe1 = new Point2D.Double(pe.getX() - strokeX / 2.0, pe.getY() - strokeY / 2.0);

            paintMarker(g, pe1, nx, ny, absSize, true);
            g.drawString(lineBean.getName(), (float) pe.getX(), (float) pe.getY());

        }
        g.dispose();
    }


    public void paintMarker(Graphics2D g, Point2D pe, double nx,
                            double ny, double size, boolean source) {
        Polygon poly = new Polygon();
        poly.addPoint((int) Math.round(pe.getX()),
                (int) Math.round(pe.getY()));
        poly.addPoint((int) Math.round(pe.getX() - nx - ny / 2),
                (int) Math.round(pe.getY() - ny + nx / 2));

        poly.addPoint((int) Math.round(pe.getX() - nx * 3 / 4),
                (int) Math.round(pe.getY() - ny * 3 / 4));

        poly.addPoint((int) Math.round(pe.getX() + ny / 2 - nx),
                (int) Math.round(pe.getY() - ny - nx / 2));

        g.fill(poly);
        g.draw(poly);


    }

    public void paintShape(Graphics2D g, String text) {


    }

}
