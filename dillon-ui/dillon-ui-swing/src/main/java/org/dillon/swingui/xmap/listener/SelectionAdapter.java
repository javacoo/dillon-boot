
package org.dillon.swingui.xmap.listener;

import org.jxmapviewer.JXMapViewer;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * Creates a selection rectangle based on mouse input
 * Also triggers repaint events in the viewer
 *
 * @author Martin Steiger
 */
public class SelectionAdapter extends MouseAdapter {
    public static Point2D movePoint = new Point2D.Double();
   private JXMapViewer mapViewer;

    public SelectionAdapter(JXMapViewer mapViewer) {
        this.mapViewer=mapViewer;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        super.mouseMoved(e);
        Rectangle rectangle=  mapViewer.getViewportBounds();
        movePoint.setLocation(rectangle.getX()+e.getX(),rectangle.getY()+ e.getY());
        mapViewer.repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        movePoint.setLocation(0, 0);
    }
}
