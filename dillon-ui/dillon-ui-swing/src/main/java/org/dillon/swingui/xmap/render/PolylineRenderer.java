
package org.dillon.swingui.xmap.render;


import org.dillon.swingui.xmap.geometry.ShapePloyLine;
import org.dillon.swingui.xmap.geometry.ShapeAttributes;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * 线渲染器
 */
public class PolylineRenderer extends AbstractRenderer<ShapePloyLine> {

    /**
     * 渲染实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param polyline
     */
    @Override
    protected void doPaint(Graphics2D g, JXMapViewer jxMapViewer, ShapePloyLine polyline) {

        Rectangle rect = jxMapViewer.getViewportBounds();

        ShapeAttributes shapeAttributes = polyline.getShapeAttributes();
        g.setColor(shapeAttributes.getOutlineColor());
        g.setStroke(new BasicStroke(shapeAttributes.getOutlineWidth()));
        drawRoute(g, jxMapViewer, polyline.getPositions(), rect);
        g.dispose();

    }

    /**
     * 渲染实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param polylines
     */
    @Override
    protected void doPaintMany(Graphics2D g, JXMapViewer jxMapViewer, Iterable<? extends ShapePloyLine> polylines) {

        if (polylines == null) {
            return;
        }

        Rectangle rect = jxMapViewer.getViewportBounds();

        for (ShapePloyLine polyline : polylines) {
            ShapeAttributes shapeAttributes = polyline.getShapeAttributes();
            g.setColor(shapeAttributes.getOutlineColor());
            g.setStroke(new BasicStroke(shapeAttributes.getOutlineWidth()));
            drawRoute(g, jxMapViewer, polyline.getPositions(), rect);
        }
    }

    /**
     * 绘制线
     *
     * @param g   the graphics object
     * @param map the map
     */
    private void drawRoute(Graphics2D g, JXMapViewer map, java.util.List<GeoPosition> positions, Rectangle rect) {
        int lastX = 0;
        int lastY = 0;

        boolean first = true;

        int nextX = 0;
        int nextY = 0;
        for (GeoPosition gp : positions) {
            // convert geo-coordinate to world bitmap pixel
            Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());
            nextX = (int) pt.getX();
            nextY = (int) pt.getY();

            if (first) {
                first = false;
            } else {
                if (rect.intersects(lastX, lastY, nextX, nextY))
                    g.drawLine(lastX, lastY, nextX, nextY);
            }

            lastX = nextX;
            lastY = nextY;
        }
    }
}
