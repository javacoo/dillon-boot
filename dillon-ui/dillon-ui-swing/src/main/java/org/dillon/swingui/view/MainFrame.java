package org.dillon.swingui.view;

import com.alibaba.cola.dto.Response;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGUtils;
import com.formdev.flatlaf.util.SystemInfo;
import org.dillon.swing.WOptionPane;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.LoginService;
import org.dillon.swingui.request.feignclient.SysUserFeign;
import org.dillon.swingui.request.feignclient.SysUserOnlineFeign;
import org.dillon.swingui.store.ApplicatonStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.util.concurrent.ExecutionException;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.WARNING_MESSAGE;

/**
 * 主框架
 *
 * @author liwen
 * @version： 0.0.1
 * @description:
 * @className: AppUI
 * @author: liwen
 * @date: 2021/7/9 09:53
 * @date 2022/07/06
 */
public class MainFrame extends JFrame {

    /**
     * 实例
     */
    private static MainFrame instance = null;
    /**
     * 主面板
     */
    private MainPane mainPane = null;
    /**
     * 登录面板
     */
    private LoginPane loginPane = null;
    /**
     * 主菜单栏
     */
    private MainMenuBar mainMenuBar;

    /**
     * 主框架
     */
    private MainFrame() {

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(900, 700);

        // macOS  (see https://www.formdev.com/flatlaf/macos/)
        if (SystemInfo.isMacOS) {


            if (SystemInfo.isMacFullWindowContentSupported) {
                // expand window content into window title bar and make title bar transparent
                getRootPane().putClientProperty("apple.awt.fullWindowContent", true);
                getRootPane().putClientProperty("apple.awt.transparentTitleBar", true);

                // hide window title
                if (SystemInfo.isJava_17_orLater) {
                    getRootPane().putClientProperty("apple.awt.windowTitleVisible", false);
                } else {
                    setTitle("aaa");
                }

                // add gap to left side of toolbar
            }

            // enable full screen mode for this window (for Java 8 - 10; not necessary for Java 11+)
            if (!SystemInfo.isJava_11_orLater){
                getRootPane().putClientProperty("apple.awt.fullscreenable", true);}
        }
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {

                systemExit();
            }
        });
    }

    /**
     * 获得实例
     *
     * @return {@link MainFrame}
     */
    public static MainFrame getInstance() {
        if (null == instance) {
            synchronized (MainFrame.class) {
                if (null == instance) {
                    instance = new MainFrame();
                }
            }
        }
        return instance;
    }

    public void systemExit() {

        SwingWorker<Response, Object> swingWorker = new SwingWorker<Response, Object>() {
            @Override
            protected Response doInBackground() throws Exception {

                return Request.connector(LoginService.class).logout();
            }

            @Override
            protected void done() {
                try {
                    if (get().isSuccess()) {
                        System.exit(0);
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();
    }

    public void loginOut() {
        SwingWorker<Response, Object> swingWorker = new SwingWorker<Response, Object>() {
            @Override
            protected Response doInBackground() throws Exception {

                return Request.connector(LoginService.class).logout();
            }

            @Override
            protected void done() {
                try {
                    if (get().isSuccess()) {
                        ApplicatonStore.setToken(null);
                        showLogin();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();
    }

    /**
     * 显示登录
     */
    public void showLogin() {
        this.setJMenuBar(null);
        this.setContentPane(getLoginPane());
        this.setLocationRelativeTo(null);
        getLoginPane().initData();

        this.setVisible(true);
    }

    /**
     * 显示主面板
     */
    public void showMainPanel() {
        this.setIconImages(null);
        this.setTitle("");
        this.setContentPane(mainPane = new MainPane());
        this.setJMenuBar(getMainMenuBar());
        revalidate();
    }

    /**
     * 显示地图主面板
     */
    public void showMapMainPanel() {
        JRootPane rootPane = this.getRootPane();
        if (rootPane != null) {
            rootPane.setWindowDecorationStyle(JRootPane.FRAME);
            rootPane.putClientProperty(FlatClientProperties.MENU_BAR_EMBEDDED, true);
        }

        this.setContentPane(new MapMainPane());
    }


    /**
     * 得到主面板
     *
     * @return {@link MainPane}
     */
    public MainPane getMainPane() {
        if (mainPane == null) {
            mainPane = new MainPane();
        }
        return mainPane;
    }


    /**
     * 获得登录面板
     *
     * @return {@link LoginPane}
     */
    public LoginPane getLoginPane() {
        if (loginPane == null) {
            loginPane = new LoginPane();
        }
        return loginPane;
    }

    /**
     * 得到主菜单栏
     *
     * @return {@link MainMenuBar}
     */
    public MainMenuBar getMainMenuBar() {
        if (mainMenuBar == null) {
            mainMenuBar = new MainMenuBar();
        }
        return mainMenuBar;
    }
}



