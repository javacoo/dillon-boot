package org.dillon.swingui.view.component;

import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.AreaRendererEndType;
import org.jfree.chart.renderer.category.AreaRenderer;
import org.jfree.chart.renderer.category.CategoryItemRendererState;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.data.category.CategoryDataset;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

/**
 * @className: MyAreaRenderer
 * @author: liwen
 * @date: 2022/4/8 22:12
 */
public class MyAreaRenderer extends AreaRenderer {

    private AreaRendererEndType endType;

    public MyAreaRenderer() {
    }

    @Override
    public void drawItem(Graphics2D g2, CategoryItemRendererState state, Rectangle2D dataArea, CategoryPlot plot, CategoryAxis domainAxis, ValueAxis rangeAxis, CategoryDataset dataset, int row, int column, int pass) {
        if (this.getItemVisible(row, column)) {
            Number value = dataset.getValue(row, column);
            if (value != null) {
                PlotOrientation orientation = plot.getOrientation();
                RectangleEdge axisEdge = plot.getDomainAxisEdge();
                int count = dataset.getColumnCount();
                float x0 = (float)domainAxis.getCategoryStart(column, count, dataArea, axisEdge);
                float x1 = (float)domainAxis.getCategoryMiddle(column, count, dataArea, axisEdge);
                float x2 = (float)domainAxis.getCategoryEnd(column, count, dataArea, axisEdge);
                x0 = (float)Math.round(x0);
                x1 = (float)Math.round(x1);
                x2 = (float)Math.round(x2);
                if (this.endType == AreaRendererEndType.TRUNCATE) {
                    if (column == 0) {
                        x0 = x1;
                    } else if (column == this.getColumnCount() - 1) {
                        x2 = x1;
                    }
                }

                double yy1 = value.doubleValue();
                double yy0 = 0.0D;
                if (this.endType == AreaRendererEndType.LEVEL) {
                    yy0 = yy1;
                }

                if (column > 0) {
                    Number n0 = dataset.getValue(row, column - 1);
                    if (n0 != null) {
                        yy0 = (n0.doubleValue() + yy1) / 2.0D;
                    }
                }

                double yy2 = 0.0D;
                if (column < dataset.getColumnCount() - 1) {
                    Number n2 = dataset.getValue(row, column + 1);
                    if (n2 != null) {
                        yy2 = (n2.doubleValue() + yy1) / 2.0D;
                    }
                } else if (this.endType == AreaRendererEndType.LEVEL) {
                    yy2 = yy1;
                }

                RectangleEdge edge = plot.getRangeAxisEdge();
                float y0 = (float)rangeAxis.valueToJava2D(yy0, dataArea, edge);
                float y1 = (float)rangeAxis.valueToJava2D(yy1, dataArea, edge);
                float y2 = (float)rangeAxis.valueToJava2D(yy2, dataArea, edge);
                float yz = (float)rangeAxis.valueToJava2D(0.0D, dataArea, edge);
                double labelXX = (double)x1;
                double labelYY = (double)y1;
                g2.setPaint(this.getItemPaint(row, column));
                g2.setStroke(this.getItemStroke(row, column));
                GeneralPath area = new GeneralPath();
                if (orientation == PlotOrientation.VERTICAL) {
                    area.moveTo(x0, yz);
                    area.lineTo(x0, y0);
                    area.lineTo(x1, y1);
                    area.lineTo(x2, y2);
                    area.lineTo(x2, yz);
                } else if (orientation == PlotOrientation.HORIZONTAL) {
                    area.moveTo(yz, x0);
                    area.lineTo(y0, x0);
                    area.lineTo(y1, x1);
                    area.lineTo(y2, x2);
                    area.lineTo(yz, x2);
                    double temp = labelXX;
                    labelXX = labelYY;
                    labelYY = temp;
                }

                area.closePath();
                GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x350FEBFF, true), 0, area.getBounds().height, new Color(0x74FFFC, true));

                g2.setPaint(gradientPaint);
                g2.fill(area);
                if (this.isItemLabelVisible(row, column)) {
                    this.drawItemLabel(g2, orientation, dataset, row, column, labelXX, labelYY, value.doubleValue() < 0.0D);
                }

                int datasetIndex = plot.indexOf(dataset);
                this.updateCrosshairValues(state.getCrosshairState(), dataset.getRowKey(row), dataset.getColumnKey(column), yy1, datasetIndex, (double)x1, (double)y1, orientation);
                EntityCollection entities = state.getEntityCollection();
                if (entities != null) {
                    this.addItemEntity(entities, dataset, row, column, area);
                }

            }
        }
    }
}
