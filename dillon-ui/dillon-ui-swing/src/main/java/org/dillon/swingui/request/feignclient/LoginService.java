package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.ValidateCodeEntity;
import org.dillon.swingui.bean.LoginVO;
import org.dillon.system.api.domain.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@FeignClient(name = "loginService", url = "${server.url}")
public interface LoginService {

    @GetMapping("/code")
    SingleResponse<ValidateCodeEntity> getCode();

    @PostMapping("/auth/login")
    SingleResponse<Map<String, Object>> login(LoginVO loginVO);

    @DeleteMapping("/auth/logout")
    Response logout();

    @GetMapping("/system/user/getInfo")
    SingleResponse<UserInfo> getInfo();


}
