package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.beans.AbstractBean;
import org.jxmapviewer.viewer.GeoBounds;

import java.awt.image.BufferedImage;

/**
 * 贴图
 */
public class SurfaceImage extends AbstractBean {
    private BufferedImage image;//贴图图片
    private GeoBounds bounds;//贴图区域

    /**
     * 构建SurfaceImage
     *
     */
    public SurfaceImage(){

    }

    /**
     * 构建SurfaceImage
     *
     * @param image 图片
     * @param bounds 贴图区域
     */
    public SurfaceImage(BufferedImage image, GeoBounds bounds){
        this.image = image;
        this.bounds = bounds;
    }

    /**
     * 获取图片
     *
     * @return
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * 设置图片
     *
     * @param image
     */
    public void setImage(BufferedImage image) {
        this.image = image;
    }

    /**
     * 获取贴图区域
     *
     * @return
     */
    public GeoBounds getBounds() {
        return bounds;
    }

    /**
     * 设置贴图区域
     *
     * @param bounds
     */
    public void setBounds(GeoBounds bounds) {
        this.bounds = bounds;
    }
}
