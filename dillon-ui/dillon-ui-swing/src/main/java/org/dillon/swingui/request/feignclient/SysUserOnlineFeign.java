package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.PageResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.system.api.model.SysUserOnline;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "sysUserOnlineFeign", url = "${server.url}", path = "/system")
public interface SysUserOnlineFeign {

    @PostMapping("/online/list")
    PageResponse<SysUserOnline> list(PageInfo<SysUserOnline> pageQuery);

    /**
     * 强退用户
     */
    @DeleteMapping("/online/{tokenId}")
    OptResult forceLogout(@PathVariable("tokenId") String tokenId);
}
