package org.dillon.swingui.view.monitor;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.icons.FlatClearIcon;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.CheckHeaderCellRenderer;
import org.dillon.swing.WOptionPane;
import org.dillon.swing.WPaginationPane;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysRoleFeign;
import org.dillon.swingui.request.feignclient.SysUserOnlineFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.swingui.view.system.role.AssignUsersPanel;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.model.SysUserOnline;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;


/**
 * 在线用户面板
 *
 * @author liwen
 * @date 2022/07/18
 */
public class UserOnlinePanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"序号", "会话编号", "登录名称", "部门名称", "主机", "登录时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;


    private JTextField addrTextField;
    private JTextField nameTextField;


    /**
     * 目录单选按钮
     */

    public UserOnlinePanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("登录地址"));

        toolBar.add(addrTextField = createTextField("请输入登录地址"));
        addrTextField.setColumns(20);
        toolBar.add(new JLabel("用户名称"));
        toolBar.add(nameTextField = createTextField("请输入用户名称"));
        nameTextField.setColumns(20);


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            addrTextField.setText("");
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);


        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 6) {
                    return true;
                }
                return false;
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn("序号").setMinWidth(40);
        table.getColumn("序号").setMaxWidth(40);
        table.getColumn("操作").setMinWidth(80);
        table.getColumn("操作").setMaxWidth(80);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(toolBar, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);


                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());

        JButton del = new JButton("强退");
        del.setIcon(new FlatSVGIcon("icons/logout.svg",20,20));
        del.addActionListener(e -> forceLogout());
        del.setForeground(UIManager.getColor("App.accentColor"));
        optBar.add(del);
        return optBar;

    }

    private void forceLogout() {


        String tokenId = (String) tableModel.getValueAt(table.getSelectedRow(), 1);
        String name = (String) tableModel.getValueAt(table.getSelectedRow(), 2);

        int opt = WOptionPane.showOptionDialog(this, "是否确认强退名称为["+name+"]的用户？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, Object> worker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {


                return Request.connector(SysUserOnlineFeign.class).forceLogout(tokenId);

            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        worker.execute();
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    public void updateData() {


        SysUserOnline sysUserOnline = new SysUserOnline();
        sysUserOnline.setUserName(nameTextField.getText());
        sysUserOnline.setIpaddr(addrTextField.getText());
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysUserOnline);

        SwingWorker<List<SysUserOnline>, Object> swingWorker = new SwingWorker<List<SysUserOnline>, Object>() {
            @Override
            protected List<SysUserOnline> doInBackground() throws Exception {
                return Request.connector(SysUserOnlineFeign.class).list(pageInfo).getData();
            }

            @Override
            protected void done() {

                try {
                    int index = 1;
                    for (SysUserOnline userOnline : get()) {
                        Vector rowV = new Vector();
                        rowV.add(index);
                        rowV.add(userOnline.getTokenId());
                        rowV.add(userOnline.getUserName());
                        rowV.add(userOnline.getDept());
                        rowV.add(userOnline.getIpaddr());
                        rowV.add(DateUtil.format(new Date(userOnline.getLoginTime()), "yyyy-MM-dd HH:mm:ss"));
                        rowV.add(userOnline);
                        tableModel.addRow(rowV);
                        index++;
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }

            }
        };
        tableModel.setRowCount(0);
        swingWorker.execute();

    }

    @Override
    public void update(Observable o, Object arg) {
        updateData();
    }
}