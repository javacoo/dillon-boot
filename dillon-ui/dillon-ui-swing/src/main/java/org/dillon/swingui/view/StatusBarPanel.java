package org.dillon.swingui.view;


import com.formdev.flatlaf.extras.FlatSVGIcon;

import javax.swing.*;
import java.awt.*;

/**
 * 系统状态栏
 * Created by liwen on 2017/7/24.
 */
public class StatusBarPanel extends JToolBar {

    private JToolBar rightTool;
    private JToolBar centerTool;
    private JLabel brandLabel;


    /**
     * 连接状态
     */
    private JButton connectionStatusButton;


    public StatusBarPanel() {
        initSwing();
    }


    private void initSwing() {
        this.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        this.setPreferredSize(new Dimension(0, 40));
        this.add(Box.createHorizontalGlue());
        this.add(getConnectionStatusButton());

    }

    public JButton getConnectionStatusButton() {
        if (connectionStatusButton == null) {
            connectionStatusButton = new JButton();
        }
        return connectionStatusButton;
    }


    public void changeStatus(boolean status) {

        if (status) {
            connectionStatusButton.setIcon(new FlatSVGIcon("icons/lianxian.svg", 20, 20));
            connectionStatusButton.setToolTipText("连接正常");
        } else {
            connectionStatusButton.setToolTipText("连接断开");
            connectionStatusButton.setIcon(new FlatSVGIcon("icons/duankailianxian.svg", 20, 20));
        }
    }
}
