package org.dillon.swingui.xmap.geometry;

public class AirportSigns {

	private String name; // 名称
	private String fourCode; // 四字码
	private String horizontalVisibility; // 水平能见度
	private String atmosphericTemperature; // 大气温度
	private String demPointTemperature; // 露点温度
	private String airPressure; // 气压
	private String windDirection; // 风向
	private String windSpeed; // 风速

	public AirportSigns() {

	}

	public AirportSigns(String name, String fourCode,
			String horizontalVisibility, String atmosphericTemperature,
			String demPointTemperature, String airPressure,
			String windDirection, String windSpeed) {
		super();
		this.name = name;
		this.fourCode = fourCode;
		this.horizontalVisibility = horizontalVisibility;
		this.atmosphericTemperature = atmosphericTemperature;
		this.demPointTemperature = demPointTemperature;
		this.airPressure = airPressure;
		this.windDirection = windDirection;
		this.windSpeed = windSpeed;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFourCode() {
		return fourCode;
	}

	public void setFourCode(String fourCode) {
		this.fourCode = fourCode;
	}

	public String getHorizontalVisibility() {
		return horizontalVisibility;
	}

	public void setHorizontalVisibility(String horizontalVisibility) {
		this.horizontalVisibility = horizontalVisibility;
	}

	public String getAtmosphericTemperature() {
		return atmosphericTemperature;
	}

	public void setAtmosphericTemperature(String atmosphericTemperature) {
		this.atmosphericTemperature = atmosphericTemperature;
	}

	public String getDemPointTemperature() {
		return demPointTemperature;
	}

	public void setDemPointTemperature(String demPointTemperature) {
		this.demPointTemperature = demPointTemperature;
	}

	public String getAirPressure() {
		return airPressure;
	}

	public void setAirPressure(String airPressure) {
		this.airPressure = airPressure;
	}

	public String getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}

	public String getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}

}
