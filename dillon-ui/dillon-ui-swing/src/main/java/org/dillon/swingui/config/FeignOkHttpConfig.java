package org.dillon.swingui.config;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import feign.Feign;
import feign.Logger;
import okhttp3.*;
import org.dillon.swing.WOptionPane;
import org.dillon.swing.notice.WMessage;
import org.dillon.swingui.view.MainFrame;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.swing.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;
import static org.dillon.common.core.web.domain.OptResult.MSG_TAG;
import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.WARNING_MESSAGE;

@Configuration
@ConditionalOnClass(Feign.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
public class FeignOkHttpConfig {


    @Bean
    public okhttp3.OkHttpClient okHttpClient() {
        return new okhttp3.OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool())
                .addInterceptor(new FeignOkHttpClientResponseInterceptor())
                .build();
    }

    @Bean
    public Logger.Level logger() {
        return Logger.Level.FULL;
    }

    /**
     * okHttp响应拦截器
     */
    public static class FeignOkHttpClientResponseInterceptor implements Interceptor {


        @Override
        public Response intercept(Chain chain)  {

            Request originalRequest = chain.request();
            Response response = null;
            try {
                response = chain.proceed(originalRequest);
            } catch (IOException e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        WMessage.showMessageError(MainFrame.getInstance(), e.getMessage());
                    }
                });
                throw new RuntimeException(e);
            }

            MediaType mediaType = response.body().contentType();
            String content = null;
            try {
                content = response.body().string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            JSONObject jsonObject = JSONUtil.parseObj(content);

            //解析content，做你想做的事情！！
            Boolean success = jsonObject.getBool("success");
            Integer code = jsonObject.getInt("code");

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {

                    if (success != null && success == false) {

                         if ("401".equals(jsonObject.getStr("errCode"))) {
                            int opt = WOptionPane.showOptionDialog(null, jsonObject.getStr("errMessage")+"您可以继续留在该页面，或者重新登录", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, new Object[]{"重新登录","取消"}, "取消");
                            if (opt == 0) {
                                MainFrame.getInstance().showLogin();
                            }
                    

                        }else {
                             WMessage.showMessageError(MainFrame.getInstance(), "[" + jsonObject.getStr("errCode") + "]--" + jsonObject.getStr("errMessage"));
                         }

                    }

                    if (code != null) {
                        if (code == 200) {
                            WMessage.showMessageSuccess(MainFrame.getInstance(), jsonObject.getStr(MSG_TAG) + "");
                        } else if (code == 401) {
                            int opt = WOptionPane.showOptionDialog(null, jsonObject.getStr(MSG_TAG)+"您可以继续留在该页面，或者重新登录", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, new Object[]{"重新登录","取消"}, "取消");
                            if (opt == 0) {
                                MainFrame.getInstance().showLogin();
                            }


                        } else {
                            WMessage.showMessageError(MainFrame.getInstance(), "[" + jsonObject.getStr(CODE_TAG) + "]--" + jsonObject.getStr(MSG_TAG) + "");
                        }
                    }
                }
            });


            //生成新的response返回，网络请求的response如果取出之后，直接返回将会抛出异常
            return response.newBuilder().
                    body(ResponseBody.create(mediaType, content))
                    .build();
        }
    }
}


