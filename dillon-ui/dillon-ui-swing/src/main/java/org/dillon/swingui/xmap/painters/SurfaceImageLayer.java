package org.dillon.swingui.xmap.painters;


import org.dillon.swingui.xmap.geometry.SurfaceImage;
import org.dillon.swingui.xmap.render.SurfaceImageRenderer;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.AbstractPainter;

import java.awt.*;
import java.util.ArrayList;

/**
 * 贴图图层
 */
public class SurfaceImageLayer extends AbstractPainter<JXMapViewer> {

    private SurfaceImageRenderer renderer = new SurfaceImageRenderer();//贴图渲染器
    private java.util.List<SurfaceImage> surfaceImages = new ArrayList<SurfaceImage>();//贴图序列

    /**
     * 添加贴图
     *
     * @param surfaceImage
     */
    public void addSurfaceImage(SurfaceImage surfaceImage){
        this.surfaceImages.add(surfaceImage);
    }

    /**
     * 删除贴图
     *
     * @param surfaceImage
     */
    public void removeSurfaceImage(SurfaceImage surfaceImage){
        this.surfaceImages.remove(surfaceImage);
    }

    /**
     * 删除所有贴图
     *
     */
    public void removeAllSurfaceImage(){
        this.surfaceImages.clear();
    }

    /**
     * 获取所有贴图
     *
     * @return
     */
    public java.util.List<SurfaceImage> getSurfaceImages(){
        return this.surfaceImages;
    }

    /**
     * 贴图图层绘制接口
     *
     * @param g The Graphics2D to render to. This must not be null.
     * @param jxMapViewer map
     * @param width width of the area to paint.
     * @param height height of the area to paint.
     */
    @Override
    protected void doPaint(Graphics2D g, JXMapViewer jxMapViewer, int width, int height) {
        renderer.drawMany(g,jxMapViewer,surfaceImages);
    }
}
