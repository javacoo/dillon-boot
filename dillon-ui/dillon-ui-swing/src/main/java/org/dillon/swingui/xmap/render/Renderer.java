/*
 * WaypointRenderer.java
 *
 * Created on March 30, 2006, 5:56 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.dillon.swingui.xmap.render;


import org.jxmapviewer.JXMapViewer;

import java.awt.*;

/**
 * A interface that draws geometry objects. Implementations of Renderer can
 * be set on a Layer to draw geometry on a JXMapViewer
 *
 * @param <W> the geometry type
 */
public interface Renderer<W> {

    void draw(Graphics2D g, JXMapViewer map, W geoObject);

    void drawMany(Graphics2D g, JXMapViewer map, Iterable<? extends W> geoObjects);

}
