package org.dillon.swingui.view.system.role;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.jidesoft.swing.CheckBoxTree;
import com.jidesoft.tree.TreeUtils;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysDeptFeign;
import org.dillon.swingui.request.feignclient.SysMenuFeign;
import org.dillon.swingui.request.feignclient.SysRoleFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysMenu;
import org.dillon.system.api.domain.SysRole;
import org.dillon.system.api.model.TreeSelect;
import org.dillon.system.api.model.TreeselectKeyModel;
import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 角色管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class RoleManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "角色编号", "角色名称", "权限字符", "显示排序", "状态", "创建时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;


    private JPanel optRolePanel;

    private JTextField perTextField;
    private JTextField nameTextField;
    private JComboBox statusCombo;
    private JComboBox scopeAuthorityComboBox;

    private JPanel deptTreePanel;
    private JTextField roleNameTextField;

    private JTextField roleKeyTextField;
    private JSpinner sortSpinner;

    private JTextArea remarkTextArea;
    private JRadioButton normalBut;
    private JRadioButton disableBut;

    private CheckBoxTree menuTree;
    private CheckBoxTree deptTree;


    public RoleManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("角色名称"));

        toolBar.add(nameTextField = createTextField("请输入角色名称"));
        nameTextField.setColumns(15);
        toolBar.add(new JLabel("权限字符"));
        toolBar.add(perTextField = createTextField("请输入权限字符"));
        perTextField.setColumns(15);
        toolBar.add(new JLabel("状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "停用"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            perTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        OperateInfoPanel operateInfoPanel = new OperateInfoPanel();
        operateInfoPanel.getInfoPanel().add(toolBar);
        JButton addButton = new JButton("新增");
        operateInfoPanel.getOperatePanel().add(addButton);
        addButton.addActionListener(e -> {
            showRoleAddDialog();
        });

        JButton eidtButton = new JButton("修改");
        operateInfoPanel.getOperatePanel().add(eidtButton);
        eidtButton.addActionListener(e -> {
            showRoleEditDialog();
        });

        JButton delButton = new JButton("删除");
        operateInfoPanel.getOperatePanel().add(delButton);
        delButton.addActionListener(e -> {
            delRole(true);
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 7 || column == 0 || column == 5) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0 || columnIndex == 5) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);
        table.getColumn("操作").setMinWidth(210);
        table.getColumn("操作").setMaxWidth(210);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(operateInfoPanel, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);

                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));


                table.getColumn("状态").setCellRenderer(new DefaultTableRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JCheckBox statusCheckBox = new JCheckBox("");
                        statusCheckBox.setIcon(new AnimatedSwitchIcon());
                        statusCheckBox.setSelected((Boolean) value);
                        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
                        statusCheckBox.setBackground(component.getBackground());
                        return statusCheckBox;
                    }
                });
                table.getColumn("状态").setCellEditor(new DefaultCellEditor(createStatus()));
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar(), 3,"admin"));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar(), 3,"admin"));
            }
        });
        super.updateUI();

    }

    private JCheckBox createStatus() {
        JCheckBox statusCheckBox = new JCheckBox();
        statusCheckBox.setIcon(new AnimatedSwitchIcon());
        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        statusCheckBox.setHorizontalTextPosition(SwingConstants.CENTER);
        statusCheckBox.addActionListener(e -> changeStatus());
        return statusCheckBox;

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showRoleEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delRole(false));
        del.setForeground(UIManager.getColor("App.accentColor"));

        JButton more = new JButton("更多");
        more.addActionListener(e -> showPopupMoreButtonActionPerformed(e));
        more.setForeground(UIManager.getColor("App.accentColor"));
        more.setIcon(new FlatSVGIcon("icons/arrow-right.svg", 15, 15));

        optBar.add(edit);
        optBar.add(del);
        optBar.add(more);
        optBar.setPreferredSize(new Dimension(150, 50));
        return optBar;

    }

    private void showPopupMoreButtonActionPerformed(ActionEvent e) {
        Component invoker = (Component) e.getSource();
        JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem menuItem9 = new JMenuItem("数据权限");
        JMenuItem menuItem10 = new JMenuItem("分配用户");
        menuItem9.addActionListener(e1 -> {
            showDataAuthorityDialog();
        });
        menuItem10.addActionListener(e1 -> {
            showAssignUsersDataTab();
        });

        popupMenu.add(menuItem9);
        popupMenu.add(menuItem10);
        popupMenu.show(invoker, 0, invoker.getHeight());

    }

    private void showAssignUsersDataTab() {

        int selRow = table.getSelectedRow();
        Long roleId = null;
        if (selRow != -1) {
            roleId = (Long) table.getValueAt(selRow, 1);
        }

        if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("分配用户") == -1) {

            MainFrame.getInstance().getMainPane().getTabbedPane().addTab("分配用户",ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.system.role.AssignUsersPanel"));
        }
        MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("分配用户"));
        AssignUsersPanel assignUsersPanel = (AssignUsersPanel) ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.system.role.AssignUsersPanel");
        assignUsersPanel.setRoleId(roleId);
    }

    private void showRoleAddDialog() {
        int opt = WOptionPane.showOptionDialog(null, getOptRolePanel(false), "添加角色", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            addRole();
        }
    }


    private void showRoleEditDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getOptRolePanel(true), "修改角色", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            editRole();
        }
    }

    private void showDataAuthorityDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getDataPerPanel(), "分配数据权限", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            saveDataScope();
        }
    }

    private void updateData() {
        final SysRole sysRole = new SysRole();
        sysRole.setRoleName(nameTextField.getText());
        sysRole.setRoleKey(perTextField.getText());
        int selectIndex = statusCombo.getSelectedIndex();
        sysRole.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysRole);

        SwingWorker<PageResponse<SysRole>, Object> worker = new SwingWorker<PageResponse<SysRole>, Object>() {
            @Override
            protected PageResponse<SysRole> doInBackground() throws Exception {


                return Request.connector(SysRoleFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysRole post : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(post.getRoleId());
                            rowV.add(post.getRoleName());
                            rowV.add(post.getRoleKey());
                            rowV.add(post.getRoleSort());
                            rowV.add("0".equals(post.getStatus()) ? true : false);
                            rowV.add(DateUtil.format(post.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(post);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        tableModel.setRowCount(0);
        worker.execute();
    }


    public JPanel getOptRolePanel(Boolean isEidt) {
        optRolePanel = new JPanel(new BorderLayout());
        JPanel infoPanel = new JPanel(new MigLayout("fill,wrap 2,insets 10", "[right][grow,240::]", "[30!]10[30!]10[30!]10[30!]10[grow,30::]"));

        roleNameTextField = createTextField("请输入角色名称");
        roleKeyTextField = createTextField("请输入权限字符");
        roleNameTextField.setEditable(true);
        roleKeyTextField.setEditable(true);
        sortSpinner = new JSpinner(new SpinnerNumberModel());
        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("停用");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);
        menuTree = new CheckBoxTree();
        menuTree.setRowHeight(30);
        menuTree.setRootVisible(false);
        menuTree.setShowsRootHandles(true);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) menuTree.getActualCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setClosedIcon(null);
        renderer.setOpenIcon(null);

//        menuTree.setRootVisible(false);
        remarkTextArea = new JTextArea();

        infoPanel.add(new JLabel("*角色名称"));
        infoPanel.add(roleNameTextField, "growx");
        infoPanel.add(new JLabel("权限字符"));
        infoPanel.add(roleKeyTextField, "growx");

        infoPanel.add(new JLabel("显示排序"));
        infoPanel.add(sortSpinner, "growx");

        infoPanel.add(new JLabel("状态"));
        JToolBar toolBar4 = new JToolBar();
        toolBar4.add(normalBut);
        toolBar4.add(disableBut);
        infoPanel.add(toolBar4, "growx");

        infoPanel.add(new JLabel("备注"), "top");
        infoPanel.add(new JScrollPane(remarkTextArea), "growx,growy");

        JPanel menuTreePanel = new JPanel(new BorderLayout());

        JPanel checkBoxPanel = new JPanel(new HorizontalLayout());

        JCheckBox expand = new JCheckBox("展开/折叠");

        expand.addActionListener(e -> {
            if (expand.isSelected()) {
                TreeUtils.expandAll(menuTree);
            } else {
                for (int i = menuTree.getRowCount() - 1; i >= 0; i--) {
                    menuTree.collapseRow(i);
                }
            }
        });
        JCheckBox sel = new JCheckBox("全选/取消全选");
        sel.addActionListener(e -> {
            if (sel.isSelected()) {
                menuTree.getCheckBoxTreeSelectionModel().addSelectionPath(new TreePath(menuTree.getModel().getRoot()));
            } else {
                menuTree.getCheckBoxTreeSelectionModel().clearSelection();
            }
        });

        checkBoxPanel.add(expand);
        checkBoxPanel.add(sel);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("菜单权限", JLabel.CENTER), BorderLayout.NORTH);
        panel.add(checkBoxPanel, BorderLayout.CENTER);
        JScrollPane scrollPane6 = new JScrollPane();
        scrollPane6.setViewportView(menuTree);
        scrollPane6.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, UIManager.getColor("Component.borderColor")));

        menuTreePanel.add(panel, BorderLayout.NORTH);
        menuTreePanel.add(scrollPane6, BorderLayout.CENTER);
        menuTreePanel.setPreferredSize(new Dimension(300, 450));

        optRolePanel.add(infoPanel, BorderLayout.CENTER);
        optRolePanel.add(menuTreePanel, BorderLayout.EAST);
        if (isEidt) {
            updateEditMenuTree();
        } else {
            updateAddMenuTree();
        }


        return optRolePanel;
    }

    public JPanel getDataPerPanel() {
        JPanel infoPanel = new JPanel(new MigLayout("fill,wrap 2,hidemode 3,insets 10", "[right][grow,240::]", "[30!]10[30!]10[30!]10[grow,350::]"));

        roleNameTextField = createTextField("请输入角色名称");
        roleKeyTextField = createTextField("请输入权限字符");
        scopeAuthorityComboBox = new JComboBox(new String[]{"全部数据权限", "自定数据权限", "本部门数据权限", "本部门及以下数据权限", "仅本人数据权限"});

        deptTree = new CheckBoxTree();
        deptTree.setRowHeight(30);
        deptTree.setRootVisible(false);
        deptTree.setShowsRootHandles(true);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) deptTree.getActualCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setClosedIcon(null);
        renderer.setOpenIcon(null);


        infoPanel.add(new JLabel("*角色名称"));
        infoPanel.add(roleNameTextField, "growx,growy");
        infoPanel.add(new JLabel("权限字符"));
        infoPanel.add(roleKeyTextField, "growx,growy");
        infoPanel.add(new JLabel("权限范围"));
        infoPanel.add(scopeAuthorityComboBox, "growx,growy");

        JPanel checkBoxPanel = new JPanel(new HorizontalLayout());

        JCheckBox expand = new JCheckBox("展开/折叠");

        expand.addActionListener(e -> {
            if (expand.isSelected()) {
                TreeUtils.expandAll(deptTree);
            } else {
                for (int i = deptTree.getRowCount() - 1; i >= 0; i--) {
                    deptTree.collapseRow(i);
                }
            }
        });
        JCheckBox sel = new JCheckBox("全选/取消全选");
        sel.addActionListener(e -> {
            if (sel.isSelected()) {
                deptTree.getCheckBoxTreeSelectionModel().addSelectionPath(new TreePath(deptTree.getModel().getRoot()));
            } else {
                deptTree.getCheckBoxTreeSelectionModel().clearSelection();
            }
        });

        checkBoxPanel.add(expand);
        checkBoxPanel.add(sel);
        JScrollPane scrollPane6 = new JScrollPane();
        scrollPane6.setViewportView(deptTree);
        scrollPane6.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, UIManager.getColor("Component.borderColor")));

        deptTreePanel = new JPanel(new BorderLayout());
        deptTreePanel.add(checkBoxPanel, BorderLayout.NORTH);
        deptTreePanel.add(scrollPane6, BorderLayout.CENTER);

        infoPanel.add(new JLabel("数据权限"), "top");
        infoPanel.add(deptTreePanel, "growx,h 350!");
        roleNameTextField.setEditable(false);
        roleKeyTextField.setEditable(false);
        scopeAuthorityComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {

                    deptTreePanel.setVisible(scopeAuthorityComboBox.getSelectedIndex() == 1);
                }
            }
        });
        createDeptMenuTree();

        return infoPanel;
    }

    private void updateAddMenuTree() {

        SwingWorker<List<TreeSelect>, Object> worker = new SwingWorker<List<TreeSelect>, Object>() {
            @Override
            protected List<TreeSelect> doInBackground() throws Exception {
                return Request.connector(SysMenuFeign.class).treeselect(new SysMenu()).getData();

            }


            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        createMenuTree(get(), null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    private void updateEditMenuTree() {
        int selRow = table.getSelectedRow();
        Long postId = null;
        if (selRow != -1) {
            postId = (Long) table.getValueAt(selRow, 1);
        }
        Long finalRoleId = postId;
        SwingWorker<TreeselectKeyModel, SysRole> worker = new SwingWorker<TreeselectKeyModel, SysRole>() {
            @Override
            protected TreeselectKeyModel doInBackground() throws Exception {
                publish(Request.connector(SysRoleFeign.class).getInfo(finalRoleId).getData());
                return Request.connector(SysMenuFeign.class).roleMenuTreeselect(finalRoleId).getData();

            }

            @Override
            protected void process(List<SysRole> chunks) {
                for (SysRole sysrole : chunks) {

                    updateValue(sysrole);
                }
            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        createMenuTree(get().getTreeSelects(), get().getKeyIds());

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    private void createDeptMenuTree() {
        int selRow = table.getSelectedRow();
        Long postId = null;
        if (selRow != -1) {
            postId = (Long) table.getValueAt(selRow, 1);
        }
        Long finalRoleId = postId;
        SwingWorker<TreeselectKeyModel, SysRole> worker = new SwingWorker<TreeselectKeyModel, SysRole>() {
            @Override
            protected TreeselectKeyModel doInBackground() throws Exception {
                publish(Request.connector(SysRoleFeign.class).getInfo(finalRoleId).getData());
                return Request.connector(SysDeptFeign.class).roleDeptTreeselect(finalRoleId).getData();

            }

            @Override
            protected void process(List<SysRole> chunks) {
                for (SysRole sysrole : chunks) {

                    roleNameTextField.setText(sysrole.getRoleName());
                    roleKeyTextField.setText(sysrole.getRoleKey());
                    scopeAuthorityComboBox.setSelectedIndex(Integer.parseInt(sysrole.getDataScope()));
                    deptTreePanel.setVisible(scopeAuthorityComboBox.getSelectedIndex() == 1);
                }
            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        createDeptTree(get().getTreeSelects(), get().getKeyIds());

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    /**
     * 创建树数据
     *
     * @param treeSelectList 树选择列表
     */
    private void createMenuTree(List<TreeSelect> treeSelectList, List<Long> menuIds) {
        List<TreeNode> selectTreeNode = new ArrayList<>();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("系统菜单");
        for (TreeSelect treeSelect : treeSelectList) {

            DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeSelect);
            if (treeSelect.getChildren() != null) {
                createTree(node, treeSelect.getChildren(), menuIds, selectTreeNode);
            }
            root.add(node);
            if (ObjectUtil.isNotEmpty(menuIds)) {
                if (menuIds.contains(treeSelect.getId())) {
                    selectTreeNode.add(node);
                }
            }
        }
        DefaultTreeModel defaultTreeModel = new DefaultTreeModel(root);
        menuTree.setModel(defaultTreeModel);
        for (TreeNode treeNode : selectTreeNode) {
            if (treeNode.isLeaf()) {
                menuTree.getCheckBoxTreeSelectionModel().addSelectionPath(new TreePath(defaultTreeModel.getPathToRoot(treeNode)));
            }
        }
        TreeUtils.expandAll(menuTree);
    }

    private void createDeptTree(List<TreeSelect> treeSelectList, List<Long> menuIds) {
        List<TreeNode> selectTreeNode = new ArrayList<>();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("系统菜单");
        for (TreeSelect treeSelect : treeSelectList) {

            DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeSelect);
            if (treeSelect.getChildren() != null) {
                createTree(node, treeSelect.getChildren(), menuIds, selectTreeNode);
            }
            root.add(node);
            if (ObjectUtil.isNotEmpty(menuIds)) {
                if (menuIds.contains(treeSelect.getId())) {
                    selectTreeNode.add(node);
                }
            }
        }
        DefaultTreeModel defaultTreeModel = new DefaultTreeModel(root);
        deptTree.setModel(defaultTreeModel);
        for (TreeNode treeNode : selectTreeNode) {
            if (treeNode.isLeaf()) {
                deptTree.getCheckBoxTreeSelectionModel().addSelectionPath(new TreePath(defaultTreeModel.getPathToRoot(treeNode)));
            }
        }
        TreeUtils.expandAll(deptTree);
    }

    /**
     * 创建树
     * 递归生成树
     *
     * @param parentNode 父节点
     * @param childlList childl列表
     */
    private void createTree(DefaultMutableTreeNode parentNode, List<TreeSelect> childlList, List<Long> menuIds, List<TreeNode> selectTreeNode) {
        for (TreeSelect sectionModel : childlList) {
            DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(sectionModel);
            parentNode.add(childNode);
            if (ObjectUtil.isNotEmpty(menuIds)) {
                if (menuIds.contains(sectionModel.getId())) {
                    selectTreeNode.add(childNode);
                }
            }
            if (sectionModel.getChildren() != null) {
                createTree(childNode, sectionModel.getChildren(), menuIds, selectTreeNode);
            }
        }
    }

    private void updateValue(SysRole sysRole) {


        roleNameTextField.setText(sysRole.getRoleName());
        roleKeyTextField.setText(sysRole.getRoleKey());
        sortSpinner.setValue(Integer.valueOf(sysRole.getRoleSort()));
        normalBut.setSelected("0".equals(sysRole.getStatus()));
        disableBut.setSelected("1".equals(sysRole.getStatus()));
        remarkTextArea.setText(sysRole.getRemark());

    }

    private SysRole getEidtValue() {
        SysRole sysRole = new SysRole();
        sysRole.setRoleName(roleNameTextField.getText());
        sysRole.setRoleKey(roleKeyTextField.getText());
        sysRole.setRoleSort(sortSpinner.getValue().toString());
        sysRole.setStatus(normalBut.isSelected() ? "0" : "1");
        sysRole.setRemark(remarkTextArea.getText());


        TreePath[] treePaths = menuTree.getCheckBoxTreeSelectionModel().getSelectionPaths();
        if (treePaths != null) {
            Set<Long> list = new HashSet<>();
            for (TreePath path : treePaths) {

                Object obj = path.getLastPathComponent();
                if (obj instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) obj;

                    if (treeNode.isRoot()) {
                        for (int i = 0; i < treeNode.getChildCount(); i++) {
                            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeNode.getChildAt(i);
                            if (node.getUserObject() instanceof TreeSelect) {
                                TreeSelect treeSelect = (TreeSelect) node.getUserObject();
                                if (node.isLeaf()) {
                                    parentMenuId(node, list);
                                } else {
                                    list.add(treeSelect.getId());
                                    childrenMenuId(treeSelect.getChildren(), list);
                                }
                            }
                        }
                    } else {
                        Object nodeUser = treeNode.getUserObject();
                        if (nodeUser instanceof TreeSelect) {
                            TreeSelect treeSelect = (TreeSelect) nodeUser;
                            if (treeNode.isLeaf()) {
                                parentMenuId(treeNode, list);
                            } else {
                                list.add(treeSelect.getId());
                                childrenMenuId(treeSelect.getChildren(), list);
                            }
                        }
                    }
                }
            }
            sysRole.setMenuIds(list.stream().toArray(Long[]::new));
        }

        return sysRole;
    }

    private SysRole getDataAuthorityValue() {
        SysRole sysRole = new SysRole();
        sysRole.setRoleName(roleNameTextField.getText());
        sysRole.setRoleKey(roleKeyTextField.getText());
        sysRole.setDataScope(scopeAuthorityComboBox.getSelectedIndex() + "");
        Set<Long> deptIdlist = new HashSet<>();
        if (scopeAuthorityComboBox.getSelectedIndex() == 1) {

            TreePath[] treePaths = deptTree.getCheckBoxTreeSelectionModel().getSelectionPaths();


            if (treePaths != null) {
                for (TreePath path : treePaths) {

                    Object obj = path.getLastPathComponent();
                    if (obj instanceof DefaultMutableTreeNode) {
                        DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) obj;

                        if (treeNode.isRoot()) {
                            for (int i = 0; i < treeNode.getChildCount(); i++) {
                                DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeNode.getChildAt(i);
                                if (node.getUserObject() instanceof TreeSelect) {
                                    TreeSelect treeSelect = (TreeSelect) node.getUserObject();
                                    if (node.isLeaf()) {
                                        parentMenuId(node, deptIdlist);
                                    } else {
                                        deptIdlist.add(treeSelect.getId());
                                        childrenMenuId(treeSelect.getChildren(), deptIdlist);
                                    }
                                }
                            }
                        } else {
                            Object nodeUser = treeNode.getUserObject();
                            if (nodeUser instanceof TreeSelect) {
                                TreeSelect treeSelect = (TreeSelect) nodeUser;
                                if (treeNode.isLeaf()) {
                                    parentMenuId(treeNode, deptIdlist);
                                } else {
                                    deptIdlist.add(treeSelect.getId());
                                    childrenMenuId(treeSelect.getChildren(), deptIdlist);
                                }
                            }
                        }
                    }
                }
                sysRole.setDeptIds(deptIdlist.stream().toArray(Long[]::new));
            }
        }
        sysRole.setDeptIds(deptIdlist.stream().toArray(Long[]::new));
        return sysRole;
    }

    private void childrenMenuId(List<TreeSelect> treeSelectList, Set<Long> menuIds) {

        for (TreeSelect treeSelect : treeSelectList) {
            menuIds.add(treeSelect.getId());
            if (ObjectUtil.isNotEmpty(treeSelect.getChildren())) {
                childrenMenuId(treeSelect.getChildren(), menuIds);
            }
        }

    }

    private void parentMenuId(DefaultMutableTreeNode treeNode, Set<Long> menuIds) {
        Object nodeUser = treeNode.getUserObject();
        if (nodeUser instanceof TreeSelect) {
            TreeSelect treeSelect = (TreeSelect) nodeUser;
            menuIds.add(treeSelect.getId());

            if (ObjectUtil.isNotEmpty(treeNode.getParent())) {
                parentMenuId((DefaultMutableTreeNode) treeNode.getParent(), menuIds);
            }
        }
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加
     */
    private void addRole() {

        SysRole sysRole = getEidtValue();
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).add(sysRole);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 保存角色
     */
    private void editRole() {

        SysRole sysRole = getEidtValue();

        int selRow = table.getSelectedRow();
        if (selRow != -1) {
            Long id = (Long) table.getValueAt(selRow, 1);
            sysRole.setRoleId(id);
        } else {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).edit(sysRole);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void saveDataScope() {

        SysRole sysRole = getDataAuthorityValue();

        int selRow = table.getSelectedRow();
        if (selRow != -1) {
            Long id = (Long) table.getValueAt(selRow, 1);
            sysRole.setRoleId(id);
        } else {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).dataScope(sysRole);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


    private void delRole(boolean isMore) {
        Long postId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 改变状态
     */
    private void changeStatus() {
        SysRole selRole = (SysRole) table.getValueAt(table.getSelectedRow(), 7);
        Boolean status = (Boolean) table.getValueAt(table.getSelectedRow(), 5);
        int opt = WOptionPane.showConfirmDialog(this, "确定要" + (status ? "启用" : "停用") + "【" + selRole.getRoleName() + "】角色吗", "提示", OK_CANCEL_OPTION);
        if (opt != 0) {
            table.setValueAt(!status, table.getSelectedRow(), 5);
            return;
        }
        SysRole sysRole = new SysRole();
        sysRole.setRoleId(selRole.getRoleId());
        sysRole.setStatus(status ? "0" : "1");
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).changeStatus(sysRole);
            }
        };
        swingWorker.execute();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
