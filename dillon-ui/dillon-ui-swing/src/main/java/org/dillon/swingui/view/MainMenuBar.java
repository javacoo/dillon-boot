package org.dillon.swingui.view;

import cn.hutool.core.util.ObjectUtil;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.extras.FlatAnimatedLafChange;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.ui.FlatLineBorder;
import com.formdev.flatlaf.ui.FlatRoundBorder;
import com.formdev.flatlaf.util.LoggingFacade;
import lombok.Data;
import net.miginfocom.swing.MigLayout;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysMenuFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.config.DefaultPrefs;
import org.dillon.swingui.theme.MyDark;
import org.dillon.swingui.theme.MyLight;
import org.dillon.common.core.utils.PinYinUtils;
import org.dillon.system.api.model.RouterVo;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import static org.dillon.swingui.config.DefaultPrefs.KEY_LAF;

/**
 * 主菜单栏
 *
 * @author liwen
 * @version： 0.0.1
 * @description:
 * @className: MainMenu
 * @author: liwen
 * @date: 2021/10/23 20:52
 * @date 2022/07/06
 */
public class MainMenuBar extends JMenuBar {

    /**
     * 全屏
     */
    private JToggleButton fullScreen;
    /**
     * 图标按钮
     */
    private JButton logoButton;

    /**
     * 菜单按钮
     */
    private JButton menuButton;
    /**
     * 皮肤按钮
     */
    private JButton skinButton;
    /**
     * 离开酒吧
     */
    private JToolBar toolBar;

    /**
     * 菜单褶皱
     */
    private boolean menuFold = true;

    private JComponent searchPanel;

    private JTextField featureSearchTextField;
    private JComboBox featureSearchComboBox;
    private DefaultComboBoxModel model;
    private List<SearchBean> searchBeans = new ArrayList<>();

    /**
     * 主菜单栏
     */
    public MainMenuBar() {

        toolBar = new JToolBar();
        toolBar.setOpaque(false);
        fullScreen = new JToggleButton(new FlatSVGIcon("icons/fullscreen.svg", 30, 30));
        fullScreen.addActionListener(e -> fullScreenChanged());

        menuButton = new JButton(new FlatSVGIcon("icons/menu-fold.svg", 30, 30));
        menuButton.setToolTipText("收起菜单栏");
        menuButton.addActionListener(e -> setMenuFold(!isMenuFold()));
        logoButton = new JButton("DillonBoot", new FlatSVGIcon("icons/xingqiu.svg", 30, 30));
        logoButton.setForeground(UIManager.getColor("Label.foreground"));

        skinButton = new JButton();
        skinButton.setIcon(new FlatSVGIcon("icons/skin.svg", 26, 26));
        skinButton.addActionListener(e -> showPopupSkinButtonActionPerformed(e));

        featureSearchTextField = new JTextField(15);
        featureSearchTextField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "搜索");
        featureSearchTextField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        featureSearchTextField.putClientProperty(FlatClientProperties.TEXT_FIELD_LEADING_ICON, new FlatSVGIcon("icons/sousuo.svg", 20, 20));
        featureSearchTextField.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        featureSearchTextField.setBackground(new Color(0f, 0f, 0f, 0f));
        createFeatureSearchComboBox();

        searchPanel = new JPanel() {
            @Override
            public Color getBackground() {
                return UIManager.getColor("App.searchBackground");
            }


            @Override
            public Dimension getMaximumSize() {
                return new Dimension(250, 30);
            }
        };
//        searchPanel.putClientProperty(FlatClientProperties.STYLE, "arc: 999");
//        searchPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        searchPanel.setLayout(new BorderLayout());
        searchPanel.add(featureSearchTextField, BorderLayout.CENTER);
        searchPanel.add(featureSearchComboBox, BorderLayout.SOUTH);
        toolBar.add(logoButton);
        toolBar.add(menuButton);
        toolBar.add(searchPanel);
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(skinButton);
        toolBar.add(fullScreen);
        this.setPreferredSize(new Dimension(0, 45));
        this.add(toolBar);
    }

    @Override
    protected void paintComponent(Graphics g) {

//        super.paintComponent(g);
        Graphics2D g2= (Graphics2D) g.create();
        g2.setColor(UIManager.getColor("App.titleBarBackground"));
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.dispose();
    }

    /**
     * 全屏了
     */
    private void fullScreenChanged() {

        GraphicsDevice gd = getGraphicsConfiguration().getDevice();
        fullScreen.setIcon(!fullScreen.isSelected() ? new FlatSVGIcon("icons/fullscreen.svg", 25, 25) : new FlatSVGIcon("icons/fullscreen-exit.svg", 25, 25));
        gd.setFullScreenWindow(fullScreen.isSelected() ? MainFrame.getInstance() : null);
    }

    /**
     * 皮肤显示弹出按钮执行操作
     *
     * @param e e
     */
    private void showPopupSkinButtonActionPerformed(ActionEvent e) {
        Component invoker = (Component) e.getSource();
        JPopupMenu themePopupMenu = new JPopupMenu();

        ButtonGroup group = new ButtonGroup();

        JCheckBoxMenuItem lighterMenuItem = new JCheckBoxMenuItem("白色");
        group.add(lighterMenuItem);
        themePopupMenu.add(lighterMenuItem);
        lighterMenuItem.addActionListener(e1 -> theme("白色"));


        JCheckBoxMenuItem darkMenuItem = new JCheckBoxMenuItem("深色");
        darkMenuItem.addActionListener(e1 -> theme("深色"));
        themePopupMenu.add(darkMenuItem);
        group.add(darkMenuItem);

        String curLaf = DefaultPrefs.getState().get(KEY_LAF, FlatLightLaf.class.getName());
        lighterMenuItem.setSelected(curLaf.equals(MyLight.class.getName()));
        darkMenuItem.setSelected(curLaf.equals(MyDark.class.getName()));

        themePopupMenu.show(invoker, 0, invoker.getHeight());

    }


    /**
     * 菜单是褶皱
     *
     * @return boolean
     */
    public boolean isMenuFold() {
        return menuFold;
    }

    /**
     * 设置菜单褶皱
     *
     * @param menuFold 菜单褶皱
     */
    public void setMenuFold(boolean menuFold) {
        this.menuFold = menuFold;
        menuButton.setIcon(menuFold ? new FlatSVGIcon("icons/menu-fold.svg", 30, 30) : new FlatSVGIcon("icons/menu-unfold.svg", 30, 30));
        menuButton.setToolTipText(menuFold ? "收起菜单栏" : "展开菜单栏");
        MainFrame.getInstance().getMainPane().showNavigationBar(menuFold);

    }


    /**
     * 主题
     *
     * @param theme 主题
     */
    private void theme(String theme) {

        EventQueue.invokeLater(() -> {
            FlatAnimatedLafChange.showSnapshot();

            String lafClassName;
            switch (theme) {
                case "白色":
                    lafClassName = MyLight.class.getName();
                    break;
                case "深色":
                    lafClassName = MyDark.class.getName();
                    break;

                default:
                    lafClassName = MyLight.class.getName();

            }
            try {
                UIManager.setLookAndFeel(lafClassName);
            } catch (Exception ex) {
                LoggingFacade.INSTANCE.logSevere(null, ex);
            }

            FlatLaf.updateUI();
            FlatSVGIcon.ColorFilter.getInstance()
                    .add(new Color(0x6e6e6e), new Color(0x6F767E), new Color(0xffffff));

            FlatAnimatedLafChange.hideSnapshotWithAnimation();

        });
    }

    @Override
    public void updateUI() {
        super.updateUI();

        if (logoButton != null) {
            Color color = UIManager.getColor("Label.foreground");
            logoButton.setForeground(new Color(color.getRGB()));
        }
    }

    public void updateMenuData() {
        SwingWorker<List<SearchBean>, Object> worker = new SwingWorker<List<SearchBean>, Object>() {
            @Override
            protected List<SearchBean> doInBackground() throws Exception {

                List<SearchBean> searchBeans = new ArrayList<>();
                List<RouterVo> menuModelList = Request.connector(SysMenuFeign.class).getRouters().getData();

                for (RouterVo tree : menuModelList) {

                    String name = tree.getMeta().getTitle();
                    if (ObjectUtil.isNotEmpty(tree.getChildren())) {
                        createMenuTree(tree.getChildren(), searchBeans, name);
                    } else {
                        searchBeans.add(new SearchBean(name, tree));
                    }
                }
                return searchBeans;

            }


            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        searchBeans.addAll(get());
//                        setAdjusting(featureSearchComboBox, false);
//                        for (SearchBean item : searchBeans) {
//                            model.addElement(item);
//                        }
//
//                        featureSearchComboBox.setSelectedItem(null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        searchBeans.clear();
        worker.execute();
    }

    private void createMenuTree(List<RouterVo> treeList, List<SearchBean> searchBeans, String parentName) {
        for (RouterVo tree : treeList) {
            String name = parentName + ">" + tree.getMeta().getTitle();
            if (ObjectUtil.isNotEmpty(tree.getChildren())) {
                createMenuTree(tree.getChildren(), searchBeans, name);
            } else {
                searchBeans.add(new SearchBean(name, tree));
            }
        }
    }

    public void createFeatureSearchComboBox() {
        featureSearchComboBox = new JComboBox(model = new DefaultComboBoxModel()) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(super.getPreferredSize().width, 0);
            }
        };

        featureSearchComboBox.setRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value != null) {
                    this.setText(value.toString());
                    this.setIcon(new FlatSVGIcon("icons/app.svg", 20, 20));
                    this.setPreferredSize(new Dimension(this.getWidth(), 40));
                }
                return this;
            }
        });


        featureSearchComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isAdjusting(featureSearchComboBox)) {
                    Object obj = featureSearchComboBox.getSelectedItem();
                    if (obj != null) {
                        featureSearchTextField.setText(featureSearchComboBox.getSelectedItem().toString());
                        addTab((SearchBean) obj);
                    }
                }
            }
        });
        featureSearchTextField.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                setAdjusting(featureSearchComboBox, true);
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    if (featureSearchComboBox.isPopupVisible()) {
                        e.setKeyCode(KeyEvent.VK_ENTER);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
                    e.setSource(featureSearchComboBox);
                    featureSearchComboBox.dispatchEvent(e);
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        Object obj = featureSearchComboBox.getSelectedItem();
                        featureSearchTextField.setText(obj.toString());
                        addTab((SearchBean) obj);
                        featureSearchComboBox.setPopupVisible(false);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    featureSearchComboBox.setPopupVisible(false);
                }
                setAdjusting(featureSearchComboBox, false);
            }
        });
        featureSearchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateList();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateList();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateList();
            }

            private void updateList() {
                setAdjusting(featureSearchComboBox, true);
                model.removeAllElements();
                String input = featureSearchTextField.getText();
                if (!input.isEmpty()) {
                    for (SearchBean item : searchBeans) {
                        String name = item.getName();
                        String pinyingName = PinYinUtils.capitalizeLetter(name).toLowerCase();
                        String inputText = input.toLowerCase();
                        if (name.toLowerCase().contains(inputText) || pinyingName.contains(inputText)) {
                            model.addElement(item);
                        }
                    }
                }
                featureSearchComboBox.setPopupVisible(model.getSize() > 0);
                setAdjusting(featureSearchComboBox, false);
            }
        });

    }

    private boolean isAdjusting(JComboBox featureSearchComboBox) {
        if (featureSearchComboBox.getClientProperty("is_adjusting") instanceof Boolean) {
            return (Boolean) featureSearchComboBox.getClientProperty("is_adjusting");
        }
        return false;
    }

    private void setAdjusting(JComboBox featureSearchComboBox, boolean adjusting) {
        featureSearchComboBox.putClientProperty("is_adjusting", adjusting);
    }



    /**
     * 添加选项卡
     *
     * @param menu 菜单
     */
    private void addTab(SearchBean menu) {
        if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab(menu.getTreeNode().getMeta().getTitle()) == -1) {
            FlatSVGIcon icon;
            icon = getSvgIcon("icons/menu/" + menu.getTreeNode().getMeta().getIcon() + ".svg", 20, 20);

            MainFrame.getInstance().getMainPane().getTabbedPane().addTab(menu.getTreeNode().getMeta().getTitle(), icon, ApplicatonStore.getNavigatonPanel(menu.getTreeNode().getComponent()));
        }
        MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab(menu.getTreeNode().getMeta().getTitle()));

    }

    private FlatSVGIcon getSvgIcon(String path, int w, int h) {
        FlatSVGIcon flatSVGIcon = new FlatSVGIcon(path, w, h);
        flatSVGIcon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
            return UIManager.getColor("Label.foreground") == null ? new Color(0xf8f8f8) : UIManager.getColor("Label.foreground");
        }));
        return flatSVGIcon;
    }


    @Data
    class SearchBean {
        private String name;
        private RouterVo treeNode;

        public SearchBean(String name, RouterVo treeNode) {
            this.name = name;
            this.treeNode = treeNode;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
