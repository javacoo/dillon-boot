package org.dillon.swingui.bean;

import lombok.Data;

import java.awt.*;

/**
 * 箭头属性
 *
 * @author xiangqian
 * @date 15:41 2019/11/03
 */
@Data
public class Attributes {
    private double height; // 箭头的高度
    private double angle; // 箭头角度
    private Color color; // 箭头颜色

    public Attributes() {
        this.height = 30;
        this.angle = 45;
        this.color = Color.BLACK;
    }
}