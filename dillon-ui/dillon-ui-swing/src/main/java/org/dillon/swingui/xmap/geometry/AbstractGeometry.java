package org.dillon.swingui.xmap.geometry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jxmapviewer.beans.AbstractBean;

/**
 * Created by user on 2015/12/24.
 */
public abstract class AbstractGeometry extends AbstractBean  {
    protected static final Log log = LogFactory.getLog(AbstractGeometry.class);
    public static final String SHAPE_NULL = "cn.adcc.eagles2d.Geometry.ShapeNull";
    public static final String SHAPE_POINT = "cn.adcc.eagles2d.Geometry.ShapePoint";
    public static final String SHAPE_POLYLINE = "cn.adcc.eagles2d.Geometry.ShapePolyline";
    public static final String SHAPE_POLYGON = "cn.adcc.eagles2d.Geometry.ShapePolygon";

    protected String shapeType = SHAPE_NULL;

    public String getShapeType() {
        return this.shapeType;
    }

}
