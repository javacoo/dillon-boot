
package org.dillon.swingui.xmap.painters;

import org.dillon.swingui.bean.SubstationBean;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.Painter;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Paints a route
 *
 * @author Martin Steiger
 */
public class SubstationPainter implements Painter<JXMapViewer> {
    private Color color = Color.RED;
    private boolean antiAlias = true;

    private List<SubstationBean> track;

    /**
     *
     */
    public SubstationPainter() {
        // copy the list so that changes in the 
        // original list do not have an effect here
        this.track = new ArrayList<SubstationBean>();
        track.add(new SubstationBean("干河口北", new GeoPosition(39.01064750994083,95.09765625000001)));
        track.add(new SubstationBean("北大桥西", new GeoPosition(39.926588421909436,95.09765625000001)));
        track.add(new SubstationBean("敦煌变", new GeoPosition(40.53050177574321,96.85546875000001)));
        track.add(new SubstationBean("甜水井牵", new GeoPosition(42.17968819665963,97.11914062500001)));
        track.add(new SubstationBean("安二", new GeoPosition(40.027614437486655,98.30566406250001)));
        track.add(new SubstationBean("酒泉", new GeoPosition(39.21523130910493,99.7998046875)));
        track.add(new SubstationBean("东大滩", new GeoPosition(38.565347844885466,103.09570312500001)));
        track.add(new SubstationBean("天水", new GeoPosition(33.63291573870479,105.42480468750001)));
        track.add(new SubstationBean("落大", new GeoPosition(34.470335121217495,104.28222656250001)));
        track.add(new SubstationBean("永橙", new GeoPosition(35.65729624809628,104.28222656250001)));
    }


    @Override
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = (Graphics2D) g.create();

        // convert from viewport to world bitmap
        Rectangle rect = map.getViewportBounds();
        g.translate(-rect.x, -rect.y);

        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        g.setStroke(new BasicStroke(2f));
        // do the drawing
        for (SubstationBean bean : track) {
            Point2D point = map.getTileFactory().geoToPixel(bean.getGeoPosition(), map.getZoom());
            // Computes the norm and the inverse norm
            int arcSize = bean.getArcSize();
            int oArcSize = arcSize + 6;

            int dx = (int) point.getX() - arcSize / 2;
            int dy = (int) point.getY() - arcSize / 2;
            int dx1 = (int) point.getX() - oArcSize / 2;
            int dy1 = (int) point.getY() - oArcSize / 2;
            g.setColor(bean.getColor());
            g.fillOval(dx, dy, arcSize, arcSize);
            g.drawOval(dx1, dy1, oArcSize, oArcSize);
            g.setColor(new Color(0xF8CBA6));
            g.drawString(bean.getName(), dx1 + oArcSize+5, dy1 + oArcSize);

        }
        g.dispose();
    }


}
