package org.dillon.swingui.xmap.geometry;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by BC on 2015/12/25.
 */
public class LatLonNet extends Line2D{
    private int level1Zoom;
    private int level2Zoom;
    private int level3Zoom;
    private int level4Zoom;
    private Color level1Color;
    private Color level2Color;
    private Color level3Color;
    private Color level4Color;

    public int getLevel1Zoom() {
        return level1Zoom;
    }

    public int getLevel2Zoom() {
        return level2Zoom;
    }


    public int getLevel3Zoom() {
        return level3Zoom;
    }


    public int getLevel4Zoom() {
        return level4Zoom;
    }

    public Color getLevel1Color() {
        return level1Color;
    }

    public void setLevel1Color(Color color,int zooom) {
        this.level1Zoom=zooom;
        this.level1Color = color;
    }

    public Color getLevel2Color() {
        return level2Color;
    }

    public void setLevel2Color(Color color,int zooom) {
        this.level2Zoom=zooom;
        this.level2Color = color;
    }

    public Color getLevel3Color() {
        return level3Color;
    }

    public void setLevel3Color(Color color,int zooom) {
        this.level3Zoom=zooom;
        this.level3Color = color;
    }

    public Color getLevel4Color() {
        return level4Color;
    }

    public void setLevel4Color(Color color,int zooom) {
        this.level4Zoom=zooom;
        this.level4Color = color;
    }

    @Override
    public double getX1() {
        return 0;
    }

    @Override
    public double getY1() {
        return 0;
    }

    @Override
    public Point2D getP1() {
        return null;
    }

    @Override
    public double getX2() {
        return 0;
    }

    @Override
    public double getY2() {
        return 0;
    }

    @Override
    public Point2D getP2() {
        return null;
    }

    @Override
    public void setLine(double x1, double y1, double x2, double y2) {

    }

    @Override
    public Rectangle2D getBounds2D() {
        return null;
    }
}
