/*
 * Created by JFormDesigner on Thu Jul 29 10:51:47 CST 2021
 */

package org.dillon.swingui.view;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.cola.dto.SingleResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.icons.FlatWindowCloseIcon;
import com.formdev.flatlaf.util.SwingUtils;
import org.dillon.common.core.web.domain.ValidateCodeEntity;
import net.miginfocom.swing.MigLayout;
import org.dillon.swing.WAutomaticTextFiled;
import org.dillon.swingui.bean.LoginVO;
import org.dillon.swingui.config.DefaultPrefs;
import org.dillon.swingui.request.feignclient.LoginService;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.utils.IconLoader;
import org.jdesktop.swingx.JXTextField;
import org.jdesktop.swingx.StackLayout;
import org.jdesktop.swingx.VerticalLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.prefs.BackingStoreException;

import static org.dillon.swingui.config.DefaultPrefs.KEY_USER;
import static org.dillon.swingui.config.DefaultPrefs.KEY_USER_CUR;


/**
 * 登录面板
 *
 * @author liwen
 * @date 2022/06/22
 */
public class LoginPane extends JPanel {
    /**
     * 背景图像
     */
    private BufferedImage backgroundImage = null;

    /**
     * 背景窗格
     */
    private JPanel backgroundPane;
    /**
     * 前面板
     */
    private JPanel topPane;
    /**
     * 登录按钮
     */
    private JButton loginButton;
    /**
     * 用户名文本提交
     */
    private WAutomaticTextFiled userNameTextFiled;
    /**
     * 密码字段
     */
    private JPasswordField passwordField;
    /**
     * 验证码文本提交
     */
    private JXTextField verificationCodeTextFiled;
    /**
     * 验证码图片
     */
    private JLabel verificationCodeImage;
    /**
     * 进度条
     */
    private JProgressBar progressBar;
    /**
     * 错误标签
     */
    private JLabel errorLabel;
    /**
     * 代码标识
     */
    private String codeId;

    private JCheckBox rememberPwd;

    /**
     * 登录面板
     */
    public LoginPane() {
        initComponents();
        initListener();
        loadBackgroundImage();
        initData();
    }

    /**
     * 加载背景图像
     */
    private void loadBackgroundImage() {
        new SwingWorker<BufferedImage, Object>() {
            @Override
            protected BufferedImage doInBackground() throws Exception {
                return IconLoader.image("/images/login.png");

            }

            @Override
            protected void done() {
                try {
                    setBackgroundImage(get());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }

            }
        }.execute();
    }

    /**
     * 初始化数据
     */
    public void initData() {

        SwingWorker<LoginVO, List<Object>> worker = new SwingWorker<LoginVO, List<Object>>() {
            @Override
            protected LoginVO doInBackground() throws Exception {

                List<Object> list = new ArrayList<>();
                String[] keys = DefaultPrefs.getState().keys();
                for (String key : keys) {
                    if (key.startsWith(KEY_USER)) {
                        LoginVO loginVO = JSONUtil.toBean(DefaultPrefs.getState().get(key, ""), LoginVO.class);
                        list.add(loginVO);
                    }
                }

                publish(list);

                LoginVO cur = JSONUtil.toBean(DefaultPrefs.getState().get(KEY_USER_CUR, ""), LoginVO.class);

                return cur;
            }

            /**
             * 过程
             *
             * @param chunks 块
             */
            @Override
            protected void process(List<List<Object>> chunks) {
                for (List<Object> list : chunks) {
                    userNameTextFiled.setSourceData(list);
                }
            }

            /**
             * 完成
             */
            @Override
            protected void done() {
                try {
                    userNameTextFiled.setText(get().getUsername());
                    userNameTextFiled.getComboBox().setPopupVisible(false);
                    updateCode();
                    repaint();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        worker.execute();


    }

    /**
     * init侦听器
     */
    private void initListener() {

        userNameTextFiled.getComboBox().addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                Object object = e.getItem();
                if (object instanceof LoginVO) {
                    passwordField.setText(((LoginVO) object).getPassword());
                }
            }
        });
    }

    /**
     * 初始化组件
     */
    private void initComponents() {
        JPanel content = new JPanel(new BorderLayout());
        content.setOpaque(false);
        content.setPreferredSize(new Dimension(600, 400));
        content.add(createWelcomPanel(), BorderLayout.WEST);
        content.add(createContentPane(), BorderLayout.CENTER);

        backgroundPane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g.create();
                if (getBackgroundImage() != null) {
                    g2.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), null);
                }
                g2.dispose();
            }
        };
        topPane = new JPanel();
        topPane.setOpaque(false);
        topPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    restore();
                }
            }
        });

        JButton closeBut = new JButton();
        closeBut.setIcon(new FlatWindowCloseIcon());
        closeBut.addActionListener(e -> System.exit(0));
//        closeBut.putClientProperty( "JButton.buttonType" , "roundRect" );
        topPane.setLayout(new MigLayout("fill,wrap 1,insets 7", "[center]", "[top]0[top]"));
        topPane.add(closeBut, "top,right,w 30!,h 30!");
        topPane.add(content, "w 600!, h 400!");

        this.setLayout(new StackLayout());
        this.add(backgroundPane, StackLayout.BOTTOM);
        this.add(topPane, StackLayout.TOP);


    }

    /**
     * 创建欢迎面板
     *
     * @return {@link JPanel}
     */
    private JPanel createWelcomPanel() {
        JPanel infoPane = new JPanel(new BorderLayout(0, 0)) {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setComposite(AlphaComposite.SrcOver.derive(0.95f));

                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x014EBD), 0, getHeight(), new Color(0x01156F));
                g2.setPaint(gradientPaint);
                g2.fillRoundRect(0, 0, getWidth() + 40, getHeight(), 20, 20);
                g2.dispose();
            }
        };
        infoPane.setOpaque(false);
        infoPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        infoPane.setPreferredSize(new Dimension(240, 0));
        JLabel logo = new JLabel(new FlatSVGIcon("icons/cq-yujuetu.svg"));
        JLabel welcom = new JLabel("欢迎访问", JLabel.CENTER);
        welcom.setForeground(new Color(0xFFFFFF));
        welcom.setFont(welcom.getFont().deriveFont(30f));

        JLabel welcomInfo = new JLabel("xxxx系统", JLabel.CENTER);
        welcomInfo.setFont(welcomInfo.getFont().deriveFont(20f));
        welcomInfo.setForeground(new Color(0xFFFFFF));

        JPanel panel = new JPanel(new VerticalLayout(10));
        panel.setOpaque(false);
        panel.add(logo);
        panel.add(welcom);
        panel.add(welcomInfo);
        JLabel company = new JLabel("©liwen", JLabel.CENTER);
        company.setForeground(new Color(0xD7D7D7));

        JPanel welPanel = new JPanel();
        welPanel.setOpaque(false);
        GridBagLayout gridbag = new GridBagLayout();
        welPanel.setLayout(gridbag);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        welPanel.add(panel, c);

        infoPane.add(company, BorderLayout.SOUTH);
        infoPane.add(welPanel, BorderLayout.CENTER);
        return infoPane;
    }


    /**
     * 创建内容窗格
     *
     * @return {@link JPanel}
     */
    private JPanel createContentPane() {
        errorLabel = new JLabel("", JLabel.CENTER);
        errorLabel.setForeground(new Color(0xe66465));

        JLabel label = new JLabel("登录", JLabel.CENTER);
        label.setFont(label.getFont().deriveFont(30f));
        label.setForeground(Color.BLACK);
        progressBar = new JProgressBar();
        progressBar.setVisible(false);
        progressBar.setBackground(new Color(0xD9D8D8));

        userNameTextFiled = new WAutomaticTextFiled();
        userNameTextFiled.getTextField().setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        userNameTextFiled.getTextField().setOpaque(false);
        userNameTextFiled.getTextField().setFont(userNameTextFiled.getFont().deriveFont(20f));
        userNameTextFiled.getTextField().setForeground(Color.BLACK);
        userNameTextFiled.getTextField().setCaretColor(Color.BLACK);


        passwordField = new JPasswordField();
        passwordField.setOpaque(false);
        passwordField.setCaretColor(Color.BLACK);
        passwordField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        passwordField.setFont(passwordField.getFont().deriveFont(20f));
        passwordField.setForeground(Color.BLACK);

        verificationCodeTextFiled = new JXTextField();
        verificationCodeTextFiled.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        verificationCodeTextFiled.setOpaque(false);
        verificationCodeTextFiled.setFont(verificationCodeTextFiled.getFont().deriveFont(20f));
        verificationCodeTextFiled.setForeground(Color.BLACK);
        verificationCodeTextFiled.setCaretColor(Color.BLACK);

        verificationCodeImage = new JLabel("<html><img src=\"aa\" ></html");
        verificationCodeImage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                updateCode();
            }
        });

        loginButton = new JButton("登录") {
            @Override
            protected void paintComponent(Graphics g) {

                Graphics2D g2 = (Graphics2D) g.create();
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x01156F), 0, getHeight(), new Color(0x014EBD));

                g2.setPaint(gradientPaint);
                g2.fillRoundRect(0, 0, getWidth(), getHeight(), getHeight(), getHeight());

                FontMetrics fontMetrics = g2.getFontMetrics();
                g2.setColor(getForeground());
                String str = getText();
                int w = fontMetrics.stringWidth(str);
                int h = fontMetrics.getHeight();
                int x = (getWidth() - w) / 2;
                int y = (getHeight() - h) / 2 + fontMetrics.getLeading() + fontMetrics.getAscent();
                g2.drawString(str, x, y);
                g2.dispose();

            }
        };
        loginButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        loginButton.setForeground(new Color(0xFFFFFF));
        loginButton.setPreferredSize(new Dimension(260, 60));
        loginButton.putClientProperty("JButton.buttonType", "roundRect");
        loginButton.addActionListener(e -> login());

        JToolBar opt = new JToolBar();
        opt.setOpaque(false);
        opt.add(rememberPwd = new JCheckBox("记住密码"));
        rememberPwd.setForeground(Color.BLACK);
        rememberPwd.setIcon(new FlatSVGIcon("icons/ui-check-box-uncheck.svg", 20, 20));
        rememberPwd.setSelectedIcon(new FlatSVGIcon("icons/ui-check-box.svg", 20, 20));
        JPanel contentPane = new JPanel(new MigLayout("fill,wrap 2,insets 0 20 0 20", "[grow,center]7[]", "[grow,center][grow,center][grow,center]15[grow,center]15[grow,center][grow,center][grow,center][grow,center][grow,center]")) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g.create();
//                g2.setComposite(AlphaComposite.SrcOver.derive(0.95f));
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setColor(new Color(0xFFFFFF));
                g2.fillRoundRect(-40, 0, getWidth() + 40, getHeight(), 20, 20);
                g2.dispose();

            }
        };
        contentPane.setOpaque(false);
        contentPane.add(label, "span 2,h 60!");
        contentPane.add(progressBar, "span 2,growx,h 5!");
        contentPane.add(createTextPane(new FlatSVGIcon("icons/user.svg", 25, 25), userNameTextFiled), "span 2,growx,h 45!");
        contentPane.add(createTextPane(new FlatSVGIcon("icons/mima.svg", 25, 25), passwordField), "span 2,growx,h 45!");
        contentPane.add(createTextPane(new FlatSVGIcon("icons/yanzhengma.svg", 25, 25), verificationCodeTextFiled), "growx,h 45!");
        contentPane.add(verificationCodeImage, "growx,h 45!,w 90!");
        contentPane.add(opt, "span 2,growx,h 25!");
        contentPane.add(errorLabel, "span 2,w 120!");
        contentPane.add(loginButton, "span 2,growx,h 60!");


        passwordField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    login();
                }
            }
        });
        verificationCodeTextFiled.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    login();
                }
            }
        });
        passwordField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "请输入密码");
        passwordField.putClientProperty(FlatClientProperties.STYLE, "showRevealButton: true");

        JToggleButton revealButton = SwingUtils.getComponentByName(passwordField, "PasswordField.revealButton");
        if (revealButton != null) {
            revealButton.putClientProperty("JButton.selectedBackground", new Color(0x0FFFF00, true));
            revealButton.setIcon(new FlatSVGIcon("icons/view.svg", 15, 15));
            revealButton.setFocusable(false);
            revealButton.setBorderPainted(false);
            revealButton.setFocusPainted(false);
            revealButton.setOpaque(false);
        }

        userNameTextFiled.getTextField().putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "请输入用户名");
        userNameTextFiled.getTextField().putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);

        verificationCodeTextFiled.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "请输入验证码");
        verificationCodeTextFiled.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);


        return contentPane;

    }

    /**
     * 创建文本窗格
     *
     * @param icon      图标
     * @param component 组件
     * @return {@link JPanel}
     */
    private JPanel createTextPane(FlatSVGIcon icon, Component component) {
        icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
            return new Color(0x525252);
        }));
        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(0xebedf2)));
        panel.add(new JLabel(icon), BorderLayout.WEST);

        panel.add(component, BorderLayout.CENTER);
        return panel;
    }

    /**
     * 恢复
     */
    private void restore() {
        if (MainFrame.getInstance().getExtendedState() == JFrame.MAXIMIZED_BOTH) {
            MainFrame.getInstance().setExtendedState(JFrame.NORMAL);

        } else {
            MainFrame.getInstance().setExtendedState(JFrame.MAXIMIZED_BOTH);

        }
    }


    /**
     * 登录
     */
    private void login() {
        final String username = userNameTextFiled.getText();
        final String password = passwordField.getText();
        final String code = verificationCodeTextFiled.getText();
        final String uuid = codeId;
        SwingWorker<SingleResponse<Map<String, Object>>, String> worker = new SwingWorker<SingleResponse<Map<String, Object>>, String>() {
            @Override
            protected SingleResponse<Map<String, Object>> doInBackground() throws Exception {

                return Request.connector(LoginService.class).login(new LoginVO(username, password, code, uuid));
            }

            @Override
            protected void done() {

                try {
                    if (get().isSuccess()) {
                        errorLabel.setText("");
                        ApplicatonStore.setToken(get().getData().get("access_token").toString());
                        MainFrame.getInstance().showMainPanel();
                        MainFrame.getInstance().getMainPane().getStatusBarPanel().changeStatus(true);
                        if (rememberPwd.isSelected()) {
                            String json = JSONUtil.toJsonStr(new LoginVO(username, password, "", ""));
                            DefaultPrefs.getState().put(KEY_USER + username, json);
                            DefaultPrefs.getState().put(KEY_USER_CUR, json);
                        }
                    } else {
                        userNameTextFiled.setRequestFocusEnabled(true);
                        errorLabel.setText(get().getErrMessage());
                        updateCode();
                    }
                } catch (Exception e) {
                    errorLabel.setText("无法连接服务器，请检查服务器是否启动。");
                    e.printStackTrace();
                    updateCode();
                } finally {
                    setLoginStatus(false);
                }

            }


        };


        setLoginStatus(true);

        worker.execute();
    }

    /**
     * 设置登录状态
     *
     * @param bool 保龄球
     */
    private void setLoginStatus(boolean bool) {
        userNameTextFiled.requestFocusInWindow();
        progressBar.setIndeterminate(bool);
        passwordField.setEnabled(!bool);
        userNameTextFiled.setEnabled(!bool);
        loginButton.setEnabled(!bool);
        loginButton.setText(bool ? "正在登录..." : "登录");
        progressBar.setVisible(bool);
    }

    /**
     * 得到背景图像
     *
     * @return {@link BufferedImage}
     */
    public BufferedImage getBackgroundImage() {

        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage) {
        this.backgroundImage = backgroundImage;
        repaint();
    }

    /**
     * 获取验证码
     */
    public void updateCode() {

        SwingWorker<ImageIcon, String> worker = new SwingWorker<ImageIcon, String>() {
            @Override
            protected ImageIcon doInBackground() throws Exception {

                SingleResponse<ValidateCodeEntity> mono = Request.connector(LoginService.class).getCode();
                String img = mono.getData().getImg();
                BufferedImage image = ImgUtil.toImage(img);
                publish(mono.getData().getUuid());
                return new ImageIcon(image);
            }

            @Override
            protected void process(List<String> chunks) {
                for (String uuid : chunks) {
                    codeId = uuid;
                }
            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {

                        get().setImage(get().getImage().getScaledInstance(90, 40, Image.SCALE_DEFAULT));
                        verificationCodeImage.setText("");
                        verificationCodeImage.setIcon(get());


                    }
                } catch (Exception e) {
                    verificationCodeImage.setText("<html><img src=\\\"aa\\\" ></html");
                    throw new RuntimeException(e);
                }
            }
        };
        worker.execute();

    }


}
