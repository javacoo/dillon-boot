package org.dillon.swingui.view.component;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Path2D;

/**
 * @className: WCarPane
 * @author: liwen
 * @date: 2022/3/30 14:58
 */
public class WCardPane extends JComponent {

    private JLabel titleLabel = new JLabel();

    public WCardPane(String title) {
        this(title, null);
    }

    public WCardPane(String title, Icon icon) {
        titleLabel.setPreferredSize(new Dimension(0,35));
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));

        titleLabel.setText(title);
        titleLabel.setFont(titleLabel.getFont().deriveFont(16f));
        titleLabel.setIcon(icon);
        this.setLayout(new BorderLayout(10, 10));
        this.add(titleLabel, BorderLayout.NORTH);
        this.setBorder(BorderFactory.createEmptyBorder(0, 7, 7, 7));

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(new Color(0x4A78A5));
        int lw = 10;
        g2.setStroke(new BasicStroke(.4f));
        g2.drawRect(1, 1, getWidth() - 2, getHeight() - 2);
//        g2.drawRect(4, 4, getWidth() - 8, getHeight() - 8);
        g2.setStroke(new BasicStroke(2f));
        g2.setColor(new Color(0x14b3bc));
        g2.drawLine(1, 1, lw + 1, 1);
        g2.drawLine(getWidth() - lw - 1, 1, getWidth() - 1, 1);
        g2.drawLine(getWidth() - 1, 1, getWidth() - 1, lw + 1);
        g2.drawLine(getWidth() - 1, getHeight() - lw - 1, getWidth() - 1, getHeight() - 1);
        g2.drawLine(getWidth() - 1, getHeight() - 1, getWidth() - 1 - lw, getHeight() - 1);
        g2.drawLine(1, getHeight() - 1, lw + 1, getHeight() - 1);
        g2.drawLine(1, getHeight() - 1, 1, getHeight() - 1 - lw);
        g2.drawLine(1, lw + 1, 1, 1);
        g2.setColor(new Color(0x12FFFFFF, true));
        g2.fillRect(1, 1, getWidth() - 2, getHeight() - 2);



        GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x38597B), getWidth(), 0, new Color(0x74FFFC, true));
        g2.setPaint(gradientPaint);
        g2.fillRect(2, 2, getWidth(), 35);

        g2.setColor(new Color(0x14b3bc));
        g2.fillRect(6, 6, 4, 24);
        g2.dispose();

    }
}
