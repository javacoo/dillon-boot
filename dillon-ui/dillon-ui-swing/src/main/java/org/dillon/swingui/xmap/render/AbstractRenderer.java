package org.dillon.swingui.xmap.render;


import org.jxmapviewer.JXMapViewer;

import java.awt.*;


/**
 * 抽象渲染器
 */
public abstract class AbstractRenderer<W> implements Renderer<W> {
    private boolean antiAlias = true;//开启抗锯齿

    /**
     * 是否开启抗锯齿
     *
     * @return
     */
    public boolean isAntiAlias() {
        return antiAlias;
    }

    /**
     * 设置是否开启抗锯齿
     *
     * @param antiAlias
     */
    public void setAntiAlias(boolean antiAlias) {
        this.antiAlias = antiAlias;
    }


    /**
     * 绘制接口
     *
     * @param g         The Graphics2D to render to
     * @param map       map
     * @param geoObject 绘制对象
     */
    public void draw(Graphics2D g, JXMapViewer map, W geoObject) {
        if (map == null) {
            return;
        }

        if (geoObject == null) {
            return;
        }

        g = (Graphics2D) g.create();
        // convert from viewport to world bitmap
        Rectangle rect = map.getViewportBounds();
        g.translate(-rect.x, -rect.y);
        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        this.doPaint(g, map, geoObject);
        g.translate(rect.x, rect.y);
    }

    /**
     * 绘制接口
     *
     * @param g          The Graphics2D to render to
     * @param map        map
     * @param geoObjects 绘制对象
     */
    public void drawMany(Graphics2D g, JXMapViewer map, Iterable<? extends W> geoObjects) {
        if (map == null) {
            return;
        }

        if (geoObjects == null) {
            return;
        }

        g = (Graphics2D) g.create();
        // convert from viewport to world bitmap
        Rectangle rect = map.getViewportBounds();
        g.translate(-rect.x, -rect.y);
        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        this.doPaintMany(g, map, geoObjects);
        g.translate(rect.x, rect.y);
    }

    /**
     * 绘制实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param geoObject   绘制对象
     */
    protected abstract void doPaint(Graphics2D g, JXMapViewer jxMapViewer, W geoObject);

    /**
     * 绘制实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param geoObjects  绘制对象
     */
    protected abstract void doPaintMany(Graphics2D g, JXMapViewer jxMapViewer, Iterable<? extends W> geoObjects);
}
