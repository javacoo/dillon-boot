package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.constant.Constants;
import org.dillon.common.core.exception.job.TaskException;
import org.dillon.common.core.utils.StringUtils;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.common.core.web.page.TableDataInfo;
import org.dillon.job.domain.SysJob;
import org.dillon.system.api.domain.SysRole;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.domain.UserInfoModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 系统用户假装
 *
 * @author liwen
 * @date 2022/07/01
 */
@FeignClient(name = "sysJobFeign", url = "${server.url}", path = "/schedule")
public interface SysJobFeign {

    /**
     * 查询定时任务列表
     */
    @PostMapping("/job/list")
    PageResponse<SysJob> list( PageInfo<SysJob> pageQuery);

    /**
     * 导出定时任务列表
     */
    @PostMapping("/job/export")
    void export(SysJob sysJob);

    /**
     * 获取定时任务详细信息
     */
    @GetMapping(value = "/job/{jobId}")
    SingleResponse<SysJob> getInfo(@PathVariable("jobId") Long jobId);

    /**
     * 新增定时任务
     */
    @PostMapping("/job")
    OptResult add(@RequestBody SysJob job);

    /**
     * 修改定时任务
     */
    @PutMapping("/job")
    OptResult edit(@RequestBody SysJob job);

    /**
     * 定时任务状态修改
     */
    @PutMapping("/job/changeStatus")
    OptResult changeStatus(@RequestBody SysJob job);

    /**
     * 定时任务立即执行一次
     */
    @PutMapping("/job/run")
    OptResult run(@RequestBody SysJob job);

    /**
     * 删除定时任务
     */
    @DeleteMapping("/job/{jobIds}")
    OptResult remove(@PathVariable("jobIds") Long[] jobIds);

}
