package org.dillon.swingui.store;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dillon.swingui.observable.MenuRefreshObservable;
import org.dillon.swingui.observable.RefreshObservable;
import org.dillon.system.api.domain.UserInfo;

import javax.swing.*;
import java.awt.*;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description:
 * @className: User
 * @author: liwen
 * @date: 2020/8/2 10:17
 */

@Slf4j
public class ApplicatonStore {
    private static String token;
    private static UserInfo userInfo;
    private static ConcurrentHashMap<String, JComponent> TAB_CONTAINER = new ConcurrentHashMap<String, JComponent>();

    private static RefreshObservable REFRESH_OBSERVABLE = new RefreshObservable();
    private static MenuRefreshObservable MENU_REFRESH_OBSERVABLE = new MenuRefreshObservable();


    public static RefreshObservable getRefreshObservable() {
        return REFRESH_OBSERVABLE;
    }

    public static void addRefreshObserver(Observer o) {
        REFRESH_OBSERVABLE.addObserver(o);
    }

    public static MenuRefreshObservable getMenuRefreshObservable() {
        return MENU_REFRESH_OBSERVABLE;
    }
    public static void addMenuRefreshObservable(Observer o) {
        MENU_REFRESH_OBSERVABLE.addObserver(o);
    }


    /**
     * 获取功能面板
     *
     * @param className
     */
    public static Container getNavigatonPanel(String className) {



        JComponent container = StringUtils.isEmpty(className)?null: TAB_CONTAINER.get(className);
        if (container == null) {
            Class<?> clazz = null;
            try {
                clazz = Class.forName(className);
                container = (JComponent) clazz.newInstance();
                container.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                container.setOpaque(false);
                TAB_CONTAINER.put(className, container);
            } catch (Exception e1) {
                container = new JLabel("暂无投运", JLabel.CENTER);

                log.error("获取功能面板出错:[" + className + "] as:" + e1);
                e1.printStackTrace();
            }
        }
        return container;
    }

    /**
     * 移除功能面板
     *
     * @param component
     */
    public static void removeNavigatonPanel(Component component) {

        TAB_CONTAINER.remove(component.getClass().getName());

    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ApplicatonStore.token = token;
    }

    public static UserInfo getUserInfo() {
        return userInfo;
    }

    public static void setUserInfo(UserInfo userInfo) {
        ApplicatonStore.userInfo = userInfo;
    }
}


