package org.dillon.swingui.view.system.dict;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysDictTypeFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysDictType;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * dict管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class DictTypeManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "字典编号", "字典名称", "字典类型", "状态", "备注", "创建时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;


    private JPanel optDictPanel;

    private JTextField typeTextField;
    private JTextField nameTextField;
    private JComboBox statusCombo;


    /**
     * 目录单选按钮
     */
    private JTextField dictNameTextField;
    private JTextField dictTypeTextField;


    private JTextArea remarkTextArea;
    private JRadioButton normalBut;
    private JRadioButton disableBut;

    private WaitPane waitPane;
    public DictTypeManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("字典名称"));

        toolBar.add(typeTextField = createTextField("请输入字典类型"));
        typeTextField.setColumns(20);
        toolBar.add(nameTextField = createTextField("请输入字典名称"));
        nameTextField.setColumns(20);
        toolBar.add(new JLabel("字典状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "停用"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            typeTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        JButton addButton = new JButton("新增");
        toolBar.add(addButton);
        addButton.addActionListener(e -> {
            showDictAddDialog();
        });

        JButton eidtButton = new JButton("修改");
        toolBar.add(eidtButton);
        eidtButton.addActionListener(e -> {
            showDictEditDialog();
        });

        JButton delButton = new JButton("删除");
        toolBar.add(delButton);
        delButton.addActionListener(e -> {
            delDict(true);
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 7 || column == 0|| column == 3) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));




        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7,7,7,7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(toolBar, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);
                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
                table.getColumn("状态").setCellRenderer(new DefaultTableRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JCheckBox statusCheckBox = new JCheckBox("");
                        statusCheckBox.setOpaque(false);
                        statusCheckBox.setIcon(new AnimatedSwitchIcon());
                        statusCheckBox.setSelected((Boolean) value);
                        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
                        statusCheckBox.setBackground(component.getBackground());
                        statusCheckBox.setBorder(null);
                        return statusCheckBox;
                    }
                });

                table.getColumnExt("状态").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        JLabel label = new JLabel("0".equals(value) ? "正常" : "停用");
                        label.setForeground("0".equals(value) ? new Color(0x1890ff) : new Color(0xf56c6c));
                        FlatSVGIcon icon = new FlatSVGIcon("icons/yuan.svg", 10, 10);
                        icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
                            return label.getForeground();
                        }));
                        label.setIcon(icon);
                        panel.add(label);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });
                table.getColumn("字典类型").setCellRenderer(new OptButtonTableCellRenderer(createLinkBut()));
                table.getColumn("字典类型").setCellEditor(new OptButtonTableCellEditor(createLinkBut()));
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showDictEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delDict(false));
        del.setForeground(UIManager.getColor("App.accentColor"));

        optBar.add(edit);
        optBar.add(del);
        optBar.setPreferredSize(new Dimension(150, 50));
        return optBar;

    }

    private JXHyperlink createLinkBut() {
        JXHyperlink jxHyperlink = new JXHyperlink();
        jxHyperlink.addActionListener(e -> showDictDataTab());
        return jxHyperlink;

    }

    private void showDictDataTab() {

        int selRow = table.getSelectedRow();
        SysDictType dictType = null;
        if (selRow != -1) {
            dictType = (SysDictType) table.getValueAt(selRow, 7);
        }

        if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("字典数据") == -1) {

            MainFrame.getInstance().getMainPane().getTabbedPane().addTab("字典数据",  ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.system.dict.DictDataManagementPanel"));
        }
        MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("字典数据"));
        DictDataManagementPanel dictDataManagementPanel = (DictDataManagementPanel) ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.system.dict.DictDataManagementPanel");
        dictDataManagementPanel.setDictType(dictType);
    }

    private void showDictAddDialog() {

        int opt = WOptionPane.showOptionDialog(null, getOptDictPanel(false), "添加字典", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            addDict();
        }
    }


    private void showDictEditDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getOptDictPanel(true), "修改字典", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            editDict();
        }
    }

    private void updateData() {
        final SysDictType sysDictType = new SysDictType();
        sysDictType.setDictName(nameTextField.getText());
        sysDictType.setDictType(typeTextField.getText());
        int selectIndex = statusCombo.getSelectedIndex();
        sysDictType.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysDictType);

        SwingWorker<PageResponse<SysDictType>, Object> worker = new SwingWorker<PageResponse<SysDictType>, Object>() {
            @Override
            protected PageResponse<SysDictType> doInBackground() throws Exception {


                return Request.connector(SysDictTypeFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysDictType dict : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(dict.getDictId());
                            rowV.add(dict.getDictName());
                            rowV.add(dict.getDictType());
                            rowV.add(dict.getStatus());
                            rowV.add(dict.getRemark());
                            rowV.add(DateUtil.format(dict.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(dict);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    
                }
            }
        };
        tableModel.setRowCount(0);
        
        worker.execute();
    }


    public JPanel getOptDictPanel(Boolean isEidt) {
        optDictPanel = new JPanel(new MigLayout("fill,wrap 2,hidemode 3,insets 10", "[right][grow,240::]", "[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]"));

        dictNameTextField = createTextField("请输入字典名称");
        dictTypeTextField = createTextField("请输入字典类型");
        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("停用");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);
        remarkTextArea = new JTextArea();

        optDictPanel.add(new JLabel("*字典名称"));
        optDictPanel.add(dictNameTextField, "growx,growy");

        optDictPanel.add(new JLabel("*字典类型"));
        optDictPanel.add(dictTypeTextField, "growx,growy");


        optDictPanel.add(new JLabel("字典状态"));
        JToolBar toolBar4 = new JToolBar();
        toolBar4.add(normalBut);
        toolBar4.add(disableBut);
        optDictPanel.add(toolBar4, "growx");

        optDictPanel.add(new JLabel("备注"));
        optDictPanel.add(new JScrollPane(remarkTextArea), "growx,h 80!");

        if (isEidt) {
            getDictTree();
        }

        return optDictPanel;
    }

    private void getDictTree() {
        int selRow = table.getSelectedRow();
        Long dictId = null;
        if (selRow != -1) {
            dictId = (Long) table.getValueAt(selRow, 1);
        }
        Long finalDictId = dictId;
        SwingWorker<SysDictType, Object> worker = new SwingWorker<SysDictType, Object>() {
            @Override
            protected SysDictType doInBackground() throws Exception {

                return Request.connector(SysDictTypeFeign.class).getInfo(finalDictId).getData();

            }


            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        SysDictType dictModel = get();
                        updateValue(dictModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }


    private void updateValue(SysDictType sysDictType) {


        dictNameTextField.setText(sysDictType.getDictName());
        dictTypeTextField.setText(sysDictType.getDictType());
        normalBut.setSelected("0".equals(sysDictType.getStatus()));
        disableBut.setSelected("1".equals(sysDictType.getStatus()));
        remarkTextArea.setText(sysDictType.getRemark());

    }

    private SysDictType getValue() {
        SysDictType sysDictType = new SysDictType();
        sysDictType.setDictName(dictNameTextField.getText());
        sysDictType.setDictType(dictTypeTextField.getText());
        sysDictType.setStatus(normalBut.isSelected() ? "0" : "1");
        sysDictType.setRemark(remarkTextArea.getText());
        return sysDictType;
    }

    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加
     */
    private void addDict() {

        SysDictType sysDictType = getValue();
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDictTypeFeign.class).add(sysDictType);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void editDict() {

        SysDictType sysDictType = getValue();

        int selRow = table.getSelectedRow();
        if (selRow != -1) {
            Long id = (Long) table.getValueAt(selRow, 1);
            sysDictType.setDictId(id);
        } else {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDictTypeFeign.class).edit(sysDictType);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delDict(boolean isMore) {
        Long dictId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalDictId = dictId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysDictTypeFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
