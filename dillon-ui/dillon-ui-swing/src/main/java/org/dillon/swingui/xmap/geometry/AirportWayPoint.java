package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.*;

import java.awt.*;

public class AirportWayPoint extends DefaultWaypoint {
    private String AirportIdent;
    private String airportName;
    private AirportSigns signs;
    private Color color;
    private boolean signsVisbale;
    private boolean signsAlhpa;
    private Color signsBackground;
    private Color signsForeground;
    private String airportType;
    private boolean select;

    public AirportWayPoint() {

    }

    public AirportWayPoint(String airportName, AirportSigns signs, Color color,
                           boolean signsVisbale, boolean signsAlhpa, Color signsBackground,
                           Color signsForeground, String airportType, boolean select,
                           GeoPosition coord) {
        super(coord);
        this.airportName = airportName;
        this.signs = signs;
        this.color = color;
        this.signsVisbale = signsVisbale;
        this.signsAlhpa = signsAlhpa;
        this.signsBackground = signsBackground;
        this.signsForeground = signsForeground;
        this.airportType = airportType;
        this.select = select;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportIdent() {
        return AirportIdent;
    }

    public void setAirportIdent(String airportIdent) {
        AirportIdent = airportIdent;
    }

    public AirportSigns getSigns() {
        return signs;
    }

    public void setSigns(AirportSigns signs) {
        this.signs = signs;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isSignsVisbale() {
        return signsVisbale;
    }

    public void setSignsVisbale(boolean signsVisbale) {
        this.signsVisbale = signsVisbale;
    }

    public boolean isSignsAlhpa() {
        return signsAlhpa;
    }

    public void setSignsAlhpa(boolean signsAlhpa) {
        this.signsAlhpa = signsAlhpa;
    }

    public Color getSignsBackground() {
        return signsBackground;
    }

    public void setSignsBackground(Color signsBackground) {
        this.signsBackground = signsBackground;
    }

    public Color getSignsForeground() {
        return signsForeground;
    }

    public void setSignsForeground(Color signsForeground) {
        this.signsForeground = signsForeground;
    }

    public String getAirportType() {
        return airportType;
    }

    public void setAirportType(String airportType) {
        this.airportType = airportType;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

}
