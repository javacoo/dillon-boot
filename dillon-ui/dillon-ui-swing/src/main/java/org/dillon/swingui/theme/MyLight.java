package org.dillon.swingui.theme;

import com.formdev.flatlaf.FlatLightLaf;

public class MyLight
        extends FlatLightLaf {
    public static final String NAME = "MyLight";

    public static boolean setup() {
        return setup(new MyLight());
    }

    public static void installLafInfo() {
        installLafInfo(NAME, MyLight.class);
    }

    @Override
    public String getName() {
        return NAME;
    }
}
