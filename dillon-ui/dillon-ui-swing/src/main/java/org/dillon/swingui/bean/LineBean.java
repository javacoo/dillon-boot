package org.dillon.swingui.bean;

import lombok.Data;
import org.jxmapviewer.viewer.GeoPosition;

/**
 * @className: LineBean
 * @author: liwen
 * @date: 2022/4/8 09:12
 */
@Data
public class LineBean {
    private String name;
    private GeoPosition sendGeoPosition;
    private GeoPosition receivingGeoPosition;

    public LineBean(String name, GeoPosition sendGeoPosition, GeoPosition receivingGeoPosition) {
        this.name = name;
        this.sendGeoPosition = sendGeoPosition;
        this.receivingGeoPosition = receivingGeoPosition;
    }
}
