package org.dillon.swingui.view.component;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.icons.FlatClearIcon;
import com.formdev.flatlaf.icons.FlatSearchIcon;
import com.jidesoft.tree.FilterableTreeModel;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.tree.TreeModel;

/**
 * @className: WTreeFilterField
 * @author: liwen
 * @date: 2022/3/24 11:10
 */
public class WTreeFilterField extends JTextField {

    private JTree tree;
    private TreeModel treeModel;
    private TreeModel filterableModel;

    public WTreeFilterField() {
        this("");
    }

    public WTreeFilterField(String text) {
        this(null, text, 0);
    }

    public WTreeFilterField(int columns) {
        this(null, null, columns);
    }

    public WTreeFilterField(String text, int columns) {
        this(null, text, columns);
    }

    public WTreeFilterField(Document doc, String text, int columns) {
        super(doc, text, columns);

        this.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Search");
        JButton clearButton = new JButton(new FlatClearIcon());
        clearButton.addActionListener(e -> {
            this.setText("");
        });
        this.putClientProperty(FlatClientProperties.TEXT_FIELD_LEADING_ICON,
                new FlatSearchIcon());
        this.putClientProperty(FlatClientProperties.TEXT_FIELD_TRAILING_COMPONENT, clearButton);

    }

    public JTree getTree() {
        return tree;
    }

    public void setTree(JTree tree) {
        this.tree = tree;
    }
}
