package org.dillon.swingui.view.system.menu;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.jidesoft.combobox.TreeExComboBox;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.swing.WOptionPane;
import org.dillon.swing.WTreeComboBox;
import org.dillon.swing.WaitPane;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysMenuFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.system.api.model.SysMenuModel;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 菜单管理面板
 *
 * @author liwen
 * @date 2022/07/17
 */
public class MenuManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"菜单名称", "图标", "排序", "权限标识", "组件路径", "状态", "创建时间", "操作"};

    private JXTreeTable treeTable;

    private JPanel optMenuPanel;

    private TreeExComboBox parentMenuCombo;

    private JTextField nameTextField;
    private JComboBox statusCombo;


    /**
     * 目录单选按钮
     */
    private JRadioButton muluRadioButton;
    private JRadioButton menuRadioButton;
    private JRadioButton butRadioButton;
    private JTextField menuIconTextField;
    private JTextField menuNameTextField;
    private JSpinner sortSpinner;
    private JRadioButton yesBut;
    private JRadioButton noBut;

    private JTextField routingTextField;
    private JTextField componentTextField;
    private JTextField permissionTextField;
    private JTextField routeParTextField;
    private JRadioButton cacheBut;
    private JRadioButton noCacheBut;
    private JRadioButton normalBut;
    private JRadioButton disableBut;
    private JRadioButton showBut;
    private JRadioButton hideBut;

    private WaitPane waitPane;

    public MenuManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("菜单名称"));
        toolBar.add(nameTextField = createTextField("请输入菜单名称"));
        nameTextField.setColumns(20);
        toolBar.add(new JLabel("状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "停用"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        JButton addButton = new JButton("新增");
        toolBar.add(addButton);
        addButton.addActionListener(e -> {
            treeTable.clearSelection();
            showMenuAddDialog();
        });

        JToggleButton exButton = new JToggleButton("展开/折叠");
        toolBar.add(exButton);
        exButton.addActionListener(e -> {
            if (exButton.isSelected()) {
                treeTable.expandAll();
            } else {
                treeTable.collapseAll();
            }
        });
        exButton.setForeground(new Color(0xFFFFFF));
        treeTable = new JXTreeTable(new MenuTreeTableModel(null));
        treeTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        treeTable.setRowHeight(50);
        treeTable.setLeafIcon(null);
        treeTable.setClosedIcon(null);
        treeTable.setOpenIcon(null);
        treeTable.setShowHorizontalLines(true);
        treeTable.setIntercellSpacing(new Dimension(1, 1));
//        treeTable.setSelectionBackground(UIManager.getColor("Table.selectionBackground"));
        JScrollPane tsp = new JScrollPane(treeTable);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.setBorder(BorderFactory.createEmptyBorder(7,7,7,7));


        this.setLayout(new BorderLayout(0,10));
        this.add(toolBar, BorderLayout.NORTH);
        this.add(panel);


    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                treeTable.setHighlighters(rollover);
                treeTable.setIntercellSpacing(new Dimension(0, 1));
                treeTable.setLeafIcon(null);
                treeTable.setClosedIcon(null);
                treeTable.setOpenIcon(null);

//                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
//                dc.setHorizontalAlignment(SwingConstants.CENTER);
//                dc.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
//                treeTable.getColumn("排序").setCellRenderer(dc);

            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showMenuEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delMenu());
        del.setForeground(UIManager.getColor("App.accentColor"));

        JButton add = new JButton("新增");
        add.addActionListener(e -> showMenuAddDialog());
        add.setForeground(UIManager.getColor("App.accentColor"));
        add.setIcon(new FlatSVGIcon("icons/xinzeng.svg", 15, 15));
        optBar.add(edit);
        optBar.add(add);
        optBar.add(del);
        optBar.setPreferredSize(new Dimension(210, 45));
        return optBar;

    }

    private int getMenuType() {
        int type = 0;
        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                String menuType = (String) tree.get("menuType");
                if ("M".equals(menuType)) {
                    type = 0;
                } else if ("C".equals(menuType)) {
                    type = 1;
                } else if ("F".equals(menuType)) {
                    type = 2;
                }
            }
        }
        return type;
    }

    private void showMenuAddDialog() {

        int opt = WOptionPane.showOptionDialog(null, getOptMenuPanel(getMenuType(), false), "添加菜单", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            addMenu();
        }
    }


    private void showMenuEditDialog() {
        int opt = WOptionPane.showOptionDialog(null, getOptMenuPanel(getMenuType(), true), "修改菜单", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            editMenu();
        }
    }

    private void updateData() {
        SysMenuModel sysMenuModel = new SysMenuModel();
        sysMenuModel.setMenuName(nameTextField.getText());
        int selectIndex = statusCombo.getSelectedIndex();
        sysMenuModel.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));
        SwingWorker<Tree<Long>, Object> worker = new SwingWorker<Tree<Long>, Object>() {
            @Override
            protected Tree<Long> doInBackground() throws Exception {

                List<SysMenuModel> sysMenuModelList = Request.connector(SysMenuFeign.class).list(sysMenuModel).getData();
                long min = sysMenuModelList.stream().mapToLong(value -> value.getParentId()).min().orElse(0L);
                TreeNodeConfig config = new TreeNodeConfig();
                config.setWeightKey("orderNum");
                config.setParentIdKey("parentId");
                config.setNameKey("menuName");
                Tree<Long> treeList = TreeUtil.buildSingle(sysMenuModelList, min, config, ((object, treeNode) -> {
                    treeNode.setId(object.getMenuId());
                    treeNode.setParentId(object.getParentId());
                    treeNode.setName(object.getMenuName());
                    treeNode.putExtra("icon", object.getIcon());
                    treeNode.putExtra("component", object.getComponent());
                    treeNode.putExtra("orderNum", object.getOrderNum());
                    treeNode.putExtra("status", object.getStatus());
                    treeNode.putExtra("perms", object.getPerms());
                    treeNode.putExtra("menuType", object.getMenuType());
                    treeNode.putExtra("createTime", DateUtil.format(object.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                }));
                return treeList;

            }

            @Override
            protected void done() {
                try {
                    if (CollUtil.isNotEmpty(get())) {
                        updateTreeTableRoot(get());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        worker.execute();
    }

    private void updateTreeTableRoot(Object root) {

        treeTable.setTreeTableModel(new MenuTreeTableModel(root));
        treeTable.getColumnExt("操作").setMinWidth(210);
        treeTable.getColumnExt("操作").setMaxWidth(210);
        treeTable.getColumnExt("图标").setMinWidth(60);
        treeTable.getColumnExt("图标").setMaxWidth(60);
        treeTable.getColumnExt("排序").setWidth(80);
        treeTable.getColumnExt("排序").setMaxWidth(80);

        treeTable.getColumnExt("组件路径").setMinWidth(300);

        treeTable.getColumnExt("图标").setCellRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                JLabel label = new JLabel("", JLabel.CENTER);
                if (ObjectUtil.isNotEmpty(value) && !"#".equals(value)) {
                    label.setIcon(new FlatSVGIcon("icons/menu/" + value + ".svg", 25, 25));
                } else {
                    label.setText(value.toString());
                }
                label.setHorizontalTextPosition(CENTER);
                JPanel panel = new JPanel(new BorderLayout());
                panel.add(label);
                panel.setBackground(component.getBackground());
                return panel;
            }
        });

        treeTable.getColumnExt("状态").setCellRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                JLabel label = new JLabel("0".equals(value) ? "正常" : "停用");
                label.setForeground("0".equals(value) ? new Color(96, 197, 104) : new Color(0xf56c6c));
                FlatSVGIcon icon = new FlatSVGIcon("icons/yuan.svg", 10, 10);
                icon.setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
                    return label.getForeground();
                }));
                label.setIcon(icon);
                panel.add(label);
                panel.setBackground(component.getBackground());
                return panel;
            }
        });
        treeTable.getColumnExt("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
        treeTable.getColumnExt("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
        treeTable.getColumn("排序").setCellRenderer(new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

                JLabel label = new JLabel(value+"",JLabel.CENTER);
                JPanel panel = new JPanel(new BorderLayout());
                panel.add(label);
                panel.setBackground(component.getBackground());
                return panel;
            }
        });
    }

    public JPanel getOptMenuPanel(int type, Boolean isEidt) {
        optMenuPanel = new JPanel(new MigLayout("fill,wrap 4,hidemode 3,insets 10", "[right][grow,180::]30[right][grow,180::]", "[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]"));
        parentMenuCombo = new WTreeComboBox();
        /**
         * 目录单选按钮
         */
        ButtonGroup group1 = new ButtonGroup();
        muluRadioButton = new JRadioButton("目录");
        menuRadioButton = new JRadioButton("菜单");
        butRadioButton = new JRadioButton("按钮");
        group1.add(muluRadioButton);
        group1.add(menuRadioButton);
        group1.add(butRadioButton);

        muluRadioButton.addActionListener(e -> optLayout(0));
        menuRadioButton.addActionListener(e -> optLayout(1));
        butRadioButton.addActionListener(e -> optLayout(2));

        menuIconTextField = createTextField("请输入图标名称");
        menuNameTextField = createTextField("请输入菜单名称");
        sortSpinner = new JSpinner(new SpinnerNumberModel());
        yesBut = new JRadioButton("是", true);
        noBut = new JRadioButton("否");
        ButtonGroup group2 = new ButtonGroup();
        group2.add(yesBut);
        group2.add(noBut);


        routingTextField = createTextField("请输入路由地址");
        componentTextField = createTextField("请输入组件路径");
        permissionTextField = createTextField("请输入权限标识");
        routeParTextField = createTextField("请输入路由参数");
        cacheBut = new JRadioButton("缓存", true);
        noCacheBut = new JRadioButton("不缓存");
        ButtonGroup group3 = new ButtonGroup();
        group3.add(cacheBut);
        group3.add(noCacheBut);
        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("停用");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);
        showBut = new JRadioButton("显示", true);
        hideBut = new JRadioButton("隐藏");
        ButtonGroup group5 = new ButtonGroup();
        group5.add(showBut);
        group5.add(hideBut);

        if (isEidt == false) {
            muluRadioButton.setSelected(type == 0);
            menuRadioButton.setSelected(type == 1);
            butRadioButton.setSelected(type == 2);
        }
        optLayout(type);
        getMenuTree(isEidt);

        return optMenuPanel;
    }

    private void getMenuTree(boolean isEidt) {
        final SysMenuModel sysMenuModel = new SysMenuModel();


        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                Long menuId = (Long) tree.get("id");
                sysMenuModel.setMenuId(menuId);
                sysMenuModel.setMenuName(tree.getName() + "");
            }
        } else {
            sysMenuModel.setMenuId(0L);
            sysMenuModel.setMenuName("主类目");
        }

        SwingWorker<SysMenuModel, Tree<Long>> worker = new SwingWorker<SysMenuModel, Tree<Long>>() {
            @Override
            protected SysMenuModel doInBackground() throws Exception {

                List<SysMenuModel> sysMenuModelList = Request.connector(SysMenuFeign.class).list(new SysMenuModel()).getData();
                TreeNodeConfig config = new TreeNodeConfig();
                config.setWeightKey("orderNum");
                config.setParentIdKey("parentId");
                config.setNameKey("menuName");
                config.setIdKey("menuId");
                Tree<Long> tree = TreeUtil.buildSingle(sysMenuModelList, 0L, config, ((object, treeNode) -> {
                    treeNode.setId(object.getMenuId());
                    treeNode.setParentId(object.getParentId());
                    treeNode.setName(object.getMenuName());
                    treeNode.putExtra("icon", object.getIcon());
                    treeNode.putExtra("component", object.getComponent());
                    treeNode.putExtra("orderNum", object.getOrderNum());
                    treeNode.putExtra("status", object.getStatus());
                    treeNode.putExtra("perms", object.getPerms());
                    treeNode.putExtra("menuType", object.getMenuType());
                    treeNode.putExtra("createTime", DateUtil.format(object.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));

                }));

                publish(tree);
                if (isEidt) {
                    SysMenuModel menuModel = Request.connector(SysMenuFeign.class).getInfo(sysMenuModel.getMenuId()).getData();
                    return menuModel;
                } else {
                    SysMenuModel parentMenuModel = new SysMenuModel();
                    parentMenuModel.setMenuId(sysMenuModel.getMenuId());
                    parentMenuModel.setMenuName(sysMenuModel.getMenuName());
                    sysMenuModel.setParentMenuModel(parentMenuModel);
                    return sysMenuModel;
                }

            }

            @Override
            protected void process(List<Tree<Long>> chunks) {
                for (Tree<Long> tree : chunks) {
                    createDectTree(tree);
                }
            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        SysMenuModel menuModel = get();
                        updateValue(menuModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    private void createDectTree(Tree tree) {
        SysMenuModel sysMenuModel = BeanUtil.toBean(tree, SysMenuModel.class);
        sysMenuModel.setMenuName("主类目");
        sysMenuModel.setMenuId(0L);
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(sysMenuModel);
        if (CollectionUtil.isNotEmpty(tree.getChildren())) {
            createDectTreeDate(rootNode, tree.getChildren());
        }
        parentMenuCombo.setTreeModel(new DefaultTreeModel(rootNode));
        parentMenuCombo.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                if (value instanceof DefaultMutableTreeNode) {
                    if (((DefaultMutableTreeNode) value).getUserObject() instanceof Tree) {
                        return new JLabel(((SysMenuModel) ((DefaultMutableTreeNode) value).getUserObject()).getMenuName());
                    }
                }
                return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            }
        });

    }

    private void createDectTreeDate(DefaultMutableTreeNode fatherNode, List<Tree> treeList) {

        for (Tree treeNode : treeList) {

            SysMenuModel sysMenuModel = BeanUtil.toBean(treeNode, SysMenuModel.class);
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(sysMenuModel);
            fatherNode.add(node);

            if (CollectionUtil.isNotEmpty(treeNode.getChildren())) {
                createDectTreeDate(node, treeNode.getChildren());
            }
        }
    }

    private void optLayout(int type) {
        optMenuPanel.removeAll();
        JLabel label1, label2, label3, label4, label5, label6, label7, label8, label9, label10, label11, label12, label13;

        optMenuPanel.add(label1 = new JLabel("上级菜单"));
        optMenuPanel.add(parentMenuCombo, "span 3,growx,growy");

        optMenuPanel.add(label2 = new JLabel("菜单类型"));
        JToolBar toolBar = new JToolBar();
        toolBar.add(muluRadioButton);
        toolBar.add(menuRadioButton);
        toolBar.add(butRadioButton);
        optMenuPanel.add(toolBar, "span 3,growx,growy");

        optMenuPanel.add(label3 = new JLabel("菜单图标"));
        optMenuPanel.add(menuIconTextField, "growx,growy");
        optMenuPanel.add(label4 = new JLabel("菜单名称"));
        optMenuPanel.add(menuNameTextField, "growx,growy");

        optMenuPanel.add(label5 = new JLabel("显示排序"));
        optMenuPanel.add(sortSpinner, "growx,growy");
        optMenuPanel.add(label9 = new JLabel("权限字符"));
        optMenuPanel.add(permissionTextField, "growx,growy");

        optMenuPanel.add(label7 = new JLabel("路由地址"));
        optMenuPanel.add(routingTextField, "span 3,growx,growy");


        optMenuPanel.add(label8 = new JLabel("组件路径"));
        optMenuPanel.add(componentTextField, "span 3,growx,growy");


        optMenuPanel.add(label10 = new JLabel("路由参数"));
        optMenuPanel.add(routeParTextField, "span 3,growx,growy");

        optMenuPanel.add(label6 = new JLabel("是否外链"));
        JToolBar toolBar1 = new JToolBar();
        toolBar1.add(yesBut);
        toolBar1.add(noBut);
        optMenuPanel.add(toolBar1, "growx,growy");
        optMenuPanel.add(label11 = new JLabel("是否缓存"));
        JToolBar toolBar2 = new JToolBar();
        toolBar2.add(cacheBut);
        toolBar2.add(noCacheBut);
        optMenuPanel.add(toolBar2, "growx,growy");

        optMenuPanel.add(label12 = new JLabel("菜单状态"));
        JToolBar toolBar4 = new JToolBar();
        toolBar4.add(normalBut);
        toolBar4.add(disableBut);
        optMenuPanel.add(toolBar4, "growx");
        optMenuPanel.add(label13 = new JLabel("显示状态"));
        JToolBar toolBar5 = new JToolBar();
        toolBar5.add(showBut);
        toolBar5.add(hideBut);
        optMenuPanel.add(toolBar5, "growx");


        switch (type) {
            case 0:
                label3.setVisible(true);
                menuIconTextField.setVisible(true);
                label4.setVisible(true);
                menuNameTextField.setVisible(true);
                label5.setVisible(true);
                sortSpinner.setVisible(true);
                label6.setVisible(true);
                toolBar1.setVisible(true);
                label7.setVisible(true);
                routingTextField.setVisible(true);
                label8.setVisible(true);
                componentTextField.setVisible(true);
                label9.setVisible(false);
                permissionTextField.setVisible(false);
                label10.setVisible(false);
                routeParTextField.setVisible(false);
                label11.setVisible(false);
                toolBar2.setVisible(false);
                label12.setVisible(true);
                toolBar4.setVisible(true);
                label13.setVisible(true);
                toolBar5.setVisible(true);
                break;
            case 2:
                label3.setVisible(false);
                menuIconTextField.setVisible(false);
                label4.setVisible(true);
                menuNameTextField.setVisible(true);
                label5.setVisible(true);
                sortSpinner.setVisible(true);
                label6.setVisible(false);
                toolBar1.setVisible(false);
                label7.setVisible(false);
                routingTextField.setVisible(false);
                label8.setVisible(false);
                componentTextField.setVisible(false);
                label9.setVisible(true);
                permissionTextField.setVisible(true);
                label10.setVisible(false);
                routeParTextField.setVisible(false);
                label11.setVisible(false);
                toolBar2.setVisible(false);
                label12.setVisible(false);
                toolBar4.setVisible(false);
                label13.setVisible(false);
                toolBar5.setVisible(false);
                break;
            default:
                label3.setVisible(true);
                menuIconTextField.setVisible(true);
                label4.setVisible(true);
                menuNameTextField.setVisible(true);
                label5.setVisible(true);
                sortSpinner.setVisible(true);
                label6.setVisible(true);
                toolBar1.setVisible(true);
                label7.setVisible(true);
                routingTextField.setVisible(true);
                label8.setVisible(true);
                componentTextField.setVisible(true);
                label9.setVisible(true);
                permissionTextField.setVisible(true);
                label10.setVisible(true);
                routeParTextField.setVisible(true);
                label11.setVisible(true);
                toolBar2.setVisible(true);
                label12.setVisible(true);
                toolBar4.setVisible(true);
                label13.setVisible(true);
                toolBar5.setVisible(true);
        }


        optMenuPanel.revalidate();
        optMenuPanel.repaint();

    }

    private void updateValue(SysMenuModel menuModel) {
        SysMenuModel parentMenuModel = menuModel.getParentMenuModel();
        if (parentMenuModel == null) {
            parentMenuModel = new SysMenuModel();
            parentMenuModel.setMenuId(0L);
            parentMenuModel.setMenuName("主类目");
        }
        parentMenuCombo.setSelectedItem(parentMenuModel);
        muluRadioButton.setSelected("M".equals(menuModel.getMenuType()));
        menuRadioButton.setSelected("C".equals(menuModel.getMenuType()));
        butRadioButton.setSelected("F".equals(menuModel.getMenuType()));
        menuIconTextField.setText(menuModel.getIcon());
        menuNameTextField.setText(menuModel.getMenuName());
        sortSpinner.setValue(menuModel.getOrderNum() == null ? 0 : menuModel.getOrderNum());
        yesBut.setSelected("0".equals(menuModel.getIsFrame()));
        noBut.setSelected("1".equals(menuModel.getIsFrame()));
        routingTextField.setText(menuModel.getPath());
        componentTextField.setText(menuModel.getComponent());
        permissionTextField.setText(menuModel.getPerms());
        routeParTextField.setText(menuModel.getQuery());
        cacheBut.setSelected("0".equals(menuModel.getIsCache()));
        noCacheBut.setSelected("1".equals(menuModel.getIsCache()));
        normalBut.setSelected("0".equals(menuModel.getStatus()));
        disableBut.setSelected("1".equals(menuModel.getStatus()));
        showBut.setSelected("0".equals(menuModel.getVisible()));
        hideBut.setSelected("1".equals(menuModel.getVisible()));

    }

    private SysMenuModel getValue(int menuType) {
        SysMenuModel menuModel = new SysMenuModel();
        Object obj = parentMenuCombo.getSelectedItem();
        SysMenuModel parentMenuModel = null;
        if (obj instanceof TreePath) {
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) ((TreePath) obj).getLastPathComponent();
            parentMenuModel = (SysMenuModel) defaultMutableTreeNode.getUserObject();
        } else if (obj instanceof SysMenuModel) {
            parentMenuModel = (SysMenuModel) obj;
        }

        switch (menuType) {
            case 0:
                menuModel.setParentId(parentMenuModel.getMenuId());
                menuModel.setMenuType(muluRadioButton.isSelected() ? "M" : (menuRadioButton.isSelected() ? "C" : "F"));
                menuModel.setIcon(menuIconTextField.getText());
                menuModel.setMenuName(menuNameTextField.getText());
                menuModel.setComponent(componentTextField.getText());
                menuModel.setOrderNum((Integer) sortSpinner.getValue());
                menuModel.setIsFrame(yesBut.isSelected() ? "0" : "1");
                menuModel.setPath(routingTextField.getText());
                menuModel.setStatus(normalBut.isSelected() ? "0" : "1");
                menuModel.setVisible(showBut.isSelected() ? "0" : "1");

                break;
            case 2:
                menuModel.setParentId(parentMenuModel.getMenuId());
                menuModel.setMenuType(muluRadioButton.isSelected() ? "M" : (menuRadioButton.isSelected() ? "C" : "F"));
                menuModel.setMenuName(menuNameTextField.getText());
                menuModel.setOrderNum((Integer) sortSpinner.getValue());
                menuModel.setPerms(permissionTextField.getText());
                break;
            default:
                menuModel.setParentId(parentMenuModel.getMenuId());
                menuModel.setMenuType(muluRadioButton.isSelected() ? "M" : (menuRadioButton.isSelected() ? "C" : "F"));
                menuModel.setIcon(menuIconTextField.getText());
                menuModel.setMenuName(menuNameTextField.getText());
                menuModel.setOrderNum((Integer) sortSpinner.getValue());
                menuModel.setIsFrame(yesBut.isSelected() ? "0" : "1");
                menuModel.setPath(routingTextField.getText());
                menuModel.setComponent(componentTextField.getText());
                menuModel.setPerms(permissionTextField.getText());
                menuModel.setQuery(routeParTextField.getText());
                menuModel.setIsCache(cacheBut.isSelected() ? "0" : "1");
                menuModel.setStatus(normalBut.isSelected() ? "0" : "1");
                menuModel.setVisible(showBut.isSelected() ? "0" : "1");

        }

        return menuModel;
    }

    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加菜单
     */
    private void addMenu() {

        SysMenuModel menuModel = getValue(muluRadioButton.isSelected() ? 0 : menuRadioButton.isSelected() ? 1 : 2);
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysMenuFeign.class).add(menuModel);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                        ApplicatonStore.getMenuRefreshObservable().notify("refresh");
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void editMenu() {

        SysMenuModel menuModel = getValue(muluRadioButton.isSelected() ? 0 : menuRadioButton.isSelected() ? 1 : 2);

        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                Long menuId = (Long) tree.get("id");
                menuModel.setMenuId(menuId);
            }
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysMenuFeign.class).edit(menuModel);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                        ApplicatonStore.getMenuRefreshObservable().notify("refresh");

                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delMenu() {
        Long menuId = null;
        String menuName = null;

        int selRow = treeTable.getSelectedRow();
        if (selRow != -1) {
            Object obj = treeTable.getPathForRow(selRow).getLastPathComponent();
            if (obj instanceof Tree) {
                Tree tree = (Tree) obj;
                menuId = (Long) tree.get("id");
                menuName = tree.getName().toString();
            }
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除[" + menuName + "]？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalMenuId = menuId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysMenuFeign.class).remove(finalMenuId);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                        ApplicatonStore.getMenuRefreshObservable().notify("refresh");
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }

    class MenuTreeTableModel extends AbstractTreeTableModel {


        public MenuTreeTableModel() {
        }

        public MenuTreeTableModel(Object root) {
            super(root);
        }


        public void setRoot(Object root) {
            this.root = root;
            modelSupport.fireNewRoot();
        }

        @Override
        public boolean isCellEditable(Object node, int column) {
            if (column == 7) {
                return true;
            }
            return super.isCellEditable(node, column);
        }

        @Override
        public Object getChild(Object parent, int index) {

            Tree parentTree = (Tree) parent;

            return parentTree.getChildren().get(index);
        }

        @Override
        public int getChildCount(Object parent) {
            if (parent instanceof Tree) {
                List<Tree> children = ((Tree) parent).getChildren();

                if (children != null) {
                    return children.size();
                }
            }
            return 0;
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            if (parent instanceof Tree && child instanceof Tree) {
                Tree parentFile = (Tree) parent;
                List<Tree> treeList = parentFile.getChildren();

                for (int i = 0, len = treeList.size(); i < len; i++) {
                    if (treeList.get(i).equals(child)) {
                        return i;
                    }
                }
            }
            return -1;
        }

        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
                case 0:
                    return String.class;
                case 1:
                    return String.class;
                case 2:
                    return String.class;
                case 3:
                    return String.class;
                case 4:
                    return String.class;
                default:
                    return super.getColumnClass(column);
            }
        }

        @Override
        public String getColumnName(int column) {
            return COLUMN_ID[column];
        }


        @Override
        public int getColumnCount() {
            return COLUMN_ID.length;
        }

        @Override
        public Object getValueAt(Object node, int column) {
            if (node instanceof Tree) {
                Tree tree = (Tree) node;
                switch (column) {
                    case 0:
                        return tree.getName();
                    case 1:
                        return tree.get("icon");
                    case 2:
                        return tree.get("orderNum");
                    case 3:
                        return tree.get("perms");
                    case 4:
                        return tree.get("component");
                    case 5:
                        return tree.get("status");
                    case 6:
                        return tree.get("createTime");


                }
            }

            return null;
        }

    }


}
