package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author user
 */
public class ShapePoint extends AbstractGeometry {
    private GeoPosition position;
    private BufferedImage image;
    private float scale = 1.f;
    private String text;
    private Color color;

    /**
     * Creates a new instance of Waypoint
     */
    public ShapePoint() {
        this(new GeoPosition(0, 0), null);
    }

    /**
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    public ShapePoint(double latitude, double longitude) {
        this(new GeoPosition(latitude, longitude), null);
    }

    /**
     * @param coord the geo coordinate
     */
    public ShapePoint(GeoPosition coord) {
        this(coord, null);
    }

    /**
     * @param coord the geo coordinate
     * @param image point icon
     */
    public ShapePoint(GeoPosition coord, BufferedImage image) {
        this.shapeType = AbstractGeometry.SHAPE_POINT;
        this.position = coord;
        this.image = image;
    }


    public GeoPosition getPosition() {
        return position;
    }

    /**
     * Set a new GeoPosition for this Waypoint
     *
     * @param coordinate a new position
     */
    public void setPosition(GeoPosition coordinate) {
        GeoPosition old = getPosition();
        this.position = coordinate;
        firePropertyChange("position", old, getPosition());
    }

    public BufferedImage getImage() {
        if (image == null) {

        }
        return image;
    }

    public void setImage(BufferedImage image) {
        BufferedImage old = this.image;
        this.image = image;
        firePropertyChange("image", old, getImage());
    }

    /**
     * 获取缩放比例
     *
     * @return
     */
    public float getScale() {
        return scale;
    }

    /**
     * 设置缩放比例
     *
     * @param scale
     */
    public void setScale(float scale) {
        this.scale = scale;
    }



    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
