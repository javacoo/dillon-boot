package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.system.api.domain.SysMenu;
import org.dillon.system.api.model.TreeselectKeyModel;
import org.dillon.system.api.model.RouterVo;
import org.dillon.system.api.model.SysMenuModel;
import org.dillon.system.api.model.TreeSelect;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "sysMenuFeign", url = "${server.url}", path = "/system")
public interface SysMenuFeign {
    @GetMapping("/menu/getRouters")
    MultiResponse<RouterVo> getRouters();


    @PostMapping("/menu/list")
    MultiResponse<SysMenuModel> list( SysMenuModel menu);

    @GetMapping(value = "/menu/{menuId}")
    SingleResponse<SysMenuModel> getInfo(@PathVariable("menuId") Long menuId);

    @PostMapping(value = "/menu")
    OptResult add( SysMenuModel menuModel);

    @PutMapping(value = "/menu")
    OptResult edit( SysMenuModel menuModel);


    @DeleteMapping("/menu/{menuId}")
     OptResult remove(@PathVariable("menuId") Long menuId);


    /**
     * 获取菜单下拉树列表
     */
    @PostMapping("/menu/treeselect")
    MultiResponse<TreeSelect> treeselect(SysMenu menu) ;

    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = "/menu/roleMenuTreeselect/{roleId}")
    SingleResponse<TreeselectKeyModel> roleMenuTreeselect(@PathVariable("roleId") Long roleId);
}
