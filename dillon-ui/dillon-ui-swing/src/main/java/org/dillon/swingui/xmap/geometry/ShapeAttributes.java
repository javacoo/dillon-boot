package org.dillon.swingui.xmap.geometry;

import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * 图形属性
 */
public class ShapeAttributes {

    protected boolean drawInterior;//是否绘制内部
    protected boolean drawOutline;//是否绘制边界
    protected Color interiorColor;//内部填充色
    protected Color selectInteriorColor;//选中填充色
    protected Color outlineColor;//边界颜色
    protected float interiorOpacity;//内部填充透明度
    protected float outlineOpacity;//边界颜色透明度
    protected float outlineWidth;//边界线宽
    protected BasicStroke lineStroke;
    protected String text;
    private Point2D movePoint;
    private GeoPosition centerPosition;

    /**
     * 构建默认图形属性
     */
    public ShapeAttributes() {
        this.setDefaultAttributes();
    }

    /**
     * 设置默认属性
     */
    protected void setDefaultAttributes() {
        this.setDrawInterior(false);
        this.setDrawOutline(true);
        this.setInteriorColor(Color.lightGray);
        this.setOutlineColor(Color.black);
        this.setInteriorOpacity(1.f);
        this.setOutlineOpacity(1.f);
    }

    /**
     * 是否绘制内部
     *
     * @return
     */
    public boolean isDrawInterior() {
        return drawInterior;
    }

    /**
     * 设置是否内部填充
     *
     * @param drawInterior
     */
    public void setDrawInterior(boolean drawInterior) {
        this.drawInterior = drawInterior;
    }

    /**
     * 是否绘制边界
     *
     * @return
     */
    public boolean isDrawOutline() {
        return drawOutline;
    }

    /**
     * 设置是否绘制边界线
     *
     * @param drawOutline
     */
    public void setDrawOutline(boolean drawOutline) {
        this.drawOutline = drawOutline;
    }

    /**
     * 获取内部填充色
     *
     * @return
     */
    public Color getInteriorColor() {
        return interiorColor;
    }

    /**
     * 设置内部填充色
     *
     * @param interiorColor
     */
    public void setInteriorColor(Color interiorColor) {
        this.interiorColor = getComponentColor(interiorColor, this.getInteriorOpacity());
    }

    /**
     * 获取边界线颜色
     *
     * @return
     */
    public Color getOutlineColor() {
        return outlineColor;
    }

    /**
     * 设置边界线填充色
     *
     * @param outlineColor
     */
    public void setOutlineColor(Color outlineColor) {
        this.outlineColor = getComponentColor(outlineColor, this.getOutlineOpacity());
    }

    /**
     * 获取内部填充透明度
     *
     * @return
     */
    public float getInteriorOpacity() {
        return interiorOpacity;
    }

    /**
     * 设置内部填充透明度
     *
     * @param interiorOpacity
     */
    public void setInteriorOpacity(float interiorOpacity) {
        this.interiorOpacity = interiorOpacity;
        this.setInteriorColor(this.getInteriorColor());
    }

    /**
     * 获取边界线透明度
     *
     * @return
     */
    public float getOutlineOpacity() {
        return outlineOpacity;
    }

    /**
     * 设置边界线透明度
     *
     * @param outlineOpacity
     */
    public void setOutlineOpacity(float outlineOpacity) {
        this.outlineOpacity = outlineOpacity;
        this.setOutlineColor(this.getOutlineColor());
    }

    /**
     * 获取边界线宽
     *
     * @return
     */
    public float getOutlineWidth() {
        return outlineWidth;
    }

    /**
     * 设置边界线宽
     *
     * @param outlineWidth
     */
    public void setOutlineWidth(float outlineWidth) {
        this.outlineWidth = outlineWidth;
    }

    /**
     * 获取混合颜色
     *
     * @param color 源颜色
     * @param alpha 混合alpha值
     * @return 目标颜色
     */
    private static Color getComponentColor(Color color, float alpha) {
        if (color == null) {
            return null;
        }
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) (alpha * 255 + 0.5));
    }

    /**
     * 线条粗细
     *
     * @return
     */
    public BasicStroke getLineStroke() {
        if (lineStroke == null) {
            lineStroke = new BasicStroke(outlineWidth);
        }
        return lineStroke;
    }

    public void setLineStroke(BasicStroke lineStroke) {
        this.lineStroke = lineStroke;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Color getSelectInteriorColor() {
        return selectInteriorColor;
    }

    public void setSelectInteriorColor(Color selectInteriorColor) {
        this.selectInteriorColor = selectInteriorColor;
    }

    public Point2D getMovePoint() {
        return movePoint;
    }

    public void setMovePoint(Point2D movePoint) {
        this.movePoint = movePoint;
    }

    public GeoPosition getCenterPosition() {
        return centerPosition;
    }

    public void setCenterPosition(GeoPosition centerPosition) {
        this.centerPosition = centerPosition;
    }
}
