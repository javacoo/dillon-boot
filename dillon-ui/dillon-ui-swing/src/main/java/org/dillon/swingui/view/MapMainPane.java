package org.dillon.swingui.view;

import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.swingui.bean.LineBean;
import org.dillon.swingui.utils.IconLoader;
import org.dillon.swingui.utils.SysFont;
import org.dillon.swingui.xmap.WXMapViewer;
import org.dillon.swingui.xmap.geometry.SurfaceImage;
import org.dillon.swingui.xmap.listener.SelectionAdapter;
import org.dillon.swingui.xmap.painters.RoutePainter;
import org.dillon.swingui.xmap.painters.ShpPainters;
import org.dillon.swingui.xmap.painters.SubstationPainter;
import org.dillon.swingui.xmap.painters.SurfaceImageLayer;
import org.jdesktop.swingx.StackLayout;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.ui.GradientPaintTransformType;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.chart.ui.StandardGradientPaintTransformer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jxmapviewer.viewer.GeoBounds;
import org.jxmapviewer.viewer.GeoPosition;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * @className: MapMainPane
 * @author: liwen
 * @date: 2022/4/7 20:20
 */
public class MapMainPane extends JPanel {

    private WXMapViewer mapViewer;
    private JPanel contentPanel;
    private JTable table;

    public MapMainPane() {
        initComponents();
    }

    private void initComponents() {
        JPanel rootPanel = new JPanel(new StackLayout());
        rootPanel.add(getMapViewer(), StackLayout.BOTTOM);
        rootPanel.add(getContentPanel(), StackLayout.TOP);

        this.setLayout(new BorderLayout());
        this.add(rootPanel, BorderLayout.CENTER);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                revalidate();
            }
        });
    }

    private void chartUI() {
        StandardChartTheme chartTheme = new StandardChartTheme("CN");
        chartTheme.setExtraLargeFont(SysFont.APP_FONT.deriveFont(14f));
        chartTheme.setLargeFont(SysFont.APP_FONT.deriveFont(14f));
        chartTheme.setRegularFont(SysFont.APP_FONT.deriveFont(12f));
        chartTheme.setSmallFont(SysFont.APP_FONT.deriveFont(10f));
        ChartFactory.setChartTheme(chartTheme);
    }

    public WXMapViewer getMapViewer() {
        if (mapViewer == null) {
            mapViewer = new WXMapViewer() {
                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    MapMainPane.this.repaint();
                }
            };

            SelectionAdapter selectionAdapter = new SelectionAdapter(mapViewer);
            mapViewer.addMouseListener(selectionAdapter);
            mapViewer.addMouseMotionListener(selectionAdapter);
            List<LineBean> lineBeans = new ArrayList<>();
            lineBeans.add(new LineBean("x新疆", new GeoPosition(40.14219, 94.66197), new GeoPosition(41.793028, 91.66781199999995)));
            lineBeans.add(new LineBean("x新疆", new GeoPosition(38.993572058209466, 93.44970703125001), new GeoPosition(41.793028, 91.66781199999995)));
            lineBeans.add(new LineBean("x宁夏", new GeoPosition(37.35269280367274, 103.55712890625001), new GeoPosition(38.471318, 106.25875399999995)));
            lineBeans.add(new LineBean("x宁夏", new GeoPosition(35.92464453144099, 107.70996093750001), new GeoPosition(38.471318, 106.25875399999995)));


            SurfaceImageLayer surfaceImageLayer = new SurfaceImageLayer();
            surfaceImageLayer.addSurfaceImage(new SurfaceImage(IconLoader.image("/images/NB-Map.png"),new GeoBounds(32.59411D,92.340129D,42.795163D,108.712336D)));
            // Create a track from the geo-positions
            mapViewer.addPainter(new ShpPainters(mapViewer));
            mapViewer.addPainter(surfaceImageLayer);

            mapViewer.addPainter(new RoutePainter(lineBeans));
            mapViewer.addPainter(new SubstationPainter());
        }
        return mapViewer;
    }

    public JPanel getContentPanel() {
        if (contentPanel == null) {
            contentPanel = new JPanel(new MigLayout("wrap 3", "[grow,left]10[grow,right]10[grow,right]", "[grow,top]10[grow,center]10[grow,bottom]"));
            contentPanel.setOpaque(false);
            contentPanel.add(a(), "growx,growy");
            contentPanel.add(b(), "growx,growy");
            contentPanel.add(c(), "growx,growy");
            contentPanel.add(d(), "growx,growy,span 1 2");
            contentPanel.add(new JLabel(), "growx,growy");
            contentPanel.add(f(), "growx,growy");
            contentPanel.add(new JLabel(), "growx,growy");
            contentPanel.add(g(), "growx,growy");
        }
        return contentPanel;
    }

    private JPanel a() {
        JPanel panel = new JPanel(new BorderLayout(10, 10));
        panel.setOpaque(false);

        JPanel tp = new JPanel(new FlowLayout());
        tp.setOpaque(false);
        JLabel label = new JLabel("全网示意");
        label.setForeground(new Color(0x19D8A5));
        label.setFont(label.getFont().deriveFont(16).deriveFont(Font.BOLD));
        label.setIcon(new FlatSVGIcon("icons/tubiao_quanwanghutong.svg", 30, 30));
        tp.add(label);
        JRadioButton radioButton = new JRadioButton("列表");
        radioButton.setForeground(new Color(0x009788));
        tp.add(radioButton);

        JPanel cp = new JPanel(new VerticalLayout());
        cp.setOpaque(false);
        cp.add(createPage("xx模式:", new JLabel("优化"), true));
        cp.add(createPage("xx状态:", new JLabel(new FlatSVGIcon("icons/yuandian.svg", 20, 20), JLabel.LEFT), false));
        cp.add(createPage("xx策略:", new JLabel("--"), true));
        cp.add(createPage("xx模式:", new JLabel("闭环"), false));
        cp.add(createPage("xx时间:", new JLabel("--"), true));
        cp.add(createPage("主/备应用:", new JLabel("主系统"), false));
        cp.add(createPage("xx时间:", new JLabel(""), true));

        panel.add(tp, BorderLayout.NORTH);
        panel.add(cp, BorderLayout.CENTER);
        return panel;
    }


    private JPanel b() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 20, 10));
        panel.setOpaque(false);
        panel.add(createPage(null, "频率(HZ)", "50.01", new Color(0x0FEBFF), SysFont.ACENS_FONT.deriveFont(40f).deriveFont(Font.BOLD)), BorderLayout.CENTER);

        JPanel cp = new JPanel(new VerticalLayout(7));
        cp.setOpaque(false);
        cp.add(createPage("xx偏差:", new JLabel("211.5"), true));
        cp.add(createPage("xx状态:", new JLabel(), false));
        cp.add(createPage("xxx/区间:", new JLabel("311.5/紧急区"), true));
        cp.add(createPage("xxxx量:", new JLabel(""), false));
        panel.add(cp);
        return panel;
    }

    private JPanel c() {
        JPanel pageP = createPanel("xxx实时监测");
        JScrollPane tsp = new JScrollPane(getTable());
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        pageP.add(tsp);
        return pageP;
    }

    private JPanel f() {
        JPanel pageP = createPanel("xxx实时监测");
        JPanel panel = new JPanel(new GridLayout(2, 2, 10, 10));
        panel.setOpaque(false);
        panel.add(createPage(new FlatSVGIcon("icons/pinshuai.svg", 45, 46), "xxx计划", "211.95", new Color(0xEC9B05), SysFont.ACENS_FONT.deriveFont(24f).deriveFont(Font.BOLD)), BorderLayout.CENTER);
        panel.add(createPage(new FlatSVGIcon("icons/pinshuai.svg", 46, 46), "xxx功率", "211.95", new Color(0x0FEBFF), SysFont.ACENS_FONT.deriveFont(24f).deriveFont(Font.BOLD)), BorderLayout.CENTER);
        panel.add(createPage(new FlatSVGIcon("icons/pinshuai.svg", 46, 46), "上/下备用", "1580/850", new Color(0x0FEBFF), SysFont.ACENS_FONT.deriveFont(24f).deriveFont(Font.BOLD)), BorderLayout.CENTER);
        panel.add(createPage(new FlatSVGIcon("icons/pinshuai.svg", 46, 45), "xxxx功率", "-11.3", new Color(0xFF6521), SysFont.ACENS_FONT.deriveFont(24f).deriveFont(Font.BOLD)), BorderLayout.CENTER);
        pageP.add(panel);
        return pageP;
    }

    private JPanel d() {
        JPanel pageP = createPanel("xxxxx总览");
        JPanel panel = new JPanel(new GridLayout(2, 1, 10, 10));
        panel.setOpaque(false);
        panel.add(createChartPanel1());
        panel.add(createChartPanel2());

        pageP.add(panel);
        return pageP;
    }

    private JPanel g() {
        JPanel pageP = createPanel("xxxx曲线");

        pageP.add(createChartPanel3());
        return pageP;
    }

    private JPanel createPage(Icon icon, String title, String value, Color color, Font font) {
        JPanel panel = new JPanel(new BorderLayout(10, 10)) {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setColor(new Color(0x2261FAE3, true));
                g2.fillRoundRect(0, 0, getWidth(), getHeight(), 8, 8);
                g2.dispose();
            }
        };
        JLabel titleLabel = new JLabel(title);
        JLabel valueLabel = new JLabel(value);
        valueLabel.setFont(font);
        valueLabel.setForeground(color);
        panel.add(titleLabel, BorderLayout.NORTH);
        panel.add(valueLabel, BorderLayout.CENTER);
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel iconPane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setColor(new Color(0x2261FAE3, true));
                g2.fillRoundRect(0, 0, getWidth(), getHeight(), 8, 8);
                g2.dispose();
            }
        };
        iconPane.setOpaque(false);

        iconPane.add(new JLabel(icon));

        JPanel resultpanel = new JPanel(new BorderLayout(5, 10));
        if (icon != null) {
            resultpanel.add(iconPane, BorderLayout.WEST);

        }
        resultpanel.add(panel, BorderLayout.CENTER);
        resultpanel.setOpaque(false);
        return resultpanel;
    }

    public JTable getTable() {
        if (table == null) {
            table = new JTable();
            table.setOpaque(false);
            DefaultTableModel defaultTableModel = new DefaultTableModel();
            defaultTableModel.setColumnIdentifiers(new String[]{"", "xxx", "xxx", "xx", "xx", "xx", "xxx"});
            Random random = new Random();
            for (int i = 0; i < 4; i++) {
                Vector vector = new Vector();
                vector.add("");
                vector.add(random.nextInt(2000));
                vector.add(random.nextInt(2000));
                vector.add(random.nextInt(2000));
                vector.add(random.nextInt(2000));
                vector.add(random.nextInt(2000));
                vector.add(random.nextInt(2000));
                defaultTableModel.addRow(vector);
            }
            table.setModel(defaultTableModel);
            table.setRowHeight(45);
            table.setShowHorizontalLines(false);
            table.setShowVerticalLines(false);
            table.getTableHeader().setBackground(new Color(0x000FFFF, true));
            table.getTableHeader().setDefaultRenderer(new DefaultTableRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    return new JLabel(value + "", JLabel.CENTER);
                }
            });
            table.setDefaultRenderer(Object.class, new DefaultTableRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

                    if (column == 0) {
                        String icons = "icons/pinshuai.svg";
                        if (row == 0) {
                            icons = "icons/tubiao_quanwanghutong.svg";
                        } else if (row == 1) {
                            icons = "icons/guangfu.svg";
                        } else if (row == 2) {
                            icons = "icons/fengdianchang.svg";
                        } else if (row == 3) {
                            icons = "icons/shuidiantu.svg";
                        }
                        JLabel jLabel = new JLabel(new FlatSVGIcon(icons, 30, 30));
                        return jLabel;
                    } else {
                        JPanel panel = new JPanel(new BorderLayout());
                        panel.setOpaque(false);
                        panel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
                        JLabel jLabel = new JLabel(value + "", JLabel.CENTER);
                        jLabel.setOpaque(true);
                        jLabel.setFont(SysFont.ACENS_FONT.deriveFont(16f).deriveFont(Font.BOLD));

                        jLabel.setForeground(isSelected ? new Color(0x0FEBFF) : new Color(0x0CB0BF));
                        if (Double.parseDouble(value + "") > 1900) {
                            jLabel.setForeground(new Color(0xFF7256));
                        }
                        jLabel.setBackground(isSelected ? new Color(0x640FEBFF, true) : new Color(0x2261FAE3, true));
                        panel.add(jLabel);

                        return panel;
                    }
                }
            });
        }
        return table;
    }

    private JPanel createPage(String title, JLabel label, boolean isFill) {
        JPanel panel = new JPanel(new BorderLayout(10, 10)) {
            @Override
            protected void paintComponent(Graphics g) {
                if (isFill) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x2D49DBFC, true), getWidth(), 0, new Color(0x74FFFC, true));
                    g2.setPaint(gradientPaint);
                    g2.fillRect(0, 0, getWidth(), getHeight());
                    g2.dispose();
                } else {
                    super.paintComponent(g);
                }
            }
        };
        label.setFont(label.getFont().deriveFont(16f).deriveFont(Font.BOLD));
        label.setForeground(new Color(0x19D8A5));
        panel.add(new JLabel(title), BorderLayout.WEST);
        panel.add(label, BorderLayout.CENTER);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 0));
        panel.setOpaque(false);
        return panel;
    }

    private JPanel createPanel(String title) {
        JLabel titleLabel = new JLabel(title);
        titleLabel.setPreferredSize(new Dimension(0, 35));
        JPanel panel = new JPanel(new BorderLayout(10, 10)) {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D) g.create();
                GradientPaint gradientPaint = new GradientPaint(0, 0, new Color(0x350FEBFF, true), getWidth(), 0, new Color(0x74FFFC, true));
                g2.setPaint(gradientPaint);
                g2.fillRect(0, 0, getWidth(), 35);
                g2.setPaint(new Color(0x0FEBFF));
                g2.fillRect(0, 0, 4, 35);
                g2.dispose();
                super.paintComponent(g);
            }
        };
        ;
        panel.setOpaque(false);
        panel.add(titleLabel, BorderLayout.NORTH);
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        return panel;
    }

    private XYSeriesCollection createSampleData1() {

        XYSeries series = new XYSeries("xxx出力");
        XYSeries series1 = new XYSeries("xxx指令");
        XYSeries series2 = new XYSeries("xxx定值");

        Random random = new Random();
        for (int i = 0; i < 24; i++) {
            series.add(i, random.nextDouble() * 6000D + 2000D);
            series1.add(i, random.nextDouble() * 6000D + 2000D);
            series2.add(i, random.nextDouble() * 6000D + 2000D);
        }
        XYSeriesCollection result = new XYSeriesCollection(series);
        result.addSeries(series1);
        result.addSeries(series2);
        return result;
    }

    private XYSeriesCollection createSampleData2() {


        XYSeries series = new XYSeries("xxx1");
        XYSeries series1 = new XYSeries("xxx2");
        XYSeries series2 = new XYSeries("xxxx3");

        Random random = new Random();
        for (int i = 0; i < 24; i++) {
            series.add(i, random.nextDouble() * 15000D + 5000D);
            series1.add(i, random.nextDouble() * 15000D + 5000D);
            series2.add(i, random.nextDouble() * 15000D + 5000D);
        }
        XYSeriesCollection result = new XYSeriesCollection(series);
        result.addSeries(series1);
        result.addSeries(series2);
        return result;
    }


    private ChartPanel createChartPanel1() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setAutoRangeIncludesZero(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRangeIncludesZero(false);
        XYSplineRenderer renderer1 = new XYSplineRenderer(5, XYSplineRenderer.FillType.TO_ZERO);
        XYPlot plot = new XYPlot(createSampleData1(), xAxis, yAxis, renderer1);
        plot.setRangePannable(true);
        plot.setRangeGridlinesVisible(false);
        plot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        JFreeChart chart = new JFreeChart("", SysFont.APP_FONT, plot, true);
        ChartUtils.applyCurrentTheme(chart);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setOpaque(false);

        XYSplineRenderer localXYLineAndShapeRenderer = (XYSplineRenderer) plot.getRenderer();
        localXYLineAndShapeRenderer.setDefaultShapesVisible(false);
        localXYLineAndShapeRenderer.setDefaultStroke(new BasicStroke(2f));
        localXYLineAndShapeRenderer.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.CENTER_VERTICAL));

        localXYLineAndShapeRenderer.setSeriesPaint(0, new Color(0x47ECFC));
        localXYLineAndShapeRenderer.setSeriesFillPaint(0, new GradientPaint(0, 0, new Color(0x47ECFC), 0, 0, new Color(0x47ECFC, true)));
        localXYLineAndShapeRenderer.setSeriesPaint(1, new Color(0xF5C26C));
        localXYLineAndShapeRenderer.setSeriesFillPaint(1, new GradientPaint(0, 0, new Color(0xF5C26C), 0, 0, new Color(0xF5C26C, true)));
        localXYLineAndShapeRenderer.setSeriesPaint(2, new Color(0xFF3D25));
        localXYLineAndShapeRenderer.setSeriesFillPaint(2, new GradientPaint(0, 0, new Color(0xFF3D25), 0, 0, new Color(0xFF3D25, true)));


        createXyChartUI(chart);
        return chartPanel;
    }

    private ChartPanel createChartPanel2() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setAutoRangeIncludesZero(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRangeIncludesZero(false);
        XYSplineRenderer renderer1 = new XYSplineRenderer(5, XYSplineRenderer.FillType.TO_ZERO);
        XYPlot plot = new XYPlot(createSampleData2(), xAxis, yAxis, renderer1);
        plot.setRangePannable(true);
        plot.setRangeGridlinesVisible(false);
        plot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        JFreeChart chart = new JFreeChart("", SysFont.APP_FONT, plot, true);
        ChartUtils.applyCurrentTheme(chart);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setOpaque(false);

        XYSplineRenderer localXYLineAndShapeRenderer = (XYSplineRenderer) plot.getRenderer();
        localXYLineAndShapeRenderer.setDefaultShapesVisible(false);
        localXYLineAndShapeRenderer.setDefaultStroke(new BasicStroke(2f));
        localXYLineAndShapeRenderer.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.CENTER_VERTICAL));
        localXYLineAndShapeRenderer.setSeriesPaint(0, new Color(0x169FF3));
        localXYLineAndShapeRenderer.setSeriesFillPaint(0, new GradientPaint(0, 0, new Color(0x169FF3), 0, 0, new Color(0x169FF3, true)));
        localXYLineAndShapeRenderer.setSeriesPaint(1, new Color(0xBC8BF6));
        localXYLineAndShapeRenderer.setSeriesFillPaint(1, new GradientPaint(0, 0, new Color(0xBC8BF6), 0, 0, new Color(0xBC8BF6, true)));
        localXYLineAndShapeRenderer.setSeriesPaint(2, new Color(0xF6D47E));
        localXYLineAndShapeRenderer.setSeriesFillPaint(2, new GradientPaint(0, 0, new Color(0xF6D47E), 0, 0, new Color(0xF6D47E, true)));


        createXyChartUI(chart);
        return chartPanel;
    }


    private XYDataset createSampleData3() {
        XYSeries series = new XYSeries("xxx力");

        Random random = new Random();
        for (int i = 0; i < 24; i++) {
            series.add(i, random.nextDouble() * 10000 + 10000);
        }

        XYSeriesCollection result = new XYSeriesCollection(series);

        return result;
    }

    private void createXyChartUI(JFreeChart chartUI) {
        Color color = new Color(0f, 0f, 0f, .0f);
        chartUI.getTitle().setPaint(new Color(0xffffff));
        chartUI.setBackgroundPaint(color);
        chartUI.getPlot().setBackgroundPaint(color);
        chartUI.getXYPlot().setDomainGridlineStroke(new BasicStroke());
        chartUI.getXYPlot().setOutlinePaint(color);
        chartUI.getXYPlot().setRangeGridlinePaint(Color.WHITE);
        chartUI.getXYPlot().setRangeGridlinesVisible(true);
        chartUI.getXYPlot().setDomainGridlinePaint(color);
        chartUI.getXYPlot().setDomainGridlinesVisible(false);
        chartUI.getXYPlot().getDomainAxis().setAxisLinePaint(Color.WHITE);
        chartUI.getXYPlot().getDomainAxis().setTickLabelPaint(Color.WHITE);
        chartUI.getXYPlot().getRangeAxis().setAxisLinePaint(Color.WHITE);
        chartUI.getXYPlot().getRangeAxis().setTickLabelPaint(Color.WHITE);
        if (chartUI.getLegend() != null) {
            chartUI.getLegend().setBackgroundPaint(color);
            chartUI.getLegend().setBorder(0, 0, 0, 0);
            chartUI.getLegend().setItemPaint(Color.WHITE);
            chartUI.getLegend().setPosition(RectangleEdge.TOP);
        }
        chartUI.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        chartUI.setAntiAlias(true);
    }

    private ChartPanel createChartPanel3() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setAutoRangeIncludesZero(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRangeIncludesZero(false);
        XYSplineRenderer renderer1 = new XYSplineRenderer(5, XYSplineRenderer.FillType.TO_ZERO);
        XYPlot plot = new XYPlot(createSampleData3(), xAxis, yAxis, renderer1);
        plot.setRangePannable(true);
        plot.setRangeGridlinesVisible(false);
        plot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        JFreeChart chart = new JFreeChart("", SysFont.APP_FONT, plot, false);
        ChartUtils.applyCurrentTheme(chart);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setOpaque(false);

        XYSplineRenderer localXYLineAndShapeRenderer = (XYSplineRenderer) plot.getRenderer();
        localXYLineAndShapeRenderer.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.CENTER_VERTICAL));
        localXYLineAndShapeRenderer.setDefaultShapesVisible(false);
        localXYLineAndShapeRenderer.setSeriesPaint(0, new Color(0xFEA815));
        localXYLineAndShapeRenderer.setSeriesFillPaint(0, new GradientPaint(0, 0, new Color(0xBDFEA815, true), 0, 0, new Color(0xFEA815, true)));
        localXYLineAndShapeRenderer.setSeriesStroke(0, new BasicStroke(3f));

        createXyChartUI(chart);
        return chartPanel;
    }

}
