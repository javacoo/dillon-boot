package org.dillon.swingui.view.rollingmonitor;

import com.formdev.flatlaf.ui.FlatLineBorder;
import org.dillon.swing.chart.WChartPanel;
import org.dillon.swing.chart.WChartUtil;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * @className: RollingMonitor
 * @author: liwen
 * @date: 2022/3/24 15:51
 * 滚动监视面板
 */
public class RollingMonitorPanel extends JPanel {

    private WChartPanel chartPanel1;
    private WChartPanel chartPanel2;
    private WChartPanel chartPanel3;
    private WChartPanel chartPanel4;
    public RollingMonitorPanel() {

        initComponents();
    }

    private void initComponents() {
        this.setOpaque(false);
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridLayout(2, 2, 10, 10));
        this.add(chartPanel1=createChartPanel1("曲线1"));
        this.add(chartPanel2=createChartPanel1("曲线2"));
        this.add(chartPanel3=createChartPanel1("曲线3"));
        this.add(chartPanel4=createChartPanel1("曲线4"));
    }

    private WChartPanel createChartPanel1(String title) {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setAutoRangeIncludesZero(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRangeIncludesZero(false);
        XYSplineRenderer renderer1 = new XYSplineRenderer();
        XYPlot plot = new XYPlot(createSampleData(), xAxis, yAxis, renderer1);
        plot.setRangePannable(true);
        plot.setRangeGridlinesVisible(false);
        plot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        ChartUtils.applyCurrentTheme(chart);
        WChartPanel  chartPanel = new WChartPanel(chart);

        XYLineAndShapeRenderer localXYLineAndShapeRenderer = (XYLineAndShapeRenderer) plot.getRenderer();
        localXYLineAndShapeRenderer.setDefaultShapesVisible(false);
        localXYLineAndShapeRenderer.setSeriesPaint(0, new Color(0x53fec0));
        localXYLineAndShapeRenderer.setSeriesPaint(1, new Color(0xFEA815));
        localXYLineAndShapeRenderer.setSeriesPaint(2, new Color(0x53fec0));
        localXYLineAndShapeRenderer.setSeriesPaint(3, new Color(0xFEA815));

        localXYLineAndShapeRenderer.setSeriesStroke(0, new BasicStroke(3f));
        localXYLineAndShapeRenderer.setSeriesStroke(1, new BasicStroke(3f));

        localXYLineAndShapeRenderer.setSeriesStroke(2, new BasicStroke(
                1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                1.0f, new float[] {10.0f, 6.0f}, 0.0f ));
        localXYLineAndShapeRenderer.setSeriesStroke(3, new BasicStroke(
                1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                1.0f, new float[] {10.0f, 6.0f}, 0.0f ));

        return chartPanel;
    }


    private XYDataset createSampleData() {
        XYSeries series = new XYSeries("A");
        series.add(2D, 56.270000000000003D);
        series.add(3D, 41.32D);
        series.add(4D, 31.449999999999999D);
        series.add(5D, 30.050000000000001D);
        series.add(6D, 24.690000000000001D);
        series.add(7D, 19.780000000000001D);
        series.add(8D, 20.940000000000001D);
        series.add(9D, 16.73D);
        series.add(10D, 14.210000000000001D);
        series.add(11D, 12.44D);

        XYSeries series2 = new XYSeries("B");
        series2.add(11D, 56.270000000000003D);
        series2.add(10D, 41.32D);
        series2.add(9D, 31.449999999999999D);
        series2.add(8D, 30.050000000000001D);
        series2.add(7D, 24.690000000000001D);
        series2.add(6D, 19.780000000000001D);
        series2.add(5D, 20.940000000000001D);
        series2.add(4D, 16.73D);
        series2.add(3D, 14.210000000000001D);
        series2.add(2D, 12.44D);

        XYSeries series3 = new XYSeries("C");
        series3.add(11D, 26.270000000000003D);
        series3.add(10D, 31.32D);
        series3.add(9D, 11.449999999999999D);
        series3.add(8D, 20.050000000000001D);
        series3.add(7D, 34.690000000000001D);
        series3.add(6D, 49.780000000000001D);
        series3.add(5D, 30.940000000000001D);
        series3.add(4D, 46.73D);
        series3.add(3D, 54.210000000000001D);
        series3.add(2D, 12.44D);

        XYSeries series4 = new XYSeries("D");
        series4.add(11D, 36.270000000000003D);
        series4.add(10D, 31.32D);
        series4.add(9D, 11.449999999999999D);
        series4.add(8D, 20.050000000000001D);
        series4.add(7D, 14.690000000000001D);
        series4.add(6D, 39.780000000000001D);
        series4.add(5D, 24.940000000000001D);
        series4.add(4D, 16.73D);
        series4.add(3D, 34.210000000000001D);
        series4.add(2D, 22.44D);

        XYSeriesCollection result = new XYSeriesCollection(series);
        result.addSeries(series2);
        result.addSeries(series3);
        result.addSeries(series4);

        return result;
    }



    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (chartPanel1 != null) {
                    chartPanel1.setBorder(new FlatLineBorder(new Insets(10, 10, 10, 10), UIManager.getColor("Component.borderColor"), 1, 8));
                    WChartUtil.createChartUI(chartPanel1.getChart());
                }
                if (chartPanel2 != null) {
                    chartPanel2.setBorder(new FlatLineBorder(new Insets(10, 10, 10, 10), UIManager.getColor("Component.borderColor"), 1, 8));
                    WChartUtil.createChartUI(chartPanel2.getChart());
                }
                if (chartPanel3 != null) {
                    chartPanel3.setBorder(new FlatLineBorder(new Insets(10, 10, 10, 10), UIManager.getColor("Component.borderColor"), 1, 8));
                    WChartUtil.createChartUI(chartPanel3.getChart());
                }
                if (chartPanel4 != null) {
                    chartPanel4.setBorder(new FlatLineBorder(new Insets(10, 10, 10, 10), UIManager.getColor("Component.borderColor"), 1, 8));
                    WChartUtil.createChartUI(chartPanel4.getChart());
                }
            }
        });
        super.updateUI();
    }
}
