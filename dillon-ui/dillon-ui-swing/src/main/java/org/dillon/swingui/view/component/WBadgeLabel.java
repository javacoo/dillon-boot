package org.dillon.swingui.view.component;

import org.dillon.swingui.utils.SysFont;

import javax.swing.*;
import java.awt.*;

/**
 * Created by liwen on 2017/5/31.
 */
public class WBadgeLabel extends JLabel {

    private int arcSize = 7;

    public WBadgeLabel(String text) {
        this(text, 5);
    }

    public WBadgeLabel(String text, int arcSize) {
        super(text, JLabel.CENTER);
        this.arcSize = arcSize;
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setForeground(new Color(0xffffff));
        this.setFont(SysFont.APP_FONT.deriveFont(13f));
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(getBackground());
        g2.fillRoundRect(0, 0, getWidth(), getHeight(), arcSize, arcSize);
        g2.dispose();
        super.paintComponent(g);

    }

    public void setArcSize(int arcSize) {
        this.arcSize = arcSize;
        repaint();
    }
}
