package org.dillon.swingui.xmap.render;


import org.dillon.swingui.xmap.geometry.ShapePoint;
import org.jxmapviewer.JXMapViewer;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * 点渲染器
 */
public class PointRender extends AbstractRenderer<ShapePoint> {

    private BufferedImage image = null;

    /**
     * 点渲染实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param point
     */
    @Override
    protected void doPaint(Graphics2D g, JXMapViewer jxMapViewer, ShapePoint point) {
        if (jxMapViewer == null) {
            return;
        }
        Rectangle rec = jxMapViewer.getViewportBounds();
        this.drawPoint(g, jxMapViewer, point, rec);

    }

    /**
     * 多点渲染实现
     *
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param points
     */
    @Override
    protected void doPaintMany(Graphics2D g, JXMapViewer jxMapViewer, Iterable<? extends ShapePoint> points) {
        if (jxMapViewer == null) {
            return;
        }
        if (points == null) {
            return;
        }

        Rectangle rec = jxMapViewer.getViewportBounds();
        for (ShapePoint point : points) {
            this.drawPoint(g, jxMapViewer, point, rec);
        }

    }

    /**
     * 点绘制
     *
     * @param g
     * @param g           The Graphics2D to render to
     * @param jxMapViewer map
     * @param rectangle
     */
    protected void drawPoint(Graphics2D g, JXMapViewer jxMapViewer, ShapePoint point, Rectangle rectangle) {
        if (point == null)
            return;

        if (image == null) {
            try {
                image = createImage(point);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }


        Point2D point2D = jxMapViewer.getTileFactory().geoToPixel(point.getPosition(), jxMapViewer.getZoom());
        if (rectangle.contains(point2D)) {
            int x = (int) (point2D.getX() - image.getWidth() / 2);
            int y = (int) (point2D.getY() - image.getHeight() / 2);
            g.drawImage(image, x, y, null);
            if (!point.getText().equals("")) {
                g.setColor(point.getColor());
                g.drawString(point.getText(), x + image.getWidth() + 2, y + image.getHeight() + 2);
            }
        }
    }

    protected BufferedImage createImage(ShapePoint point) throws Throwable {
        BufferedImage img = new BufferedImage((int) point.getScale() * 2, (int) point.getScale() * 2, BufferedImage.TRANSLUCENT);
        Graphics2D g2 = img.createGraphics();
        g2.setColor(point.getColor());
        g2.fillRect(0, 0, img.getWidth(), img.getHeight());
        g2.dispose();
        return img;
    }
}
