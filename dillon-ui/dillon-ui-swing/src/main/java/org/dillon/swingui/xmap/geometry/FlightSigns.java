package org.dillon.swingui.xmap.geometry;

import java.util.Date;

public class FlightSigns  {

	private String an; // 机尾号
	private String fi; // 航班号
	private double fob; // 油量
	private double alt; // 高度
	private double cas; // 空速
	private String wd; // 风向
	private double ws; // 风速
	private Date etatime; // 时间

	public FlightSigns(String an, String fi, double fob, double alt,
			double cas, String wd, double ws, Date etatime) {
		super();
		this.an = an;
		this.fi = fi;
		this.fob = fob;
		this.alt = alt;
		this.cas = cas;
		this.wd = wd;
		this.ws = ws;
		this.etatime = etatime;
	}

	public String getAn() {
		return an;
	}

	public void setAn(String an) {
		this.an = an;
	}

	public String getFi() {
		return fi;
	}

	public void setFi(String fi) {
		this.fi = fi;
	}

	public double getFob() {
		return fob;
	}

	public void setFob(double fob) {
		this.fob = fob;
	}

	public double getAlt() {
		return alt;
	}

	public void setAlt(double alt) {
		this.alt = alt;
	}

	public double getCas() {
		return cas;
	}

	public void setCas(double cas) {
		this.cas = cas;
	}

	public String getWd() {
		return wd;
	}

	public void setWd(String wd) {
		this.wd = wd;
	}

	public double getWs() {
		return ws;
	}

	public void setWs(double ws) {
		this.ws = ws;
	}

	public Date getEtatime() {
		return etatime;
	}

	public void setEtatime(Date etatime) {
		this.etatime = etatime;
	}

}
