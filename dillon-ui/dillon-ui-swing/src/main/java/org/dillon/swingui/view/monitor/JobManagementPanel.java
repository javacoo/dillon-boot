package org.dillon.swingui.view.monitor;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.constant.ScheduleConstants;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.job.domain.SysJob;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysJobFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 定时任务管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class JobManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "任务编号", "任务名称", "任务组名", "调用目标", "cron执行表达式", "状态", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;


    private JPanel optPanel;

    private JComboBox jobGComboBox;
    private JTextField nameTextField;
    private JComboBox statusCombo;


    private JTextField jobNameTextField;

    private JComboBox jobGroupCombox;
    private JTextField invokeTargetTextField;
    private JTextField cronTextField;

    private JRadioButton executeBut;
    private JRadioButton executeOneBut;
    private JRadioButton executeGiveUpBut;

    private JRadioButton allowBut;
    private JRadioButton prohibitBut;

    private JRadioButton normalBut;
    private JRadioButton disableBut;


    public JobManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("任务名称"));

        toolBar.add(nameTextField = createTextField("请输入任务名称"));
        nameTextField.setColumns(15);
        toolBar.add(new JLabel("任务组名称"));
        toolBar.add(jobGComboBox = new JComboBox(new String[]{"全部", "默认", "系统"}));
        toolBar.add(new JLabel("任务状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "暂停"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            jobGComboBox.setSelectedIndex(0);
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        OperateInfoPanel operateInfoPanel = new OperateInfoPanel();
        operateInfoPanel.getInfoPanel().add(toolBar);
        JButton addButton = new JButton("新增");
        operateInfoPanel.getOperatePanel().add(addButton);
        addButton.addActionListener(e -> {
            showAddDialog();
        });

        JButton eidtButton = new JButton("修改");
        operateInfoPanel.getOperatePanel().add(eidtButton);
        eidtButton.addActionListener(e -> {
            showEditDialog();
        });

        JButton delButton = new JButton("删除");
        operateInfoPanel.getOperatePanel().add(delButton);
        delButton.addActionListener(e -> {
            delRole(true);
        });

        JButton logButton = new JButton("调度日志");
        operateInfoPanel.getOperatePanel().add(logButton);
        logButton.addActionListener(e -> {
            showSysJobLogTab(new SysJob());
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0 || column == 7 || column == 6) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0 || columnIndex == 6) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);
        table.getColumn("操作").setMinWidth(210);
        table.getColumn("操作").setMaxWidth(210);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(  operateInfoPanel, BorderLayout.NORTH);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout());
        this.add(panel);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);

                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));


                table.getColumn("状态").setCellRenderer(new DefaultTableRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JCheckBox statusCheckBox = new JCheckBox("");
                        statusCheckBox.setIcon(new AnimatedSwitchIcon());
                        statusCheckBox.setSelected((Boolean) value);
                        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
                        statusCheckBox.setBackground(component.getBackground());
                        return statusCheckBox;
                    }
                });
                table.getColumn("状态").setCellEditor(new DefaultCellEditor(createStatus()));
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar(), 3, "admin"));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar(), 3, "admin"));
            }
        });
        super.updateUI();

    }

    private void showSysJobLogTab(SysJob sysJob) {


        if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("调度日志") == -1) {

            MainFrame.getInstance().getMainPane().getTabbedPane().addTab("调度日志",  ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.monitor.JobLogManagementPanel"));
        }
        MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("调度日志"));
        JobLogManagementPanel  jobLogManagementPanel = (JobLogManagementPanel) ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.monitor.JobLogManagementPanel");
        jobLogManagementPanel.queryJobName(sysJob.getJobName());
    }


    private JCheckBox createStatus() {
        JCheckBox statusCheckBox = new JCheckBox();
        statusCheckBox.setIcon(new AnimatedSwitchIcon());
        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        statusCheckBox.setHorizontalTextPosition(SwingConstants.CENTER);
        statusCheckBox.addActionListener(e -> changeStatus());
        return statusCheckBox;

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delRole(false));
        del.setForeground(UIManager.getColor("App.accentColor"));

        JButton more = new JButton("更多");
        more.addActionListener(e -> showPopupMoreButtonActionPerformed(e));
        more.setForeground(UIManager.getColor("App.accentColor"));
        more.setIcon(new FlatSVGIcon("icons/arrow-right.svg", 15, 15));

        optBar.add(edit);
        optBar.add(del);
        optBar.add(more);
        optBar.setPreferredSize(new Dimension(150, 50));
        return optBar;

    }

    private void showPopupMoreButtonActionPerformed(ActionEvent e) {
        Component invoker = (Component) e.getSource();
        JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem menuItem1 = new JMenuItem("执行一次");
        JMenuItem menuItem2 = new JMenuItem("任务详细");
        JMenuItem menuItem3 = new JMenuItem("调度日志");
        menuItem1.addActionListener(e1 -> {
            run();
        });
        menuItem2.addActionListener(e1 -> {
            WOptionPane.showOptionDialog(null, getDetailed(), "任务详细", CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"关闭"}, "关闭");
        });
        menuItem3.addActionListener(e1 -> {
            int selRow = table.getSelectedRow();
            SysJob sysJob = new SysJob();
            if (selRow != -1) {
                sysJob = (SysJob) table.getValueAt(selRow, 7);
            }

            showSysJobLogTab(sysJob);
        });

        popupMenu.add(menuItem1);
        popupMenu.add(menuItem2);
        popupMenu.add(menuItem3);
        popupMenu.show(invoker, 0, invoker.getHeight());

    }


    private void showAddDialog() {
        int opt = WOptionPane.showOptionDialog(null, getOptJobPanel(false), "添加任务", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            add();
        }
    }


    private void showEditDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getOptJobPanel(true), "修改任务", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            edit();
        }
    }


    private void updateData() {
        final SysJob sysJob = new SysJob();
        sysJob.setJobName(nameTextField.getText());
        sysJob.setJobGroup(jobGComboBox.getSelectedIndex() == 0 ? null : jobGComboBox.getSelectedItem().toString());
        int selectIndex = statusCombo.getSelectedIndex();
        sysJob.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysJob);

        SwingWorker<PageResponse<SysJob>, Object> worker = new SwingWorker<PageResponse<SysJob>, Object>() {
            @Override
            protected PageResponse<SysJob> doInBackground() throws Exception {


                return Request.connector(SysJobFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysJob job : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(job.getJobId());
                            rowV.add(job.getJobName());
                            rowV.add(job.getJobGroup());
                            rowV.add(job.getInvokeTarget());
                            rowV.add(job.getCronExpression());
                            rowV.add("0".equals(job.getStatus()) ? true : false);
                            rowV.add(job);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        tableModel.setRowCount(0);
        worker.execute();
    }


    public JPanel getOptJobPanel(Boolean isEidt) {
        optPanel = new JPanel(new BorderLayout());
        JPanel infoPanel = new JPanel(new MigLayout("fill,wrap 2,insets 10", "[right][grow,320::]", "[30!]10[30!]10[30!]10[30!]10[30!]10[30!]10[30!]"));

        jobNameTextField = createTextField("请输入任务名称");
        jobGroupCombox = new JComboBox(new String[]{"默认", "系统"});
        invokeTargetTextField = createTextField("请输入调用目标字符串");
        cronTextField = createTextField("请输入cron表达式");

        executeBut = new JRadioButton("立即执行", true);
        executeOneBut = new JRadioButton("执行一次");
        executeGiveUpBut = new JRadioButton("放弃执行");
        ButtonGroup group1 = new ButtonGroup();
        group1.add(executeBut);
        group1.add(executeOneBut);
        group1.add(executeGiveUpBut);

        allowBut = new JRadioButton("允许", true);
        prohibitBut = new JRadioButton("禁止");
        ButtonGroup group2 = new ButtonGroup();
        group2.add(allowBut);
        group2.add(prohibitBut);

        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("停用");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);


        infoPanel.add(new JLabel("*任务名称"));
        infoPanel.add(jobNameTextField, "growx");

        infoPanel.add(new JLabel("任务分组"));
        infoPanel.add(jobGroupCombox, "growx");

        infoPanel.add(new JLabel("*调用方法 "));
        infoPanel.add(invokeTargetTextField, "growx");

        infoPanel.add(new JLabel("*cron表达式 "));
        infoPanel.add(cronTextField, "growx");

        infoPanel.add(new JLabel("执行策略 "));
        infoPanel.add(executeBut, "split 3");
        infoPanel.add(executeOneBut);
        infoPanel.add(executeGiveUpBut);

        infoPanel.add(new JLabel("是否并发 "));
        infoPanel.add(allowBut, "split 2");
        infoPanel.add(prohibitBut);


        infoPanel.add(new JLabel("状态"));
        infoPanel.add(normalBut, "split 2");
        infoPanel.add(disableBut);

        optPanel.add(infoPanel);

        if (isEidt) {
            getInfo();
        }

        return optPanel;
    }

    public JPanel getDetailed() {
        JPanel infoPanel = new JPanel(new MigLayout("fill,wrap 2,insets 10", "[right]25[grow,280::]", "[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]"));

        JLabel jobId = createLabe();
        JLabel jobName = createLabe();
        JLabel jobGroup = createLabe();
        JLabel invokeTarget = createLabe();
        JLabel cronExpression = createLabe();
        JLabel misfirePolicy = createLabe();
        JLabel concurrent = createLabe();
        JLabel status = createLabe();
        JLabel nextValidTime = createLabe();
        JLabel createTime = createLabe();

        infoPanel.add(new JLabel("任务编号:"));
        infoPanel.add(jobId, "growx");

        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("任务名称:"));
        infoPanel.add(jobName, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("任务分组:"));
        infoPanel.add(jobGroup, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("调用方法:"));
        infoPanel.add(invokeTarget, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("cron表达式:"));
        infoPanel.add(cronExpression, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("执行策略:"));
        infoPanel.add(misfirePolicy, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("是否并发:"));
        infoPanel.add(concurrent, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("状态:"));
        infoPanel.add(status, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("下次执行时间:"));
        infoPanel.add(nextValidTime, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("创建时间:"));
        infoPanel.add(createTime, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");


        int selRow = table.getSelectedRow();
        Long id = null;
        if (selRow != -1) {
            id = (Long) table.getValueAt(selRow, 1);
        }
        Long finalId = id;
        SwingWorker<SysJob, Object> worker = new SwingWorker<SysJob, Object>() {
            @Override
            protected SysJob doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).getInfo(finalId).getData();

            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {

                        jobId.setText(get().getJobId() + "");
                        jobName.setText(get().getJobName());
                        jobGroup.setText(get().getJobGroup());
                        invokeTarget.setText(get().getInvokeTarget());
                        cronExpression.setText(get().getCronExpression());

                        switch (get().getMisfirePolicy()) {
                            case ScheduleConstants.MISFIRE_IGNORE_MISFIRES:
                                misfirePolicy.setText("立即执行");
                                break;
                            case ScheduleConstants.MISFIRE_FIRE_AND_PROCEED:
                                misfirePolicy.setText("执行一次");
                                break;
                            case ScheduleConstants.MISFIRE_DO_NOTHING:
                                misfirePolicy.setText("放弃执行");
                                break;
                            default:
                                misfirePolicy.setText("立即执行");
                        }

                        concurrent.setText("0".equals(get().getConcurrent()) ? "允许" : "禁止");
                        status.setText("0".equals(get().getStatus()) ? "正常" : "暂停");
                        nextValidTime.setText(DateUtil.format(get().getNextValidTime(), "yyyy-MM-dd HH:mm:ss"));
                        createTime.setText(DateUtil.format(get().getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();
        return infoPanel;
    }

    private JLabel createLabe() {
        JLabel label = new JLabel();
        label.setFont(label.getFont().deriveFont(Font.BOLD));

        return label;
    }

    private void updateValue(SysJob sysJob) {

        jobNameTextField.setText(sysJob.getJobName());
        jobGroupCombox.setSelectedItem(sysJob.getJobGroup());
        invokeTargetTextField.setText(sysJob.getInvokeTarget());
        cronTextField.setText(sysJob.getCronExpression());

        executeBut.setSelected(sysJob.getMisfirePolicy().equals(ScheduleConstants.MISFIRE_DEFAULT) || sysJob.getMisfirePolicy().equals(ScheduleConstants.MISFIRE_IGNORE_MISFIRES));
        executeOneBut.setSelected(sysJob.getMisfirePolicy().equals(ScheduleConstants.MISFIRE_FIRE_AND_PROCEED));
        executeGiveUpBut.setSelected(sysJob.getMisfirePolicy().equals(ScheduleConstants.MISFIRE_DO_NOTHING));

        allowBut.setSelected("0".equals(sysJob.getConcurrent()));
        prohibitBut.setSelected("1".equals(sysJob.getConcurrent()));

        normalBut.setSelected("0".equals(sysJob.getStatus()));
        disableBut.setSelected("1".equals(sysJob.getStatus()));

    }

    private SysJob getEidtValue() {
        SysJob sysJob = new SysJob();
        sysJob.setJobName(jobNameTextField.getText());
        sysJob.setJobGroup(jobGroupCombox.getSelectedItem().toString());
        sysJob.setInvokeTarget(invokeTargetTextField.getText());
        sysJob.setCronExpression(cronTextField.getText());

        if (executeBut.isSelected()) {
            sysJob.setMisfirePolicy(ScheduleConstants.MISFIRE_IGNORE_MISFIRES);
        }

        if (executeOneBut.isSelected()) {
            sysJob.setMisfirePolicy(ScheduleConstants.MISFIRE_FIRE_AND_PROCEED);
        }
        if (executeGiveUpBut.isSelected()) {
            sysJob.setMisfirePolicy(ScheduleConstants.MISFIRE_DO_NOTHING);
        }
        sysJob.setConcurrent(allowBut.isSelected() ? "0" : "1");
        sysJob.setStatus(normalBut.isSelected() ? "0" : "1");

        return sysJob;
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加
     */
    private void add() {

        SysJob sysJob = getEidtValue();
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).add(sysJob);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 保存角色
     */
    private void edit() {

        SysJob sysJob = getEidtValue();

        int selRow = table.getSelectedRow();
        if (selRow != -1) {
            Long id = (Long) table.getValueAt(selRow, 1);
            sysJob.setJobId(id);
        } else {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).edit(sysJob);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


    private void delRole(boolean isMore) {
        Long postId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 改变状态
     */
    private void changeStatus() {
        SysJob selJob = (SysJob) table.getValueAt(table.getSelectedRow(), 7);
        Boolean status = (Boolean) table.getValueAt(table.getSelectedRow(), 6);
        int opt = WOptionPane.showConfirmDialog(this, "确定要" + (status ? "启用" : "停用") + "【" + selJob.getJobName() + "】任务吗", "提示", OK_CANCEL_OPTION);
        if (opt != 0) {
            table.setValueAt(!status, table.getSelectedRow(), 6);
            return;
        }
        SysJob sysJob = new SysJob();
        sysJob.setJobId(selJob.getJobId());
        sysJob.setStatus(status ? "0" : "1");
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).changeStatus(sysJob);
            }
        };
        swingWorker.execute();
    }

    private void run() {
        SysJob selJob = (SysJob) table.getValueAt(table.getSelectedRow(), 7);

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).run(selJob);
            }
        };
        swingWorker.execute();
    }

    private void getInfo() {
        int selRow = table.getSelectedRow();
        Long id = null;
        if (selRow != -1) {
            id = (Long) table.getValueAt(selRow, 1);
        }
        Long finalId = id;
        SwingWorker<SysJob, Object> worker = new SwingWorker<SysJob, Object>() {
            @Override
            protected SysJob doInBackground() throws Exception {
                return Request.connector(SysJobFeign.class).getInfo(finalId).getData();

            }


            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        updateValue(get());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
