package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.GeoPosition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2015/12/17.
 */
public class ShapePloyLine extends AbstractGeometry {
    private List<GeoPosition> positions;//线坐标序列
    private ShapeAttributes shapeAttributes = new ShapeAttributes();//线图形属性

    /**
     * 构建线实例
     */
    public ShapePloyLine() {
        this.shapeType = AbstractGeometry.SHAPE_POLYLINE;
    }

    /**
     * 构建线实例
     *
     * @param positions
     */
    public ShapePloyLine(Iterable<? extends GeoPosition> positions) {
        this();
        this.setPositions(positions);
    }

    /**
     * 获取线坐标序列
     *
     * @return
     */
    public List<GeoPosition> getPositions() {
        return positions;
    }

    /**
     * 设置线坐标序列
     *
     * @param positions
     */
    public void setPositions(Iterable<? extends GeoPosition> positions) {

        if (this.positions == null) {
            this.positions = new ArrayList<GeoPosition>();
        }
        this.positions.clear();
        if (positions != null) {
            for (GeoPosition position : positions) {
                this.positions.add(position);
            }
        }
    }

    /**
     * 获取线图形属性
     *
     * @return
     */
    public ShapeAttributes getShapeAttributes() {
        return shapeAttributes;
    }

    /**
     * 设置线图形属性
     *
     * @param shapeAttributes
     */
    public void setShapeAttributes(ShapeAttributes shapeAttributes) {
        this.shapeAttributes = shapeAttributes;
    }


}
