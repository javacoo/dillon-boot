package org.dillon.swingui.xmap.geometry;

import org.jxmapviewer.viewer.DefaultWaypoint;

import java.awt.*;
import java.awt.Point;

/**
 * Created by liwen on 16/1/22.
 */
public class Signs extends DefaultWaypoint {
    private String[] labelText;
    private Color signsLabelColor;//标牌标题颜色
    private Color signsValueColor;//标牌内容颜色
    private Color signsBackground;//标牌背景色
    private Float alpa = 1f;//标牌透明度
    private Font signsFont;//标牌字体颜色
    private boolean signsInfoVisable=false;//标牌标签是否见

    public Signs() {
        defaultStyle();
    }

    private void defaultStyle() {
        signsLabelColor = Color.white;
        signsValueColor = Color.yellow;
        signsBackground = Color.black;
        signsFont=new Font("宋体",Font.PLAIN,12);
    }

    public String[] getLabelText() {
        return labelText;
    }

    public void setLabelText(String[] labelText) {
        this.labelText = labelText;
    }

    public Color getSignsLabelColor() {
        return signsLabelColor;
    }

    public void setSignsLabelColor(Color signsLabelColor) {

        this.signsLabelColor = signsLabelColor;
    }

    public Color getSignsValueColor() {
        return signsValueColor;
    }

    public void setSignsValueColor(Color signsValueColor) {
        this.signsValueColor = signsValueColor;
    }

    public Color getSignsBackground() {
        return signsBackground;
    }

    public void setSignsBackground(Color signsBackground) {
        this.signsBackground = signsBackground;
    }

    public Float getAlpa() {
        return alpa;
    }

    public void setAlpa(Float alpa) {
        this.alpa = alpa;
    }

    public Font getSignsFont() {

        return signsFont;
    }

    public void setSignsFont(Font signsFont) {
        this.signsFont = signsFont;
    }

    public boolean isSignsInfoVisable() {
        return signsInfoVisable;
    }

    public void setSignsInfoVisable(boolean signsInfoVisable) {
        this.signsInfoVisable = signsInfoVisable;
    }
}
