
package org.dillon.swingui.xmap.render;


import org.dillon.swingui.xmap.geometry.SurfaceImage;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoBounds;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * 贴图渲染器
 */
public class SurfaceImageRenderer extends AbstractRenderer<SurfaceImage> {

    /**
     * 渲染器实现
     *
     * @param g            The Graphics2D to render to
     * @param map
     * @param surfaceImage
     */
    @Override
    protected void doPaint(Graphics2D g, JXMapViewer map, SurfaceImage surfaceImage) {
        GeoBounds bounds = surfaceImage.getBounds();
        BufferedImage image = surfaceImage.getImage();
        GeoPosition nw = bounds.getNorthWest();
        GeoPosition se = bounds.getSouthEast();
        Point2D ptNW = map.getTileFactory().geoToPixel(nw, map.getZoom());
        Point2D ptSE = map.getTileFactory().geoToPixel(se, map.getZoom());

        int width = (int) (ptSE.getX() - ptNW.getX());
        int height = (int) (ptNW.getY() - ptSE.getY());
        g.drawImage(image, (int) ptNW.getX(), (int) ptNW.getY(), width, height, null);

        g.dispose();
    }

    /**
     * 渲染多个实现
     *
     * @param g      The Graphics2D to render to
     * @param map
     * @param images
     */
    @Override
    protected void doPaintMany(Graphics2D g, JXMapViewer map, Iterable<? extends SurfaceImage> images) {

        if (images == null) {
            return;
        }

        for (SurfaceImage surfaceImage : images) {
            GeoBounds bounds = surfaceImage.getBounds();
            BufferedImage image = surfaceImage.getImage();
            GeoPosition nw = bounds.getNorthWest();
            GeoPosition se = bounds.getSouthEast();
            Point2D ptNW = map.getTileFactory().geoToPixel(nw, map.getZoom());
            Point2D ptSE = map.getTileFactory().geoToPixel(se, map.getZoom());

            int width = (int) (ptSE.getX() - ptNW.getX());
            int height = (int) (ptSE.getY() - ptNW.getY());
            g.drawImage(image, (int) ptNW.getX(), (int) ptNW.getY(), width, height, null);


        }
        g.dispose();
    }
}
