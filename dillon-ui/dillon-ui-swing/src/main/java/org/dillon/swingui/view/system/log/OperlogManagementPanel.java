package org.dillon.swingui.view.system.log;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysDictDataFeign;
import org.dillon.swingui.request.feignclient.SysOperlogFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysDictData;
import org.dillon.system.api.domain.SysOperLog;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 操作日志面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class OperlogManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "日志编号", "系统模块", "操作类型", "请求方式", "操作人", "操作地址", "操作状态", "操作日期", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;
    private WLocalDateCombo beginTime;
    private WLocalDateCombo endTime;
    private JPanel optPostPanel;
    private JTextField modelTextField;
    private JTextField optPeoField;
    private JComboBox typeCombo;
    private JComboBox statusCombo;

    private JTextField titleLabel;
    private JTextField operUrlLabel;
    private JTextField loginInfoLabel;
    private JTextField requestMethodLabel;
    private JTextField methodLabel;
    private JTextArea operParamArea;
    private JTextField statusLabel;
    private JTextField operTimeLabel;
    private JTextArea jsonResultTextArea;

    private Map<String, SysDictData> operTypeMap = new HashMap();
    private Map<String, SysDictData> statusMap = new HashMap();
    private WaitPane waitPane;

    public OperlogManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        initData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(new JLabel("系统模块"));

        toolBar.add(modelTextField = createPlaceholderTextField("请输入系统模块"));
        modelTextField.setColumns(10);
        toolBar.add(new JLabel("操作人"));
        toolBar.add(optPeoField = createPlaceholderTextField("请输入操作人"));
        optPeoField.setColumns(10);
        toolBar.add(new JLabel("类型"));
        toolBar.add(typeCombo = new JComboBox(new String[]{"全部", "通知", "公告"}));
        toolBar.add(new JLabel("状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "成功", "失败"}));
        toolBar.add(new JLabel("操作时间"));
        toolBar.add(beginTime=new WLocalDateCombo());
        toolBar.add(new JLabel("-"));
        toolBar.add(endTime=new WLocalDateCombo());
        beginTime.setValue(DateUtil.offsetDay(new Date(),-30).toLocalDateTime().toLocalDate());

        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> queryData());
        toolBar.add(searchButton);
        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            modelTextField.setText("");
            optPeoField.setText("");
            typeCombo.setSelectedItem(null);
            statusCombo.setSelectedItem(null);
        });

        OperateInfoPanel operateInfoPanel = new OperateInfoPanel();
        operateInfoPanel.getInfoPanel().add(toolBar);

        JButton emptyButton = new JButton("清空");
        operateInfoPanel.getOperatePanel().add(emptyButton);
        emptyButton.addActionListener(e -> {
            clean();
        });

        JButton delButton = new JButton("删除");
        operateInfoPanel.getOperatePanel().add(delButton);
        delButton.addActionListener(e -> {
            delPost(true);
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 9 || column == 0) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);
        table.getColumn("操作").setMinWidth(80);
        table.getColumn("操作").setMaxWidth(80);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                queryData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(operateInfoPanel, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);
                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
                table.getColumnExt("操作状态").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        Integer status = (Integer) value;
                        JButton button = new JButton(status == 0 ? "成功" : "失败");
                        button.setBackground(status == 0 ? new Color(24, 144, 255, 88) : new Color(245, 108, 108, 41));
                        button.setForeground(status == 0 ? new Color(0x1890ff) : new Color(0xf56c6c));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });
                table.getColumnExt("操作类型").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        String status = operTypeMap.get(value.toString()).getDictLabel();
                        JButton button = new JButton(status);
                        button.setBackground(new Color(24, 144, 255, 88));
                        button.setForeground(new Color(0x1890ff));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("详细");
        edit.setIcon(new FlatSVGIcon("icons/view.svg", 15, 15));
        edit.addActionListener(e -> showDetailedPanelDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));
        optBar.add(edit);
        return optBar;

    }


    private void showDetailedPanelDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getDetailedPanel((SysOperLog) table.getValueAt(selRow, 9)), "操作日志详细", CLOSED_OPTION, PLAIN_MESSAGE, null, new Object[]{"关闭"}, "关闭");

    }


    private void initData() {
        final SysOperLog sysOperLog = new SysOperLog();
        sysOperLog.setTitle(modelTextField.getText());
        sysOperLog.setCreateBy(optPeoField.getText());
        sysOperLog.setStatus(statusCombo.getSelectedIndex() == 0 ? null : (statusCombo.getSelectedIndex() == 0 ? 0 : 1));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysOperLog);

        SwingWorker<Object, Map<String, Object>> worker = new SwingWorker<Object, Map<String, Object>>() {
            @Override
            protected Object doInBackground() throws Exception {
                Map<String, Object> map = new HashMap<>();
                List<SysDictData> operType = Request.connector(SysDictDataFeign.class).dictType("sys_oper_type").getData();
                List<SysDictData> status = Request.connector(SysDictDataFeign.class).dictType("sys_common_status").getData();
                map.put("operTypeMap", operType.stream().collect(Collectors.toMap(SysDictData::getDictValue, dict -> dict)));
                map.put("statusMap", status.stream().collect(Collectors.toMap(SysDictData::getDictValue, dict -> dict)));
                operType.add(0, null);
                status.add(0, null);
                map.put("operType", ArrayUtil.toArray(operType, SysDictData.class));
                map.put("status", ArrayUtil.toArray(status, SysDictData.class));
                publish(map);
                return null;

            }

            @Override
            protected void process(List<Map<String, Object>> chunks) {
                for (Map<String, Object> map : chunks) {
                    SysDictData[] operType = (SysDictData[]) map.get("operType");
                    SysDictData[] status = (SysDictData[]) map.get("status");

                    operTypeMap = (Map) map.get("operTypeMap");
                    statusMap = (Map) map.get("statusMap");
                    typeCombo.setModel(new DefaultComboBoxModel(operType));
                    statusCombo.setModel(new DefaultComboBoxModel(status));
                    typeCombo.setSelectedItem(null);
                    statusCombo.setSelectedItem(null);
                }
            }

            @Override
            protected void done() {
                queryData();
            }
        };
        tableModel.setRowCount(0);
        worker.execute();
    }

    private void queryData() {
        final SysOperLog sysOperLog = new SysOperLog();
        sysOperLog.setTitle(modelTextField.getText());
        sysOperLog.setCreateBy(optPeoField.getText());
        sysOperLog.getParams().put("beginTime", beginTime.getValue());
        sysOperLog.getParams().put("endTime", endTime.getValue());
        Object type = typeCombo.getSelectedItem();
        if (type instanceof SysDictData) {
            sysOperLog.setBusinessType(Integer.valueOf(((SysDictData) type).getDictValue()));
        }

        Object state = statusCombo.getSelectedItem();
        if (state instanceof SysDictData) {
            sysOperLog.setStatus(Integer.valueOf(((SysDictData) state).getDictValue()));
        }


        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysOperLog);

        SwingWorker<PageResponse<SysOperLog>, Object> worker = new SwingWorker<PageResponse<SysOperLog>, Object>() {
            @Override
            protected PageResponse<SysOperLog> doInBackground() throws Exception {


                return Request.connector(SysOperlogFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysOperLog sysOperLog : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(sysOperLog.getOperId());
                            rowV.add(sysOperLog.getTitle());
                            rowV.add(sysOperLog.getBusinessType());
                            rowV.add(sysOperLog.getRequestMethod());
                            rowV.add(sysOperLog.getOperName());
                            rowV.add(sysOperLog.getOperIp());
                            rowV.add(sysOperLog.getStatus());
                            rowV.add(DateUtil.format(sysOperLog.getOperTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(sysOperLog);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };
        tableModel.setRowCount(0);

        worker.execute();
    }


    public JPanel getDetailedPanel(SysOperLog sysOperLog) {
        optPostPanel = new JPanel(new MigLayout("fill,wrap 4,hidemode 3,insets 10", "[right][grow,140::]30[right][grow,140::]", "[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]"));

        titleLabel = createTextField(sysOperLog.getTitle());
        operUrlLabel = createTextField(sysOperLog.getOperUrl());
        loginInfoLabel = createTextField(sysOperLog.getOperName());
        requestMethodLabel = createTextField(sysOperLog.getRequestMethod());
        methodLabel = createTextField(sysOperLog.getMethod());
        operParamArea = new JTextArea(sysOperLog.getOperParam());
        operParamArea.setEditable(false);
        operParamArea.setLineWrap(true);

        jsonResultTextArea = new JTextArea(sysOperLog.getJsonResult());
        jsonResultTextArea.setEditable(false);
        jsonResultTextArea.setLineWrap(true);
        statusLabel = createTextField(sysOperLog.getStatus() == 0 ? "正常" : "失败");
        operTimeLabel = createTextField(DateUtil.format(sysOperLog.getOperTime(), "yyyy-MM-dd HH:mm:ss"));

        optPostPanel.add(new JLabel("操作模块："));
        optPostPanel.add(titleLabel, "growx");
        optPostPanel.add(new JLabel("请求地址："));
        optPostPanel.add(operUrlLabel, "growx");

        optPostPanel.add(new JLabel("登录信息："));
        optPostPanel.add(loginInfoLabel, "growx");
        optPostPanel.add(new JLabel("请求方式："));
        optPostPanel.add(requestMethodLabel, "growx");


        optPostPanel.add(new JLabel("操作方法："));
        optPostPanel.add(methodLabel, "growx,span 3");

        optPostPanel.add(new JLabel("请求参数："));
        optPostPanel.add(new JScrollPane(operParamArea), "growx,span 3,h 180!,w 450");

        optPostPanel.add(new JLabel("返回参数："));
        optPostPanel.add(new JScrollPane(jsonResultTextArea), "growx,span 3,h 100!,w 450");

        optPostPanel.add(new JLabel("操作状态："));
        optPostPanel.add(statusLabel, "growx");
        optPostPanel.add(new JLabel("操作时间："));
        optPostPanel.add(operTimeLabel, "growx");

        return optPostPanel;
    }


    private JTextField createPlaceholderTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    private JTextField createTextField(String text) {
        JTextField textField = new JTextField(text);
        textField.setEditable(false);
        return textField;
    }


    private void clean() {

        int opt = WOptionPane.showOptionDialog(this, "确定要清空所有日志数据吗", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysOperlogFeign.class).clean();
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        queryData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delPost(boolean isMore) {
        Long sysOperLogId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalPostId = sysOperLogId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysOperlogFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        queryData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            initData();
        }
    }
}
