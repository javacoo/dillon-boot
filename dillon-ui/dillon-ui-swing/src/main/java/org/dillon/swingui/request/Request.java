package org.dillon.swingui.request;

import org.dillon.common.core.utils.SpringUtils;

public class Request {

    public static <T> T connector(Class<T> clz){
        return SpringUtils.getBean(clz);
    }
}
