package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.DefaultWaypoint;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class FlightWayPoint extends DefaultWaypoint {

    private String name; // 飞机名称
    private Color flightColor; // 飞机颜色
    private boolean flicker; // 飞机闪烁
    private Signs signs; // 飞机标牌
    private String text; // 标牌展示内容
    private boolean signsVisable;// 标牌是否可见
    private boolean signsInfoVisable;//标牌标签是否见
    private boolean signsAlhpa; // 标牌透明度
    private Color signsBackground;// 标牌背景色
    private Color signsForeground;// 标牌前景色
    private List<TailPoint> tailPoints = new ArrayList<TailPoint>();// 飞机拖尾点
    private boolean tailVisabel;//拖尾是否可见
    private int tailMaxNum; // 拖尾点最大上限
    private Color tailColor; // 拖尾点颜色
    private int tailSize; // 拖尾点大小
    private String datagramId;
    private String rcvAddress;
    private String sndAddress;
    private String smi;
    private String rgs;
    private String msg;
    private String an;// 机尾号
    private String fi;// 航班号
    private String fob;// 油量(KG)
    private Double alt;// 高度(M)
    private String lat;// 纬度
    private String lon;// 经度
    private boolean reality;// 是否为真实报
    private String source;// 报文来源
    private String gatewayTime;// 报文发送时间网关时间
    private String etaTime;
    private Double direction;
    private String cas;// 飞机空速
    private String wd;// 风向
    private String ws;// 风速
    private String tkoField;
    private String desField;
    private boolean select; // 是否被选中
    private String reportTime;
    private String clientTime;
    private Properties pros;
    private GeoPosition cruisingRange;//巡航范围
    private boolean isCruisingVisble;//是否显示巡航范围
    private boolean isFocus;//是否显示焦点飞机
    private boolean isVisable = true;
    private boolean isTrack;//是否跟踪
    private boolean isShowProfile;//是否显示飞行剖面
    private boolean isShowPlaneHistory;//是否显示历史航迹
    /*
     * 飞行状态
     * DataKeyConst.FLIGHT_STATUS_XXXXX：0未知1地面2上升3平飞4降落
     */
    private String flightStatus;

    public FlightWayPoint() {

    }


    //添加拖尾点
    public synchronized void addTail(TailPoint geopostion) {

        if (getTailPoints().size() > tailMaxNum) {
            getTailPoints().remove(0);
        }
        getTailPoints().add(geopostion);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getFlightColor() {
        return flightColor;
    }

    public void setFlightColor(Color flightColor) {
        this.flightColor = flightColor;
    }


    public boolean isFlicker() {
        return flicker;
    }

    public void setFlicker(boolean flicker) {
        this.flicker = flicker;
    }

    public Signs getSigns() {
        return signs;
    }

    public void setSigns(Signs signs) {
        this.signs = signs;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSignsVisable() {
        return signsVisable;
    }

    public void setSignsVisable(boolean signsVisable) {
        this.signsVisable = signsVisable;
    }

    public boolean isSignsAlhpa() {
        return signsAlhpa;
    }

    public void setSignsAlhpa(boolean signsAlhpa) {
        this.signsAlhpa = signsAlhpa;
    }

    public Color getSignsBackground() {
        return signsBackground;
    }

    public void setSignsBackground(Color signsBackground) {
        this.signsBackground = signsBackground;
    }

    public Color getSignsForeground() {
        return signsForeground;
    }

    public void setSignsForeground(Color signsForeground) {
        this.signsForeground = signsForeground;
    }

    public List<TailPoint> getTailPoints() {

        return tailPoints;
    }


    public void setTailMaxNum(int tailMaxNum) {
        if (this.tailMaxNum == tailMaxNum) return;

        int rmNum = getTailPoints().size() - tailMaxNum;
        if (rmNum > 0) {

            List<TailPoint> rmGpList = new ArrayList<TailPoint>();
            for (int i = 0; i < rmNum; i++) {
                rmGpList.add(tailPoints.get(i));
            }
            getTailPoints().removeAll(rmGpList);
        }
        this.tailMaxNum = tailMaxNum;
    }

    public Color getTailColor() {
        return tailColor;
    }

    public void setTailColor(Color tailColor) {
        this.tailColor = tailColor;
    }

    public int getTailSize() {
        return tailSize;
    }

    public void setTailSize(int tailSize) {
        this.tailSize = tailSize;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getDatagramId() {
        return datagramId;
    }

    public void setDatagramId(String datagramId) {
        this.datagramId = datagramId;
    }

    public String getRcvAddress() {
        return rcvAddress;
    }

    public void setRcvAddress(String rcvAddress) {
        this.rcvAddress = rcvAddress;
    }

    public String getSndAddress() {
        return sndAddress;
    }

    public void setSndAddress(String sndAddress) {
        this.sndAddress = sndAddress;
    }

    public String getSmi() {
        return smi;
    }

    public void setSmi(String smi) {
        this.smi = smi;
    }

    public String getRgs() {
        return rgs;
    }

    public void setRgs(String rgs) {
        this.rgs = rgs;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAn() {
        return an;
    }

    public void setAn(String an) {
        this.an = an;
    }

    public String getFi() {
        return fi;
    }

    public void setFi(String fi) {
        this.fi = fi;
    }

    public String getFob() {
        return fob;
    }

    public void setFob(String fob) {
        this.fob = fob;
    }

    public Double getAlt() {
        return alt;
    }

    public void setAlt(Double alt) {
        this.alt = alt;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public boolean isReality() {
        return reality;
    }

    public void setReality(boolean reality) {
        this.reality = reality;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getGatewayTime() {
        return gatewayTime;
    }

    public void setGatewayTime(String gatewayTime) {
        this.gatewayTime = gatewayTime;
    }

    public String getEtaTime() {
        return etaTime;
    }

    public void setEtaTime(String etaTime) {
        this.etaTime = etaTime;
    }

    public Double getDirection() {
        return direction;
    }

    public void setDirection(Double direction) {
        this.direction = direction;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }

    public String getWs() {
        return ws;
    }

    public void setWs(String ws) {
        this.ws = ws;
    }

    public String getTkoField() {
        return tkoField;
    }

    public void setTkoField(String tkoField) {
        this.tkoField = tkoField;
    }

    public String getDesField() {
        return desField;
    }

    public void setDesField(String desField) {
        this.desField = desField;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getClientTime() {
        return clientTime;
    }

    public void setClientTime(String clientTime) {
        this.clientTime = clientTime;
    }

    public boolean isTailVisabel() {
        return tailVisabel;
    }

    public void setTailVisabel(boolean tailVisabel) {
        this.tailVisabel = tailVisabel;
    }

    public Properties getPros() {
        return pros;
    }

    public void setPros(Properties pros) {
        this.pros = pros;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public boolean isCruisingVisble() {
        return isCruisingVisble;
    }

    public boolean isVisable() {
        return isVisable;
    }

    public void setVisable(boolean visable) {
        isVisable = visable;
    }

    public void setCruisingVisble(boolean cruisingVisble) {
        isCruisingVisble = cruisingVisble;
    }

    public GeoPosition getCruisingRange() {
        return cruisingRange;
    }

    public void setCruisingRange(GeoPosition cruisingRange) {
        this.cruisingRange = cruisingRange;
    }

    public boolean isFocus() {
        return isFocus;
    }

    public void setFocus(boolean focus) {
        isFocus = focus;
    }

    public boolean isTrack() {
        return isTrack;
    }

    public void setTrack(boolean track) {
        isTrack = track;
    }

    public boolean isShowProfile() {
        return isShowProfile;
    }

    public void setShowProfile(boolean showProfile) {
        isShowProfile = showProfile;
    }

    public boolean isShowPlaneHistory() {
        return isShowPlaneHistory;
    }

    public void setShowPlaneHistory(boolean showPlaneHistory) {
        isShowPlaneHistory = showPlaneHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlightWayPoint that = (FlightWayPoint) o;

        if (an != null ? !an.equals(that.an) : that.an != null) return false;
        return fi != null ? fi.equals(that.fi) : that.fi == null;

    }

    @Override
    public int hashCode() {
        int result = an != null ? an.hashCode() : 0;
        result = 31 * result + (fi != null ? fi.hashCode() : 0);
        return result;
    }
}
