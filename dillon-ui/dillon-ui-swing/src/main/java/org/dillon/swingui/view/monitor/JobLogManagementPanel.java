package org.dillon.swingui.view.monitor;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.job.domain.SysJobLog;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysJobLogFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.swingui.view.system.dict.DictDataManagementPanel;
import org.dillon.system.api.domain.SysDictType;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.time.LocalDate;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 定时任务日志管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class JobLogManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "日志编号", "任务名称", "任务组名", "调用目标", "日志信息", "执行状态", "执行时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;
    private WPaginationPane wPaginationPane;

    private JComboBox jobGComboBox;
    private JTextField nameTextField;
    private JComboBox statusCombo;

    private WLocalDateCombo beginTime;
    private WLocalDateCombo endTime;


    public JobLogManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("任务名称"));

        toolBar.add(nameTextField = createTextField("请输入任务名称"));
        nameTextField.setColumns(15);
        toolBar.add(new JLabel("任务组名称"));
        toolBar.add(jobGComboBox = new JComboBox(new String[]{"全部", "默认", "系统"}));
        toolBar.add(new JLabel("任务状态"));
        toolBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "暂停"}));

        toolBar.add(new JLabel("执行时间"));
        toolBar.add(beginTime=new WLocalDateCombo());
        toolBar.add(new JLabel("-"));
        toolBar.add(endTime=new WLocalDateCombo());

        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            jobGComboBox.setSelectedIndex(0);
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);


        OperateInfoPanel operateInfoPanel = new OperateInfoPanel();

        operateInfoPanel.getInfoPanel().add(toolBar);
        JButton delButton = new JButton("删除");
        operateInfoPanel.getOperatePanel().add(delButton);
        delButton.addActionListener(e -> {
            del();
        });

        JButton emptyButton = new JButton("清空");
        operateInfoPanel.getOperatePanel().add(emptyButton);
        emptyButton.addActionListener(e -> {
            clean();
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 8 || column == 0) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);
        table.getColumn("操作").setMinWidth(80);
        table.getColumn("操作").setMaxWidth(80);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(operateInfoPanel, BorderLayout.NORTH);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout());
        this.add(panel);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);

                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));


                table.getColumn("执行状态").setCellRenderer(new DefaultTableRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JCheckBox statusCheckBox = new JCheckBox("");
                        statusCheckBox.setIcon(new AnimatedSwitchIcon());
                        statusCheckBox.setSelected((Boolean) value);
                        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
                        statusCheckBox.setBackground(component.getBackground());
                        return statusCheckBox;
                    }
                });
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }


    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton detailed = new JButton("详细");
        detailed.setIcon(new FlatSVGIcon("icons/view.svg", 15, 15));

        detailed.addActionListener(e -> showDetailedDialog());
        detailed.setForeground(UIManager.getColor("App.accentColor"));

        optBar.add(detailed);
        optBar.setPreferredSize(new Dimension(80, 50));
        return optBar;

    }


    private void showDetailedDialog() {
        int opt = WOptionPane.showOptionDialog(null, getDetailed(), "调度日志详细", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"关闭"}, "关闭");

    }


    private void updateData() {
        final SysJobLog sysJob = new SysJobLog();
        sysJob.getParams().put("beginTime", beginTime.getValue());
        sysJob.getParams().put("endTime", endTime.getValue());
        sysJob.setJobName(nameTextField.getText());
        sysJob.setJobGroup(jobGComboBox.getSelectedIndex() == 0 ? null : jobGComboBox.getSelectedItem().toString());
        int selectIndex = statusCombo.getSelectedIndex();
        sysJob.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysJob);

        SwingWorker<PageResponse<SysJobLog>, Object> worker = new SwingWorker<PageResponse<SysJobLog>, Object>() {
            @Override
            protected PageResponse<SysJobLog> doInBackground() throws Exception {

                return Request.connector(SysJobLogFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysJobLog job : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(job.getJobLogId());
                            rowV.add(job.getJobName());
                            rowV.add(job.getJobGroup());
                            rowV.add(job.getInvokeTarget());
                            rowV.add(job.getJobMessage());
                            rowV.add("0".equals(job.getStatus()) ? true : false);
                            rowV.add(DateUtil.format(job.getCreateTime(),"yyyy-MM-dd HH:mm:ss"));
                            rowV.add(job);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        tableModel.setRowCount(0);
        worker.execute();
    }


    public JPanel getDetailed() {
        JPanel infoPanel = new JPanel(new MigLayout("fill,wrap 2,insets 10", "[right]25[grow,280::]", "[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]10[30!]0[]"));

        JLabel jobId = createLabe();
        JLabel jobName = createLabe();
        JLabel jobGroup = createLabe();
        JLabel invokeTarget = createLabe();
        JLabel jobMessage = createLabe();
        JLabel status = createLabe();
        JLabel startTime = createLabe();
        JLabel exceptionInfo = createLabe();

        infoPanel.add(new JLabel("日志编号:"));
        infoPanel.add(jobId, "growx");

        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("任务名称:"));
        infoPanel.add(jobName, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("任务分组:"));
        infoPanel.add(jobGroup, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("执行时间:"));
        infoPanel.add(startTime, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("调用方法:"));
        infoPanel.add(invokeTarget, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("日志信息:"));
        infoPanel.add(jobMessage, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        infoPanel.add(new JLabel("执行状态:"));
        infoPanel.add(status, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");


        infoPanel.add(new JLabel("异常信息:"));
        infoPanel.add(exceptionInfo, "growx");
        infoPanel.add(new JSeparator(), "span 2,growx");

        int selRow = table.getSelectedRow();
        Long id = null;
        if (selRow != -1) {
            id = (Long) table.getValueAt(selRow, 1);
        }
        Long finalId = id;
        SwingWorker<SysJobLog, Object> worker = new SwingWorker<SysJobLog, Object>() {
            @Override
            protected SysJobLog doInBackground() throws Exception {
                return Request.connector(SysJobLogFeign.class).getInfo(finalId).getData();

            }

            @Override
            protected void done() {
                try {
                    if (get() != null) {

                        jobId.setText(get().getJobLogId() + "");
                        jobName.setText(get().getJobName());
                        jobGroup.setText(get().getJobGroup());
                        invokeTarget.setText(get().getInvokeTarget());
                        status.setText("0".equals(get().getStatus()) ? "正常" : "暂停");
                        startTime.setText(DateUtil.format(get().getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                        jobMessage.setText(get().getJobMessage());
                        exceptionInfo.setText(get().getExceptionInfo());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();
        return infoPanel;
    }

    private JLabel createLabe() {
        JLabel label = new JLabel();
        label.setFont(label.getFont().deriveFont(Font.BOLD));

        return label;
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    private void del() {
        Long postId = null;

        List<Long> ids = new ArrayList<>();


        for (int i = 0; i < tableModel.getRowCount(); i++) {
            Boolean b = (Boolean) tableModel.getValueAt(i, 0);
            long id = (long) tableModel.getValueAt(i, 1);
            if (b) {
                ids.add(id);
            }
        }

        if (ids.isEmpty()) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
            return;
        }


        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobLogFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void clean() {
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysJobLogFeign.class).clean();
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();
    }

    public void queryJobName(String jobName) {
        nameTextField.setText(jobName);
        updateData();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
