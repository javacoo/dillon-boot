package org.dillon.swingui.view;

import com.formdev.flatlaf.extras.FlatSVGIcon;
import org.dillon.swingui.store.ApplicatonStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @className: InforPane
 * @author: liwen
 * @date: 2022/4/5 10:48
 */
public class InforPane extends JPanel {
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss EEEE");
    final Calendar calendar = Calendar.getInstance();
    private ButtonGroup buttonGroup = new ButtonGroup();
    private JToolBar inforBar;
    private JToolBar buttonBar;
    private JButton serverTimeButton;
    private JToggleButton fullScreen;

    public InforPane() {
        initComponents();
        initTiemr();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout(10, 15));
        this.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        this.setOpaque(false);
        this.add(getInforBar(), BorderLayout.NORTH);
        this.add(getButtonBar(), BorderLayout.CENTER);
    }

    public JToolBar getButtonBar() {
        if (buttonBar == null) {
            buttonBar = new JToolBar();
            buttonBar.setOpaque(false);
            buttonBar.setFloatable(false);
            buttonBar.add(Box.createHorizontalGlue());
            buttonBar.add(createButton("实时监视", "icons/cq-shuangzhoutu.svg"));
            buttonBar.add(createButton("滚动监视", "icons/cq-meiguitu.svg"));
            buttonBar.add(createButton("实时控制", "icons/cq-paihang.svg"));
            buttonBar.add(createButton("指标计算", "icons/cq-relituquyu.svg"));
            buttonBar.add(createButton("系统维护", "icons/cq-tiaoxingtu.svg"));
            buttonBar.add(createButton("历史查询", "icons/cq-yujuetu.svg"));
            buttonBar.add(createButton("运行信息", "icons/cq-zhuzhungtu.svg"));
            buttonBar.add(Box.createHorizontalGlue());
        }
        return buttonBar;
    }

    private JToggleButton createButton(String text, String icon) {
        JToggleButton button = new JToggleButton(text, new FlatSVGIcon(icon, 32, 32));
        buttonGroup.add(button);
        button.setFont(button.getFont().deriveFont(Font.BOLD));
        button.setPreferredSize(new Dimension(120, 40));
        button.setForeground(new Color(0x66FFFF));
        button.putClientProperty("JButton.buttonType", "roundRect");
        return button;
    }

    public JToolBar getInforBar() {
        if (inforBar == null) {
            inforBar = new JToolBar();
            inforBar.setOpaque(false);
            inforBar.setFloatable(false);
            inforBar.add(getServerTimeButton());
            inforBar.add(Box.createHorizontalGlue());
            JButton userButton = new JButton("admin", new FlatSVGIcon("icons/user-filling.svg", 30, 30));
            userButton.addActionListener(e -> showPopupMenuButtonActionPerformed(e));

            JButton reButton = new JButton(new FlatSVGIcon("icons/shuaxin.svg", 30, 30));
            reButton.setToolTipText("刷新");
            reButton.addActionListener(e -> ApplicatonStore.getRefreshObservable().notify("Refresh"));

            inforBar.add(reButton);
            inforBar.add(new JButton("AGC对比", new FlatSVGIcon("icons/fsux_tubiao_duibitu.svg", 30, 30)));
            inforBar.add(userButton);
            inforBar.add(fullScreen = new JToggleButton(new FlatSVGIcon("icons/fullscreen.svg", 30, 30)));
            fullScreen.addActionListener(e -> fullScreenChanged());
        }
        return inforBar;
    }

    private void fullScreenChanged() {

        GraphicsDevice gd = getGraphicsConfiguration().getDevice();
        fullScreen.setIcon(!fullScreen.isSelected() ? new FlatSVGIcon("icons/fullscreen.svg", 25, 25) : new FlatSVGIcon("icons/fullscreen-exit.svg", 25, 25));
        gd.setFullScreenWindow(fullScreen.isSelected() ? SwingUtilities.windowForComponent(this) : null);

    }

    private void initTiemr() {


        Timer timer = new Timer(1000, new AbstractAction() {
            int i = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (i == 60) {
                    i = 0;
//                    updateServerDate();
                }
                calendar.add(Calendar.SECOND, 1);
                getServerTimeButton().setText(formatter.format(calendar.getTime()));
                i++;
            }
        });
        timer.start();
    }

    public JButton getServerTimeButton() {
        if (serverTimeButton == null) {
            serverTimeButton = new JButton(new FlatSVGIcon("icons/time.svg", 20, 20));
            serverTimeButton.setToolTipText("当前服务器时间");
        }
        return serverTimeButton;
    }

    private void showPopupMenuButtonActionPerformed(ActionEvent e) {
        Component invoker = (Component) e.getSource();
        JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem menuItem9 = new JMenuItem("个人信息");
        JMenuItem menuItem10 = new JMenuItem("用户设置");
        JMenuItem menuItem11 = new JMenuItem("退出");
        menuItem11.addActionListener(e1->{
            MainFrame.getInstance().showLogin();
        });

        popupMenu.add(menuItem9);
        popupMenu.add(menuItem10);
        popupMenu.addSeparator();
        popupMenu.add(menuItem11);
        popupMenu.show(invoker, 0, invoker.getHeight());

    }

}
