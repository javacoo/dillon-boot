package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.utils.poi.ExcelUtil;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.common.core.web.page.TableDataInfo;
import org.dillon.job.domain.SysJob;
import org.dillon.job.domain.SysJobLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 系统用户假装
 *
 * @author liwen
 * @date 2022/07/01
 */
@FeignClient(name = "sysJobLogFeign", url = "${server.url}", path = "/schedule")
public interface SysJobLogFeign {

    /**
     * 查询定时任务调度日志列表
     */
    @PostMapping("/job/log/list")
    PageResponse<SysJobLog> list(PageInfo<SysJobLog> pageQuery);

    /**
     * 导出定时任务调度日志列表
     */
    @PostMapping("/job/log/export")
    void export(SysJobLog sysJobLog);

    /**
     * 根据调度编号获取详细信息
     */
    @GetMapping(value = "/job/log/{jobLogId}")
    SingleResponse<SysJobLog> getInfo(@PathVariable("jobLogId") Long jobLogId);

    /**
     * 删除定时任务调度日志
     */
    @DeleteMapping("/job/log/{jobLogIds}")
    OptResult remove(@PathVariable("jobLogIds") Long[] jobLogIds);

    /**
     * 清空定时任务调度日志
     */
    @DeleteMapping("/job/log/clean")
    OptResult clean();

}
