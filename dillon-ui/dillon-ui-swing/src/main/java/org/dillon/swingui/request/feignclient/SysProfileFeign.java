package org.dillon.swingui.request.feignclient;

import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.domain.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 系统概要假装
 *
 * @author liwen
 * @date 2022/07/08
 */
@FeignClient(name = "sysProfileFeign", url = "${server.url}", path = "/system")
public interface SysProfileFeign {

    /**
     * 配置文件
     *
     * @return {@link OptResult}
     */
    @GetMapping("/user/profile")
    SingleResponse<UserInfo> profile();

    /**
     * 更新配置文件
     *
     * @param user 用户
     * @return {@link OptResult}
     */
    @PutMapping("/user/profile")
    OptResult updateProfile( SysUser user);


    /**
     * 更新pwd
     *
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return {@link OptResult}
     */
    @PutMapping("/user/profile/updatePwd")
    OptResult updatePwd(@RequestParam("oldPassword")String oldPassword, @RequestParam("newPassword")String newPassword);

}
