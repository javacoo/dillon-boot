package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.GeoPosition;

/**
 * Created by liwen on 16/2/2.
 */
public class PointBean {

    private GeoPosition position;
    private String text;
    private String name;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoPosition getPosition() {

        return position;
    }

    public void setPosition(GeoPosition position) {
        this.position = position;
    }
}
