package org.dillon.swingui.view.system.role;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.icons.FlatClearIcon;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.CheckHeaderCellRenderer;
import org.dillon.swing.WOptionPane;
import org.dillon.swing.WPaginationPane;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysRoleFeign;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysUser;
import org.dillon.system.api.domain.SysUserRole;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 分配用户面板
 *
 * @author liwen
 * @date 2022/07/17
 */
public class AssignUsersPanel extends JPanel {
    private final static String[] COLUMN_ID = {"", "用户名称", "用户昵称", "邮箱", "手机", "状态", "创建时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;


    private JTextField phoneTextField;
    private JTextField nameTextField;


    private Long roleId;

    public AssignUsersPanel() {
        initComponents();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("用户名称"));

        toolBar.add(nameTextField = createTextField("请输入用户名称"));
        nameTextField.setColumns(20);
        toolBar.add(new JLabel("手机号码"));
        toolBar.add(phoneTextField = createTextField("请输入手机号码"));
        phoneTextField.setColumns(20);


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            phoneTextField.setText("");
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        JButton addUserButton = new JButton("添加用户");
        toolBar.add(addUserButton);
        addUserButton.addActionListener(e -> {
            showAssignUsersAddDialog();
        });


        JButton delButton = new JButton("批量取消授权");
        toolBar.add(delButton);
        delButton.addActionListener(e -> {
            cancelAuthUserAll();
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 7 || column == 0) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(toolBar, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);

                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));

                table.getColumnExt("状态").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        JButton button = new JButton("0".equals(value) ? "正常" : "停用");
                        button.setBackground("0".equals(value) ? new Color(24, 144, 255, 88) : new Color(245, 108, 108, 41));
                        button.setForeground("0".equals(value) ? new Color(0x1890ff) : new Color(0xf56c6c));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());

        JButton del = new JButton("取消授权");
        del.setIcon(new FlatClearIcon());
        del.addActionListener(e -> authUserCancel());
        del.setForeground(UIManager.getColor("App.accentColor"));
        optBar.add(del);
        return optBar;

    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
        updateData();
    }

    private void showAssignUsersAddDialog() {
        AssignUsersAddPanel assignUsersAddPanel = new AssignUsersAddPanel(this);
        assignUsersAddPanel.setPreferredSize(new Dimension(900, 500));
        int opt = WOptionPane.showOptionDialog(null, assignUsersAddPanel, "添加部门", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            assignUsersAddPanel.addAuthUserAll();
        }
    }


    public void updateData() {
        final SysUser sysUser = new SysUser();
        sysUser.setUserName(nameTextField.getText());
        sysUser.setPhonenumber(phoneTextField.getText());
        sysUser.setRoleId(getRoleId());

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysUser);

        SwingWorker<PageResponse<SysUser>, Object> worker = new SwingWorker<PageResponse<SysUser>, Object>() {
            @Override
            protected PageResponse<SysUser> doInBackground() throws Exception {


                return Request.connector(SysRoleFeign.class).allocatedList(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysUser user : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(user.getUserName());
                            rowV.add(user.getNickName());
                            rowV.add(user.getEmail());
                            rowV.add(user.getPhonenumber());
                            rowV.add(user.getStatus());
                            rowV.add(DateUtil.format(user.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(user);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
        };

        tableModel.setRowCount(0);
        worker.execute();
    }


    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    private void cancelAuthUserAll() {

        List<Long> ids = new ArrayList<>();

        for (int i = 0; i < tableModel.getRowCount(); i++) {
            Boolean b = (Boolean) tableModel.getValueAt(i, 0);

            if (b) {
                SysUser sysUser = (SysUser) tableModel.getValueAt(table.getSelectedRow(), 7);
                ids.add(sysUser.getUserId());
            }
        }

        if (ids.isEmpty()) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(this, "是否取消选中用户授权数据项？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).cancelAuthUserAll(getRoleId(), Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void authUserCancel() {

        SysUser sysUser = (SysUser) tableModel.getValueAt(table.getSelectedRow(), 7);

        int opt = WOptionPane.showOptionDialog(this, "确认要取消该用户[" + sysUser.getUserName() + "]角色吗？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(sysUser.getUserId());
        sysUserRole.setRoleId(getRoleId());
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysRoleFeign.class).cancelAuthUser(sysUserRole);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


}