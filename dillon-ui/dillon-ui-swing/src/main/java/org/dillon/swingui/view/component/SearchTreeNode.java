package org.dillon.swingui.view.component;

import org.dillon.common.core.utils.PinYinUtils;

import javax.swing.tree.DefaultMutableTreeNode;

public class SearchTreeNode extends DefaultMutableTreeNode {
    private String id;
    private String name;

    public SearchTreeNode(String id, String name) {
        this.id = PinYinUtils.capitalizeLetter(id);
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id;
    }
}