package org.dillon.swingui.xmap.painters;

import org.dillon.swingui.utils.IconLoader;
import org.dillon.swingui.xmap.geometry.ShapeAttributes;
import org.dillon.swingui.xmap.geometry.ShapePloyLine;
import org.dillon.swingui.xmap.geometry.ShapePoint;
import org.dillon.swingui.xmap.geometry.ShapePolygon;
import org.dillon.swingui.xmap.render.PointRender;
import org.dillon.swingui.xmap.render.PolygonRenderer;
import org.dillon.swingui.xmap.render.PolylineRenderer;
import org.dillon.swingui.xmap.render.Renderer;
import org.geotools.data.geojson.GeoJSONDataStore;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.AbstractPainter;
import org.jxmapviewer.viewer.GeoPosition;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.WKTReader;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class FeatureLayer extends AbstractPainter<JXMapViewer> {
    protected GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
    protected List<ShapePolygon> polygons;
    protected List<ShapePoint> points;
    protected List<ShapePloyLine> lines;
    protected List<GeoPosition> track = new ArrayList<GeoPosition>();
    protected Renderer polygonRenderer;
    protected Renderer pointRenderer;
    protected Renderer polyLineRenderer;
    private BufferedImage backgroundImage = null;
    private JXMapViewer mapViewer;

    public FeatureLayer(JXMapViewer mapViewer) {
        this.mapViewer = mapViewer;
        this.polygonRenderer = new PolygonRenderer();
        this.polyLineRenderer = new PolylineRenderer();
        this.pointRenderer = new PointRender();
    }

    /**
     * 添加一个shp资源
     *
     * @param url shp资源
     */

    public void addShpFromSource(URL url) throws Exception {

        if (url == null)
            return;
        GeoJSONDataStore store;
        SimpleFeatureSource featureSource;
        store = new GeoJSONDataStore(url);
        featureSource = store.getFeatureSource();

        FeatureCollection<SimpleFeatureType, SimpleFeature> collection = featureSource.getFeatures();
        FeatureIterator<SimpleFeature> features = collection.features();
        while (features.hasNext()) {
            SimpleFeature feature = features.next();
            ShapeAttributes attributes = new ShapeAttributes();
            attributes.setOutlineColor(new Color(0x029C60));
            attributes.setDrawOutline(true);
            attributes.setOutlineWidth(2f);
            attributes.setDrawInterior(true);
            attributes.setInteriorColor(new Color(0x029C60));
            attributes.setInteriorOpacity(.2f);
            attributes.setSelectInteriorColor(new Color(0x6C02F396, true));
            attributes.setText(feature.getAttribute("name").toString());
            ArrayList coordinates = (ArrayList) feature.getAttribute("center");
            attributes.setCenterPosition(new GeoPosition((Double) coordinates.get(1), (Double) coordinates.get(0)));
            WKTReader reader = new WKTReader(geometryFactory);
            Geometry geome = reader.read(feature.getDefaultGeometryProperty().getValue().toString());
            if (geome != null)
                addGeometry(geome, attributes);
        }

        mapViewer.zoomToBestFit(new HashSet<GeoPosition>(track), 0.7);
//		featureSource = null;
    }

    /**
     * 分解Multipoint
     *
     * @param multiPoint
     */
    protected void resolveMultiPoint(MultiPoint multiPoint) {
        for (int i = 0; i < multiPoint.getNumGeometries(); i++) {
            addGeometry(multiPoint.getGeometryN(i), null);
        }
    }

    /**
     * 分解multiLine
     *
     * @param multiLine
     */
    protected void resolveMultiLine(MultiLineString multiLine, ShapeAttributes shapeAttributes) {
        for (int i = 0; i < multiLine.getNumGeometries(); i++) {
            addGeometry(multiLine.getGeometryN(i), shapeAttributes);
        }
    }

    /**
     * 分解multiPolygon
     *
     * @param multiPolygon
     */
    protected void resolveMultiPolygon(MultiPolygon multiPolygon, ShapeAttributes shapeAttributes) {
        for (int i = 0; i < multiPolygon.getNumGeometries(); i++) {
            addGeometry(multiPolygon.getGeometryN(i), shapeAttributes);
        }
    }

    /**
     * 添加图形
     *
     * @param geom
     */
    protected void addGeometry(Geometry geom, ShapeAttributes shapeAttributes) {
        if (geom.isEmpty())
            return;

        if (geom instanceof Point) {
            addPoint((Point) geom, shapeAttributes);
        } else if (geom instanceof LineString) {
            addLine((LineString) geom, shapeAttributes);
        } else if (geom instanceof Polygon) {

            addPolygon((Polygon) geom, shapeAttributes);
        } else if (geom instanceof MultiPoint) {
            resolveMultiPoint((MultiPoint) geom);
        } else if (geom instanceof MultiLineString) {
            resolveMultiLine((MultiLineString) geom, shapeAttributes);
        } else if (geom instanceof MultiPolygon) {
            resolveMultiPolygon((MultiPolygon) geom, shapeAttributes);
        } else {
            throw new IllegalArgumentException("Unhandled geometry type: " + geom.getGeometryType());
        }
    }

    protected void addLine(LineString line, ShapeAttributes shapeAttributes) {

        if (this.lines == null) {
            this.lines = new ArrayList<ShapePloyLine>();
        }

        ShapePloyLine shapeLine = new ShapePloyLine();
        Coordinate[] exteriorCoordinates = line.getCoordinates();
        List<GeoPosition> list = new ArrayList<GeoPosition>();
        for (Coordinate coord : exteriorCoordinates) {
            list.add(new GeoPosition(coord.y, coord.x));
        }
        shapeLine.setPositions(list);
        shapeLine.setShapeAttributes(shapeAttributes);
        lines.add(shapeLine);
    }

    protected void addPoint(Point point, ShapeAttributes shapeAttributes) {

        if (this.points == null) {
            this.points = new ArrayList<ShapePoint>();
        }

        Coordinate[] exteriorCoordinates = point.getCoordinates();
        for (Coordinate coord : exteriorCoordinates) {
            ShapePoint shapePoint = new ShapePoint();
            shapePoint.setPosition(new GeoPosition(coord.y, coord.x));
            shapePoint.setScale(shapeAttributes.getOutlineWidth());
            shapePoint.setColor(shapeAttributes.getOutlineColor());
            shapePoint.setText(shapeAttributes.getText());
            points.add(shapePoint);
        }

    }

    protected void addPolygon(Polygon polygon, ShapeAttributes attributes) {

        if (polygon.isEmpty())
            return;
        if (this.polygons == null) {
            this.polygons = new ArrayList<ShapePolygon>();
        }
        ShapePolygon shapePolygon = new ShapePolygon();
        List<GeoPosition> exterList = new ArrayList<GeoPosition>();
        Coordinate[] exteriorCoordinates = polygon.getExteriorRing().getCoordinates();
        for (Coordinate coord : exteriorCoordinates) {
            GeoPosition geoPosition = new GeoPosition(coord.y, coord.x);
            exterList.add(geoPosition);
            track.add(geoPosition);
        }

        for (int j = 0; j < polygon.getNumInteriorRing(); j++) {
            List<GeoPosition> innerList = new ArrayList<GeoPosition>();
            for (Coordinate coord : polygon.getInteriorRingN(j).getCoordinates()) {
                innerList.add(new GeoPosition(coord.y, coord.x));
            }
            shapePolygon.addInnerBoundary(innerList);
        }
        shapePolygon.setOuterBoundary(exterList);
        shapePolygon.setShapeAttributes(attributes);

        polygons.add(shapePolygon);
    }

    @Override
    protected void doPaint(Graphics2D g, JXMapViewer map, int width, int height) {
        Graphics2D graphics2D = (Graphics2D) g.create();
        Rectangle rect = map.getViewportBounds();
        graphics2D.drawImage(getBackgroundImage(), 0, 0, rect.width, rect.height, null);
        graphics2D.dispose();
        if (polygons == null)
            return;
        polygonRenderer.drawMany(g, map, polygons);
        polyLineRenderer.drawMany(g, map, lines);
        pointRenderer.drawMany(g, map, points);
    }

    public void clearAllShp() {
        if (this.points != null)
            this.points.clear();
        if (this.polygons != null)
            this.polygons.clear();
        if (this.lines != null)
            this.lines.clear();
    }

    public BufferedImage getBackgroundImage() {

        if (backgroundImage == null) {
            backgroundImage = IconLoader.image("/images/aa.png");
        }

        return backgroundImage;
    }

    public List<ShapePolygon> getPolygons() {
        return polygons;
    }

    public List<ShapePoint> getPoints() {
        return points;
    }

    public List<ShapePloyLine> getLines() {
        return lines;
    }
}