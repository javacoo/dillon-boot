package org.dillon.swingui.xmap.render;


import org.dillon.swingui.xmap.geometry.ShapeAttributes;
import org.dillon.swingui.xmap.geometry.ShapePolygon;
import org.dillon.swingui.xmap.listener.SelectionAdapter;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.List;

/**
 * 区域渲染器
 */
public class PolygonRenderer extends AbstractRenderer<ShapePolygon> {

    /**
     * 渲染实现
     *
     * @param g         The Graphics2D to render to
     * @param mapViewer
     * @param polygon
     */
    @Override
    protected void doPaint(Graphics2D g, JXMapViewer mapViewer,
                           ShapePolygon polygon) {

        Polygon poly = new Polygon();
        for (GeoPosition geoPosition : polygon.getOuterBoundary()) {
            Point2D point = mapViewer.getTileFactory().geoToPixel(geoPosition, mapViewer.getZoom());
            poly.addPoint((int) point.getX(), (int) point.getY());
        }

        drawPolygon(g, mapViewer, polygon.getShapeAttributes(), poly);

        for (int j = 0; j < polygon.getHoleSize(); j++) {
            poly = new Polygon();
            for (GeoPosition geoPosition : polygon.getInnerBoundary(j)) {
                Point2D point = mapViewer.getTileFactory().geoToPixel(geoPosition, mapViewer.getZoom());
                poly.addPoint((int) point.getX(), (int) point.getY());

            }
            drawPolygon(g, mapViewer, polygon.getShapeAttributes(), poly);
        }

    }

    /**
     * 渲染实现
     *
     * @param g         The Graphics2D to render to
     * @param mapViewer
     * @param polygons
     */
    @Override
    protected void doPaintMany(Graphics2D g, JXMapViewer mapViewer,
                               Iterable<? extends ShapePolygon> polygons) {

        if (polygons == null) {
            return;
        }

        for (ShapePolygon polygon : polygons) {
            Polygon poly = new Polygon();
            for (GeoPosition geoPosition : polygon.getOuterBoundary()) {
                Point2D point = mapViewer.getTileFactory().geoToPixel(geoPosition, mapViewer.getZoom());
                poly.addPoint((int) point.getX(), (int) point.getY());
            }

            drawPolygon(g, mapViewer, polygon.getShapeAttributes(), poly);

            for (int j = 0; j < polygon.getHoleSize(); j++) {
                poly = new Polygon();
                for (GeoPosition geoPosition : polygon.getInnerBoundary(j)) {
                    Point2D point = mapViewer.getTileFactory().geoToPixel(geoPosition, mapViewer.getZoom());
                    poly.addPoint((int) point.getX(), (int) point.getY());

                }
                drawPolygon(g, mapViewer, polygon.getShapeAttributes(), poly);
            }
        }
    }

    /**
     * 添加点到Polygon
     *
     * @param polygon   awt polygon
     * @param map       the map
     * @param positions coordinate list
     */
    private void addPointToPolygon(Polygon polygon, JXMapViewer map,
                                   List<? extends GeoPosition> positions) {
        if (polygon == null)
            return;
        if (positions == null)
            return;

        for (GeoPosition gp : positions) {
            // convert geo-coordinate to world bitmap pixel
            Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());
            polygon.addPoint((int) pt.getX(), (int) pt.getY());
        }
    }

    /**
     * 区域绘制
     *
     * @param g           the graphics object
     * @param jxMapViewer the map
     * @param polygon
     */
    protected void drawPolygon(Graphics2D g, JXMapViewer jxMapViewer, ShapeAttributes attributes,
                               Polygon polygon) {


        if (polygon.intersects(jxMapViewer.getViewportBounds())) {
            if (attributes.isDrawOutline() && attributes.getOutlineColor() != null) {
                g.setStroke(attributes.getLineStroke());
                g.setColor(attributes.getOutlineColor());
                g.draw(polygon);
            }
            if (attributes.isDrawInterior() && attributes.getInteriorColor() != null) {


                if (polygon.contains(SelectionAdapter.movePoint)) {
                    g.setColor(attributes.getSelectInteriorColor());
                    g.fill(polygon);
                    g.setColor(new Color(0xffffff));
                    Point2D p0 = jxMapViewer.getTileFactory().geoToPixel(attributes.getCenterPosition(), jxMapViewer.getZoom());
                    g.drawString(attributes.getText(), (int)p0.getX(), (int)p0.getY());
                }else {
                    g.setColor(attributes.getInteriorColor());
                    g.fill(polygon);
                }


            }
        }

    }

}
