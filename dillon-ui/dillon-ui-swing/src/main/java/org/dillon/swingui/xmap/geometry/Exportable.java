package org.dillon.swingui.xmap.geometry;

/**
 * 导出接口
 */
public interface Exportable {
    /**
     *  导出序列化
     */
    void export();
}
