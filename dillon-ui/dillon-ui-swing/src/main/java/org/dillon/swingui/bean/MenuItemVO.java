package org.dillon.swingui.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class MenuItemVO implements Serializable {
    private String id;
    private String text;
    private String icon;
    private String tooltipText;
    private String panelClass;
    private boolean colse;
    private boolean haveCase;
    private Map<String, String> itemMap = new HashMap();

    public MenuItemVO() {
    }

    public MenuItemVO(String id, String text, String icon) {
        this.id = id;
        this.text = text;
        this.icon = icon;
    }
}