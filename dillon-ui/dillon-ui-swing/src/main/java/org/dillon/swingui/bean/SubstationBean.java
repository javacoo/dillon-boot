package org.dillon.swingui.bean;

import lombok.Data;
import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;

/**
 * @className: LineBean
 * @author: liwen
 * @date: 2022/4/8 09:12
 */
@Data
public class SubstationBean {
    private String name;
    private GeoPosition geoPosition;
    private Color color = new Color(0xFA813B);
    private Color foreground = new Color(0xF56D1F);
    private int arcSize = 10;

    public SubstationBean(String name, GeoPosition geoPosition) {
        this.name = name;
        this.geoPosition = geoPosition;
    }
}
