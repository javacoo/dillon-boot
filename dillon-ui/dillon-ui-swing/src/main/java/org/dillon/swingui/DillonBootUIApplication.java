package org.dillon.swingui;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.util.SystemInfo;
import org.dillon.swingui.config.DefaultPrefs;
import org.dillon.swingui.utils.SysFont;
import org.dillon.swingui.view.MainFrame;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.TimingSource;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;

import javax.swing.*;
import java.awt.*;

/**
 * @className: MepscUIApplication
 * @author: liwen
 * @date: 2022/3/22 17:40
 */
@EnableFeignClients("org.dillon.swingui.request") // 激活 @FeignClient
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
public class DillonBootUIApplication {


    @Value("${spring.application.name}")
    private String appName="Dillon-Boot";

    public DillonBootUIApplication() {


        // macOS  (see https://www.formdev.com/flatlaf/macos/)
        if (SystemInfo.isMacOS) {
            // enable screen menu bar
            // (moves menu bar from JFrame window to top of screen)
            System.setProperty("apple.laf.useScreenMenuBar", "true");

            // application name used in screen menu bar
            // (in first menu after the "apple" menu)
            System.setProperty("apple.awt.application.name", appName);

            // appearance of window title bars
            // possible values:
            //   - "system": use current macOS appearance (light or dark)
            //   - "NSAppearanceNameAqua": use light appearance
            //   - "NSAppearanceNameDarkAqua": use dark appearance
            // (needs to be set on main thread; setting it on AWT thread does not work)
            System.setProperty("apple.awt.application.appearance", "system");
        }

        // Linux
        if (SystemInfo.isLinux) {
            // enable custom window decorations
            JFrame.setDefaultLookAndFeelDecorated(true);
            JDialog.setDefaultLookAndFeelDecorated(true);
        }

        SwingUtilities.invokeLater(() -> {
            initUI();
            MainFrame.getInstance().showLogin();
            MainFrame.getInstance().setTitle(appName);
        });
    }

    public void initUI() {
        TimingSource ts = new SwingTimerTimingSource();
        Animator.setDefaultTimingSource(ts);
        ts.init();
        UIManager.put("TitlePane.unifiedBackground", true);
        UIManager.put("TitlePane.centerTitleIfMenuBarEmbedded", false);
        UIManager.put("TitlePane.iconSize", new Dimension(30, 30));
        UIManager.put("TitlePane.unifiedBackground", true);
        UIManager.put("MenuItem.selectionType", "underline");
        UIManager.put("defaultFont", SysFont.APP_FONT);
        UIManager.put("JButton.buttonType", "tab");
        UIManager.put("Component.arrowType", "chevron");


        // 设置中文主题样式 解决乱码
        StandardChartTheme chartTheme = new StandardChartTheme("unicode") {
            //重写apply(...)方法是为了借机消除文字锯齿.VALUE_TEXT_ANTIALIAS_OFF
            @Override
            public void apply(JFreeChart chart) {

                chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING,
                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                chart.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                super.apply(chart);
            }
        };
        // 设置标题字体
        chartTheme.setExtraLargeFont(SysFont.APP_FONT.deriveFont(20f).deriveFont(Font.BOLD));
        // 设置图例的字体
        chartTheme.setRegularFont(SysFont.APP_FONT);
        // 设置轴向的字体
        chartTheme.setLargeFont(SysFont.APP_FONT);
        chartTheme.setSmallFont(SysFont.APP_FONT);
        ChartFactory.setChartTheme(chartTheme);

        UIManager.put("TristateCheckBox.icon", new FlatSVGIcon("icons/fuxuankuang-weiquanxuan.svg", 20, 20));

        DefaultPrefs.init("/dillon-ui-swing");
        DefaultPrefs.setupLaf(new String[]{});

        UIManager.put("ExComboBoxUI", "com.jidesoft.plaf.basic.BasicExComboBoxUI");
        FlatSVGIcon.ColorFilter.getInstance().add(new Color(0x6e6e6e), new Color(0x6F767E), new Color(0xffffff));

    }

    public static void main(String[] args) {


        new SpringApplicationBuilder(DillonBootUIApplication.class).headless(false).run(args);

    }
}
