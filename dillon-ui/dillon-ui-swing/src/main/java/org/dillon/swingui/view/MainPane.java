package org.dillon.swingui.view;

import com.alibaba.cola.dto.SingleResponse;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.extras.components.FlatTabbedPane;
import org.dillon.common.core.constant.HttpStatus;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.swing.WTabbedPanel;
import org.dillon.swingui.request.feignclient.LoginService;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.utils.IconLoader;
import org.dillon.system.api.domain.UserInfo;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingTargetAdapter;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

import static com.formdev.flatlaf.FlatClientProperties.*;
import static com.formdev.flatlaf.FlatClientProperties.TABBED_PANE_TAB_CLOSE_CALLBACK;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 主面板
 *
 * @author liwen
 * @className: MainPane
 * @author: liwen
 * @date: 2022/3/22 17:42
 * @date 2022/07/06
 */
public class MainPane extends JPanel {

    /**
     * 背景图像
     */
    private BufferedImage backgroundImage = null;
    /**
     * 根窗格
     */
    private JSplitPane rootPane;
    /**
     * 状态栏面板
     */
    private StatusBarPanel statusBarPanel;
    /**
     * 选项卡式窗格
     */
    private FlatTabbedPane tabbedPane = null;
    /**
     * 落后于工具栏
     */
    private JToolBar trailingToolBar = null;
    /**
     * 主要工具栏
     */
    private JToolBar leadingToolBar = null;
    /**
     * 导航窗格酒吧
     */
    private NavigationPaneBar navigationPaneBar;
    /**
     * 动画师
     */
    private Animator animator;
    /**
     * 分频器位置
     */
    private Integer dividerLocation = 300;
    /**
     * narlocation
     */
    private int narlocation;

    /**
     * 主面板
     */
    public MainPane() {
        initComponents();
        initListener();
        initData();
    }

    private void initListener() {


    }

    /**
     * 初始化组件
     */
    private void initComponents() {


        rootPane = new JSplitPane();
        rootPane.setLeftComponent(getNavigationPaneBar());
        rootPane.setRightComponent(getTabbedPane());
        rootPane.setDividerSize(3);

        this.setLayout(new BorderLayout());
        this.add(rootPane, BorderLayout.CENTER);
        this.add(getStatusBarPanel(), BorderLayout.SOUTH);
    }


    /**
     * 显示导航栏
     *
     * @param visible 可见
     */
    public void showNavigationBar(boolean visible) {

        if (animator == null) {
            animator = (new Animator.Builder()).setDuration(300, TimeUnit.MILLISECONDS).build();
        }

        Integer dividerLocation1 = rootPane.getDividerLocation();
        Integer dividerLocation2 = 0;

        float ta1 = 0f;
        float ta2 = 0f;
        float ma1 = 0f;
        float ma2 = 0f;
        animator.clearTargets();
        if (visible) {
            ta1 = 0f;
            ta2 = 1f;
            ma1 = 1f;
            ma2 = 0f;
            dividerLocation2 = dividerLocation < 49 ? 300 : dividerLocation;
        } else {
            ta1 = 1f;
            ta2 = 0f;
            ma1 = 0f;
            ma2 = 1f;
            dividerLocation2 = 48;
        }

        animator.addTarget(PropertySetter.getTarget(getNavigationPaneBar(), "treeAlpha", new AccelerationInterpolator(0.5D, 0.5D), ta1, ta2));
        animator.addTarget(PropertySetter.getTarget(getNavigationPaneBar(), "menuAlpha", new AccelerationInterpolator(0.5D, 0.5D), ma1, ma2));
        animator.addTarget(PropertySetter.getTarget(this, "narlocation", new AccelerationInterpolator(0.5D, 0.5D), dividerLocation1, dividerLocation2));
        animator.addTarget(new TimingTargetAdapter() {
            @Override
            public void begin(Animator source) {
                getNavigationPaneBar().getMenuPane().setVisible(!visible);
                getNavigationPaneBar().getTreePane().setVisible(visible);
                dividerLocation = rootPane.getDividerLocation();
            }

            @Override
            public void end(Animator source) {
                rootPane.setDividerSize(visible ? 3 : 0);

            }
        });

        if (animator.isRunning()) {
            animator.restart();
        } else {
            animator.start();
        }
    }


    /**
     * 把状态栏面板
     *
     * @return {@link StatusBarPanel}
     */
    public StatusBarPanel getStatusBarPanel() {
        if (statusBarPanel == null) {
            statusBarPanel = new StatusBarPanel();
        }
        return statusBarPanel;
    }


    /**
     * 得到背景图像
     *
     * @return {@link BufferedImage}
     */
    public BufferedImage getBackgroundImage() {

        if (backgroundImage == null) {
            backgroundImage = IconLoader.image("/images/bj.png");
        }

        return backgroundImage;
    }

    /**
     * 得到选项卡式窗格
     *
     * @return {@link JTabbedPane}
     */
    public FlatTabbedPane getTabbedPane() {
        if (tabbedPane == null) {
            tabbedPane = new WTabbedPanel() {

                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    Graphics2D g2d = (Graphics2D) g.create();
                    g2d.setColor(getBackground());
                    int tabH = UIManager.getInt("TabbedPane.tabHeight");
                    g2d.fillRect(0, 0, getWidth(), tabH);
                    g2d.setColor(UIManager.getColor("App.background"));
                    g2d.fillRect(0, tabH, getWidth(), getHeight() - tabH);
                    g2d.dispose();
                }

                @Override
                protected void closeTab(int tabIndex) {
                    if (tabbedPane.getComponentAt(tabIndex) != null) {
                        ApplicatonStore.removeNavigatonPanel(tabbedPane.getComponentAt(tabIndex));
                    }
                    super.closeTab(tabIndex);
                }
            };
            tabbedPane.setOpaque(false);
            tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
            tabbedPane.putClientProperty(TABBED_PANE_TRAILING_COMPONENT, getTrailingToolBar());
            tabbedPane.putClientProperty(TABBED_PANE_LEADING_COMPONENT, getLeadingToolBar());
            tabbedPane.putClientProperty(TABBED_PANE_TAB_CLOSE_TOOLTIPTEXT, "Close");
            tabbedPane.putClientProperty(TABBED_PANE_TAB_CLOSABLE, true);
            tabbedPane.putClientProperty(TABBED_PANE_SHOW_TAB_SEPARATORS, false);
            tabbedPane.putClientProperty(TABBED_PANE_TAB_CLOSE_CALLBACK,
                    (BiConsumer<JTabbedPane, Integer>) (tabPane, tabIndex) -> {

                        if (tabPane.getComponentAt(tabIndex) != null) {
                            ApplicatonStore.removeNavigatonPanel(tabPane.getComponentAt(tabIndex));
                        }
                        tabPane.remove(tabIndex);

                    });

        }
        return tabbedPane;
    }

    /**
     * 会落后于工具栏
     *
     * @return {@link JToolBar}
     */
    private JToolBar getTrailingToolBar() {
        if (trailingToolBar == null) {
            trailingToolBar = new JToolBar();
            trailingToolBar.setFloatable(false);
            trailingToolBar.setBorder(null);
            trailingToolBar.setOpaque(false);

            JButton reButton = new JButton(new FlatSVGIcon("icons/shuaxin.svg", 30, 30));
            reButton.setToolTipText("刷新");
            reButton.addActionListener(e -> {
                ApplicatonStore.getRefreshObservable().notify("refresh");
            });
            JButton userButton = new JButton("", new FlatSVGIcon("icons/user-filling.svg", 30, 30));
            userButton.addActionListener(e -> showPopupMenuButtonActionPerformed((Component) e.getSource()));
            userButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    showPopupMenuButtonActionPerformed((Component) e.getSource());
                }
            });

            trailingToolBar.add(reButton);
            trailingToolBar.add(Box.createGlue());
            trailingToolBar.add(userButton);


        }

        return trailingToolBar;
    }

    /**
     * 显示弹出菜单按钮执行操作
     */
    private void showPopupMenuButtonActionPerformed(Component invoker) {
        JPopupMenu popupMenu = new JPopupMenu();

        JPanel infoPanel = new JPanel(new BorderLayout());
        JLabel label = new JLabel("", JLabel.CENTER);
        label.setIcon(new FlatSVGIcon("icons/user-filling.svg", 80, 80));
        label.setText(ApplicatonStore.getUserInfo().getUser().getUserName());
        label.setVerticalTextPosition(SwingConstants.BOTTOM);  //必须设置文字树直方向位置
        label.setHorizontalTextPosition(SwingConstants.CENTER);
        infoPanel.add(label, BorderLayout.CENTER);
        infoPanel.add(new JLabel(ApplicatonStore.getUserInfo().getUser().getRoles() + "", JLabel.CENTER), BorderLayout.SOUTH);
        label.setPreferredSize(new Dimension(240, 100));
        JMenuItem menuItem9 = new JMenuItem("个人信息");
        menuItem9.setIcon(new FlatSVGIcon("icons/gerenxinxi.svg", 25, 25));
        JMenuItem menuItem11 = new JMenuItem("退出");
        menuItem11.setIcon(new FlatSVGIcon("icons/logout.svg", 25, 25));

        menuItem11.addActionListener(e1 -> {
            MainFrame.getInstance().loginOut();
        });
        menuItem9.addActionListener(e1 -> {

            if (MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("个人信息") == -1) {
                FlatSVGIcon icon = new FlatSVGIcon("icons/user.svg", 25, 25);

                MainFrame.getInstance().getMainPane().getTabbedPane().addTab("个人信息", icon, ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.system.user.PersonalCenterPanel"));
            }
            MainFrame.getInstance().getMainPane().getTabbedPane().setSelectedIndex(MainFrame.getInstance().getMainPane().getTabbedPane().indexOfTab("个人信息"));

        });

        popupMenu.add(infoPanel);
        popupMenu.addSeparator();
        popupMenu.add(menuItem9);
        popupMenu.addSeparator();
        popupMenu.add(menuItem11);
        popupMenu.show(invoker, 0, invoker.getHeight());

    }

    /**
     * 得到主要工具栏
     *
     * @return {@link JToolBar}
     */
    private JToolBar getLeadingToolBar() {
        if (leadingToolBar == null) {
            leadingToolBar = new JToolBar();
            leadingToolBar.setFloatable(false);
            leadingToolBar.setBorder(null);
            trailingToolBar.setOpaque(false);
        }

        return leadingToolBar;
    }

    /**
     * 得到导航窗格酒吧
     *
     * @return {@link NavigationPaneBar}
     */
    public NavigationPaneBar getNavigationPaneBar() {
        if (navigationPaneBar == null) {
            navigationPaneBar = new NavigationPaneBar();
        }
        return navigationPaneBar;
    }

    /**
     * 得到narlocation
     *
     * @return int
     */
    public int getNarlocation() {
        return narlocation;
    }

    /**
     * 设置narlocation
     *
     * @param narlocation narlocation
     */
    public void setNarlocation(int narlocation) {
        this.narlocation = narlocation;
        rootPane.setDividerLocation(narlocation);
    }

    /**
     * 更新用户界面
     */
    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                rootPane.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, UIManager.getColor("App.background")));
                tabbedPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, UIManager.getColor("App.background")));
            }
        });
        super.updateUI();

    }

    /**
     * 初始化数据
     */
    private void initData() {
        SwingWorker<SingleResponse<UserInfo>, Object> worker = new SwingWorker<SingleResponse<UserInfo>, Object>() {
            @Override
            protected SingleResponse<UserInfo> doInBackground() throws Exception {
                return Request.connector(LoginService.class).getInfo();
            }

            @Override
            protected void done() {
                try {
                    if (get().isSuccess()) {
                        ApplicatonStore.setUserInfo(get().getData());
                        getNavigationPaneBar().updateData();
                        MainFrame.getInstance().getMainMenuBar().updateMenuData();
                        FlatSVGIcon icon = new FlatSVGIcon("icons/home.svg", 25, 25);
                        tabbedPane.addTab("主页", icon, ApplicatonStore.getNavigatonPanel("org.dillon.swingui.view.MapMainPane"));
                        tabbedPane.setTabClosable(0, false);
                    } else {
                        ApplicatonStore.setUserInfo(new UserInfo());
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        worker.execute();
    }

}
