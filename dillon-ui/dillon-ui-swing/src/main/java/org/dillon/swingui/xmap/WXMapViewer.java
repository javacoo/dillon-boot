package org.dillon.swingui.xmap;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.input.CenterMapListener;
import org.jxmapviewer.input.PanKeyListener;
import org.jxmapviewer.input.PanMouseInputListener;
import org.jxmapviewer.input.ZoomMouseWheelListenerCursor;
import org.jxmapviewer.painter.CompoundPainter;
import org.jxmapviewer.painter.Painter;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactoryInfo;

import javax.swing.event.MouseInputListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @className: WXMapViewer
 * @author: liwen
 * @date: 2022/4/7 20:05
 */
public class WXMapViewer extends JXMapViewer {
    private List<Painter<JXMapViewer>> painters = new ArrayList<Painter<JXMapViewer>>();
    private CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);

    public WXMapViewer() {


        TileFactoryInfo info = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.SATELLITE);
        DefaultTileFactory stateTileFactory = new DefaultTileFactory(info);
        stateTileFactory.setThreadPoolSize(8);
        setTileFactory(stateTileFactory);
        setZoom(13);
        setAddressLocation(new GeoPosition(36.03, 103.40));

        // Add interactions
        MouseInputListener mia = new PanMouseInputListener(this);
        addMouseListener(mia);
        addMouseMotionListener(mia);

        addMouseListener(new CenterMapListener(this));

        addMouseWheelListener(new ZoomMouseWheelListenerCursor(this));

        addKeyListener(new PanKeyListener(this));


    }

    public void addPainter(Painter<JXMapViewer> painter) {
        painters.add(painter);
        setOverlayPainter(new CompoundPainter<JXMapViewer>(painters));
    }



}
