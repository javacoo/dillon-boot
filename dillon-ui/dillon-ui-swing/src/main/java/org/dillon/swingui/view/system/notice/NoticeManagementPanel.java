package org.dillon.swingui.view.system.notice;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.PageResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import net.miginfocom.swing.MigLayout;
import org.dillon.common.core.utils.StringUtils;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysNoticeFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.system.api.domain.SysNotice;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 公告 信息管理面板
 *
 * @author liwen
 * @date 2022/07/12
 */
public class NoticeManagementPanel extends JPanel implements Observer {
    private final static String[] COLUMN_ID = {"", "序号", "公告标题", "公告类型", "状态", "创建者", "创建时间", "操作"};

    private JXTable table;

    private DefaultTableModel tableModel;

    private WPaginationPane wPaginationPane;

    private JPanel optPostPanel;
    private JTextField nameTextField;
    private JTextField optPeoField;
    private JComboBox typeCombo;

    private JTextField noticeNameTextField;
    private JComboBox noticeTypeCombo;
    private JTextArea noticeContentTextArea;
    private JRadioButton normalBut;
    private JRadioButton disableBut;

    private WaitPane waitPane;
    public NoticeManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);

        initComponents();
        updateData();
    }

    private void initComponents() {

        JPanel toolBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toolBar.add(new JLabel("公告标题"));

        toolBar.add(nameTextField = createTextField("请输入公告标题"));
        nameTextField.setColumns(15);
        toolBar.add(new JLabel("操作人"));
        toolBar.add(optPeoField = createTextField("请输入操作人"));
        optPeoField.setColumns(15);
        toolBar.add(new JLabel("类型"));
        toolBar.add(typeCombo = new JComboBox(new String[]{"全部","通知", "公告"}));


        JButton restButton = new JButton("重置");
        toolBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            optPeoField.setText("");
            typeCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> updateData());
        toolBar.add(searchButton);

        JButton addButton = new JButton("新增");
        toolBar.add(addButton);
        addButton.addActionListener(e -> {
            showPostAddDialog();
        });

        JButton eidtButton = new JButton("修改");
        toolBar.add(eidtButton);
        eidtButton.addActionListener(e -> {
            showPostEditDialog();
        });

        JButton delButton = new JButton("删除");
        toolBar.add(delButton);
        delButton.addActionListener(e -> {
            delPost(true);
        });

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 7 || column == 0) {
                    return true;
                }
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return Boolean.class;
                }
                return super.getColumnClass(columnIndex);
            }
        };
        tableModel.setColumnIdentifiers(COLUMN_ID);

        table = new JXTable(tableModel);
        table.setRowHeight(50);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 0));
        table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
        table.getColumn("").setMinWidth(40);
        table.getColumn("").setMaxWidth(40);
        table.getColumn("公告标题").setMinWidth(300);
        table.getColumn("操作").setMinWidth(150);
        table.getColumn("操作").setMaxWidth(150);

        JScrollPane tsp = new JScrollPane(table);
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        Component view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tsp);
        panel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                updateData();
            }
        }, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));


        this.setLayout(new BorderLayout(0,10));
        this.add(panel);
        this.add(toolBar, BorderLayout.NORTH);
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));
                table.setShowVerticalLines(false);
                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);

                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
                table.getColumnExt("公告类型").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        JButton button = new JButton("1".equals(value) ? "通知" : "公告");
                        button.setBackground("1".equals(value) ? new Color(0xe7faf0) : new Color(0xfff8e6));
                        button.setForeground("1".equals(value) ? new Color(0x13ce66) : new Color(0xffba00));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });

                table.getColumnExt("状态").setCellRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
                        JButton button = new JButton("0".equals(value) ? "正常" : "关闭");
                        button.setBackground("0".equals(value) ? new Color(0xe8f4ff) : new Color(0xffeded));
                        button.setForeground("0".equals(value) ? new Color(0x1890ff) : new Color(0xff9292));
                        panel.add(button);
                        panel.setBackground(component.getBackground());
                        return panel;
                    }
                });
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar()));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar()));
            }
        });
        super.updateUI();

    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        optBar.setLayout(new FlowLayout());
        JButton edit = new JButton("修改");
        edit.setIcon(new FlatSVGIcon("icons/xiugai.svg", 15, 15));

        edit.addActionListener(e -> showPostEditDialog());
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> delPost(false));
        del.setForeground(UIManager.getColor("App.accentColor"));

        optBar.add(edit);
        optBar.add(del);
        optBar.setPreferredSize(new Dimension(150, 50));
        return optBar;

    }


    private void showPostAddDialog() {

        int opt = WOptionPane.showOptionDialog(null, getOptPostPanel(false), "添加部门", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            addPost();
        }
    }


    private void showPostEditDialog() {
        int selRow = table.getSelectedRow();
        if (selRow == -1) {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        int opt = WOptionPane.showOptionDialog(null, getOptPostPanel(true), "修改部门", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"确定", "取消"}, "确定");
        if (opt == 0) {
            editPost();
        }
    }

    private void updateData() {
        final SysNotice sysNotice = new SysNotice();
        sysNotice.setNoticeTitle(nameTextField.getText());
        sysNotice.setCreateBy(optPeoField.getText());
        sysNotice.setNoticeType(typeCombo.getSelectedIndex()==0?null:(typeCombo.getSelectedIndex()==1?"1":"2"));

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysNotice);

        SwingWorker<PageResponse<SysNotice>, Object> worker = new SwingWorker<PageResponse<SysNotice>, Object>() {
            @Override
            protected PageResponse<SysNotice> doInBackground() throws Exception {


                return Request.connector(SysNoticeFeign.class).list(pageInfo);

            }

            @Override
            protected void done() {
                try {
                    if (ObjectUtil.isNotEmpty(get())) {
                        for (SysNotice sysNotice : get().getData()) {
                            Vector rowV = new Vector();
                            rowV.add(false);
                            rowV.add(sysNotice.getNoticeId());
                            rowV.add(sysNotice.getNoticeTitle());
                            rowV.add(sysNotice.getNoticeType());
                            rowV.add(sysNotice.getStatus());
                            rowV.add(sysNotice.getCreateBy());
                            rowV.add(DateUtil.format(sysNotice.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(sysNotice);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    
                }
            }
        };
        tableModel.setRowCount(0);
        
        worker.execute();
    }


    public JPanel getOptPostPanel(Boolean isEidt) {
        optPostPanel = new JPanel(new MigLayout("fill,wrap 2,hidemode 3,insets 10", "[right][grow,240::]", "[grow,30::]10[grow,30::]10[grow,30::]10[grow,30::]"));

        noticeNameTextField = createTextField("请输入公告标题");
        noticeTypeCombo = new JComboBox(new String[]{"通知","公告"});
        normalBut = new JRadioButton("正常", true);
        disableBut = new JRadioButton("关闭");
        ButtonGroup group4 = new ButtonGroup();
        group4.add(normalBut);
        group4.add(disableBut);
        noticeContentTextArea = new JTextArea();

        optPostPanel.add(new JLabel("*公告标题"));
        optPostPanel.add(noticeNameTextField, "growx,growy");

        optPostPanel.add(new JLabel("*公告类型"));
        optPostPanel.add(noticeTypeCombo, "growx,growy");

        optPostPanel.add(new JLabel("状态"));
        JToolBar toolBar4 = new JToolBar();
        toolBar4.add(normalBut);
        toolBar4.add(disableBut);
        optPostPanel.add(toolBar4, "growx");

        optPostPanel.add(new JLabel("内容"));
        optPostPanel.add(new JScrollPane(noticeContentTextArea), "growx,h 350!,w 450");

        if (isEidt) {
            getPostTree();
        }

        return optPostPanel;
    }

    private void getPostTree() {
        int selRow = table.getSelectedRow();
        Long sysNoticeId = null;
        if (selRow != -1) {
            sysNoticeId = (Long) table.getValueAt(selRow, 1);
        }
        Long finalPostId = sysNoticeId;
        SwingWorker<SysNotice, Object> worker = new SwingWorker<SysNotice, Object>() {
            @Override
            protected SysNotice doInBackground() throws Exception {

                return Request.connector(SysNoticeFeign.class).getInfo(finalPostId).getData();

            }


            @Override
            protected void done() {
                try {
                    if (get() != null) {
                        SysNotice sysNoticeModel = get();
                        updateValue(sysNoticeModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        worker.execute();

    }


    private void updateValue(SysNotice sysNotice) {


        noticeNameTextField.setText(sysNotice.getNoticeTitle());
        noticeTypeCombo.setSelectedIndex("1".equals(sysNotice.getNoticeType())?0:1);
        normalBut.setSelected("0".equals(sysNotice.getStatus()));
        disableBut.setSelected("1".equals(sysNotice.getStatus()));
        noticeContentTextArea.setText(sysNotice.getNoticeContent());

    }

    private SysNotice getValue() {
        SysNotice sysNotice = new SysNotice();
        sysNotice.setNoticeTitle(noticeNameTextField.getText());
        sysNotice.setNoticeType(noticeTypeCombo.getSelectedIndex()==0?"1":"2");
        sysNotice.setStatus(normalBut.isSelected()?"0":"1");
        sysNotice.setNoticeContent(noticeContentTextArea.getText());
        return sysNotice;
    }

    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
        return textField;
    }


    /**
     * 添加
     */
    private void addPost() {

        SysNotice sysNotice = getValue();
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysNoticeFeign.class).add(sysNotice);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void editPost() {

        SysNotice sysNotice = getValue();

        int selRow = table.getSelectedRow();
        if (selRow != -1) {
            Long id = (Long) table.getValueAt(selRow, 1);
            sysNotice.setNoticeId(id);
        } else {
            WMessage.showMessageWarning(MainFrame.getInstance(), "请选择一条要修改的记录！");
            return;
        }
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysNoticeFeign.class).edit(sysNotice);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    private void delPost(boolean isMore) {
        Long sysNoticeId = null;

        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean b = (Boolean) tableModel.getValueAt(i, 0);
                long id = (long) tableModel.getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) tableModel.getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除编号为" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        Long finalPostId = sysNoticeId;
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysNoticeFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        updateData();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }


    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }
}
