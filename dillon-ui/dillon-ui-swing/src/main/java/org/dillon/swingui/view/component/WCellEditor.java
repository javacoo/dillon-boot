package org.dillon.swingui.view.component;

import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;

/**
 * @className: WCellEditor
 * @author: liwen
 * @date: 2022/3/9 08:42
 */
public class WCellEditor extends DefaultCellEditor {
    private Object oldV;

    public WCellEditor(JTextField textField) {
        super(textField);
    }


    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        delegate.setValue(null);
        this.oldV = value;
        return editorComponent;
    }

    @Override
    public Object getCellEditorValue() {

        if (super.getCellEditorValue() == null || StringUtils.isBlank(super.getCellEditorValue().toString())) {
            delegate.setValue(oldV);

        }
        return super.getCellEditorValue();
    }
}
