package org.dillon.swingui.xmap.geometry;


import org.jxmapviewer.viewer.GeoPosition;

import java.util.ArrayList;
import java.util.List;

/**
 * 面、区域
 */
public class ShapePolygon extends AbstractGeometry {
    private List<List<? extends GeoPosition>> boundaries;
    private ShapeAttributes shapeAttributes = new ShapeAttributes();//图形属性

    /**
     * 构建区域
     */
    public ShapePolygon() {
        this.shapeType = AbstractGeometry.SHAPE_POLYGON;
        boundaries = new ArrayList<List<? extends GeoPosition>>();
        boundaries.add(new ArrayList<GeoPosition>());
    }

    /**
     * 构建区域
     *
     * @param positions
     */
    public ShapePolygon(Iterable<? extends GeoPosition> positions) {
        this();
        this.setOuterBoundary(positions);
    }

    /**
     * 获边界坐标序列
     *
     * @return
     */
    public List<? extends GeoPosition> getOuterBoundary() {
        return this.boundaries.get(0);
    }

    /**
     * 设置边界坐标序列
     *
     * @param positions
     */
    public void setOuterBoundary(Iterable<? extends GeoPosition> positions) {

        this.boundaries.set(0, this.fillBoundary(positions));
    }

    /**
     * 添加岛洞
     * hole
     *
     * @param corners
     */
    public void addInnerBoundary(Iterable<? extends GeoPosition> corners) {

        this.boundaries.add(this.fillBoundary(corners));
    }

    /**
     * 获取岛洞数量
     *
     * @return
     */
    public int getHoleSize() {
        return this.boundaries.size() - 1;
    }

    /**
     * 获取岛洞坐标序列
     *
     * @param index 岛洞索引
     * @return
     */
    public List<? extends GeoPosition> getInnerBoundary(int index) {

        if (index < 0)
            return null;

        index += 1;
        if (this.boundaries.size() > index) {
            return this.boundaries.get(index);
        }

        return null;
    }

    protected List<? extends GeoPosition> fillBoundary(
            Iterable<? extends GeoPosition> corners) {
        ArrayList<GeoPosition> list = new ArrayList<GeoPosition>();

        if (corners == null) {
            return null;
        }

        for (GeoPosition corner : corners) {
            if (corner != null)
                list.add(corner);
        }

        if (list.size() < 3) {
            String message = "generic.InsufficientPositions";
            log.warn(this.getClass().getName() + ":" + message + ", skipping");
            return null;
        }

        if (list.size() > 0 && !list.get(0).equals(list.get(list.size() - 1)))
            list.add(list.get(0));

        return list;
    }

    /**
     * 获取图形属性
     *
     * @return
     */
    public ShapeAttributes getShapeAttributes() {
        return shapeAttributes;
    }

    /**
     * 设置图形属性
     *
     * @param shapeAttributes
     */
    public void setShapeAttributes(ShapeAttributes shapeAttributes) {
        this.shapeAttributes = shapeAttributes;
    }


}
