package org.dillon.swingui.bean;

import lombok.Data;

import java.awt.geom.Point2D;

/**
 * 箭头实体类
 *
 * @author xiangqian
 * @date 16:06 2019/10/31
 */

@Data
public class Arrow {
    private Attributes attributes;
    private Point2D.Double point1;
    private Point2D.Double point2;
    private Point2D.Double point3;

    public Arrow(Attributes attributes) {
        this.attributes = attributes;
    }


}