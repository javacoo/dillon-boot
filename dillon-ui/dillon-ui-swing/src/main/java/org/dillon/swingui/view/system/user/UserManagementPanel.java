package org.dillon.swingui.view.system.user;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.PageResponse;
import com.alibaba.cola.dto.SingleResponse;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.icons.FlatClearIcon;
import com.formdev.flatlaf.icons.FlatSearchIcon;
import com.jidesoft.combobox.CheckBoxListExComboBox;
import com.jidesoft.combobox.TreeExComboBox;
import com.jidesoft.tree.QuickTreeFilterField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;
import org.dillon.common.core.web.domain.OptResult;
import org.dillon.common.core.web.domain.PageInfo;
import org.dillon.swing.*;
import org.dillon.swing.notice.WMessage;
import org.dillon.swing.table.renderer.OptButtonTableCellEditor;
import org.dillon.swing.table.renderer.OptButtonTableCellRenderer;
import org.dillon.swingui.request.Request;
import org.dillon.swingui.request.feignclient.SysDeptFeign;
import org.dillon.swingui.request.feignclient.SysUserFeign;
import org.dillon.swingui.store.ApplicatonStore;
import org.dillon.swingui.view.MainFrame;
import org.dillon.swingui.view.component.SearchTreeNode;
import org.dillon.system.api.domain.*;
import org.dillon.system.api.model.TreeSelect;
import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static javax.swing.JOptionPane.*;
import static org.dillon.common.core.web.domain.OptResult.CODE_TAG;

/**
 * 用户管理面板
 *
 * @author liwen
 * @date 2022/06/23
 */
public class UserManagementPanel extends JPanel implements Observer {


    /**
     * 列id
     */
    private String[] COLUMN_ID = {"", "用户编号", "用户名称", "用户昵称", "部门", "手机号", "状态", "创建时间", "操作", "serId"};
    private String[] ROLE_COLUMN_ID = {"", "角色编号", "角色名称", "权限字符", "创建时间"};
    /**
     * 树
     */
    private JXTree tree;
    /**
     * 表格
     */
    private JXTable table;

    private JXTable roleTable;
    /**
     * 表格模型
     */
    private DefaultTableModel tableModel;
    private DefaultTableModel roleTableModel;
    /**
     * 过滤字段
     */
    private QuickTreeFilterField filterField;

    /**
     * 选择路径
     */
    private TreePath selectPath;

    private JTextField nameTextField;
    private JTextField phoneField;
    private JComboBox statusCombo;

    private JTextField nickNameTextField;
    private TreeExComboBox dectComboBox;
    private JTextField phoneTextField;
    private JTextField emailTextField;

    private JTextField userNameTextField;
    private JPasswordField pwdTextField;
    private JComboBox<String> sexComboBox;
    private JCheckBox statusCheckBox;
    private CheckBoxListExComboBox postComboBox;
    private CheckBoxListExComboBox roleComboBox;
    private JTextArea remarkTextArea;
    private JLabel userNameLabel;
    private JLabel pwdLabel;
    private JButton saveAddButton;
    private JButton saveEditButton;
    private JButton cancel;

    private JButton addBut;
    private JButton delBut;

    private JPanel optPanel;

    private WPaginationPane wPaginationPane;
    private WPaginationPane rolePaginationPane;

    private JDialog dialog;

    private JPanel assigningRolesPane;
    private WaitPane waitPane;

    public UserManagementPanel() {
        ApplicatonStore.addRefreshObserver(this);
        initComponents();
        initData();
    }


    /**
     * 初始化组件
     */
    private void initComponents() {


        filterField = new QuickTreeFilterField() {
            @Override
            public void applyFilter(String text) {
                super.applyFilter(text);
                tree.expandAll();
            }
        };
        filterField.setHintText("首字母搜索");
        filterField.setOpaque(false);
        filterField.setFilterIcon(new FlatSearchIcon());
        filterField.setResetIcon(new FlatClearIcon());
        filterField.getTextField().setOpaque(false);
        filterField.setMatchesLeafNodeOnly(true);
        filterField.setHideEmptyParentNode(true);
        filterField.setWildcardEnabled(true);
        filterField.setTree(getTree());


        JScrollPane treesp = new JScrollPane(getTree());
        Component view = treesp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        treesp.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        JScrollPane tsp = new JScrollPane(getTable());
        tsp.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        tsp.setOpaque(false);
        tsp.getViewport().setOpaque(false);
        view = tsp.getViewport().getView();
        ((JComponent) view).putClientProperty(
                FlatClientProperties.COMPONENT_FOCUS_OWNER,
                (Predicate<JComponent>) c -> false);
        tsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        JPanel treePanel = new JPanel(new BorderLayout(7, 7));
        treePanel.setBorder(BorderFactory.createEmptyBorder(7, 7, 0, 7));
        treePanel.add(filterField, BorderLayout.NORTH);
        treePanel.add(treesp);


        JPanel searchBar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        searchBar.add(new JLabel("用户名称"));
        searchBar.add(nameTextField = createTextField("请输入用户名称"));
        nameTextField.setColumns(10);

        searchBar.add(new JLabel("手机号码"));
        searchBar.add(phoneField = createTextField("请输入手机号码"));
        phoneField.setColumns(10);

        searchBar.add(new JLabel("用户状态"));
        searchBar.add(statusCombo = new JComboBox(new String[]{"全部", "正常", "停用"}));

        JButton restButton = new JButton("重置");
        searchBar.add(restButton);
        restButton.addActionListener(e -> {
            nameTextField.setText("");
            statusCombo.setSelectedIndex(0);
        });
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(e -> queryUser());
        searchBar.add(searchButton);

        JPanel butPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        butPanel.add(addBut = new WButton("新增"));
        butPanel.add(delBut = new WButton("删除"));
        addBut.addActionListener(e -> showAddPanel());
        delBut.addActionListener(e -> delUser(true));

        JPanel nPanel = new JPanel(new VerticalLayout(0));
        nPanel.add(searchBar);
        nPanel.add(butPanel);

        JPanel tablePanel = new JPanel(new BorderLayout(7, 7));
        tablePanel.add(tsp);
        tablePanel.add(nPanel, BorderLayout.NORTH);
        tablePanel.add(wPaginationPane = new WPaginationPane() {
            @Override
            public void setPageIndex(int pageIndex) {
                super.setPageIndex(pageIndex);
                queryUser();
            }
        }, BorderLayout.SOUTH);

        tablePanel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
        JSplitPane splitPane = new JSplitPane();
        splitPane.setDividerLocation(300);

        splitPane.setResizeWeight(0.3D);

        splitPane.setLeftComponent(treePanel);
        splitPane.setRightComponent(tablePanel);
        splitPane.setOpaque(false);

        this.setLayout(new BorderLayout());
        this.add(waitPane = new WaitPane(splitPane));


    }


    /**
     * 让树
     *
     * @return {@link JXTree}
     */
    public JXTree getTree() {
        if (tree == null) {
            tree = new JXTree(new DefaultTreeModel(new DefaultMutableTreeNode("")));


            tree.setHighlighters(new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, Color.RED, UIManager.getColor("App.rolloverColor")));
            tree.setRolloverEnabled(true);

            tree.setOpaque(false);
            tree.setCellRenderer(new TreeCellRenderer() {

                @Override
                public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

                    JLabel label = new JLabel(value + "");
                    if (value instanceof SearchTreeNode) {
                        label.setText(((SearchTreeNode) value).getName());
                    }

                    return label;
                }
            });
            tree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    queryUser();
                }
            });
        }
        return tree;
    }


    /**
     * 得到表
     *
     * @return {@link JTable}
     */
    public JXTable getTable() {
        if (table == null) {
            table = new JXTable(getTableModel());

            table.setRowHeight(50);
            table.setShowHorizontalLines(true);
            table.setShowVerticalLines(false);
            table.setIntercellSpacing(new Dimension(0, 1));
            table.getColumn("serId").setMinWidth(0);
            table.getColumn("serId").setMaxWidth(0);
            table.getColumn("").setMinWidth(40);
            table.getColumn("").setMaxWidth(40);
            table.getColumn("操作").setMinWidth(180);
            table.getColumn("操作").setMaxWidth(180);
            table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));


        }
        return table;
    }

    private JToolBar creatBar() {
        JToolBar optBar = new JToolBar();
        JButton edit = new JButton("编辑");
        edit.setIcon(new FlatSVGIcon("icons/edit-outline.svg", 15, 15));

        edit.addActionListener(e -> {
            showEditPanel();
        });
        edit.setForeground(UIManager.getColor("App.accentColor"));

        JButton del = new JButton("删除");
        del.setIcon(new FlatSVGIcon("icons/delte.svg", 15, 15));
        del.addActionListener(e -> {
            delUser(false);
        });
        del.setForeground(UIManager.getColor("App.accentColor"));

        JButton more = new JButton("更多");
        more.addActionListener(e -> showPopupMoreButtonActionPerformed(e));
        more.setForeground(UIManager.getColor("App.accentColor"));
        more.setIcon(new FlatSVGIcon("icons/arrow-right.svg", 15, 15));
        optBar.add(edit);
        optBar.add(del);
        optBar.add(more);
        return optBar;

    }

    private void showPopupMoreButtonActionPerformed(ActionEvent e) {
        Component invoker = (Component) e.getSource();
        JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem menuItem9 = new JMenuItem("重置密码");
        JMenuItem menuItem10 = new JMenuItem("分配角色");
        menuItem9.addActionListener(e1 -> {
            resetPwd();
        });
        menuItem10.addActionListener(e1 -> {
            showRolesDialog();
        });

        popupMenu.add(menuItem9);
        popupMenu.add(menuItem10);
        popupMenu.show(invoker, 0, invoker.getHeight());

    }

    private JCheckBox createStatus() {
        JCheckBox statusCheckBox = new JCheckBox();
        statusCheckBox.setIcon(new AnimatedSwitchIcon());
        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
        statusCheckBox.setHorizontalTextPosition(SwingConstants.CENTER);
        statusCheckBox.addActionListener(e -> changeStatus());
        return statusCheckBox;

    }

    /**
     * 得到表格模型
     *
     * @return {@link DefaultTableModel}
     */
    public DefaultTableModel getTableModel() {
        if (tableModel == null) {
            tableModel = new DefaultTableModel() {
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    if (columnIndex == 0 || columnIndex == 6) {
                        return Boolean.class;
                    }
                    return super.getColumnClass(columnIndex);
                }

                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column == 0 || column == 6 || column == 8) {
                        return true;
                    }
                    return false;
                }
            };

            tableModel.setColumnIdentifiers(COLUMN_ID);
        }
        return tableModel;
    }


    public JPanel getOptPanel(boolean isAdd) {
        if (optPanel == null) {
            optPanel = new JPanel(new MigLayout("fill,wrap 4,insets 10", "[right,60!]7[grow,180:180:]25[right,60!]7[grow,180:180:]", "[35!]15[]15[35!]15[35!]15[35!]15[grow]15[35!]"));
            optPanel.add(new JLabel("*用户昵称"));
            optPanel.add(nickNameTextField = createTextField("请输入用户昵称"), "growx,h 35!");
            optPanel.add(new JLabel("归属部门"));
            optPanel.add(dectComboBox = new WTreeComboBox(), "growx,h 35!");
            dectComboBox.setEditable(false);
            optPanel.add(new JLabel("手机号码"));
            optPanel.add(phoneTextField = createTextField("请输入手机号码"), "growx,h 35!");
            optPanel.add(new JLabel("邮箱"));
            optPanel.add(emailTextField = createTextField("请输入邮箱"), "growx,h 35!");

            optPanel.add(userNameLabel = new JLabel("*用户名称"));
            optPanel.add(userNameTextField = createTextField("请输入用户名称"), "growx,h 35!");
            optPanel.add(pwdLabel = new JLabel("*用户密码"));
            optPanel.add(pwdTextField = new JPasswordField(), "growx,h 35!");
            pwdTextField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "请输入密码");
            pwdTextField.putClientProperty(FlatClientProperties.STYLE, "showRevealButton: true");

            optPanel.add(new JLabel("用户性别"));
            optPanel.add(sexComboBox = new JComboBox(new Object[]{"男", "女", "未知"}), "growx,h 35!");
            optPanel.add(new JLabel("状态"));
            optPanel.add(statusCheckBox = new JCheckBox("正常"), "growx,h 35!");
            statusCheckBox.setIcon(new AnimatedSwitchIcon());
            statusCheckBox.setSelected(true);
            statusCheckBox.addActionListener(e -> {
                statusCheckBox.setText(statusCheckBox.isSelected() ? "正常" : "停用");
            });

            optPanel.add(new JLabel("岗位"));
            optPanel.add(postComboBox = new WCheckBoxListComboBox(new Object[]{"a", "b", "c"}), "growx,h 35!");
            postComboBox.setEditable(false);

            optPanel.add(new JLabel("角色"));
            optPanel.add(roleComboBox = new WCheckBoxListComboBox(new Object[]{"a", "b", "c"}), "growx,h 35!");
            roleComboBox.setEditable(false);

            optPanel.add(new JLabel("备注"), "top");
            optPanel.add(new JScrollPane(remarkTextArea = new JTextArea()), "span 3,h 80:80:,growx,growy");

            JPanel optButPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            optButPanel.add(saveAddButton = new JButton("确定"));
            optButPanel.add(saveEditButton = new JButton("确定"));
            optButPanel.add(cancel = new JButton("取消"));

            optPanel.add(optButPanel, "span 4,h 40!");

            cancel.addActionListener(e -> dialog.dispose());
            saveAddButton.addActionListener(e -> addUser());
            saveEditButton.addActionListener(e -> editUser());

        }

        saveAddButton.setVisible(isAdd);
        saveEditButton.setVisible(!isAdd);
        userNameLabel.setEnabled(isAdd);
        userNameTextField.setEnabled(isAdd);
        pwdLabel.setEnabled(isAdd);
        pwdTextField.setEnabled(isAdd);

        nickNameTextField.setText("");
        dectComboBox.setSelectedItem("");
        phoneTextField.setText("");
        emailTextField.setText("");
        userNameTextField.setText("");
        pwdTextField.setText("");
        sexComboBox.setSelectedItem("");
        statusCheckBox.setSelected(true);
        remarkTextArea.setText("");

        showInfo(isAdd);
        postComboBox.setSelectedObjects(new Object[]{});
        roleComboBox.setSelectedObjects(new Object[]{});

        return optPanel;
    }

    private JPanel getAssigningRolesPane() {
        if (assigningRolesPane == null) {
            assigningRolesPane = new JPanel(new BorderLayout());
            assigningRolesPane.setPreferredSize(new Dimension(600, 450));
            roleTable = new JXTable(getRoleTableModel());

            assigningRolesPane.add(new JScrollPane(roleTable));

        }
        queryRoles();
        return assigningRolesPane;
    }

    private JTextField createTextField(String placeholderText) {
        JTextField textField = new JTextField();
        textField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, placeholderText);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);
//        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_LEADING_ICON, new FlatSVGIcon("icons/sousuo.svg",20,20));
        return textField;
    }

    private void showRolesDialog() {
        int opt = WOptionPane.showOptionDialog(null, getAssigningRolesPane(), "分配角色", OK_CANCEL_OPTION, PLAIN_MESSAGE, null, new Object[]{"提交", "取消"}, "提交");
        if (opt == 0) {
            insertAuthRole();
        }
    }

    private void showAddPanel() {
        dialog = new JDialog(MainFrame.getInstance(), true);
        dialog.setUndecorated(true);
        JRootPane rootPane = dialog.getRootPane();
        if (rootPane != null) {
            rootPane.setWindowDecorationStyle(JRootPane.COLOR_CHOOSER_DIALOG);
            rootPane.putClientProperty(FlatClientProperties.TITLE_BAR_BACKGROUND, UIManager.getColor("App.background"));

        }


        dialog.setTitle("添加用户");
        dialog.setContentPane(getOptPanel(true));
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    private void showEditPanel() {
        dialog = new JDialog(MainFrame.getInstance(), true);
        dialog.setUndecorated(true);
        JRootPane rootPane = dialog.getRootPane();
        if (rootPane != null) {
            rootPane.setWindowDecorationStyle(JRootPane.COLOR_CHOOSER_DIALOG);
            rootPane.putClientProperty(FlatClientProperties.TITLE_BAR_BACKGROUND, UIManager.getColor("App.background"));

        }

        dialog.setTitle("修改用户");
        dialog.setContentPane(getOptPanel(false));
        dialog.pack();

        dialog.setLocationRelativeTo(null);

        dialog.setVisible(true);
    }


    /**
     * 初始化数据
     */
    private void initData() {
        updateData();
        queryUser();
    }

    /**
     * 更新数据
     */
    private void updateData() {


        SwingWorker<MultiResponse<TreeSelect>, String> worker = new SwingWorker<MultiResponse<TreeSelect>, String>() {
            @Override
            protected MultiResponse<TreeSelect> doInBackground() throws Exception {

                return Request.connector(SysDeptFeign.class).treeselect(new SysDept());
            }

            @Override
            protected void done() {

                try {
                    if (get().isSuccess()) {
                        createTreeData(get().getData());
                        if (selectPath != null) {
                            getTree().expandPath(selectPath);
                            getTree().setSelectionPath(selectPath);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    
                }

            }
        };
        
        worker.execute();
    }


    /**
     * 查询用户
     */
    private void queryUser() {


        SysUser sysUser = new SysUser();

        sysUser.setUserName(nameTextField.getText());
        int selectIndex = statusCombo.getSelectedIndex();
        sysUser.setStatus(selectIndex == 0 ? null : ("正常".equals(statusCombo.getSelectedItem()) ? "0" : "1"));
        sysUser.setPhonenumber(phoneField.getText());

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(wPaginationPane.getPageSize());
        pageInfo.setPageIndex(wPaginationPane.getPageIndex());
        pageInfo.setData(sysUser);
        // TODO 自动生成的方法存根
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node != null) {
            if (node.getUserObject() instanceof TreeSelect) {

                TreeSelect menu = (TreeSelect) node.getUserObject();
                sysUser.setDeptId(menu.getId());
            }
        }


        SwingWorker<PageResponse<SysUser>, String> worker = new SwingWorker<PageResponse<SysUser>, String>() {
            @Override
            protected PageResponse<SysUser> doInBackground() throws Exception {

                return Request.connector(SysUserFeign.class).list(pageInfo);
            }

            @Override
            protected void done() {

                try {
                    if (get().isSuccess()) {
                        tableModel.setRowCount(0);
                        for (SysUser sysUser : get().getData()) {
                            Vector rowV = new Vector<>();
                            rowV.add(false);
                            rowV.add(sysUser.getUserId());
                            rowV.add(sysUser.getUserName());
                            rowV.add(sysUser.getNickName());
                            rowV.add(sysUser.getDept().getDeptName());
                            rowV.add(sysUser.getPhonenumber());
                            rowV.add("0".equals(sysUser.getStatus()) ? true : false);
                            rowV.add(DateUtil.format(sysUser.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                            rowV.add(sysUser);
                            tableModel.addRow(rowV);
                        }
                        wPaginationPane.setTotal(get().getTotalCount());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    
                }

            }
        };
        tableModel.setRowCount(0);
        
        worker.execute();
    }

    /**
     * 查询角色
     */
    private void queryRoles() {


        Long userId = (Long) table.getValueAt(table.getSelectedRow(), 1);

        SwingWorker<SingleResponse<UserInfoModel>, String> worker = new SwingWorker<SingleResponse<UserInfoModel>, String>() {
            @Override
            protected SingleResponse<UserInfoModel> doInBackground() throws Exception {

                return Request.connector(SysUserFeign.class).authRole(userId);
            }

            @Override
            protected void done() {

                try {
                    if (get().isSuccess()) {
                        updateRoleTable(get().getData().getRoles(), get().getData().getRoleIds());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }

            }
        };
        roleTableModel.setRowCount(0);
        worker.execute();
    }

    /**
     * 删除用户
     */
    private void delUser(boolean isMore) {

        // TODO 自动生成的方法存根


        List<Long> ids = new ArrayList<>();
        if (isMore) {

            for (int i = 0; i < getTableModel().getRowCount(); i++) {
                Boolean b = (Boolean) getTableModel().getValueAt(i, 0);
                long id = (long) getTableModel().getValueAt(i, 1);
                if (b) {
                    ids.add(id);
                }
            }

            if (ids.isEmpty()) {
                WMessage.showMessageWarning(MainFrame.getInstance(), "请选择要删除的数据！");
                return;
            }
        } else {
            ids.add((long) getTableModel().getValueAt(table.getSelectedRow(), 1));
        }

        int opt = WOptionPane.showOptionDialog(this, "是否确定删除" + ids + "？", "提示", OK_CANCEL_OPTION, WARNING_MESSAGE, null, null, null);

        if (opt != 0) {
            return;
        }
        SwingWorker<OptResult, String> worker = new SwingWorker<OptResult, String>() {
            @Override
            protected OptResult doInBackground() throws Exception {

                return Request.connector(SysUserFeign.class).remove(Convert.toLongArray(ids));
            }

            @Override
            protected void done() {

                try {
                    int code = (int) get().get(CODE_TAG);
                    if (code == 200) {
                        queryUser();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }

            }
        };
        worker.execute();
    }


    /**
     * 创建树数据
     *
     * @param treeSelectList 树选择列表
     */
    private void createTreeData(List<TreeSelect> treeSelectList) {

        for (TreeSelect treeSelect : treeSelectList) {
            SearchTreeNode root = new SearchTreeNode(treeSelect.getId().toString(), treeSelect.getLabel());
            if (treeSelect.getChildren() != null) {
                createTree(root, treeSelect.getChildren());
            }
            filterField.setTreeModel(new DefaultTreeModel(root));
            getTree().setModel(filterField.getDisplayTreeModel());
        }

    }

    /**
     * 创建树
     * 递归生成树
     *
     * @param parentNode 父节点
     * @param childlList childl列表
     */
    private void createTree(SearchTreeNode parentNode, List<TreeSelect> childlList) {
        for (TreeSelect sectionModel : childlList) {
            SearchTreeNode childNode = new SearchTreeNode(sectionModel.getId() + "", sectionModel.getLabel());
            childNode.setUserObject(sectionModel);
            parentNode.add(childNode);
            if (sectionModel.getChildren() != null) {
                createTree(childNode, sectionModel.getChildren());
            }
        }
    }

    /**
     * 更新
     *
     * @param o   o
     * @param arg 参数
     */
    @Override
    public void update(Observable o, Object arg) {
        if (this.isDisplayable()) {
            updateData();
        }
    }

    @Override
    public void updateUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ColorHighlighter rollover = new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, UIManager.getColor("App.rolloverColor"), null);
                table.setHighlighters(rollover);
                table.setIntercellSpacing(new Dimension(0, 1));

                DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
                dc.setHorizontalAlignment(SwingConstants.CENTER);
                table.setDefaultRenderer(Object.class, dc);
                table.getColumn(0).setHeaderRenderer(new CheckHeaderCellRenderer(table));
                table.getColumn("状态").setCellRenderer(new DefaultTableRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        JCheckBox statusCheckBox = new JCheckBox("");
                        statusCheckBox.setIcon(new AnimatedSwitchIcon());
                        statusCheckBox.setSelected((Boolean) value);
                        statusCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
                        statusCheckBox.setBackground(component.getBackground());
                        return statusCheckBox;
                    }
                });

                table.getColumn("状态").setCellEditor(new DefaultCellEditor(createStatus()));
                table.getColumn("操作").setCellRenderer(new OptButtonTableCellRenderer(creatBar(), 2,"admin"));
                table.getColumn("操作").setCellEditor(new OptButtonTableCellEditor(creatBar(), 2,"admin"));
                if (roleTable != null) {
                    roleTable.updateUI();
                }
            }
        });
        super.updateUI();

    }

    /**
     * 分配角色
     */
    private void insertAuthRole() {

        SysUser user = (SysUser) table.getValueAt(table.getSelectedRow(), 8);
        Long userId = user.getUserId();
        List<Long> roleIds = new ArrayList<>();


        for (int i = 0; i < roleTable.getRowCount(); i++) {

            Boolean sel = (Boolean) roleTable.getValueAt(i, 0);

            if (sel) {
                roleIds.add((Long) roleTable.getValueAt(i, 1));
            }

        }

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).insertAuthRole(userId, roleIds.stream().toArray(Long[]::new));
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        queryUser();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 编辑用户
     */
    private void editUser() {

        SysUser user = (SysUser) table.getValueAt(table.getSelectedRow(), 8);
        SysUser editUser = new SysUser();
        editUser.setUserId(user.getUserId());
        editUser.setUserName(user.getUserName());
        editUser.setNickName(nickNameTextField.getText());

        Long deptId = null;
        Object deptObj = dectComboBox.getSelectedItem();
        if (deptObj instanceof TreePath) {
            TreePath treePath = (TreePath) dectComboBox.getSelectedItem();
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
            TreeSelect treeSelect = (TreeSelect) defaultMutableTreeNode.getUserObject();
            deptId = treeSelect.getId();
        } else if (deptObj instanceof SysDept) {
            deptId = ((SysDept) deptObj).getDeptId();
        }

        editUser.setDeptId(deptId);
        editUser.setPhonenumber(phoneTextField.getText());
        editUser.setEmail(emailTextField.getText());
        editUser.setSex((String) sexComboBox.getSelectedItem());
        editUser.setStatus(statusCheckBox.isSelected() ? "0" : "1");

        List<Long> postIds = new ArrayList<>();
        for (Object post : postComboBox.getSelectedObjects()) {
            if (post instanceof SysPost) {
                postIds.add(((SysPost) post).getPostId());
            }
        }
        editUser.setPostIds(postIds.stream().toArray(Long[]::new));
        List<Long> roleIds = new ArrayList<>();
        for (Object role : roleComboBox.getSelectedObjects()) {
            if (role instanceof SysRole) {
                roleIds.add(((SysRole) role).getRoleId());
            }
        }
        editUser.setRoleIds(roleIds.stream().toArray(Long[]::new));
        editUser.setRemark(remarkTextArea.getText());

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).edit(editUser);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        dialog.dispose();
                        queryUser();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 添加用户
     */
    private void addUser() {

        SysUser sysUser = new SysUser();
        sysUser.setNickName(nickNameTextField.getText());
        TreePath treePath = (TreePath) dectComboBox.getSelectedItem();
        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
        TreeSelect treeSelect = (TreeSelect) defaultMutableTreeNode.getUserObject();
        sysUser.setDeptId(treeSelect.getId());
        sysUser.setPhonenumber(phoneTextField.getText());
        sysUser.setEmail(emailTextField.getText());
        sysUser.setUserName(userNameTextField.getText());
        sysUser.setPassword(pwdTextField.getText());
        sysUser.setSex((String) sexComboBox.getSelectedItem());
        sysUser.setStatus(statusCheckBox.isSelected() ? "0" : "1");

        List<Long> postIds = new ArrayList<>();
        for (Object post : postComboBox.getSelectedObjects()) {
            if (post instanceof SysPost) {
                postIds.add(((SysPost) post).getPostId());
            }
        }
        sysUser.setPostIds(postIds.stream().toArray(Long[]::new));
        List<Long> roleIds = new ArrayList<>();
        for (Object role : roleComboBox.getSelectedObjects()) {
            if (role instanceof SysRole) {
                roleIds.add(((SysRole) role).getRoleId());
            }
        }
        sysUser.setRoleIds(roleIds.stream().toArray(Long[]::new));
        sysUser.setRemark(remarkTextArea.getText());

        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).add(sysUser);
            }

            @Override
            protected void done() {
                try {
                    if ((int) get().get(CODE_TAG) == 200) {
                        dialog.dispose();
                        queryUser();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();

    }

    /**
     * 重置pwd
     */
    private void resetPwd() {
        SysUser selUser = (SysUser) table.getValueAt(table.getSelectedRow(), 8);
        String pwd = WOptionPane.showInputDialog(this, "请输入【" + table.getValueAt(table.getSelectedRow(), 2) + "】的密码", "重置密码", INFORMATION_MESSAGE);
        if (StringUtils.isBlank(pwd)) {
            return;
        }
        SysUser sysUser = new SysUser();
        sysUser.setUserId(selUser.getUserId());
        sysUser.setUserName(selUser.getUserName());
        sysUser.setPassword(pwd);
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).resetPwd(sysUser);
            }
        };
        swingWorker.execute();

    }

    /**
     * 改变状态
     */
    private void changeStatus() {
        SysUser selUser = (SysUser) table.getValueAt(table.getSelectedRow(), 8);
        Boolean status = (Boolean) table.getValueAt(table.getSelectedRow(), 6);
        int opt = WOptionPane.showConfirmDialog(this, "确定要" + (status ? "启用" : "停用") + "【" + table.getValueAt(table.getSelectedRow(), 4) + "】用户吗", "提示", OK_CANCEL_OPTION);
        if (opt != 0) {
            table.setValueAt(!status, table.getSelectedRow(), 6);
            return;
        }
        SysUser sysUser = new SysUser();
        sysUser.setUserId(selUser.getUserId());
        sysUser.setUserName(selUser.getUserName());
        sysUser.setStatus(status ? "0" : "1");
        SwingWorker<OptResult, Object> swingWorker = new SwingWorker<OptResult, Object>() {
            @Override
            protected OptResult doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).changeStatus(sysUser);
            }
        };
        swingWorker.execute();
    }


    private void showInfo(boolean isAdd) {

        Long userId = null;

        if (isAdd == false) {
            userId = (Long) table.getValueAt(table.getSelectedRow(), 1);
        }

        Long finalUserId = userId;
        SwingWorker<SingleResponse<UserInfoModel>, Object> swingWorker = new SwingWorker<SingleResponse<UserInfoModel>, Object>() {
            @Override
            protected SingleResponse<UserInfoModel> doInBackground() throws Exception {
                return Request.connector(SysUserFeign.class).getInfo(finalUserId);
            }

            @Override
            protected void done() {
                try {
                    if (get().isSuccess()) {

                        initComboBoxData(get().getData());
                        if (isAdd == false) {
                            updateEidtPaneData(get().getData());
                        }
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        swingWorker.execute();
    }

    public DefaultTableModel getRoleTableModel() {
        if (roleTableModel == null) {
            roleTableModel = new DefaultTableModel() {
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    if (columnIndex == 0) {
                        return Boolean.class;
                    }
                    return super.getColumnClass(columnIndex);
                }

                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column == 0) {
                        return true;
                    }
                    return false;
                }
            };
            roleTableModel.setColumnIdentifiers(ROLE_COLUMN_ID);
        }
        return roleTableModel;
    }

    private void updateRoleTable(List<SysRole> roles, List<Long> roleIds) {

        getRoleTableModel().setRowCount(0);
        for (SysRole sysRole : roles) {
            Vector rowV = new Vector();
            rowV.add(roleIds.contains(sysRole.getRoleId()));
            rowV.add(sysRole.getRoleId());
            rowV.add(sysRole.getRoleName());
            rowV.add(sysRole.getRoleKey());
            rowV.add(sysRole.getCreateTime());
            getRoleTableModel().addRow(rowV);
        }
    }


    private void updateEidtPaneData(UserInfoModel userInfoModel) {

        SysUser sysUser = userInfoModel.getUser();
        List<SysRole> roles = new ArrayList<>();
        List<Long> roleIdList = userInfoModel.getRoleIds();
        for (int i = 0; i < roleComboBox.getItemCount(); i++) {
            Object obj = roleComboBox.getItemAt(i);
            if (obj instanceof SysRole) {
                Long id = ((SysRole) obj).getRoleId();
                if (roleIdList.contains(id)) {
                    roles.add((SysRole) obj);
                }
            }
        }
        List<SysPost> posts = new ArrayList<>();
        List<Long> postIdList = userInfoModel.getPostsIds();
        for (int i = 0; i < postComboBox.getItemCount(); i++) {
            Object obj = postComboBox.getItemAt(i);
            if (obj instanceof SysPost) {
                Long id = ((SysPost) obj).getPostId();
                if (postIdList.contains(id)) {
                    posts.add((SysPost) obj);
                }
            }
        }

        nickNameTextField.setText(sysUser.getNickName());
        dectComboBox.setSelectedItem(sysUser.getDept());
        phoneTextField.setText(sysUser.getPhonenumber());
        emailTextField.setText(sysUser.getEmail());
        userNameTextField.setText(sysUser.getUserName());
        pwdTextField.setText(sysUser.getPassword());
        sexComboBox.setSelectedItem(sysUser.getSex());
        statusCheckBox.setSelected("0".equals(sysUser.getStatus()));
        statusCheckBox.setText("0".equals(sysUser.getStatus()) ? "正常" : "停用");
        postComboBox.setSelectedObjects(posts.toArray());
        roleComboBox.setSelectedObjects(roles.toArray());
        remarkTextArea.setText(sysUser.getRemark());
    }

    private void initComboBoxData(UserInfoModel userInfoModel) {

        createDectTree(userInfoModel.getDepts());
        createPostComboBoxData(userInfoModel.getPosts());
        createRoleComboBoxData(userInfoModel.getRoles());
    }

    private void createDectTree(List<TreeSelect> treeSelectList) {
        for (TreeSelect treeNode : treeSelectList) {

            DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(treeNode);

            if (CollectionUtil.isNotEmpty(treeNode.getChildren())) {
                createDectTreeDate(rootNode, treeNode.getChildren());
            }
            dectComboBox.setTreeModel(new DefaultTreeModel(rootNode));
            dectComboBox.setCellRenderer(new DefaultTreeCellRenderer() {
                @Override
                public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                    if (value instanceof DefaultMutableTreeNode) {
                        if (((DefaultMutableTreeNode) value).getUserObject() instanceof TreeSelect) {
                            return new JLabel(((TreeSelect) ((DefaultMutableTreeNode) value).getUserObject()).getLabel());
                        }
                    }
                    return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
                }
            });
        }
    }

    private void createDectTreeDate(DefaultMutableTreeNode fatherNode, List<TreeSelect> treeSelectList) {

        for (TreeSelect treeNode : treeSelectList) {

            DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeNode);
            fatherNode.add(node);

            if (CollectionUtil.isNotEmpty(treeNode.getChildren())) {
                createDectTreeDate(node, treeNode.getChildren());
            }
        }
    }

    private void createPostComboBoxData(List<SysPost> postList) {

        postComboBox.removeAllItems();
        for (SysPost sysRole : postList) {
            postComboBox.addItem(sysRole);
        }
    }

    private void createRoleComboBoxData(List<SysRole> roleList) {
        roleComboBox.removeAllItems();
        for (SysRole sysRole : roleList) {
            roleComboBox.addItem(sysRole);
        }
    }

}
