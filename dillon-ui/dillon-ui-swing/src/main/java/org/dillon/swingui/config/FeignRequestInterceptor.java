package org.dillon.swingui.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.dillon.swingui.store.ApplicatonStore;
import org.springframework.context.annotation.Configuration;

/**
 * @Description Feign接口请求拦截器
 **/
@Configuration
public class FeignRequestInterceptor implements RequestInterceptor {
 
    /**
     * @description: 将traceId设置到请求头
     */
    @Override
    public void apply(RequestTemplate template) {
        template.header("Authorization", ApplicatonStore.getToken());
    }
}
