package org.dillon.swing.table;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

public class WTable12hTo96Model extends DefaultTableModel {

    private List<Double> value = new ArrayList<>();


    @Override
    public boolean isCellEditable(int row, int column) {

        if (column == 0 || row == 4) {
            return false;
        }
        return super.isCellEditable(row, column);
    }

    @Override
    public int getColumnCount() {
        return 13;
    }

    @Override
    public int getRowCount() {
        return 9;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        return Double.class;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "时/分";
        } else {
            int h = column-1;
            return h + "时";
        }
    }

    @Override
    public Object getValueAt(int row, int column) {

        if (row == 4 || column == 0) {
            return null;
        }

        int index = 0;
        if (row < 4) {
            index = row + (column - 1) * 4;
        }
        if (row > 4) {
            index = (row - 5) + (column - 1) * 4 + 48;
        }
        if (getValue() == null || getValue().isEmpty()) {
            return null;
        } else {
            return getValue().get(index);
        }

    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {

        if (row == 4 || column == 0) {
            return;
        }
        int index = 0;
        if (row < 4) {
            index = row + (column - 1) * 4;
        }
        if (row > 4) {
            index = (row - 5) + (column - 1) * 4 + 48;
        }
        if (aValue instanceof Double) {
            value.set(index, (Double) aValue);
        }
        super.setValueAt(aValue, row, column);
    }

    public List<Double> getValue() {
        return value;
    }

    public void setValue(List<Double> value) {
        this.value = value;
    }
}
