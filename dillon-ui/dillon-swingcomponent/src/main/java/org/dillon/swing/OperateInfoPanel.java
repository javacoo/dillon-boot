package org.dillon.swing;

import com.formdev.flatlaf.extras.FlatSVGIcon;
import org.fife.rsta.ui.CollapsibleSectionPanel;

import javax.swing.*;
import java.awt.*;

public class OperateInfoPanel extends JPanel {
    private CollapsibleSectionPanel collapsibleSectionPanel;
    private JPanel operatePanel;
    private JPanel infoPanel;

    private JToggleButton searchBut;


    public OperateInfoPanel() {
        infoPanel = new JPanel(new BorderLayout(0,0));

        operatePanel = new JPanel();
        operatePanel.setLayout(new FlowLayout(FlowLayout.RIGHT ));
        operatePanel.add(searchBut = new JToggleButton());
        searchBut.setIcon(new WButtonSVGIcon("icons/sousuo.svg", 20, 20));
        searchBut.addActionListener(e -> {

            if (searchBut.isSelected()) {
                collapsibleSectionPanel.showBottomComponent(infoPanel);
            } else {
                collapsibleSectionPanel.hideBottomComponent();
            }
        });
        searchBut.setToolTipText("显示搜索");

        collapsibleSectionPanel = new CollapsibleSectionPanel();
        collapsibleSectionPanel.addBottomComponent(infoPanel);
        JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(0,0));
        collapsibleSectionPanel.add(label);

        this.setLayout(new BorderLayout(0,0));
        this.add(operatePanel, BorderLayout.SOUTH);
        this.add(collapsibleSectionPanel, BorderLayout.CENTER);
    }

    public JPanel getOperatePanel() {
        return operatePanel;
    }


    public JPanel getInfoPanel() {
        return infoPanel;
    }


}
