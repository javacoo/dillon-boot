package org.dillon.swing.table;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.dillon.swing.table.renderer.WNumberEditor;
import org.dillon.swing.table.renderer.WTable4To96CellRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class WTable4hTo96 extends MultiCellSelectionTable {

    private static final Type DATA_TYPE = new TypeToken<List<Cell>>() {
    }.getType();

    public WTable4hTo96() {
        this(new WTable4hTo96Model());
    }

    public WTable4hTo96(WTable4hTo96Model dm) {
        super(dm);
        this.setDefaultRenderer(Double.class, new WTable4To96CellRenderer());
        this.setDefaultEditor(Double.class, new WNumberEditor(new JTextField()));
        this.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        this.setCellSelectionEnabled(true);
        this.setRowHeight(35);
        this.setRowHeight(4, 40);
        this.getColumn("时/分").setMinWidth(120);
    }


    /**
     * 拷贝填充
     */
    @Override
    protected void copyStuff() {


        List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }

        Object indexValue = cellList.get(0).getValue();
        for (int i = 1; i < size; i++) {
            Cell cell = cellList.get(i);
            setValueAt(indexValue, cell.getRow(), cell.getCol());
        }
    }

    /**
     * 浮动比例填充
     */
    @Override
    protected void fixedRatioStuff(Double fv) {


        List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }
        try {
            for (int i = 0; i < size; i++) {
                Cell cell = cellList.get(i);
                Object cellValue = cell.getValue();

                Double value = cellValue instanceof Double ? (Double) cellValue : Double.parseDouble(cellValue.toString());
                Double newValue = value + value * fv / 100.0;
                setValueAt(newValue, cell.getRow(), cell.getCol());
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    /**
     * 固定数据填充
     */
    @Override
    protected void fixedValueStuff(Double fv) {


        List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }
        try {
            for (int i = 0; i < size; i++) {
                Cell cell = cellList.get(i);
                Object cellValue = cell.getValue();
                Double value = cellValue instanceof Double ? (Double) cellValue : Double.parseDouble(cellValue.toString());

                setValueAt(value + fv, cell.getRow(), cell.getCol());
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }


    @Override
    public java.util.List<Cell> getSelectCellIndex() {

        java.util.List<Cell> cellList = new ArrayList<>();

        for (int r = 0; r < getRowCount(); r++) {
            for (int c = 0; c < getColumnCount(); c++) {
                if (isCellEditable(r, c) && isCellSelected(r, c)) {
                    cellList.add(new Cell(r, c, getValueAt(r, c)));
                }
            }
        }
        return cellList;
    }

    /**
     * 线性递增填充
     */
    @Override
    protected void linearIncreaseStuff() {
        List<Cell> cellList = getSelectCellIndex();

        int size = cellList.size();
        if (size < 3) {
            return;
        }

        Object obj1 = cellList.get(0).getValue();
        Object obj2 = cellList.get(size - 1).getValue();

        if (null == obj1 || null == obj2) {
            return;
        }
        Double startV = null;
        Double endV = null;
        try {
            startV = obj1 instanceof Double ? (Double) obj1 : Double.parseDouble(obj1.toString());
            endV = obj2 instanceof Double ? (Double) obj2 : Double.parseDouble(obj2.toString());
            Double d = (endV - startV) / (size - 1);
            Double cellV = startV;

            for (int i = 1; i < size; i++) {

                Cell cell = cellList.get(i);
                cellV = cellV + d;
                setValueAt(cellV, cell.getRow(), cell.getCol());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 线性递增填充
     */
    @Override
    protected void increaseStuff() {
        List<Cell> cellList = getSelectCellIndex();

        int size = cellList.size();
        if (size < 3) {
            return;
        }

        Object obj1 = cellList.get(0).getValue();
        Object obj2 = cellList.get(1).getValue();

        if (null == obj1 || null == obj2) {
            return;
        }
        Double startV = null;
        Double endV = null;
        try {
            startV = obj1 instanceof Double ? (Double) obj1 : Double.parseDouble(obj1.toString());
            endV = obj2 instanceof Double ? (Double) obj2 : Double.parseDouble(obj2.toString());
            Double d = (endV - startV);
            Double cellV = endV;

            for (int i = 2; i < size - 1; i++) {

                Cell cell = cellList.get(i);
                cellV = cellV + d;
                setValueAt(cellV, cell.getRow(), cell.getCol());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void copy() {

        List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }

        String dataJson = new Gson().toJson(cellList, DATA_TYPE);

        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable tText = new StringSelection(dataJson);
        clip.setContents(tText, null);
    }

    @Override
    protected void paste() {


        if (getSelectedRow() < 0) {
            return;
        }


        String ret = "";
        Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 获取剪切板中的内容
        Transferable clipTf = sysClip.getContents(null);

        if (clipTf != null) {
            // 检查内容是否是文本类型
            if (clipTf.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    ret = (String) clipTf.getTransferData(DataFlavor.stringFlavor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        List<Cell> dataJson = new Gson().fromJson(ret, DATA_TYPE);

        int index = 0;
        for (int r = getSelectedRow(); r < getRowCount(); r++) {

            for (int c = getSelectedColumn(); c < getColumnCount(); c++) {

                if (index < dataJson.size()) {
                    Cell cell = dataJson.get(index);
                    setValueAt(cell.getValue(), r, c);
                }
                index++;
            }
        }
    }


}



