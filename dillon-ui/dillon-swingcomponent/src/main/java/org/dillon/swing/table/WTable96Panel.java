package org.dillon.swing.table;


import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.JXTextField;

import javax.swing.*;
import java.awt.*;

public class WTable96Panel extends JPanel {
    private WTable table;
    private JToolBar toolBar;

    private JButton copyBut;
    private JButton pasteBut;
    private JButton tianChongBut;
    private JButton dizengBut;
    private JButton xianxingBut;
    private JXTextField fixedValueTextField;
    private JXTextField fixedRatioTextField;

    public WTable96Panel(WTable table) {
        this.table = table;
        toolBar = new JToolBar();
        toolBar.add(copyBut = new JButton(new FlatSVGIcon("icons/fuzhi.svg", 16, 16)));
        toolBar.add(pasteBut = new JButton(new FlatSVGIcon("icons/niantie.svg", 16, 16)));
        toolBar.add(tianChongBut = new JButton(new FlatSVGIcon("icons/tianchong.svg", 16, 16)));
        toolBar.add(dizengBut = new JButton(new FlatSVGIcon("icons/shengxu.svg", 16, 16)));
        toolBar.add(xianxingBut = new JButton(new FlatSVGIcon("icons/xianxing.svg", 16, 16)));
        JPanel textPanel = new JPanel(new HorizontalLayout(7));
        textPanel.add(fixedValueTextField = createTextField("固定数值填充"));
        textPanel.add(fixedRatioTextField = createTextField("固定比例填充"));
        toolBar.add(textPanel);
        toolBar.add(Box.createGlue());

        copyBut.setToolTipText("复制");
        pasteBut.setToolTipText("粘帖");
        tianChongBut.setToolTipText("拷贝填充");
        dizengBut.setToolTipText("递增填充");
        xianxingBut.setToolTipText("线性填充");

        copyBut.addActionListener(e -> table.copy());
        pasteBut.addActionListener(e -> table.paste());
        tianChongBut.addActionListener(e -> table.copyStuff());
        dizengBut.addActionListener(e -> table.increaseStuff());
        xianxingBut.addActionListener(e -> table.linearIncreaseStuff());

        // search toolbar

        this.setLayout(new BorderLayout());
        this.add(toolBar, BorderLayout.NORTH);
        this.add(new JScrollPane(table));
    }

    private JXTextField createTextField(String promptText) {
        JXTextField textField = new JXTextField(promptText);
        textField.setColumns(10);
        JButton queding = new JButton(new FlatSVGIcon("icons/queding.svg", 16, 16));
        JToolBar optBar = new JToolBar();
        optBar.add(queding);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_TRAILING_COMPONENT, optBar);
        textField.putClientProperty(FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON, true);

        queding.addActionListener(e -> {
                    Double fv = StringUtils.isEmpty(textField.getText()) ? 0 : Double.parseDouble(textField.getText());

                    if ("固定数值填充".equals(promptText)) {
                        table.fixedValueStuff(fv);
                    } else {
                        table.fixedRatioStuff(fv);
                    }
                }
        );
        return textField;
    }


    public JTable getTable() {
        return table;
    }

    public JToolBar getToolBar() {
        return toolBar;
    }
}
