package org.dillon.swing;

import javax.swing.*;

public class WButton extends JButton {

    public WButton() {
    }

    public WButton(Icon icon) {
        super(icon);
    }

    public WButton(String text) {
        super(text);
    }

    public WButton(Action a) {
        super(a);
    }

    public WButton(String text, Icon icon) {
        super(text, icon);
    }

    @Override
    public boolean isDefaultButton() {
        return true;
    }
}
