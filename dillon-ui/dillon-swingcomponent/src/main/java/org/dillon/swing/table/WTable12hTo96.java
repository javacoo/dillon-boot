package org.dillon.swing.table;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.dillon.swing.table.renderer.WNumberEditor;
import org.dillon.swing.table.renderer.WTable12To96CellRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class WTable12hTo96 extends Multi12CellSelectionTable {
    private static final Type DATA_TYPE = new TypeToken<List<Cell>>() {
    }.getType();

    public WTable12hTo96() {
        this(new WTable12hTo96Model());
    }

    public WTable12hTo96(WTable12hTo96Model dm) {
        super(dm);
        this.setDefaultRenderer(Double.class, new WTable12To96CellRenderer());
        this.setDefaultEditor(Double.class, new WNumberEditor(new JTextField()));
        this.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        this.setCellSelectionEnabled(true);
        this.setRowHeight(35);
        this.setRowHeight(4, 40);

    }

    /**
     * 拷贝填充
     */
    @Override
    protected void copyStuff() {


        java.util.List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }

        Object indexValue = cellList.get(0).getValue();
        for (int i = 1; i < size; i++) {
            Cell cell = cellList.get(i);
            setValueAt(indexValue, cell.getRow(), cell.getCol());
        }
    }

    /**
     * 浮动比例填充
     */
    @Override
    protected void fixedRatioStuff(Double fv) {


        java.util.List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }
        try {
            for (int i = 0; i < size; i++) {
                Cell cell = cellList.get(i);
                Object cellValue = cell.getValue();

                Double value = cellValue instanceof Double ? (Double) cellValue : Double.parseDouble(cellValue.toString());
                Double newValue = value + value * fv / 100.0;
                setValueAt(newValue, cell.getRow(), cell.getCol());
            }

        } catch (
                NumberFormatException e) {
            e.printStackTrace();
        }

    }

    /**
     * 固定数据填充
     */
    @Override
    protected void fixedValueStuff(Double fv) {


        java.util.List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }
        try {
            for (int i = 0; i < size; i++) {
                Cell cell = cellList.get(i);
                Object cellValue = cell.getValue();
                Double value = cellValue instanceof Double ? (Double) cellValue : Double.parseDouble(cellValue.toString());

                setValueAt(value + fv, cell.getRow(), cell.getCol());
            }

        } catch (
                NumberFormatException e) {
            e.printStackTrace();
        }

    }


    @Override
    public java.util.List<Cell> getSelectCellIndex() {

        java.util.List<Cell> cellList = new ArrayList<>();


        for (int c = 0; c < getColumnCount(); c++) {
            for (int r = 0; r < 4; r++) {
                if (isCellEditable(r, c) && isCellSelected(r, c)) {
                    cellList.add(new Cell(r, c, getValueAt(r, c)));
                }
            }
        }

        for (int c = 0; c < getColumnCount(); c++) {
            for (int r = 5; r < getRowCount(); r++) {
                if (isCellEditable(r, c) && isCellSelected(r, c)) {
                    cellList.add(new Cell(r, c, getValueAt(r, c)));
                }
            }
        }
        return cellList;
    }

    /**
     * 线性递增填充
     */
    @Override
    protected void linearIncreaseStuff() {
        java.util.List<Cell> cellList = getSelectCellIndex();

        int size = cellList.size();
        if (size < 3) {
            return;
        }

        Object obj1 = cellList.get(0).getValue();
        Object obj2 = cellList.get(size - 1).getValue();

        if (null == obj1 || null == obj2) {
            return;
        }
        Double startV = null;
        Double endV = null;
        try {
            startV = obj1 instanceof Double ? (Double) obj1 : Double.parseDouble(obj1.toString());
            endV = obj2 instanceof Double ? (Double) obj2 : Double.parseDouble(obj2.toString());
            Double d = (endV - startV) / (size - 1);
            Double cellV = startV;

            for (int i = 1; i < size - 1; i++) {

                Cell cell = cellList.get(i);
                cellV = cellV + d;
                setValueAt(cellV, cell.getRow(), cell.getCol());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 线性递增填充
     */
    @Override
    protected void increaseStuff() {
        java.util.List<Cell> cellList = getSelectCellIndex();

        int size = cellList.size();
        if (size < 3) {
            return;
        }

        Object obj1 = cellList.get(0).getValue();
        Object obj2 = cellList.get(1).getValue();

        if (null == obj1 || null == obj2) {
            return;
        }
        Double startV = null;
        Double endV = null;
        try {
            startV = obj1 instanceof Double ? (Double) obj1 : Double.parseDouble(obj1.toString());
            endV = obj2 instanceof Double ? (Double) obj2 : Double.parseDouble(obj2.toString());
            Double d = (endV - startV);
            Double cellV = endV;

            for (int i = 2; i < size; i++) {

                Cell cell = cellList.get(i);
                cellV = cellV + d;
                setValueAt(cellV, cell.getRow(), cell.getCol());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void copy() {

        java.util.List<Cell> cellList = getSelectCellIndex();
        int size = cellList.size();
        if (size < 1) {
            return;
        }

        String dataJson = new Gson().toJson(cellList, DATA_TYPE);

        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable tText = new StringSelection(dataJson);
        clip.setContents(tText, null);
    }

    @Override
    protected void paste() {


        int row = getSelectedRow();
        int col = getSelectedColumn();

        if (getSelectedRow() < 0) {
            return;
        }
        int index = 0;
        if (row < 4) {
            index = row + col * 4;
        } else {
            index = row + col * 4 + 52;
        }


        String ret = "";
        Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 获取剪切板中的内容
        Transferable clipTf = sysClip.getContents(null);

        if (clipTf != null) {
            // 检查内容是否是文本类型
            if (clipTf.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    ret = (String) clipTf
                            .getTransferData(DataFlavor.stringFlavor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        List<Cell> dataJson = new Gson().fromJson(ret, DATA_TYPE);

        int p = 0;


        for (int c = 0; c < getColumnCount(); c++) {
            for (int r = 0; r < 4; r++) {

                int cellIndex = r + c * 4;

                if (cellIndex >= index && p < dataJson.size() && isCellEditable(r, c)) {
                    Cell cell = dataJson.get(p);

                    setValueAt(cell.getValue(), r, c);
                    p++;
                }

            }
        }
        for (int c = 0; c < getColumnCount(); c++) {
            for (int r = 5; r < getRowCount(); r++) {

                int cellIndex = r + c * 4 + 52;

                if (cellIndex >= index && p < dataJson.size() && isCellEditable(r, c)) {
                    Cell cell = dataJson.get(p);

                    setValueAt(cell.getValue(), r, c);
                    p++;
                }

            }
        }

    }


}
