package org.dillon.swing.chart;

import org.apache.commons.lang3.StringUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.TextAnchor;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by liwen on 2016/10/21.
 */
public class WCrosshairOverlay extends CrosshairOverlay {

    private int mouseX = 0;
    private int mouseY = 0;
    private int currentSeries, currentItem;
    private Paint labelBackgroundPaint = new Color(50, 50, 50);
    private Paint labelTitlePaint = new Color(0xFEA815);
    private float alpha = .7f;
    private Font labelFont = new Font("华文正楷", Font.PLAIN, 14);
    private SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public Paint getLabelBackgroundPaint() {
        return labelBackgroundPaint;
    }

    public void setLabelBackgroundPaint(Paint labelBackgroundPaint) {
        this.labelBackgroundPaint = labelBackgroundPaint;
    }

    public Paint getLabelTitlePaint() {
        return labelTitlePaint;
    }

    public void setLabelTitlePaint(Paint labelTitlePaint) {
        this.labelTitlePaint = labelTitlePaint;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public Font getLabelFont() {
        return labelFont;
    }

    public void setLabelFont(Font labelFont) {
        this.labelFont = labelFont;
    }

    public int getCurrentSeries() {
        return currentSeries;
    }

    public void setCurrentSeries(int currentSeries) {
        this.currentSeries = currentSeries;
    }

    public int getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(int currentItem) {
        this.currentItem = currentItem;
    }


    /**
     * Calculates the anchor point for a label.
     *
     * @param line   the line for the crosshair.
     * @param anchor the anchor point.
     * @param deltaX the x-offset.
     * @param deltaY the y-offset.
     * @return The anchor point.
     */
    private Point2D calculateLabelPoint(Line2D line, RectangleAnchor anchor,
                                        double deltaX, double deltaY) {
        double x, y;
        boolean left = (anchor == RectangleAnchor.BOTTOM_LEFT
                || anchor == RectangleAnchor.LEFT
                || anchor == RectangleAnchor.TOP_LEFT);
        boolean right = (anchor == RectangleAnchor.BOTTOM_RIGHT
                || anchor == RectangleAnchor.RIGHT
                || anchor == RectangleAnchor.TOP_RIGHT);
        boolean top = (anchor == RectangleAnchor.TOP_LEFT
                || anchor == RectangleAnchor.TOP
                || anchor == RectangleAnchor.TOP_RIGHT);
        boolean bottom = (anchor == RectangleAnchor.BOTTOM_LEFT
                || anchor == RectangleAnchor.BOTTOM
                || anchor == RectangleAnchor.BOTTOM_RIGHT);
        Rectangle rect = line.getBounds();

        // we expect the line to be vertical or horizontal
        if (line.getX1() == line.getX2()) {  // vertical
            x = line.getX1();
            y = (line.getY1() + line.getY2()) / 2.0;
            if (left) {
                x = x - deltaX;
            }
            if (right) {
                x = x + deltaX;
            }
            if (top) {
                y = Math.min(line.getY1(), line.getY2()) + deltaY;
            }
            if (bottom) {
                y = Math.max(line.getY1(), line.getY2()) - deltaY;
            }
        } else {  // horizontal
            x = (line.getX1() + line.getX2()) / 2.0;
            y = line.getY1();
            if (left) {
                x = Math.min(line.getX1(), line.getX2()) + deltaX;
            }
            if (right) {
                x = Math.max(line.getX1(), line.getX2()) - deltaX;
            }
            if (top) {
                y = y - deltaY;
            }
            if (bottom) {
                y = y + deltaY;
            }
        }
        return new Point2D.Double(x, y);
    }

    private RectangleAnchor flipAnchorH(RectangleAnchor anchor) {
        RectangleAnchor result = anchor;
        if (anchor.equals(RectangleAnchor.TOP_LEFT)) {
            result = RectangleAnchor.TOP_RIGHT;
        } else if (anchor.equals(RectangleAnchor.TOP_RIGHT)) {
            result = RectangleAnchor.TOP_LEFT;
        } else if (anchor.equals(RectangleAnchor.LEFT)) {
            result = RectangleAnchor.RIGHT;
        } else if (anchor.equals(RectangleAnchor.RIGHT)) {
            result = RectangleAnchor.LEFT;
        } else if (anchor.equals(RectangleAnchor.BOTTOM_LEFT)) {
            result = RectangleAnchor.BOTTOM_RIGHT;
        } else if (anchor.equals(RectangleAnchor.BOTTOM_RIGHT)) {
            result = RectangleAnchor.BOTTOM_LEFT;
        }
        return result;
    }

    /**
     * Returns the text anchor that is used to align a label to its anchor
     * point.
     *
     * @param anchor the anchor.
     * @return The text alignment point.
     */
    private TextAnchor textAlignPtForLabelAnchorV(RectangleAnchor anchor) {
        TextAnchor result = TextAnchor.CENTER;
        if (anchor.equals(RectangleAnchor.TOP_LEFT)) {
            result = TextAnchor.TOP_RIGHT;
        } else if (anchor.equals(RectangleAnchor.TOP)) {
            result = TextAnchor.TOP_CENTER;
        } else if (anchor.equals(RectangleAnchor.TOP_RIGHT)) {
            result = TextAnchor.TOP_LEFT;
        } else if (anchor.equals(RectangleAnchor.LEFT)) {
            result = TextAnchor.HALF_ASCENT_RIGHT;
        } else if (anchor.equals(RectangleAnchor.RIGHT)) {
            result = TextAnchor.HALF_ASCENT_LEFT;
        } else if (anchor.equals(RectangleAnchor.BOTTOM_LEFT)) {
            result = TextAnchor.BOTTOM_RIGHT;
        } else if (anchor.equals(RectangleAnchor.BOTTOM)) {
            result = TextAnchor.BOTTOM_CENTER;
        } else if (anchor.equals(RectangleAnchor.BOTTOM_RIGHT)) {
            result = TextAnchor.BOTTOM_LEFT;
        }
        return result;
    }

    @Override
    public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
        super.paintOverlay(g2, chartPanel);

        JFreeChart chart = chartPanel.getChart();

        if (chart.getPlot() instanceof XYPlot) {
            paintXYPlot(g2, chartPanel);
        }


    }

    private void paintXYPlot(Graphics2D g2, ChartPanel chartPanel) {
        JFreeChart chart = chartPanel.getChart();
        XYPlot plot = (XYPlot) chart.getPlot();
        ValueAxis xAxis = plot.getDomainAxis();
        ValueAxis yAxis = plot.getRangeAxis();
        RectangleEdge xAxisEdge = plot.getDomainAxisEdge();
        RectangleEdge yAxisEdge = plot.getRangeAxisEdge();
        Rectangle2D dataArea = chartPanel.getScreenDataArea();
        double xp = 0, yp = 0;
        Crosshair chx = (Crosshair) getDomainCrosshairs().get(0);
        xp = xAxis.valueToJava2D(chx.getValue(), dataArea, xAxisEdge);

        String info = dateformat.format(new Date((long) chx.getValue()));
        String maxInfo = info;
        int infoMaxW = info.length();
        String[] draS = new String[plot.getSeriesCount() + 1];
        draS[0]=info;
        XYItemRenderer renderer =plot.getRenderer();
        if (renderer == null) {
            renderer = plot.getRenderer(0);
        }

        for (int i = 0; i < plot.getSeriesCount(); i++) {

            if (plot.getDataset().getItemCount(i) < 1 || getCurrentItem() > plot.getDataset().getItemCount(i)) {
                continue;
            }

            if (renderer.isSeriesVisible(i)
                    && renderer.isSeriesVisibleInLegend(i)) {
                LegendItem item = renderer.getLegendItem(
                        0, i);
                if (item != null) {
                    Double value = plot.getDataset().getYValue(i, getCurrentItem());
                    String tem = item.getLabel() + "：[ " + value + " ]";
                    if (tem.length() > infoMaxW) {
                        maxInfo = tem.trim();
                        infoMaxW = tem.length();
                    }
                    draS[i+1]=tem;
                }
            }

        }
        g2.setFont(labelFont);
        Rectangle2D infoRct = g2.getFontMetrics().getStringBounds(maxInfo, g2);


        int indexW = 10;
        int indexX = 10;
        int indexY = 10;
        int rectW = (int) infoRct.getWidth() + 2 * indexX + 10;
        int rectH = (int) infoRct.getHeight() * draS.length + draS.length * 5 + 2 * indexY;
        int rectx = mouseX + 10;
        int recty = mouseY;

        if (rectx + rectW > (dataArea.getWidth())) {
            rectx = (int) xp - rectW - indexW;
        }
        if (recty + rectH > (dataArea.getHeight())) {
            recty = (int) dataArea.getHeight() - rectH - indexW;
        }

        int x = rectx + indexX;
        int y = recty + indexY + (int) infoRct.getHeight();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Composite composite = g2.getComposite();
        if (chx.isLabelVisible()) {
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER).derive(alpha));
            g2.setPaint(UIManager.getColor("App.chartHoverBackground"));
            g2.fillRoundRect(rectx, recty, rectW, rectH, 10, 10);
        }
        g2.setComposite(composite);

        for (int i = 0; i < draS.length; i++) {
            if(StringUtils.isEmpty(draS[i])){continue;}
            if (i == 0) {
                g2.setPaint(labelTitlePaint);
                if (chx.isLabelVisible()) {
                    int w = g2.getFontMetrics().stringWidth(draS[i]);
                    g2.drawString(draS[i], x, y - (int) infoRct.getHeight() + g2.getFontMetrics().getAscent() + g2.getFontMetrics().getLeading());
                }
            } else {

                if (getCurrentItem() > plot.getDataset().getItemCount(i - 1)) {
                    continue;
                }
                yp = yAxis.valueToJava2D(plot.getDataset().getYValue(i - 1, getCurrentItem()), dataArea, yAxisEdge);


                if (chx.isLabelVisible()) {

                    if (dataArea.contains(xp, yp)) {
                        g2.setPaint(plot.getRenderer().getSeriesPaint(i - 1));
                        g2.fillOval((int) xp - 5, (int) yp - 5, 10, 10);
                        g2.setPaint(UIManager.getColor("Label.foreground"));
                        g2.fillOval((int) xp - 3, (int) yp - 3, 6, 6);
                    }

                    g2.setPaint(plot.getRenderer().getSeriesPaint(i - 1));
                    int textY = (int) (y + (i - 1) * infoRct.getHeight() + (i) * 5);
                    g2.fillOval(x, (int) (textY + (infoRct.getHeight() - 8) / 2), 8, 8);
                    g2.setPaint(UIManager.getColor("Label.foreground"));
                    g2.drawString(draS[i], x + 15, textY + g2.getFontMetrics().getAscent() + g2.getFontMetrics().getLeading());
                }
            }
        }
    }

    @Override
    protected void drawVerticalCrosshair(Graphics2D g2, Rectangle2D dataArea, double x, Crosshair crosshair) {
        if (x >= dataArea.getMinX() && x <= dataArea.getMaxX()&& crosshair.isLabelVisible()) {
            Line2D line = new Line2D.Double(x, dataArea.getMinY(), x,
                    dataArea.getMaxY());
            g2.setPaint(crosshair.getPaint());
            g2.setStroke(crosshair.getStroke());
            g2.draw(line);
        }
    }

    public int getMouseX() {
        return mouseX;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
    }

    public void setDateformat(SimpleDateFormat dateformat) {
        this.dateformat = dateformat;
    }
}
