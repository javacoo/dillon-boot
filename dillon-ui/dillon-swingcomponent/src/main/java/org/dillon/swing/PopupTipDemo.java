package org.dillon.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PopupTipDemo extends JFrame {

    String[] messages = new String[] {
            "getTWaverJava()",
            "getTWaverWeb()",
            "getTWaverFlex()",
            "getTWaverDotNET()",
            "getTWaverGIS()",
            "getTWaverHTML5()",
            "getTWaverJavaFX()",
            "getTWaver", };

    JLabel label = new JLabel("TWaver makes everything easy!");
    JList list = new JList(messages);
    JComponent tip = new JScrollPane(list);
    JTextArea text = new JTextArea();
    JPopupMenu popup = new JPopupMenu();

    public PopupTipDemo() {
        super("www.servasoftware.com");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(new JScrollPane(text), BorderLayout.CENTER);
        this.tip.setPreferredSize(new Dimension(230, 80));
        this.label.setForeground(Color.BLUE);
        this.label.setHorizontalAlignment(SwingConstants.CENTER);
        this.popup.setLayout(new BorderLayout());
        this.popup.add(label, BorderLayout.NORTH);
        this.popup.add(tip, BorderLayout.CENTER);

        this.text.setText("// Try to press '.'\nimport twaver.Node;\nimport twaver.Link;\nimport twaver.network");
        this.text.setBackground(Color.WHITE);
        this.text.setForeground(Color.BLUE);
        this.text.setCaretColor(Color.RED);

        this.text.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                if (popup.isShowing()) {
                    popup.setVisible(false);
                } else if (e.getKeyCode() == KeyEvent.VK_PERIOD) {
                    Point point = text.getCaret().getMagicCaretPosition();
                    if (point != null) {
                        popup.show(text, point.x, point.y);
                    }
                    text.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                PopupTipDemo demo = new PopupTipDemo();
                demo.setSize(400, 200);
                demo.setVisible(true);
            }
        });
    }
}

