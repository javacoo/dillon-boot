package org.dillon.swing.chart;

import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingTargetAdapter;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.data.general.DatasetUtils;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by liwen on 2016/10/25.
 */
public class WChartPanel extends ChartPanel implements ChartMouseListener {

    private Crosshair xCrosshair;
    private Crosshair yCrosshair;
    private int mouseX = 0;
    private int mouseY = 0;
    private WCrosshairOverlay crosshairOverlay;
    private Point mousePoint = new Point(0, 0);
    private Point mouseOldPoint = new Point(0, 0);
    private Animator animationStart;

    public WChartPanel(JFreeChart chart) {
        super(chart);

        crosshairOverlay = new WCrosshairOverlay();
        this.xCrosshair = new Crosshair(0D, Color.GRAY, new BasicStroke(0.0F));
        this.xCrosshair.setLabelVisible(true);
        this.xCrosshair.setPaint(Color.gray);
        this.xCrosshair.setStroke(new BasicStroke(2f));
        this.yCrosshair = new Crosshair(0D, Color.GRAY, new BasicStroke(0.0F));
        this.yCrosshair.setLabelVisible(false);
        crosshairOverlay.addDomainCrosshair(this.xCrosshair);
        crosshairOverlay.addRangeCrosshair(this.yCrosshair);
        this.addOverlay(crosshairOverlay);
        this.addChartMouseListener(this);
        xCrosshair.setLabelVisible(false);
    }

    public Animator getAnimationStart() {
        if (animationStart == null) {
            animationStart = new Animator.Builder().setDuration(300, MILLISECONDS).setInterpolator(new AccelerationInterpolator(.3f, .3f)).build();
        }
        return animationStart;
    }


    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
    }

    @Override
    public void chartMouseMoved(ChartMouseEvent paramChartMouseEvent) {

        Rectangle2D localRectangle2D = getScreenDataArea();
        JFreeChart localJFreeChart = paramChartMouseEvent.getChart();
        Plot plot = localJFreeChart.getPlot();
        Point mousePont = paramChartMouseEvent.getTrigger().getPoint();
        if (plot instanceof XYPlot) {
            XYPlot localXYPlot = (XYPlot) plot;
            xyPlotChartMouseMoved(localXYPlot, mousePont);
        } else if (plot instanceof CategoryPlot) {
            CategoryPlot localXYPlot = (CategoryPlot) plot;
            CategoryAxis localValueAxis = localXYPlot.getDomainAxis();
        } else {
            return;
        }

        setMousePoint(mousePont);
    }

    public void xyPlotChartMouseMoved(XYPlot localXYPlot, Point point) {
        if (localXYPlot != null && localXYPlot.getSeriesCount() < 1) {
            return;
        }
        Rectangle2D localRectangle2D = getScreenDataArea();
        ValueAxis localValueAxis = localXYPlot.getDomainAxis();

        if (localXYPlot.getDataset() == null) {
            return;
        }

        if (!getScreenDataArea().contains(point)) {
            xCrosshair.setLabelVisible(false);
            return;
        }
        double xv = localValueAxis.java2DToValue(point.getX(), localRectangle2D, RectangleEdge.BOTTOM);
        if (!localValueAxis.getRange().contains(xv)) {
            xv = 0D;
        }

        int[] ints = DatasetUtils.findItemIndicesForX(localXYPlot.getDataset(), 0, xv);
        int min = ints[0];
        int max = ints[1];

        if (min == -1 || max == -1) {
            xCrosshair.setLabelVisible(false);
            return;
        } else {
            xCrosshair.setLabelVisible(true);
        }
        double xMaxV = localXYPlot.getDataset().getXValue(0, max);
        double yMaxV = localXYPlot.getDataset().getYValue(0, max);
        double xMinV = localXYPlot.getDataset().getXValue(0, min);
        double yMinV = localXYPlot.getDataset().getYValue(0, min);

        if (Math.abs(xv - xMinV) < Math.abs(xMaxV - xv)) {

            this.xCrosshair.setValue(xMinV);
            this.yCrosshair.setValue(yMinV);
            crosshairOverlay.setCurrentItem(min);
        } else {
            this.xCrosshair.setValue(xMaxV);
            this.yCrosshair.setValue(yMaxV);
            crosshairOverlay.setCurrentItem(max);
        }

    }

    public void categoryPlotChartMouseMoved(CategoryPlot localXYPlot, Point point) {

    }

    public Crosshair getxCrosshair() {
        return xCrosshair;
    }

    public void setxCrosshair(Crosshair xCrosshair) {
        this.xCrosshair = xCrosshair;
    }

    public Crosshair getyCrosshair() {
        return yCrosshair;
    }

    public void setyCrosshair(Crosshair yCrosshair) {
        this.yCrosshair = yCrosshair;
    }

    public WCrosshairOverlay getCrosshairOverlay() {
        return crosshairOverlay;
    }

    public void setCrosshairOverlay(WCrosshairOverlay crosshairOverlay) {
        this.crosshairOverlay = crosshairOverlay;
    }


    public Point getMousePoint() {
        return mousePoint;
    }

    public void setMousePoint(Point mousePoint) {
        this.mousePoint = mousePoint;
        getAnimationStart().addTarget(PropertySetter.getTarget(WChartPanel.this, "mouseX", mouseOldPoint.x, mousePoint.x));
        getAnimationStart().addTarget(PropertySetter.getTarget(WChartPanel.this, "mouseY", mouseOldPoint.y, mousePoint.y));
        getAnimationStart().restart();
        getAnimationStart().addTarget(new TimingTargetAdapter() {
            @Override
            public void end(Animator source) {
                setMouseOldPoint(getMousePoint());
            }
        });
    }

    public Point getMouseOldPoint() {
        return mouseOldPoint;
    }

    public void setMouseOldPoint(Point mouseOldPoint) {
        this.mouseOldPoint = mouseOldPoint;
    }

    public int getMouseX() {
        return mouseX;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
        crosshairOverlay.setMouseX(mouseX);
        repaint();
    }

    public int getMouseY() {
        return mouseY;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
        crosshairOverlay.setMouseY(mouseY);
        repaint();
    }

    public void setDateformat(SimpleDateFormat dateformat) {
        crosshairOverlay.setDateformat(dateformat);
    }
}
