package org.dillon.swing;

import com.formdev.flatlaf.icons.FlatAnimatedIcon;
import com.formdev.flatlaf.util.ColorFunctions;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class AnimatedSwitchIcon extends FlatAnimatedIcon {
    private final Color offColor = Color.lightGray;
    private final Color onColor = UIManager.getColor("App.successColor");

    public AnimatedSwitchIcon() {
        super(28, 16, null);
    }

    @Override
    public void paintIconAnimated(Component c, Graphics g, int x, int y, float animatedValue) {
        Color color = ColorFunctions.mix(onColor, offColor, animatedValue);

        g.setColor(color);
        g.fillRoundRect(x, y, width, height, height, height);

        int thumbSize = height - 4;
        float thumbX = x + 2 + ((width - 4 - thumbSize) * animatedValue);
        int thumbY = y + 2;
        g.setColor(Color.white);
        ((Graphics2D) g).fill(new Ellipse2D.Float(thumbX, thumbY, thumbSize, thumbSize));
    }

    @Override
    public float getValue(Component c) {
        return ((AbstractButton) c).isSelected() ? 1 : 0;
    }




}