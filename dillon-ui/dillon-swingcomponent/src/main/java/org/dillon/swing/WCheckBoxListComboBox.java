package org.dillon.swing;

import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.ui.FlatArrowButton;
import com.jidesoft.combobox.CheckBoxListExComboBox;
import com.jidesoft.combobox.PopupPanel;
import com.jidesoft.combobox.TreeExComboBox;
import com.jidesoft.plaf.UIDefaultsLookup;

import javax.swing.*;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.util.Hashtable;
import java.util.Vector;

public class WCheckBoxListComboBox extends CheckBoxListExComboBox {


    public WCheckBoxListComboBox() {
        super();
    }

    public WCheckBoxListComboBox(Object[] objects) {
        super(objects);
    }

    public WCheckBoxListComboBox(Vector<?> vector) {
        super(vector);
    }

    public WCheckBoxListComboBox(ComboBoxModel comboBoxModel) {
        super(comboBoxModel);
    }

    public WCheckBoxListComboBox(Object[] objects, Class<?> aClass) {
        super(objects, aClass);
    }

    public WCheckBoxListComboBox(Vector<?> vector, Class<?> aClass) {
        super(vector, aClass);
    }

    public WCheckBoxListComboBox(ComboBoxModel comboBoxModel, Class<?> aClass) {
        super(comboBoxModel, aClass);
    }

    @Override
    public AbstractButton createButtonComponent() {

        return new FlatArrowButton(SwingConstants.SOUTH, UIManager.getString("Component.arrowType"), UIManager.getColor("ComboBox.buttonArrowColor"), UIManager.getColor("ComboBox.buttonDisabledArrowColor"), UIManager.getColor("ComboBox.buttonHoverArrowColor"), (Color) null, UIManager.getColor("ComboBox.buttonPressedArrowColor"), (Color) null);

    }

    @Override
    public PopupPanel createPopupComponent() {
        UIManager.put("controlLtHighlight",UIManager.get("App.background"));

        PopupPanel popupPanel=   super.createPopupComponent();
        popupPanel.setBackground(Color.GREEN);

        return popupPanel;
    }
}
