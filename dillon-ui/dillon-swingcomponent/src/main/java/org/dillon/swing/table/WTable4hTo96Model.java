package org.dillon.swing.table;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

public class WTable4hTo96Model extends DefaultTableModel {
    public final static String[] COLUMN_NAME = {"15分", "30分", "45分", "00分", "15分", "30分", "45分", "00分", "15分", "30分", "45分", "00分", "15分", "30分", "45分", "00分"};

    private List<Double> value = new ArrayList<>();


    @Override
    public boolean isCellEditable(int row, int column) {

        if (column == 0) {
            return false;
        }
        return super.isCellEditable(row, column);
    }

    @Override
    public int getColumnCount() {
        return 17;
    }

    @Override
    public int getRowCount() {
        return 6;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        return Double.class;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "时/分";
        } else {
            return COLUMN_NAME[column - 1];
        }
    }

    @Override
    public Object getValueAt(int row, int column) {

        if (column == 0) {
            return null;
        }

        int index = (column - 1) + row * 16;
        if (getValue() == null || getValue().isEmpty()) {
            return null;
        } else {
            return getValue().get(index);
        }

    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {

        if (column == 0) {
            return;
        }
        int index = (column - 1) + row * 16;
        if (aValue instanceof Double) {
            value.set(index, (Double) aValue);
        }
        super.setValueAt(aValue, row, column);
    }

    public List<Double> getValue() {
        return value;
    }

    public void setValue(List<Double> value) {
        this.value = value;
    }
}
