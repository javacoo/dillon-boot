package org.dillon.swing.chart;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.ui.RectangleEdge;

import javax.swing.*;
import java.awt.*;

public class WChartUtil {

    public static void createChartUI(JFreeChart chart) {
        Color color = new Color(0f, 0f, 0f, .0f);
        Color foreground = UIManager.getColor("Label.foreground");
        chart.setTextAntiAlias(true);
        chart.getTitle().setPaint(foreground);
        chart.setBackgroundPaint(color);
        chart.getPlot().setBackgroundPaint(color);
        chart.getXYPlot().setDomainGridlineStroke(new BasicStroke());
        chart.getXYPlot().setOutlinePaint(color);
        chart.getXYPlot().setRangeGridlinePaint(foreground);
        chart.getXYPlot().setRangeGridlinesVisible(true);
        chart.getXYPlot().setDomainGridlinePaint(color);
        chart.getXYPlot().setDomainGridlinesVisible(false);
        chart.getXYPlot().getDomainAxis().setAxisLinePaint(foreground);
        chart.getXYPlot().getDomainAxis().setTickLabelPaint(foreground);
        chart.getXYPlot().getRangeAxis().setAxisLinePaint(foreground);
        chart.getXYPlot().getRangeAxis().setTickLabelPaint(foreground);
        chart.getLegend().setBackgroundPaint(color);
        chart.setAntiAlias(true);
        chart.getLegend().setBorder(0, 0, 0, 0);
        chart.getLegend().setItemPaint(foreground);
        chart.getLegend().setPosition(RectangleEdge.TOP);
    }
}
