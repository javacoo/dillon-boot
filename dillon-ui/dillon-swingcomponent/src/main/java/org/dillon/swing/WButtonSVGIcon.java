package org.dillon.swing;

import com.formdev.flatlaf.extras.FlatSVGIcon;

import java.awt.*;

public class WButtonSVGIcon extends FlatSVGIcon {
    public WButtonSVGIcon(String name, int width, int height) {
        super(name, width, height);
       setColorFilter(new FlatSVGIcon.ColorFilter(color -> {
           return new Color(0xFFFFFF);
        }));
    }
}
