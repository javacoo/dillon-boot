package org.dillon.swing;


import com.formdev.flatlaf.extras.components.FlatTabbedPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @className: WTabbedPanel
 * @author: liwen
 * @date: 2022/4/4 10:49
 */
public class WTabbedPanel extends FlatTabbedPane {


    public WTabbedPanel() {

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    showPopupMenu(e);
                }
            }
        });
    }



    protected void closeTab(int tabIndex) {
        if (tabIndex == -1 || this.getTitleAt(tabIndex).equals("主页")) {
            return;
        }
        this.removeTabAt(tabIndex);
    }

    private void showPopupMenu(final MouseEvent event) {

        // 如果当前事件与右键菜单有关（单击右键），则弹出菜单
        final int index = ((JTabbedPane) event.getComponent()).getUI().tabForCoordinate(this, event.getX(), event.getY());
        final int count = ((JTabbedPane) event.getComponent()).getTabCount();
        final String title = getTitleAt(index);

        if (index == -1) {
            return;
        }
        JPopupMenu pop = new JPopupMenu();
        JMenuItem closeCurrent = new JMenuItem("关闭当前");
        closeCurrent.setPreferredSize(new Dimension(140, 30));

        closeCurrent.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                closeTab(index);
            }
        });
        pop.add(closeCurrent);
        pop.addSeparator();

        JMenuItem closeLeft = new JMenuItem("关闭左侧标签");
        closeLeft.setPreferredSize(new Dimension(140, 30));

        closeLeft.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                for (int j = (index - 1); j >= 0; j--) {
                    closeTab(j);
                }
            }
        });
        pop.add(closeLeft);

        JMenuItem closeRight = new JMenuItem("关闭右侧标签");
        closeRight.setPreferredSize(new Dimension(140, 30));

        closeRight.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                for (int j = (count - 1); j > index; j--) {
                    closeTab(j);
                }
            }
        });
        closeRight.setPreferredSize(new Dimension(140, 30));
        pop.add(closeRight);

        JMenuItem other = new JMenuItem("关闭其它标签");
        other.setPreferredSize(new Dimension(140, 30));

        other.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int tabCount = count;
                while (tabCount-- > 0) {
                    if (title.equals(getTitleAt(tabCount))) {
                        continue;
                    }
                    closeTab(tabCount);
                }
            }
        });
        pop.add(other);
        pop.addSeparator();

        JMenuItem all = new JMenuItem("关闭所有标签");
        all.setPreferredSize(new Dimension(140, 30));

        all.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int tabCount = count;
                // We invoke removeTabAt for each tab, otherwise we may end up
                // removing Components added by the UI.
                while (tabCount-- > 0) {

                    closeTab(tabCount);
                }
            }
        });
        pop.add(all);

        pop.show(event.getComponent(), event.getX(), event.getY());

    }

}
