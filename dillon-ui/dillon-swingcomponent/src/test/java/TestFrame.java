import com.formdev.flatlaf.FlatLightLaf;
import com.jidesoft.combobox.TreeExComboBoxSearchable;
import org.dillon.swing.WPaginationPane;
import org.dillon.swing.WTreeComboBox;
import org.dillon.swing.table.*;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.TimingSource;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TestFrame extends JFrame {


    public TestFrame() {

        SwingUtilities.invokeLater(() -> {
            initUI();
            showTestPanel();
        });
    }

    public void initUI() {
        TimingSource ts = new SwingTimerTimingSource();
        Animator.setDefaultTimingSource(ts);
        ts.init();
        UIManager.put("TitlePane.centerTitleIfMenuBarEmbedded", true);
        UIManager.put("TitlePane.iconSize", new Dimension(30, 30));
        UIManager.put("TitlePane.unifiedBackground", true);
        UIManager.put("MenuItem.selectionType", "underline");

        UIManager.put("Table.showHorizontalLines", true);
        UIManager.put("Table.showVerticalLines", true);
        UIManager.put("TableHeader.background",  new Color(180,180,109));
        UIManager.put("TableHeader.height", 40);
        UIManager.put("Table.intercellSpacing", new Dimension(1,1));

//        UIDefaults defaults = UIManager.getDefaults();
//        defaults.put("ExComboBoxUI", "org.dillon.swing.WTreeComboBoxUI");
        FlatLightLaf.setup();

    }

    public void showTestPanel() {
        WPaginationPane wpa = new WPaginationPane();
        JPanel panel = new JPanel(new BorderLayout());
        wpa.setTotal((int) (Math.random() * 10000));
        JComboBox box = new JComboBox(new Object[]{"a", "b", "c"});
        box.setEditable(false);

        WTreeComboBox treeComboBox2 = new WTreeComboBox(){
            @Override
            protected boolean isValidSelection(TreePath path) {
                TreeNode treeNode = (TreeNode) path.getLastPathComponent();
                return treeNode.isLeaf();
            }
        };
        treeComboBox2.setSearchUserObjectToSelect(true);
        TreeExComboBoxSearchable treeComboBoxSearchable = new TreeExComboBoxSearchable(treeComboBox2);
        treeComboBoxSearchable.setRecursive(true);

        panel.add(box, BorderLayout.NORTH);
        panel.add(treeComboBox2, BorderLayout.CENTER);
        panel.add(wpa, BorderLayout.SOUTH);

        WTable12hTo96Model model = new WTable12hTo96Model();
        WTable4hTo96Model model1 = new WTable4hTo96Model();
        List<Double> doubleList = new ArrayList<>();
        List<Double> doubleList1 = new ArrayList<>();
        for (int i = 0; i < 96; i++) {
            doubleList.add((double) i);
            doubleList1.add((double) i);
        }
        model.setValue(doubleList);
        model1.setValue(doubleList1);

        JPanel pa = new JPanel(new BorderLayout());
        pa.add(new WTable96Panel( new WTable12hTo96(model)), BorderLayout.NORTH);
        pa.add(new WTable96Panel( new WTable4hTo96(model1)), BorderLayout.CENTER);
        this.setJMenuBar(null);
        this.setContentPane(pa);
        this.setLocationRelativeTo(null);
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {
//                    System.err.println(table96.getValueAt(e.getFirstRow(),e.getColumn()));
                }
            }
        });
        JRootPane rootPane = this.getRootPane();
        if (rootPane != null) {
            rootPane.setWindowDecorationStyle(JRootPane.NONE);
        }
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new TestFrame();
    }


}
