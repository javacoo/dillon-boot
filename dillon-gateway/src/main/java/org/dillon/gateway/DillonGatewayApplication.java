package org.dillon.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 网关启动程序
 * 
 * @author liwen
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class DillonGatewayApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DillonGatewayApplication.class, args);

    }
}
