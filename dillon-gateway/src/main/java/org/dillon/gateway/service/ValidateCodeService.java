package org.dillon.gateway.service;

import java.io.IOException;

import com.alibaba.cola.dto.SingleResponse;
import org.dillon.common.core.exception.CaptchaException;
import org.dillon.common.core.web.domain.ValidateCodeEntity;

/**
 * 验证码处理
 *
 * @author liwen
 */
public interface ValidateCodeService
{
    /**
     * 生成验证码
     */
    public SingleResponse<ValidateCodeEntity> createCaptcha() throws IOException, CaptchaException;

    /**
     * 校验验证码
     */
    public void checkCaptcha(String key, String value) throws CaptchaException;
}
