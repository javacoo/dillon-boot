package org.dillon.common.log.enums;

/**
 * 操作状态
 * 
 * @author liwen
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
