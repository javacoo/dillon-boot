package org.dillon.common.core.exception;

/**
 * 权限异常
 * 
 * @author liwen
 */
public class PreAuthorizeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException()
    {
    }
}
