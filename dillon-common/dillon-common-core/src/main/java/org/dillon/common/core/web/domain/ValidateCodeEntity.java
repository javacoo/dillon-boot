package org.dillon.common.core.web.domain;


import lombok.Data;

/**
 * 验证码实体
 *
 * @author liwen
 * @date 2022/06/29
 */
@Data
public class ValidateCodeEntity {

    private boolean captchaOnOff;
    private String uuid;
    private String img;

}
