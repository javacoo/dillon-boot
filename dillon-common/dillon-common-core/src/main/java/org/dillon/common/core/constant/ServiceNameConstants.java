package org.dillon.common.core.constant;

/**
 * 服务名称
 * 
 * @author liwen
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "dillon-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "dillon-system-biz";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "dillon-file";
}
