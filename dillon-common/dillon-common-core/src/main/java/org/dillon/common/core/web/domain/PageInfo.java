package org.dillon.common.core.web.domain;

import com.alibaba.cola.dto.PageQuery;

public class PageInfo<T> extends PageQuery {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
