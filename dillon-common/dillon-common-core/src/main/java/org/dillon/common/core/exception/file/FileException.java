package org.dillon.common.core.exception.file;

import org.dillon.common.core.exception.base.BaseException;

/**
 * 文件信息异常类
 * 
 * @author liwen
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

}
