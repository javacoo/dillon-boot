package org.dillon.auth.controller;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import org.dillon.auth.service.SysLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.dillon.auth.form.LoginBody;
import org.dillon.auth.form.RegisterBody;
import org.dillon.common.core.utils.JwtUtils;
import org.dillon.common.core.utils.StringUtils;
import org.dillon.common.security.auth.AuthUtil;
import org.dillon.common.security.service.TokenService;
import org.dillon.common.security.utils.SecurityUtils;
import org.dillon.system.api.model.LoginUser;

import java.util.Map;

/**
 * token 控制
 * 
 * @author liwen
 */
@RestController
public class TokenController
{
    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysLoginService sysLoginService;

    @PostMapping("login")
    public SingleResponse<Map<String, Object>> login(@RequestBody LoginBody form)
    {
        // 用户登录
        LoginUser userInfo = sysLoginService.login(form.getUsername(), form.getPassword());
        // 获取登录token
        return SingleResponse.of(tokenService.createToken(userInfo));
    }

    @DeleteMapping("logout")
    public Response logout(HttpServletRequest request)
    {
        String token = SecurityUtils.getToken(request);
        if (StringUtils.isNotEmpty(token))
        {
            String username = JwtUtils.getUserName(token);
            // 删除用户缓存记录
            AuthUtil.logoutByToken(token);
            // 记录用户退出日志
            sysLoginService.logout(username);
        }
        return Response.buildSuccess();
    }

    @PostMapping("refresh")
    public Response refresh(HttpServletRequest request)
    {
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser))
        {
            // 刷新令牌有效期
            tokenService.refreshToken(loginUser);
            return Response.buildSuccess();
        }
        return Response.buildSuccess();
    }

    @PostMapping("register")
    public Response register(@RequestBody RegisterBody registerBody)
    {
        // 用户注册
        sysLoginService.register(registerBody.getUsername(), registerBody.getPassword());
        return Response.buildSuccess();
    }
}
