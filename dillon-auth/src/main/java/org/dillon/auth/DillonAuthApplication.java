package org.dillon.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.dillon.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 * 
 * @author liwen
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class DillonAuthApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DillonAuthApplication.class, args);

    }
}
